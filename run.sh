SCRIPT_DIR=$(cd $(dirname $0); pwd)
xhost +
nvidia-docker run --rm \
    -e DISPLAY=$DISPLAY \
    -it -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $SCRIPT_DIR/app:/app \
    -t gnistk /bin/bash
