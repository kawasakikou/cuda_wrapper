//****CUDAのカーネル関数内で二次元配列を扱うためのプログラム****//

#include <iostream>
#include <cuda.h>

#define WIDTH 5

//デバイスでのポインタを取得しセットする関数
__global__ void set_pointer(int **array, int *pointer, int index)
{
  array[index] = pointer;
}

//テスト用カーネル関数(計算に意味はなし)
__global__ void kernel(int **array, int *out)
{
  for(int i = 0; i < WIDTH; i++)
    {
      for(int j = 0; j < WIDTH; j++)
	{
	  out[WIDTH * i + j] = array[i][j] * 2;
	}
    }
  return;
}

int main()
{
  int **array_host; //ホスト側での二次元配列
  int **array_device; //デバイス側で使用する二次元配列(引数としてカーネル関数に送る二次元配列)
  int **ary_device_buff; //デバイス側で使用する二次元配列の行要素

  //ホスト側の二次元配列を確保し初期化する
  array_host = new int * [WIDTH];
  for(int i = 0; i < WIDTH; i++)
    {
      array_host[i] = new int[WIDTH];
    }

  for(int i = 0; i < WIDTH; i++)
    {
      for(int j = 0; j < WIDTH; j++)
	{
	  array_host[i][j] = i * WIDTH + j;
	}
    }

  //デバイス側で使用する二次元配列を行要素分確保
  cudaMalloc(&array_device, sizeof(int *) * WIDTH);
  //デバイス側で使用する二次元配列の各行要素を確保するため、行数分の配列を確保
  ary_device_buff = new int * [WIDTH];

  //デバイス側で使用する二次元配列の各行要素1行ずつを確保し、転送する
  for(int i = 0; i < WIDTH; i++)
    {
      cudaMalloc(&ary_device_buff[i], sizeof(int) * 5);
      cudaMemcpy(ary_device_buff[i], array_host[i], sizeof(int) * 5,
		 cudaMemcpyHostToDevice);

      //確保した行要素を1行ずつデバイス側で使用する二次元配列にセットする
      set_pointer <<< 1, 1 >>> (array_device, ary_device_buff[i], i);
    }

  //出力用配列
  int *out_host = new int[WIDTH * WIDTH], *out_device;
  //念の為初期化
  for(int i = 0; i < WIDTH * WIDTH; i++)
    {
       out_host[i] = 0;
    }

  //出力用配列をデバイス側で確保し、転送する
  cudaMalloc(&out_device, sizeof(int) * WIDTH * WIDTH);
  cudaMemcpy(out_device, out_host, sizeof(int) * WIDTH * WIDTH, cudaMemcpyHostToDevice);

  //カーネル実行
  kernel  <<< 1, 1 >>>(array_device, out_device);
  
  //結果をホストへ転送
  cudaMemcpy(out_host, out_device, sizeof(int) * 25, cudaMemcpyDeviceToHost);
  
  //結果を表示
  for(int i = 0; i < WIDTH * WIDTH; i++)
    {
      std::cout << out_host[i] << " ";
    }
  std::cout << std::endl;
  
  return 0;
}
