#ifndef GCALC
#define GCALC

#include <simdata.h>
#include <simdataarray.h>

namespace Gnistk {
  namespace Calc {
    double weighted_sum(Nistk::SimData *in, Nistk::SimData *weight, int center_x, int center_y);
    double weighted_sum2(Nistk::SimData *in, Nistk::SimData *weight, 
            int start_x, int start_y, int wx_start, int wx_end, int wy_start, int wy_end);
    void input_single_weight(Nistk::SimData *in, Nistk::SimData *weight, Nistk::SimData *out, int start_x, int start_y, int step);
    void input_array_weight(Nistk::SimData *in, Nistk::SimDataArray *weight, Nistk::SimData *out, int start_x, int start_y, int step);
  }
}

struct SimDataParam
{
  int height;
  int width;
  int length;
};


#endif
