#ifndef SET
#define SET

#include <simdata.h>
#include <simdataarray.h>
#include <gcalc.h>
#include <ghhnewron.h>
#include <givnewron.h>

template<class T>
void SetSimDataParam(SimDataParam *param, T *data)
{
  param->height = data->get_height();
  param->width = data->get_width();
  param->length = data->get_length();
}

template<class T>
void SetIVParams(struct IVParam *param, T *data) {
  param->a      = data->a;
  param->b      = data->b;
  param->c      = data->c;
  param->d      = data->d;
  param->v_rest = data->v_rest;
  param->vp_1   = data->vp_1;
  param->vp_2   = data->vp_2;
  param->vp_3   = data->vp_3;
  param->vp_4   = data->vp_4;
  param->vp_5   = data->vp_5;
  param->vp_6   = data->vp_6;
  param->vp_7   = data->vp_7;
  param->up_1   = data->up_1;
  param->up_2   = data->up_2;
  param->up_3   = data->up_3;
}

template<class T>
void SetIVVariable(struct IVVariable *val, T *data) {
  val->v = data->v;
  val->u = data->u;
  val->I = data->I;
}

template<class T>
void SetHHParam(struct HHParam *param, T *data) {
  param->C_m_bar   = data->C_m_bar;
  param->G_K_bar   = data->G_K_bar;
  param->V_rest    = data->V_rest;
  param->E_Na      = data->E_Na;
  param->E_K       = data->E_K;
  param->V_L       = data->V_L;
  param->T         = data->T;
  param->area_size = data->area_size;
  param->T_scale   = data->T_scale;
  param->C_m       = data->C_m;
  param->G_m       = data->G_m;
  param->G_Na      = data->G_Na;
  param->G_K       = data->G_K;
  param->mhn_scale = data->mhn_scale;
  param->am_1      = data->am_1;
  param->am_2      = data->am_2;
  param->am_3      = data->am_3;
  param->am_4      = data->am_4;
  param->am_5      = data->am_5;
  param->bm_1      = data->bm_1;
  param->bm_2      = data->bm_2;
  param->ah_1      = data->ah_1;
  param->ah_2      = data->ah_2;
  param->bh_1      = data->bh_1;
  param->bh_2      = data->bh_2;
  param->bh_3      = data->bh_3;
  param->bh_4      = data->bh_4;
  param->an_1      = data->an_1;
  param->an_2      = data->an_2;
  param->an_3      = data->an_3;
  param->an_4      = data->an_4;
  param->an_5      = data->an_5;
  param->bn_1      = data->bn_1;
  param->bn_2      = data->bn_2;
}

template<class T>
void SetHHVariable(struct HHVariable *val, T *data) {
  val->V      = data->V;
  val->I_Na   = data->I_Na;
  val->I_K    = data->I_K;
  val->I_leak = data->I_leak;
  val->I_inj  = data->I_inj;
  val->m      = data->m;
  val->h      = data->h;
  val->n      = data->n;
}


#endif
