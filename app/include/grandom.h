#ifndef RAND
#define RAND

namespace Gnistk{
  namespace Rand {
    __global__ void random(   unsigned int seed, int arr_num, int*    res);
    __global__ void range(    unsigned int seed, int arr_num, int*    res, int start, int end, int shift);
    __global__ void rand_0to1(unsigned int seed, int arr_num, double* res);
    __global__ void exp(      unsigned int seed, int arr_num, double* res, double alpha);
    __global__ void poisson(  unsigned int seed, int arr_num, double* res, double m);
    __global__ void erlang(   unsigned int seed, int arr_num, int*    res, double lambda, int n); 
    __global__ void gaussian( unsigned int seed, int arr_num, double* res, double ex, double vx, int n);
    __global__ void hist(     unsigned int seed, int arr_num, double* res, double *x, double *p, int num);
    __device__ void rand_0to1_device(unsigned int seed, double* res);
  }
}

#endif
