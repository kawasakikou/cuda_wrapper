#ifndef IZHIKEVICH
#define IZHIKEVICH

#include <ivparameter.h>
#include <ivvariable.h>


namespace Gnistk{
  namespace Izhikevich {
    static void   init(                          Nistk::Neuron::IVVariable *ivv, Nistk::Neuron::IVParameter *ivp);
    static void   calc(  double dt,              Nistk::Neuron::IVVariable *ivv, Nistk::Neuron::IVParameter *ivp);
    static double calc_v(double dt, double *val, Nistk::Neuron::IVVariable *ivv, Nistk::Neuron::IVParameter *ivp);
    static double calc_u(double dt, double *val, Nistk::Neuron::IVVariable *ivv, Nistk::Neuron::IVParameter *ivp);
  }
}

struct IVParam {
  double a; double b; double c; double d;
  double v_rest;
  double vp_1; double vp_2; double vp_3; double vp_4; double vp_5; double vp_6; double vp_7;
  double up_1; double up_2; double up_3;
};

struct IVVariable {
  double v; double u; double I;
};

#endif
