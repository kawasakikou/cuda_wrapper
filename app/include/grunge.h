#ifndef RUNGE
#define RUNGE


typedef void (*fptr_t)(double&, double&, double&, double&, double*);

template <fptr_t... Functions>
__global__ void kernel(double x, double *y, double para1, double para2, double dx) {
  constexpr auto num_f = sizeof...(Functions);
  constexpr fptr_t table[] = { Functions...  };
  double *k1  = new double[num_f];
  double *k2  = new double[num_f];
  double *k3  = new double[num_f];
  double *k4  = new double[num_f];
  double *ret = new double[num_f];

  if (threadIdx.x < num_f) {
    fptr_t f = table[threadIdx.x];

    double x_org = x;
    double y_org = y[threadIdx.x];
    double dx2   = dx / 2.0;

    f(x, y[threadIdx.x], para1, para2, &ret[threadIdx.x]);

    k1[threadIdx.x] = dx * ret[threadIdx.x];

    x = x_org + dx2;
    y[threadIdx.x] = y_org + (k1[threadIdx.x]/2.0);

    f(x, y[threadIdx.x], para1, para2, &ret[threadIdx.x]);
    k2[threadIdx.x] = dx * ret[threadIdx.x]; 

    x = x_org + dx2;
    y[threadIdx.x]  = y_org + (k2[threadIdx.x]/2.0);
    f(x, y[threadIdx.x], para1, para2, &ret[threadIdx.x]);
    k3[threadIdx.x] = dx * ret[threadIdx.x];

    x = x_org + dx;
    y[threadIdx.x] = y_org + k3[threadIdx.x];
    f(x, y[threadIdx.x], para1, para2, &ret[threadIdx.x]);
    k4[threadIdx.x] = dx * ret[threadIdx.x];

    y[threadIdx.x] = y_org + 
        (k1[threadIdx.x] + 2.0*k2[threadIdx.x] 
         + 2.0*k3[threadIdx.x] +k4[threadIdx.x]) / 6.0;
  }

  delete[] k1;
  delete[] k2;
  delete[] k3;
  delete[] k4;
  delete[] ret;
}

#endif
