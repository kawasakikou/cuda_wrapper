#ifndef __CUDA_CALC_H__
#define __CUDA_CALC_H__

#include <simdata.h>
#include <simdataarray.h>

namespace cuda
{
  namespace Calc
  {
    // void input_single_weight(Nistk::SimData *in, Nistk::SimData *weight, Nistk::SimData *out, int start_x, int start_y, int step);
    // void input_array_weight(Nistk::SimData *in, Nistk::SimDataArray *weight, Nistk::SimData *out, int start_x, int start_y, int step);
    double gpu_weighted_sum( double *in_device, double *weight_device,
				   struct SimDataParam in_param, struct SimDataParam weight_param,
				   int x, int y);
  }
}

#endif
