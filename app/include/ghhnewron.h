#ifndef HODGIKIN
#define HODGIKIN

#include <hhparameter.h>
#include <hhvariable.h>

struct HHParam {
  double C_m_bar;
  double G_m_bar;
  double G_Na_bar;
  double G_K_bar;
  double V_rest;
  double E_Na;
  double E_K;
  double V_L;
  double T;
  double area_size;
  double T_scale;
  double C_m;
  double G_m;
  double G_Na;
  double G_K;
  double mhn_scale;
  double am_1; double am_2; double am_3; double am_4; double am_5;
  double bm_1; double bm_2;
  double ah_1; double ah_2;
  double bh_1; double bh_2; double bh_3; double bh_4;
  double an_1; double an_2; double an_3; double an_4; double an_5;
  double bn_1; double bn_2;
};

struct HHVariable {
  double V;
  double I_Na;
  double I_K;
  double I_leak;
  double I_inj;
  double m;
  double h;
  double n;
};


namespace Gnistk{
  namespace Hodgikin {
    void   init(HHVariable *hhv, HHParam *hhp);

    void   convert_mhn_to_MSAV(HHParam *hhp);
    void   calc(     double dt,              HHVariable *hhv, HHParam *hhp);
    double calc_v(   double dt, double *val, HHVariable *hhv, HHParam *hhp);
    double calc_m_si(double dt, double *val, HHVariable *hhv, HHParam *hhp);
    double calc_h_si(double dt, double *val, HHVariable *hhv, HHParam *hhp);
    double calc_n_si(double dt, double *val, HHVariable *hhv, HHParam *hhp);

    double calc_m(   double dt, double m, double v, HHParam *hhp);
    double calc_h(   double dt, double h, double v, HHParam *hhp);
    double calc_n(   double dt, double n, double v, HHParam *hhp);

    double calc_alpha_m(double v, HHParam *hhp);
    double calc_beta_m( double v, HHParam *hhp);
    double calc_alpha_h(double v, HHParam *hhp);
    double calc_beta_h( double v, HHParam *hhp);
    double calc_alpha_n(double v, HHParam *hhp);
    double calc_beta_n( double v, HHParam *hhp);
  }
}

#endif

