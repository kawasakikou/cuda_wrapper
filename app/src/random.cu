#include <unistd.h>
#include <stdio.h>
#include <curand.h>
#include <curand_kernel.h>
#include <grandom.h>



__global__ void Gnistk::Rand::random(unsigned int seed, int num_arr, int* res) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);

  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    res[tid] = curand(&state) % RAND_MAX;
    tid += blockDim.x * gridDim.x;
  }
}

__global__ void Gnistk::Rand::range(unsigned int seed, int num_arr, int* res, int start, int end, int shift) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);
  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    res[tid] = (curand(&state)>>shift)%(end-start+1)+start;
    tid += blockDim.x * gridDim.x;
  }
}

__global__ void Gnistk::Rand::rand_0to1(unsigned int seed, int num_arr, double* res) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);

  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    res[tid] = ((double)curand(&state))/RAND_MAX;
    tid += blockDim.x * gridDim.x;
  }
}

__global__ void Gnistk::Rand::exp(unsigned int seed, int num_arr, double* res, double alpha) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);
  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    res[tid] = (double)((-1.0 / alpha) * __logf(1 - (double)curand(&state)/RAND_MAX));
    tid += blockDim.x * gridDim.x;
  }
}

__global__ void Gnistk::Rand::poisson(unsigned int seed, int num_arr, double* res, double m) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);

  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    res[tid] = 0;
    double m_k = 1.0;
    double k   = 1.0;
    double tmp = (expm1(-m)+1) * (m_k / k);

    for(;;){
      if((double)curand(&state)/RAND_MAX < tmp) break;
      res[tid] += 1.0;
      m_k *= m;
      k   *= *res;
      tmp += (expm1(-m)+1) * (m_k / k);
    }

    tid += blockDim.x * gridDim.x;
  }
}

__global__ void Gnistk::Rand::erlang(unsigned int seed, int num_arr, int* res, double lambda, int n) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);

  double prod = 1.0;
 
  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    for(int i = 0; i < n; i++){
      prod *= (1 - (double)curand(&state)/RAND_MAX);
    }

    res[tid] = (-1.0 / lambda) * log(prod);
    tid += blockDim.x * gridDim.x;
  }

}

__global__ void Gnistk::Rand::gaussian(unsigned int seed, int num_arr, double* res, double ex, double vx, int n) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);
 
  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    double total_sum = 1.0;
    for(int i = 0; i < n; i++)
      total_sum += (double)curand(&state)/RAND_MAX ;

    res[tid] = sqrt(vx) * sqrt(12.0 / (double)n) * (total_sum - ((double)n / 2)) + ex;
    tid += blockDim.x * gridDim.x;
  }

}

__global__ void Gnistk::Rand::hist(unsigned int seed, int num_arr, double* res, double *x, double *p, int num) {
  curandState_t state;
  curand_init(seed, 0, 0, &state);
  double rand_num = (double)curand(&state)/RAND_MAX ;
  double g_i,g_i2;
  int l;

  int tid = threadIdx.x + blockIdx.x * blockDim.x;
  while (tid < num_arr) {
    g_i = p[0];
    for(int i = 1; i < num; i++){
      g_i2 = g_i + p[i];
      if(rand_num < g_i2){ l = i - 1; break; }
      g_i = g_i2;
    }
    res[tid] = ((rand_num - g_i) / (g_i2 - g_i)) * (x[l+1] - x[l]) + x[l];
    tid += blockDim.x * gridDim.x;
  }

}

