#include <nistk/runge.h>
#include <nistk/ivneuron.h>
#include <nistk/ivvariable.h>
#include <nistk/ivparameter.h>
#include <iostream>
#include <iomanip>
#include <gcalc.h>
#include <set.h>


__device__ void Gnistk::Izhikevich::init(Gnistk::Izhikevich::IVVariable *ivv, Gnistk::Izhikevich::IVParameter *ivp) {
  ivv->v = ivp->c;
  ivv->u = ivp->b * ivv->v;
  ivv->I = 0.0;
}

__device__ void Gnistk::Izhikevich::calc(double dt, Gnistk::Izhikevich::IVVariable *ivv, Gnistk::Izhikevich::IVParameter *ivp) {
  double val[2];
  double (*func[2])(double dt, double *val, Gnistk::Izhikevich::IVVariable *ivv, Gnistk::Izhikevich::IVParameter *ivp) 
    = { &Gnistk::Izhikevich::calc_v, &Gnistk::Izhikevich::calc_u};
  val[0] = ivv->v;
  val[1] = ivv->u;

  if(val[0] >= ivp->v_rest){
    val[0] = ivp->vp_7 * val[0] + ivp->c;
    val[1] = ivp->up_3 * val[1] + ivp->d;
  }

  Nistk::Temp::Runge4Si<Gnistk::Izhikevich::IVVariable*, Gnistk::Izhikevich::IVParameter*>(func, dt, val, ivv, ivp, dt, 2);
  ivv->v = val[0]; 
  ivv->u = val[1];
}


__device__ double Gnistk::Izhikevich::calc_v(double dt, double *val, Gnistk::Izhikevich::IVVariable *ivv, Gnistk::Izhikevich::IVParameter *ivp) {
  return (ivp->vp_1 * val[0] * val[0] + ivp->vp_2 * val[0] + ivp->vp_3 - ivp->vp_4 * val[1] + ivp->vp_5 + ivv->I) / ivp->vp_6;
}

__device__ double Gnistk::Izhikevich::calc_u(double dt, double *val, Gnistk::Izhikevich::IVVariable *ivv, Gnistk::Izhikevich::IVParameter *ivp) {
  return ivp->a * (ivp->b * (val[0] + ivp->up_1) - ivp->up_2 * val[1]);
}
