#include <simdata.h>
#include <simdataarray.h>
#include <iostream>
#include <iomanip>
#include <gcalc.h>
#include <set.h>


__device__ double kernel_weighted_sum(
           double *in_device,
           double *weight_device,
           SimDataParam in_param,
           SimDataParam weight_param,
           int x, int y, 
           int wx_start, int wx_end, 
           int wy_start, int wy_end)
{
  int wc_x = weight_param.width/2;
  int wc_y = weight_param.height/2;

  double sum = 0.0;
  for(int i = wy_start; i <= wy_end; i++){
    if(((y+i) >= 0) && ((y+i) < in_param.height) && ((wc_y+i) >= 0) && ((wc_y+i) < weight_param.height)){
      for(int j = wx_start; j <= wx_end; j++){
        if(((x+j) >= 0) && ((x+j) < in_param.width) && ((wc_x+j) >= 0) && ((wc_x+j) < weight_param.width)){
          sum += weight_device[weight_param.width * (wc_y + i) + (wc_x + j)] 
            *in_device[in_param.width * (y + i) + (x + j)];
        }
      }
    }
  }
  return sum;
}

__global__ void wrap_weighted_sum(
    double *in_device,
    double *weight_device,
    double *out,
    SimDataParam in_param, SimDataParam weight_param,
    int x, int y,
    int wx_start, int wx_end,
    int wy_start, int wy_end) {
  *out = kernel_weighted_sum(in_device, weight_device, in_param, weight_param, x, y,
      wx_start, wx_end, wy_start, wy_end);
}

double Gnistk::Calc::weighted_sum(Nistk::SimData *in, Nistk::SimData *weight, int start_x, int start_y) {
  SimDataParam in_param;     SetSimDataParam(&in_param, in);
  SimDataParam weight_param; SetSimDataParam(&weight_param, weight);
  double out;

  dim3 blocks(in_param.height, 1, 1);
  dim3 threads(in_param.width, 1, 1);

  double *in_device, *weight_device, *out_dev;
  cudaMalloc((void**) &in_device,     in_param.length     * sizeof(double));
  cudaMalloc((void**) &weight_device, weight_param.length * sizeof(double));
  cudaMalloc((void**) &out_dev,                             sizeof(double));

  cudaMemcpy(in_device,     in->get_data_ptr(),     in_param.length     * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(weight_device, weight->get_data_ptr(), weight_param.length * sizeof(double), cudaMemcpyHostToDevice);

  wrap_weighted_sum<<<blocks, threads>>>(
      in_device, weight_device, out_dev,
      in_param, weight_param,
      start_x, start_y,
      -1*(int)(weight_param.height/2), (int)(weight_param.height/2),
      -1*(int)(weight_param.width/2), (int)(weight_param.width/2));

  cudaThreadSynchronize();

  cudaMemcpy(&out, out_dev, sizeof(double), cudaMemcpyDeviceToHost);

  cudaFree(in_device);
  cudaFree(weight_device);
  cudaFree(out_dev);
  return out;
}

double Gnistk::Calc::weighted_sum2( Nistk::SimData *in, Nistk::SimData *weight,
    int start_x, int start_y,
    int wx_start, int wx_end,
    int wy_start, int wy_end) {

  SimDataParam in_param;     SetSimDataParam(&in_param, in);
  SimDataParam weight_param; SetSimDataParam(&weight_param, weight);
  double out;

  dim3 blocks(in_param.height, 1, 1);
  dim3 threads(in_param.width, 1, 1);

  double *in_device, *weight_device, *out_dev;
  cudaMalloc((void**) &in_device,     in_param.length     * sizeof(double));
  cudaMalloc((void**) &weight_device, weight_param.length * sizeof(double));
  cudaMalloc((void**) &out_dev,                             sizeof(double));

  cudaMemcpy(in_device,     in->get_data_ptr(),     in_param.length     * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(weight_device, weight->get_data_ptr(), weight_param.length * sizeof(double), cudaMemcpyHostToDevice);

  wrap_weighted_sum<<<blocks, threads>>>(
      in_device, weight_device, out_dev,
      in_param, weight_param,
      start_x, start_y,
      wx_start, wx_end,
      wy_start, wy_end);

  cudaThreadSynchronize();

  cudaMemcpy(&out, out_dev, sizeof(double), cudaMemcpyDeviceToHost);

  cudaFree(in_device);
  cudaFree(weight_device);
  cudaFree(out_dev);
  return out;
}

__global__ void gpu_input_single_weight(
    double *in_device,
    double *weight_device,
    double *out_device,
    SimDataParam in_param,
    SimDataParam weight_param,
    SimDataParam out_param,
    int start_x, int start_y, int step)
{
  double tmp = 0.0;
  int i = blockIdx.x;
  int j = threadIdx.x;

  int y = step * i + start_y;
  int x = step * j + start_x;

  if((y >= 0) && (y < in_param.height) && (x >= 0) && (x < in_param.width))
    tmp = kernel_weighted_sum(in_device, weight_device, in_param, weight_param, x, y,
      -1*(int)(weight_param.height/2), (int)(weight_param.height/2),
      -1*(int)(weight_param.width/2), (int)(weight_param.width/2));

  out_device[threadIdx.x + out_param.width * blockIdx.x] = tmp;
}

void Gnistk::Calc::input_single_weight(
    Nistk::SimData *in,
    Nistk::SimData *weight,
    Nistk::SimData *out,
    int start_x, int start_y, int step)
{
  SimDataParam in_param; SetSimDataParam(&in_param, in);
  SimDataParam weight_param; SetSimDataParam(&weight_param, weight);
  SimDataParam out_param; SetSimDataParam(&out_param, out);

  dim3 blocks(out_param.height, 1, 1);
  dim3 threads(out_param.width, 1, 1);

  double *in_device, *weight_device, *out_device;
  cudaMalloc((void**) &in_device, in_param.length * sizeof(double));
  cudaMalloc((void**) &weight_device, weight_param.length * sizeof(double));
  cudaMalloc((void**) &out_device, out_param.length * sizeof(double));

  cudaMemcpy(in_device, in->get_data_ptr(), in_param.length * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(out_device, out->get_data_ptr(), out_param.length * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(weight_device, weight->get_data_ptr(), weight_param.length * sizeof(double), cudaMemcpyHostToDevice);

  gpu_input_single_weight <<< blocks, threads >>>(
      in_device,
      weight_device,
      out_device,
      in_param,
      weight_param,
      out_param,
      start_x, start_y, step);
  cudaThreadSynchronize();

  cudaMemcpy(out->get_data_ptr(), out_device, out_param.length * sizeof(double), cudaMemcpyDeviceToHost);

  cudaFree(in_device);
  cudaFree(weight_device);
  cudaFree(out_device);
}

__global__ void set_pointer(double **array, double *pointer, int index) {
  array[index] = pointer;
}

__global__ void gpu_input_array_weight(double *in_device,
    double **weight_device,
    double *out_device,
    SimDataParam in_param,
    SimDataParam array_param,
    SimDataParam * weight_param,
    SimDataParam out_param,
    int start_x, int start_y, int step)
{
  int i = blockIdx.x;
  int j = threadIdx.x;

  int y = step * i + start_y;
  int x = step * j + start_x;

  double tmp = 0.0;

  if((y >= 0) && (y < in_param.height) && (i < array_param.height) && (x >= 0) && (x < in_param.width) && (j < array_param.width)) {
    tmp = kernel_weighted_sum(in_device,
        weight_device[j + i * array_param.width],
        in_param,
        weight_param[j + i * array_param.width],
        x, y,
        -1*(int)(weight_param->height/2), (int)(weight_param->height/2),
        -1*(int)(weight_param->width/2), (int)(weight_param->width/2));
  }

  out_device[i * out_param.width + j] = tmp;
}

void Gnistk::Calc::input_array_weight(
    Nistk::SimData *in,
    Nistk::SimDataArray *weight,
    Nistk::SimData *out,
    int start_x, int start_y, int step)
{
  SimDataParam in_param;    SetSimDataParam(&in_param, in);
  SimDataParam out_param;   SetSimDataParam(&out_param, out);
  SimDataParam array_param; SetSimDataParam(&array_param, weight);

  SimDataParam *weight_param;
  weight_param = new SimDataParam[weight->get_length()];

  dim3 blocks(out_param.height, 1, 1);
  dim3 threads(out_param.width, 1, 1);

  double *in_device, *out_device;
  cudaMalloc((void**) &in_device, in_param.length * sizeof(double));
  cudaMalloc((void**) &out_device, out_param.length * sizeof(double));
  cudaMemcpy(in_device, in->get_data_ptr(), in_param.length * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(out_device, out->get_data_ptr(), out_param.length * sizeof(double), cudaMemcpyHostToDevice);

  double **weight_device;  //カーネルに送るためのweight
  double **weight_device_buff;  //各weightの確保用

  weight_device_buff = new double * [weight->get_length()];
  cudaMalloc((void**)&weight_device, weight->get_length() * sizeof(double *));

  SimDataParam *weight_param_device;
  weight_param_device = new SimDataParam[weight->get_length()];

  for(int i = 0; i < weight->get_length(); i++) {
    SetSimDataParam(&weight_param[i], weight->get_simdata_ptr1d(i));
    cudaMalloc((void**) &weight_device_buff[i], weight_param[i].length * sizeof(double));
    cudaMemcpy(weight_device_buff[i], weight->get_simdata_ptr1d(i)->get_data_ptr(),
        weight_param[i].length * sizeof(double), cudaMemcpyHostToDevice);

    set_pointer <<< 1, 1 >>> (weight_device, weight_device_buff[i], i);
  }


  cudaMalloc((void**) &weight_param_device, weight->get_length() * sizeof(SimDataParam));
  cudaMemcpy(weight_param_device, weight_param, weight->get_length() * sizeof(SimDataParam), cudaMemcpyHostToDevice);

  gpu_input_array_weight <<< blocks, threads >>>(in_device,
      weight_device,
      out_device,
      in_param,
      array_param,
      weight_param_device,
      out_param,
      start_x, start_y, step);

  cudaThreadSynchronize();

  cudaMemcpy(out->get_data_ptr(), out_device, out_param.length * sizeof(double), cudaMemcpyDeviceToHost);

  cudaFree(in_device);
  cudaFree(out_device);
  for(int i = 0; i < weight->get_length(); i++){
    cudaFree(weight_device_buff[i]);
  }

  delete[] weight_param;
  delete[] weight_device_buff;
  cudaFree(weight_device);
}
