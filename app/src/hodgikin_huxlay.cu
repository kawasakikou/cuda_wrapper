#include <hhnewron.h>
#include <hhparameter.h>
#include <hhvariable.h>
#include <iostream>
#include <iomanip>
#include <gcalc.h>
#include <set.h>


__device__ void Gnistk::Hodgikin::calc(double dt, 
    Gnistk::Hodgikin::HHVariable *hhv, 
    Gnistk::Hodgikin::HHParameter *hhp) {

  double val[4];
  double (*func[4])(double dt, double *val, Gnistk::Hodgikin::HHVariable *hhv, Gnistk::Hodgikin::HHParameter *hhp) 
    = { &Gnistk::Hodgikin::calc_v, &Gnistk::Hodgikin::calc_m_si,
      &Gnistk::Hodgikin::calc_h_si, &Gnistk::Hodgikin::calc_n_si};

  val[0] = hhv->V - hhp->V_rest;
  val[1] = hhv->m;
  val[2] = hhv->h;
  val[3] = hhv->n;

  // ルンゲクッタ法を用いて計算(2番目の引数dtはダミー)
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Nistk::Temp::Runge4Si<Gnistk::Hodgikin::HHVariable*, Gnistk::Hodgikin::HHParameter*>(func, dt, val, hhv, hhp, dt, 4);

  hhv->V = val[0] + hhp->V_rest;
  hhv->m = val[1];
  hhv->h = val[2];
  hhv->n = val[3];
  hhv->I_Na = hhp->G_Na  * pow(hhv->m,3) * hhv->h * (hhv->V - hhp->E_Na);
  hhv->I_K = hhp->G_K    * pow(hhv->n,4) * (hhv->V - hhp->E_K);
  hhv->I_leak = hhp->G_m * (hhv->V - hhp->V_L);
}

/* 
__device__ void Gnistk::Hodgikin::convert_mhn_to_MSAV(Gnistk::Hodgikin::HHParameter *hhp) {
  double scale;
  scale = pow(10,-3);
  hhp->mhn_scale = 1000.0;
  hhp->am_1 = hhp->am_1 * scale;
  hhp->am_2 = hhp->am_2 * scale;
  hhp->am_3 = hhp->am_3 * scale;
  hhp->am_4 = hhp->am_4 * scale;
  hhp->bm_2 = hhp->bm_2 * scale;
  hhp->ah_2 = hhp->ah_2 * scale;
  hhp->bh_2 = hhp->bh_2 * scale;
  hhp->bh_3 = hhp->bh_3 * scale;
  hhp->an_1 = hhp->an_1 * scale;
  hhp->an_2 = hhp->an_2 * scale;
  hhp->an_3 = hhp->an_3 * scale;
  hhp->an_4 = hhp->an_4 * scale;
  hhp->bn_2 = hhp->bn_2 * scale;
}

__device__ double Gnistk::Hodgikin::calc_v(double dt, double *val, 
    Gnistk::Hodgikin::HHVariable *hhv, Gnistk::Hodgikin::HHParameter *hhp) {
  double diff;
  diff = (hhp->G_Na * pow(val[1],3) * val[2] * ((hhp->E_Na - hhp->V_rest) - val[0])
      + hhp->G_K * pow(val[3],4) * ((hhp->E_K - hhp->V_rest) - val[0])
      + hhp->G_m * ((hhp->V_L - hhp->V_rest) - val[0])
      + hhv->I_inj) / hhp->C_m;

  return diff;
}

__device__ double Gnistk::Hodgikin::calc_m_si(double dt, double *val, Gnistk::Hodgikin::HHVariable *hhv, Gnistk::Hodgikin::HHParameter *hhp) {
  return Gnistk::Hodgikin::calc_m(dt, val[1], val[0], hhp);
}

__device__ double Gnistk::Hodgikin::calc_h_si(double dt, double *val, Gnistk::Hodgikin::HHVariable *hhv, Gnistk::Hodgikin::HHParameter *hhp) {
  return Gnistk::Hodgikin::calc_h(dt, val[2], val[0], hhp);
}

__device__ double Gnistk::Hodgikin::calc_n_si(double dt, double *val, Gnistk::Hodgikin::HHVariable *hhv, Gnistk::Hodgikin::HHParameter *hhp) {
  return Gnistk::Hodgikin::calc_n(dt, val[3], val[0], hhp);
}

__device__ double Gnistk::Hodgikin::calc_m(double dt, double m, double v, Gnistk::Hodgikin::HHParameter *hhp)
{
  double alpha_m = Gnistk::Hodgikin::calc_alpha_m(v, hhp) * hhp->T_scale;
  double beta_m = Gnistk::Hodgikin::calc_beta_m(v, hhp) * hhp->T_scale;
  double diff = (alpha_m * (1 - m) - beta_m * m) * hhp->mhn_scale; 

  return diff;
}

__device__ double Gnistk::Hodgikin::calc_h(double dt, double h, double v, Gnistk::Hodgikin::HHParameter *hhp) {
  double alpha_h = Gnistk::Hodgikin::calc_alpha_h(v, hhp) * hhp->T_scale;
  double beta_h = Gnistk::Hodgikin::calc_beta_h(v, hhp) * hhp->T_scale;
  double diff = (alpha_h * (1 - h) - beta_h * h) * hhp->mhn_scale; 

  return diff;
}

__device__ double Gnistk::Hodgikin::calc_n(double dt, double n, double v, Gnistk::Hodgikin::HHParameter *hhp) {
  double alpha_n = Gnistk::Hodgikin::calc_alpha_n(v, hhp) * hhp->T_scale;
  double beta_n = Gnistk::Hodgikin::calc_beta_n(v, hhp) * hhp->T_scale;
  double diff = (alpha_n * (1 - n) - beta_n * n) * hhp->mhn_scale; 
  return diff;
}

__device__ double Gnistk::Hodgikin::calc_alpha_m(double v, Gnistk::Hodgikin::HHParameter *hhp) {
  double ret;    // 戻り値
  double f1_1,f1_2; // 分子バッファ
  double f2_1,f2_2; // 分母バッファ
  double v_buf;
  double f1 = hhp->am_1 - v;
  double f2 = hhp->am_2 * (exp((hhp->am_3 - v) / hhp->am_4) - hhp->am_5);

  if (f2 == 0.0){
    v_buf = v - pow(10.0, -12);
    f1_1 = hhp->am_1 - v_buf;
    f2_1 = hhp->am_2 * (exp((hhp->am_3 - v_buf) / hhp->am_4) - hhp->am_5);
    v_buf = v + pow(10.0, -12);
    f1_2 = hhp->am_1 - v_buf;
    f2_2 = hhp->am_2 * (exp((hhp->am_3 - v_buf) / hhp->am_4) - hhp->am_5);
    ret = ((f1_1 / f2_1) + (f1_2 / f2_2)) / 2.0;
  }

  else ret = f1 / f2;
  return ret;
}

__device__ double Gnistk::Hodgikin::calc_beta_m(double v, Gnistk::Hodgikin::HHParameter *hhp) {
  return hhp->bm_1 * exp((-1.0 * v) / hhp->bm_2);
}

__device__ double Gnistk::Hodgikin::calc_alpha_h(double v, Gnistk::Hodgikin::HHParameter *hhp) {
  return hhp->ah_1 * exp((-1.0 * v) / hhp->ah_2);
}

__device__ double Gnistk::Hodgikin::calc_beta_h(double v, Gnistk::Hodgikin::HHParameter *hhp) {
  return hhp->bh_1 / (exp((hhp->bh_2 - v) / hhp->bh_3) + hhp->bh_4);
}

__device__ double Gnistk::Hodgikin::calc_alpha_n(double v, Gnistk::Hodgikin::HHParameter *hhp) {
  double ret;    // 戻り値
  double f1_1,f1_2; // 分子バッファ
  double f2_1,f2_2; // 分母バッファ
  double v_buf;

  double f1 = hhp->an_1 - v;
  double f2 = hhp->an_2 * (exp((hhp->an_3 - v) / hhp->an_4) - hhp->an_5);

  if (f2 == 0.0){
    v_buf = v - pow(10.0, -12);
    f1_1 = hhp->an_1 - v_buf;
    f2_1 = hhp->an_2 * (exp((hhp->an_3 - v_buf) / hhp->an_4) - hhp->an_5);
    v_buf = v + pow(10.0, -12);
    f1_2 = hhp->an_1 - v_buf;
    f2_2 = hhp->an_2 * (exp((hhp->an_3 - v_buf) / hhp->an_4) - hhp->an_5);
    ret = ((f1_1 / f2_1) + (f1_2 / f2_2)) / 2.0;
  }
  else ret = f1 / f2;
  return ret;
}

__device__ double Gnistk::Hodgikin::calc_beta_n(double v, Gnistk::Hodgikin::HHParameter *hhp) {
  return hhp->bn_1 * exp((-1.0 * v) / hhp->bn_2);
}
 */
