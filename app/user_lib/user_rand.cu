#include <unistd.h>
#include <stdio.h>
#include <iostream>

#include <curand.h>
#include <curand_kernel.h>
#include <grandom.h>


void user_random(int *arr, int num_arr) {
  int* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(int));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::random<<<128, 128>>>(time(NULL), num_arr, gpu_x);
  cudaDeviceSynchronize();

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(int), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_rand_range(int *arr, int num_arr, int start, int end, int shift) {
  int* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(int));

  float calc_time = 0.0f;
  cudaEvent_t tstart, stop; cudaEventCreate(&tstart); cudaEventCreate(&stop);
  cudaEventRecord(tstart, 0);

  Gnistk::Rand::range<<<128, 128>>>(time(NULL), num_arr, gpu_x, start, end, shift);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, tstart, stop);
  cudaEventDestroy(tstart); cudaEventDestroy(stop);
  std::cout << "random range time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(int), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_rand_0to1(double *arr, int num_arr) {
  double* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(double));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::rand_0to1<<<128, 128>>>(time(NULL), num_arr, gpu_x);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random 0to1 time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(double), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_exp(double *arr, int num_arr, double alpha) {
  double* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(double));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::exp<<<128, 128>>>(time(NULL), num_arr, gpu_x, alpha);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random exp time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(double), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_poisson(double *arr, int num_arr, double m) {
  double* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(double));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::poisson<<<128, 128>>>(time(NULL), num_arr, gpu_x, m);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random poisson time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(double), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_erlang(int *arr, int num_arr, double rambda, int n) {
  int* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(int));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::erlang<<<128, 128>>>(time(NULL), num_arr, gpu_x, rambda, n);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random erlang time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(int), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_gaussian(double *arr, int num_arr, double ex, double vx, int n=240) {
  double* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(double));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::gaussian<<<128, 128>>>(time(NULL), num_arr, gpu_x, ex, vx, n);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random gaussian time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(double), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}

void user_hist(double *arr, int num_arr, double *x, double *p, int num) {
  double* gpu_x;
  cudaMalloc((void**) &gpu_x, num_arr*sizeof(double));

  float calc_time = 0.0f;
  cudaEvent_t start, stop; cudaEventCreate(&start); cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  Gnistk::Rand::hist<<<128, 128>>>(time(NULL), num_arr, gpu_x, x, p, num);

  cudaEventRecord(stop, 0); cudaEventSynchronize(stop);
  cudaEventElapsedTime(&calc_time, start, stop);
  cudaEventDestroy(start); cudaEventDestroy(stop);
  std::cout << "random hist time : " << calc_time << "ms" << std::endl;

  cudaMemcpy(arr, gpu_x, num_arr*sizeof(double), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
}
