#include <unistd.h>
#include <stdio.h>
#include <nistk.h>

#include <curand.h>
#include <curand_kernel.h>
#include <grandom.h>


using sda = Nistk::SimDataArray;


double Gnistk::Calc::weighted_sum(Nistk::SimData *in, Nistk::SimData *weight, int start_x, int start_y) {
double Gnistk::Conv::convolution(sda in, sda w, sda preactive, int stride) {
  SimDataParam in_param;     SetSimDataParam(&in_param, in);
  SimDataParam weight_param; SetSimDataParam(&weight_param, weight);
  double out;

  dim3 blocks(in_param.height, 1, 1);
  dim3 threads(in_param.width, 1, 1);

  double *in_device, *weight_device, *out_dev;
  cudaMalloc((void**) &in_device,     in_param.length     * sizeof(double));
  cudaMalloc((void**) &weight_device, weight_param.length * sizeof(double));
  cudaMalloc((void**) &out_dev,                             sizeof(double));

  cudaMemcpy(in_device,     in->get_data_ptr(),     in_param.length     * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(weight_device, weight->get_data_ptr(), weight_param.length * sizeof(double), cudaMemcpyHostToDevice);

  wrap_weighted_sum<<<blocks, threads>>>(
      in_device, weight_device, out_dev,
      in_param, weight_param,
      start_x, start_y,
      -1*(int)(weight_param.height/2), (int)(weight_param.height/2),
      -1*(int)(weight_param.width/2), (int)(weight_param.width/2));

  cudaThreadSynchronize();

  cudaMemcpy(&out, out_dev, sizeof(double), cudaMemcpyDeviceToHost);

  cudaFree(in_device);
  cudaFree(weight_device);
  cudaFree(out_dev);
  return out;
}

__global__ void wrap_weighted_sum(
    double *in_device,
    double *weight_device,
    double *out,
    SimDataParam in_param, SimDataParam weight_param,
    int x, int y,
    int wx_start, int wx_end,
    int wy_start, int wy_end) {
  *out = kernel_weighted_sum(in_device, weight_device, in_param, weight_param, x, y,
      wx_start, wx_end, wy_start, wy_end);
}

