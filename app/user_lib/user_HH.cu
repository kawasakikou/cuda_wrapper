#include <unistd.h>
#include <stdio.h>

#include <curand.h>
#include <curand_kernel.h>
#include <grandom.h>
#include <set.h>


int user_calc() {
  int rand;
  int* gpu_x;
  cudaMalloc((void**) &gpu_x, sizeof(int));

  Gnistk::Rand::random<<<1, 1>>>(time(NULL), gpu_x);

  cudaMemcpy(&rand, gpu_x, sizeof(int), cudaMemcpyDeviceToHost);
  cudaFree(gpu_x);
  return rand;
}
