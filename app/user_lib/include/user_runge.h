#ifndef USERRUNGE
#define USERRUNGE

#include <stdio.h>
#include <iostream>

#define num_functions 4

__constant__ fptr_t constant_fList[num_functions];

__device__ void Func0(double &x, double &y, double &para1, double &para2, double *ret) {*ret = 0;}
__device__ void Func1(double &x, double &y, double &para1, double &para2, double *ret) {*ret = 1;}
__device__ void Func2(double &x, double &y, double &para1, double &para2, double *ret) {*ret = 2;}
__device__ void Func3(double &x, double &y, double &para1, double &para2, double *ret) {*ret = 3;}


#endif
