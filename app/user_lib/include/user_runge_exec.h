#ifndef USERRUNGEEXEC
#define USERRUNGEEXEC

#include <stdio.h>
#include <iostream>

namespace Gnistk {
  namespace Runge {
    void Si(double x, double *y, double para1, double para2, double dx);
  }
}

#endif
