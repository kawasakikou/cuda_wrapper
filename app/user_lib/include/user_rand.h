#ifndef USERRAND
#define USERRAND


void user_random(    int    *arr, int num_arr);
void user_rand_range(int    *arr, int num_arr, int start, int end, int shift);
void user_rand_0to1( double *arr, int num_arr);
void user_exp(       double *arr, int num_arr, double alpha);
void user_poisson(   double *arr, int num_arr, double m);
void user_erlang(    int    *arr, int num_arr, double rambda, int n);
void user_gaussian(  double *arr, int num_arr, double ex, double vx, int n=240);
void user_hist(      double *arr, int num_arr, double *x, double *p, int num);

#endif
