#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <grunge.h>

#include "user_runge.h"
#include "user_runge_exec.h"
#include <cuda.h>


__device__ fptr_t p_f0 = Func0;
__device__ fptr_t p_f1 = Func1;
__device__ fptr_t p_f2 = Func2;
__device__ fptr_t p_f3 = Func3;

void loadList_staticpointers() {
  fptr_t h_fList[num_functions];
  cudaMemcpyToSymbol(constant_fList, h_fList, num_functions * sizeof(fptr_t));
}

void Gnistk::Runge::Si(double x, double *y, double para1, double para2, double dx) {
  loadList_staticpointers();
  int num_array = 4;

  double* gpu_res;
  cudaMalloc((void**) &gpu_res, num_array * sizeof(double));

  kernel<Func0, Func1, Func2, Func3><<<1,4>>>(x, gpu_res, para1, para2, dx);
  cudaDeviceSynchronize();

  cudaMemcpy(y, gpu_res, num_array * sizeof(double), cudaMemcpyDeviceToHost);

  cudaFree(gpu_res);
}
