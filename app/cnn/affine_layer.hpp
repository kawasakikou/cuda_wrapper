#ifndef AFFINE_LAYER
#define AFFINE_LAYER

#include <nistk.h>
#include "momentum.hpp"
#include "layer_params.hpp"

using sda = Nistk::SimDataArray;

/** 畳み込みニューラルネットワーク(CNN)の出力層のためのクラス
 *
 * CNNの出力層のためのクラス。<br>
 * ・重みはSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦をユニット数、横を入力チャネル数とする。<br>
 * ・入力の総和と出力格納用はdouble型のポインタのポインタを使い、
 *   縦を学習用サンプル数として横を畳み込み層の出力ユニット数とする。<br>
 *
 * ・学習での畳み込み層の計算で使うbackprop用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 *
 **/

class AffineLayer
{
  public:
    Nistk::SimDataArray in; ///< 入力x格納用SimDataArray 
    Nistk::SimDataArray W;  ///< 重みw 格納用SimDataArray 
    Nistk::SimDataArray dW; ///< 重み修正量
    Momentum momentum;      ///< momentオブジェクト

    AffineLayer(AffineLayerParams p);
    ~AffineLayer();

    std::vector<double> forward(sda in_val);
    Nistk::SimDataArray backward(std::vector<double> teach_data);
    void learn();

    void save_param(const char *dir_name, const char *base_name, int layer_num);
    void load_param(const char *dir_name, const char *base_name, int layer_num);

  private:
    const int unit_num;         ///< 出力ユニットの数
    const int in_ch_num;        ///< 入力チャネル数
    const int w_width;          ///< 重みの幅
    const int w_height;         ///< 重みの高さ
    const int minibatch_size;   ///< ミニバッチ用サンプル数
    const double learn_ratio;
    const double alpha;
    const double lambda;
};

#endif
