#include <nistk.h>
#include "conv_layer.h"
#include "sim_data_factory.hpp"
#include "layer_params.hpp"
#include "calc.hpp"
#include <random>

#define repeat(i,n) for (int i = 0; (i) < (n); (i)++)

using sda = Nistk::SimDataArray;


ConvLayer::ConvLayer() {
  conv_ch_num = 0;
  in_ch_num = 0;
  w_width = 0;
  w_height = 0;
  width = 0;
  height = 0;
  stride = 1;
  //
  minibatch_size = 1;
  minibatch_count = 0;
  learn_ratio = 0.001;
  alpha = 0.5;
  lambda = 0.1;
}

ConvLayer::~ConvLayer() {
  if(w_width > 0) W.delete_array();
  if(width > 0){
    preactive.delete_array();
    out.delete_array();
    bias.delete_array();
  }
}

/** 初期化を行う関数
 *
 * ・フィルタはSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦を畳み込み層のチャネル、横を入力チャネル数とする。<br>
 * ・畳み込み結果と出力格納用はSimDataArrayを使い、縦を学習用サンプル数として
 *   横を畳み込み層のチャネルとする。<br>
 * ・バイアス用はSimDataArrayを使い、縦を1として
 *   横を畳み込み層のチャネルとする。<br>
 * ・学習でのプーリング層の計算で使う∂C/∂x格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 */
void ConvLayer::init(ConvLayerParams p) {
  conv_ch_num    = p.conv_ch_num;
  in_ch_num      = p.in_ch_num;
  w_width        = p.w_width;
  w_height       = p.w_height;
  width          = p.width;
  height         = p.height;
  stride         = p.stride;
  minibatch_size = p.minibatch_size;
  learn_ratio    = p.learn_ratio;
  alpha          = p.alpha;
  lambda         = p.lambda;

  W.create_data_size( in_ch_num, conv_ch_num, w_width, w_height);
  SimDataFactory::replace_random_filter(&W);

  preactive.create_data_size(conv_ch_num, minibatch_size, width, height);
  out.create_data_size(      conv_ch_num, minibatch_size, width, height);
  bias.create_data_size(     conv_ch_num, 1,              width, height);
}

sda ConvLayer::forward(sda in){
  // Keep input to update gradient
  X = in;

  repeat(in_minibatch, in.get_height())
    repeat(in_ch, in.get_width())
      repeat(conv_ch, preactive.get_width())
        Calc::input_single_weight(
            in.get_simdata_ptr(in_ch, in_minibatch),
            W.get_simdata_ptr(in_ch, conv_ch),
            preactive.get_simdata_ptr(conv_ch, in_minibatch), 
            stride);

  ReLu_activate();
  return out;
}

void ConvLayer::ReLu_activate(){
  repeat(in_minibatch, preactive.get_height()){
    repeat(conv_ch, preactive.get_width()){
      int b = bias.get_data1d(conv_ch, 0, 0);
      repeat(h, preactive.get_array_height(0,0)){
        repeat(w, preactive.get_array_width(0,0)){
          double data = 0;
          if ( preactive.get_data(conv_ch, in_minibatch, w, h) > 0) {
            data = preactive.get_data(conv_ch, in_minibatch, w, h);
          }
          out.put_data(conv_ch, in_minibatch, w, h, data * 0.1);
        }
      }
    }
  }
}


sda ConvLayer::backward(sda dY){
  sda dW = update_dW(dY); 
  sda dX = update_dX(dY);

  learn(dW);
  return dX;
}

sda ConvLayer::update_dX(sda dY) {
  sda dX(X.get_width(), X.get_height(), X.get_array_width(0,0), X.get_array_height(0,0));

  repeat(in_minibatch, dY.get_height()){
    repeat(conv_ch, dY.get_width()){
      repeat(out_y, dY.get_array_height(0,0)){
        repeat(out_x, dY.get_array_width(0,0)){
          repeat(in_ch, X.get_width()){
            for (int in_y = out_y -(w_height/2); in_y <= out_y + (w_height/2); in_y++) {
              for (int in_x = out_x -(w_width/2); in_x <= out_x + (w_width/2); in_x++) {
                if (0 <= in_x && in_x < X.get_array_width(in_ch,in_minibatch) && 
                    0 <= in_y && in_y < X.get_array_height(in_ch,in_minibatch)) {

                  double d = dY.get_data(conv_ch, in_minibatch, out_x, out_y) 
                    * preactive.get_data(conv_ch, in_minibatch, out_x, out_y)
                    * W.get_data(in_ch, conv_ch, in_x - out_x + (w_width/2), 
                                                 in_y - out_y + (w_height/2)); 

                  dX.put_data(in_ch, in_minibatch, in_x, in_y, d);
                }
              }
            }
          }
        }
      }
    } 
  }
  return dX;
}

sda ConvLayer::update_dW(sda dY) {
  sda dW(in_ch_num, conv_ch_num, w_width, w_height);

  repeat(in_minibatch, dY.get_height()){
    repeat(conv_ch, dY.get_width()){
      repeat(out_y, dY.get_array_height(0,0)){
        repeat(out_x, dY.get_array_width(0,0)){
          double d = dY.get_data(conv_ch, in_minibatch, out_x, out_y) 
            * preactive.get_data(conv_ch, in_minibatch, out_x, out_y);

          repeat(in_ch, X.get_width()){//Xheightはin_minibatch
            for (int in_y = out_y -(w_height/2); in_y <= out_y + (w_height/2); in_y++) {
              for (int in_x = out_x -(w_width/2); in_x <= out_x + (w_width/2); in_x++) {
                if (0 <= in_x && in_x < X.get_array_width(0,0) && 
                    0 <= in_y && in_y < X.get_array_height(0,0)) {
                  double data = d * X.get_data(in_ch, in_minibatch, in_x, in_y);
                  dW.put_data(in_ch, conv_ch, in_x - out_x + (w_width/2), in_y - out_y + (w_height/2), data);
                }
              }
            }
          }
        }
      }
    }
  }
  return dW;
}


void ConvLayer::learn(sda dW) {
  repeat(conv_ch, W.get_height()){
    repeat(in_ch, W.get_width()){
      repeat(w_h, W.get_array_height(0,0)){
        repeat(w_w, W.get_array_width(0,0)){
          double data = W.get_data(in_ch, conv_ch, w_w, w_h) - 
              learn_ratio * 0.00001* (dW.get_data(in_ch, conv_ch, w_w, w_h) / minibatch_size);
          W.put_data(in_ch, conv_ch, w_w, w_h, data);
        }
      }
    }
  }
}

/** パラメータ保存のための関数
 * 
 * ConvLayerのパラメータおよびW、preactive、out、bias、
 * の値をバイナリ形式で保存する関数。パラメータと各結合荷重は
 * 別々のファイルに保存することにしたので、引数で指定する
 * 文字列はディレクトリ名とベースファイル名にしているので注意すること。
 *
 *  @param dir_name 保存先ディレクトリ名
 *  @param base_name ファイル名の先頭に付けるベース名
 *  @param layer_num base_nameの後ろにつける数字(層番号など)
 */
void ConvLayer::save_param(const char *dir_name, const char *base_name, int layer_num) {
  std::ofstream fout;                            // 出力ファイル用
  std::ofstream fout_t;                          // text出力ファイル用
  std::string file_name;                         // 出力ファイル用文字列
  std::string txt_file_name;                     // text出力ファイル用文字列
  std::string base_name_t;                       // ベースファイル名用
  std::string filename_para = "_parameter";      // パラメータ用文字列
  std::string filename_W = "_W";       // フィルタ用文字列
  std::string filename_preactive = "_preactive";   // 畳み込み結果x 用文字列
  std::string filename_out = "_out";     // 出力y 用文字列
  std::string filename_bias = "_bias";   // バイアスb 用文字列
  char *file_name_char;
  bool flag;

  std::cout << "Save parameter=" << base_name << '\n';
  // ファイル名のベースとなる文字列の設定
  base_name_t = dir_name;
  base_name_t += "/";
  base_name_t += base_name;
  base_name_t += "_";
  base_name_t += std::to_string(layer_num);

  // パラメータの保存
  // ファイルのオープン　失敗すると処理せずリターン
  file_name = base_name_t + filename_para + ".dat"; 
  fout.open(file_name.c_str(), std::ios_base::binary);
  if(!fout){
    std::cout << "Can not open " << file_name << '\n';
    return;
  }
  txt_file_name = base_name_t + filename_para + ".txt"; 
  fout_t.open(txt_file_name.c_str());
  if(!fout_t){
    std::cout << "Can not open " << txt_file_name << '\n';
    return;
  }
  fout_t << "Parameter of " << base_name << '\n';
  // 基本パラメータの保存
  fout.write((char*)&conv_ch_num, sizeof(int)); fout_t << "conv_ch_num=" << conv_ch_num << '\n';
  fout.write((char*)&in_ch_num, sizeof(int)); fout_t << "in_ch_num=" << in_ch_num << '\n';
  fout.write((char*)&w_width, sizeof(int)); fout_t << "w_width=" << w_width << '\n';
  fout.write((char*)&w_height, sizeof(int)); fout_t << "w_height=" << w_height << '\n';
  fout.write((char*)&width, sizeof(int)); fout_t << "width=" << width << '\n';
  fout.write((char*)&height, sizeof(int)); fout_t << "height=" << height << '\n';
  fout.write((char*)&stride, sizeof(int)); fout_t << "stride=" << stride << '\n';
  // 学習に関するパラメータ
  fout.write((char*)&minibatch_size, sizeof(int)); fout_t << "minibatch_size=" << minibatch_size << '\n';
  fout.write((char*)&minibatch_count, sizeof(int)); fout_t << "minibatch_count=" << minibatch_count << '\n';
  fout.write((char*)&learn_ratio, sizeof(double)); fout_t << "learn_ratio=" << learn_ratio << '\n';
  fout.write((char*)&alpha, sizeof(double)); fout_t << "alpha=" << alpha << '\n';
  fout.write((char*)&lambda, sizeof(double)); fout_t << "lambda=" << lambda << '\n';

  fout.close();
  fout_t.close();

  std::cout << "Output file=" << txt_file_name << '\n';
  std::cout << "            " << file_name << '\n';

  // SimDataArray型のパラメータ保存
  // Wの保存
  file_name = base_name_t + filename_W + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::save_array(&W, file_name_char);
  if(flag == true) std::cout << "            " << file_name << '\n';
  delete[] file_name_char;

  // preactiveの保存
  file_name = base_name_t + filename_preactive + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::save_array(&preactive, file_name_char);
  if(flag == true) std::cout << "            " << file_name << '\n';
  delete[] file_name_char;

  // outの保存
  file_name = base_name_t + filename_out + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::save_array(&out, file_name_char);
  if(flag == true) std::cout << "            " << file_name << '\n';
  delete[] file_name_char;

  // biasの保存
  file_name = base_name_t + filename_bias + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::save_array(&bias, file_name_char);
  if(flag == true) std::cout << "            " << file_name << '\n';
  delete[] file_name_char;
}

/** パラメータ読み込みのための関数
 * 
 * save_paraで保存したConvLayerのパラメータおよびW、
 * preactive、out、biasの値を
 * の値を読み込む関数。
 * 当たり前だがこの関数を利用すると初期化する必要がない。
 *
 *  @param dir_name 保存先ディレクトリ名
 *  @param base_name ファイル名の先頭に付けたベース名
 */
void ConvLayer::load_param(const char *dir_name, const char *base_name, int layer_num) {
  std::ifstream fin;                            // 入力ファイル用
  std::string file_name;                        // 入力ファイル用文字列
  std::string base_name_t;                      // ベースファイル名用
  std::string filename_para = "_parameter";      // パラメータ用文字列
  std::string filename_W = "_W";       // フィルタ用文字列
  std::string filename_preactive = "_preactive";   // 畳み込み結果x 用文字列
  std::string filename_out = "_out";     // 出力y 用文字列
  std::string filename_bias = "_bias";   // バイアスb 用文字列
  char *file_name_char;
  bool flag;

  std::cout << "Load parameter=" << base_name << '\n';
  // ファイル名のベースとなる文字列の設定
  base_name_t = dir_name;
  base_name_t += "/";
  base_name_t += base_name;
  base_name_t += "_";
  base_name_t += std::to_string(layer_num);

  // パラメータの読み込み
  // ファイルのオープン　失敗すると処理せずリターン
  file_name = base_name_t + filename_para + ".dat"; 
  fin.open(file_name.c_str(), std::ios_base::binary);
  if(!fin){
    std::cout << "Can not open " << file_name << '\n';
    return;
  }
  // 基本パラメータ
  fin.read((char*)&conv_ch_num, sizeof(int));
  fin.read((char*)&in_ch_num, sizeof(int));
  fin.read((char*)&w_width, sizeof(int));
  fin.read((char*)&w_height, sizeof(int));
  fin.read((char*)&width, sizeof(int));
  fin.read((char*)&height, sizeof(int));
  fin.read((char*)&stride, sizeof(int));
  // 学習に関するパラメータ
  fin.read((char*)&minibatch_size, sizeof(int));
  fin.read((char*)&minibatch_count, sizeof(int));
  fin.read((char*)&learn_ratio, sizeof(double));
  fin.read((char*)&alpha, sizeof(double));
  fin.read((char*)&lambda, sizeof(double));
  fin.close();
  std::cout << "Input file=" << file_name << '\n';

  // SimDataArray型のパラメータ読み込み
  // Wの読み込み
  file_name = base_name_t + filename_W + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::load_array(&W, file_name_char);
  if(flag == true) std::cout << "           " << file_name << '\n';
  delete[] file_name_char;
  // preactiveの読み込み
  file_name = base_name_t + filename_preactive + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::load_array(&preactive, file_name_char);
  if(flag == true) std::cout << "           " << file_name << '\n';
  delete[] file_name_char;
  // outの読み込み
  file_name = base_name_t + filename_out + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::load_array(&out, file_name_char);
  if(flag == true) std::cout << "           " << file_name << '\n';
  delete[] file_name_char;
  // biasの読み込み
  file_name = base_name_t + filename_bias + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::load_array(&bias, file_name_char);
  if(flag == true) std::cout << "           " << file_name << '\n';
  delete[] file_name_char;

  return;
}
