// CNNのパラメータファイル

// ネットワーク全体に関するパラメータ
#define CL_NUM     2           // 畳み込み層の層数
#define PL_NUM     2           // プーリング層の層数
#define CL_PL_PAIR 2           // 畳み込み層とプーリング層のペアの数

// 学習に関する設定
#define LEARN_NUM        60000     // 学習用サンプル数
#define TEST_NUM         10000     // テスト用サンプル数
#define MINIBATCH_SIZE   1         // ミニバッチ用サンプル数
#define LEARN_RATIO      0.001     // 学習率
#define LEARN_ALPHA      0.3       // モーメント係数
#define LEARN_LAMBDA     0.1       // モーメント減衰率

// 入力層の設定
#define IN_CH_NUM 1            // 入力層のチャネル数
#define IN_WIDTH  32           // 入力画像の幅
#define IN_HEIGHT 32           // 入力画像の高さ

// 出力層の設定
#define OL_UNIT_NUM 10         // 出力層のユニット数
#define OL_CH_NUM   16         // 出力層の入力チャネル数
#define OL_WIDTH    6          // 重みの幅(入力側のプーリング層の幅と同じ)
#define OL_HEIGHT   6          // 重みの高さ(入力側のプーリング層の高さと同じ)

// 1層目の畳み込み層の設定
#define CL0_CH_NUM    16       // 畳み込み層のチャネル数
#define CL0_IN_CH_NUM 1        // 畳み込み層の入力チャネル数(入力層のチャネル数と同じ)
#define CL0_F_WIDTH   11       // フィルタの幅
#define CL0_F_HEIGHT  11       // フィルタの幅
#define CL0_WIDTH     26       // 畳み込みそうの一画像の幅
#define CL0_HEIGHT    26       // 畳み込みそうの一画像の高さ
#define CL0_STRIDE    1        // ストライド

// 2層目のプーリング層の設定
#define PL0_CH_NUM      16     // プーリング層のチャネル数
#define PL0_POOL_WIDTH  2      // プーリングの幅
#define PL0_POOL_HEIGHT 2      // プーリングの高さ
#define PL0_WIDTH       13     // プーリング層の一画像の幅
#define PL0_HEIGHT      13     // プーリング層の一画像の高さ
#define PL0_STRIDE      2      // ストライド

// 3層目の畳み込み層の設定
#define CL1_CH_NUM    16       // 畳み込み層のチャネル数
#define CL1_IN_CH_NUM 16       // 畳み込み層の入力チャネル数(入力層のチャネル数と同じ)
#define CL1_F_WIDTH   5        // フィルタの幅
#define CL1_F_HEIGHT  5        // フィルタの幅
#define CL1_WIDTH     13       // 畳み込みそうの幅
#define CL1_HEIGHT    13       // 畳み込みそうの高さ
#define CL1_STRIDE    1        // ストライド

// 4層目のプーリング層の設定
#define PL1_CH_NUM      16     // プーリング層のチャネル数
#define PL1_POOL_WIDTH  3      // プーリングの幅
#define PL1_POOL_HEIGHT 3      // プーリングの高さ
#define PL1_WIDTH       6      // プーリング層の幅
#define PL1_HEIGHT      6      // プーリング層の高さ
#define PL1_STRIDE      2      // ストライド

#define MAX_POOLING_FLAG true
