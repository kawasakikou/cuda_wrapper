#include <nistk.h>
#include <numeric>
#include "affine_layer.hpp"
#include "calc.hpp"
#include "momentum.hpp"
#include "layer_params.hpp"
#include "sim_data_factory.hpp"

#define repeat(i,n) for (int i = 0; (i) < (n); (i)++)

using sda = Nistk::SimDataArray;

/** 初期化を行う関数
 *
 * ・重みはSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦をユニット数、横を入力チャネル数とする。<br>
 * ・入力の総和と出力格納用はdouble型のポインタのポインタを使い、
 *   縦を学習用サンプル数として横を畳み込み層の出力ユニット数とする。<br>
 * ・学習での畳み込み層の計算で使う∂C/∂y格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 *
 *  @param unit_num 出力ユニットの数
 *  @param in_num 入力チャネル数
 *  @param w 重みの幅
 *  @param h 重みの高さ
 *  @param s_num ミニバッチ用サンプル数
 */
AffineLayer::AffineLayer(AffineLayerParams p) :
  unit_num(p.unit_num),
  in_ch_num(p.in_ch_num),
  w_width(p.w_width),
  w_height(p.w_height),
  minibatch_size(p.minibatch_size),
  learn_ratio(p.learn_ratio),
  alpha(p.alpha),
  lambda(p.lambda) {

  // SimData型の二次元配列の確保
  in.create_data_size(in_ch_num, unit_num, w_width, w_height);
  dW.create_data_size(in_ch_num, unit_num, w_width, w_height);
  W.create_data_size(in_ch_num, unit_num, w_width, w_height);
  SimDataFactory::replace_random_filter(&W);
}

AffineLayer::~AffineLayer()
{
  if(unit_num > 0){
    W.delete_array();
    dW.delete_array();
  }
}


/** 入出力計算をする関数
 *
 *  @param  *in クラスPoolLayer型のポインタ
 */
std::vector<double> AffineLayer::forward(Nistk::SimDataArray input) {
  in = input;
  std::vector<double> out;

  repeat(in_sample, in.get_height()){
    repeat(unit_num, W.get_height()){
      double summation;
      repeat(in_ch, in.get_width()){
        summation += Calc::dot(
          in.get_simdata_ptr(in_ch, in_sample),
          W.get_simdata_ptr(in_ch, unit_num));
      }
      // TODO: 下手くそhack
      out.push_back(summation);
      summation = 0;
    }
  }
  return out;
}

Nistk::SimDataArray AffineLayer::backward(std::vector<double> dout) {
  Nistk::SimDataArray dX(in.get_width(), in.get_height(), in.get_array_width(0,0), in.get_array_height(0,0));

  repeat(minibatch_size, dX.get_height()){
    // dXのwidth == Wのwidth
    repeat(ch, dX.get_width()){
      repeat(unit_num, W.get_height()){
        repeat(x, w_width){
          repeat(y, w_height){
            double dx_data = dout[unit_num] * W.get_data(ch, unit_num, x, y);
            double dx_sum  = dx_data + dX.get_data(ch, minibatch_size, x, y);
            dX.put_data(ch, minibatch_size, x, y, dx_sum);

            double dw_data = dout[unit_num] * in.get_data(ch, minibatch_size, x, y);
            double dw_sum  = dw_data + dW.get_data(ch, minibatch_size, x, y);
            dW.put_data(ch, unit_num, x, y, dw_data);
          }
        }
      }
    }
  }
  learn();
  return dX;
}

/** 学習の計算をする関数 */
void AffineLayer::learn() {
  // w_hはdxの数と同じ(ユニット数)
  // TODO:momentum.update(W.get_data(in_ch, u_num, x, y), dW.get_data(in_ch, u_num, x, y)); //
  repeat(in_ch, in_ch_num){
    repeat(u_num, unit_num){
      repeat(x, w_width){
        repeat(y, w_height){
          W.put_data(in_ch, u_num, x, y, 
            W.get_data(in_ch, u_num, x, y) - learn_ratio * dW.get_data(in_ch, u_num, x, y));
        }
      }
    }
  }
}

/** パラメータ保存のための関数
 * 
 * AffineLayerのパラメータおよびw、x、y、b
 * dWの値をバイナリ形式で保存する関数。パラメータと値は
 * 別々のファイルに保存することにしたので、引数で指定する
 * 文字列はディレクトリ名とベースファイル名にしているので注意すること。
 * また、x、y、bについてはパラメータファイルに保存することにする。
 *
 *  @param dir_name 保存先ディレクトリ名
 *  @param base_name ファイル名の先頭に付けるベース名
 *  @param layer_num base_nameの後ろにつける数字(層番号など)
 */
void AffineLayer::save_param(const char *dir_name, const char *base_name, int layer_num) {
  std::ofstream fout;                            // 出力ファイル用
  std::ofstream fout_t;                          // text出力ファイル用
  std::string file_name;                         // 出力ファイル用文字列
  std::string txt_file_name;                     // text出力ファイル用文字列
  std::string base_name_t;                       // ベースファイル名用
  std::string filename_para = "_parameter";      // パラメータ用文字列
  std::string filename_w = "_w";                 // w用文字列
  std::string filename_dW = "_dW";         // ∂C/∂y用文字列
  char *file_name_char;
  bool flag;
  int i,j;

  std::cout << "Save parameter=" << base_name << '\n';
  // ファイル名のベースとなる文字列の設定
  base_name_t = dir_name;
  base_name_t += "/";
  base_name_t += base_name;
  base_name_t += "_";
  base_name_t += std::to_string(layer_num);

  // パラメータの保存
  // ファイルのオープン　失敗すると処理せずリターン
  file_name = base_name_t + filename_para + ".dat"; 
  fout.open(file_name.c_str(), std::ios_base::binary);
  if(!fout){
    std::cout << "Can not open " << file_name << '\n';
    return;
  }
  txt_file_name = base_name_t + filename_para + ".txt"; 
  fout_t.open(txt_file_name.c_str());
  if(!fout_t){
    std::cout << "Can not open " << txt_file_name << '\n';
    return;
  }
  fout_t << "Parameter of " << base_name << '\n';
  // 基本パラメータの保存
  fout.write((char*)&unit_num, sizeof(int)); fout_t << "unit_num=" << unit_num << '\n';
  fout.write((char*)&in_ch_num, sizeof(int)); fout_t << "in_ch_num=" << in_ch_num << '\n';
  fout.write((char*)&w_width, sizeof(int)); fout_t << "w_width=" << w_width << '\n';
  fout.write((char*)&w_height, sizeof(int)); fout_t << "w_height=" << w_height << '\n';
  // 学習に関するパラメータ
  fout.write((char*)&minibatch_size, sizeof(int)); fout_t << "minibatch_size=" << minibatch_size << '\n';
  fout_t << '\n';
  fout.close();
  fout_t.close();
  std::cout << "Output file=" << txt_file_name << '\n';
  std::cout << "            " << file_name << '\n';

  // SimDataArray型のパラメータ保存
  // Wの保存
  file_name = base_name_t + filename_w + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::save_array(&W, file_name_char);
  if(flag == true) std::cout << "            " << file_name << '\n';
  delete[] file_name_char;
  // dWの保存
  file_name = base_name_t + filename_dW + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::save_array(&dW, file_name_char);
  if(flag == true) std::cout << "            " << file_name << '\n';
  delete[] file_name_char;
 
  return;
}

/** パラメータ読み込みのための関数
 * 
 * save_paraで保存したAffineLayerのパラメータおよびw、x、y、b、
 * dWの値をの値を読み込む関数。
 * また、x、y、bについてはパラメータファイルに保存することに
 * しているので、パラメータファイルより読み込む。
 * 当たり前だがこの関数を利用すると初期化する必要がない。
 *
 *  @param dir_name 保存先ディレクトリ名
 *  @param base_name ファイル名の先頭に付けたベース名
 */
void AffineLayer::load_param(const char *dir_name, const char *base_name, int layer_num) {
  std::ifstream fin;                            // 入力ファイル用
  std::string file_name;                        // 入力ファイル用文字列
  std::string base_name_t;                      // ベースファイル名用
  std::string filename_para = "_parameter";     // パラメータ用文字列
  std::string filename_w = "_w";                // w用文字列
  std::string filename_dW = "_dW";        // ∂C/∂y用文字列
  char *file_name_char;
  bool flag;
  int i,j;

  std::cout << "Load parameter=" << base_name << '\n';
  // ファイル名のベースとなる文字列の設定
  base_name_t = dir_name;
  base_name_t += "/";
  base_name_t += base_name;
  base_name_t += "_";
  base_name_t += std::to_string(layer_num);

  // パラメータの読み込み
  // ファイルのオープン　失敗すると処理せずリターン
  file_name = base_name_t + filename_para + ".dat"; 
  fin.open(file_name.c_str(), std::ios_base::binary);
  if(!fin){
    std::cout << "Can not open " << file_name << '\n';
    return;
  }
  // 基本パラメータ
  fin.read((char*)&unit_num, sizeof(int));
  fin.read((char*)&in_ch_num, sizeof(int));
  fin.read((char*)&w_width, sizeof(int));
  fin.read((char*)&w_height, sizeof(int));
  fin.read((char*)&minibatch_size, sizeof(int));

  // SimDataArray型のパラメータ読み込み
  // Wの読み込み
  file_name = base_name_t + filename_w + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::load_array(&W, file_name_char);
  if(flag == true) std::cout << "           " << file_name << '\n';
  delete[] file_name_char;
  // dWの読み込み
  file_name = base_name_t + filename_dW + ".dat"; 
  file_name_char = new char[file_name.size()+1];
  strcpy(file_name_char, file_name.c_str());
  flag = Nistk::DataIO::load_array(&dW, file_name_char);
  if(flag == true) std::cout << "           " << file_name << '\n';
  delete[] file_name_char;

  return;
}
