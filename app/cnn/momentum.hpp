#ifndef MOMENTUM
#define MOMENTUM

#include <nistk.h>

class Momentum 
{
  public:
    double learn_raito;      ///< 学習率
    double learn_alpha;      ///< モーメント係数
    double learn_lambda;     ///< モーメント減衰率
    double before_moment;  ///< 前回の修正量
    double v;                ///< 速度

    Momentum();          ///< コンストラクタ
    ~Momentum();         ///< デストラクタ
    void   init(double lr, double alpha, double lambda);
    double update(double param, double grad);
};

#endif 
