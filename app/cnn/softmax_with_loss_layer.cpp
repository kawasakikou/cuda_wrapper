#include <nistk.h>
#include <numeric>
#include "softmax_with_loss_layer.hpp"


/** ・重みはSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦をユニット数、横を入力チャネル数とする。<br>
 * ・入力の総和と出力格納用はdouble型のポインタのポインタを使い、
 *   縦を学習用サンプル数として横を畳み込み層の出力ユニット数とする。<br>
 * ・学習での畳み込み層の計算で使う∂C/∂y格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 *
 *  @param u_num 出力ユニットの数
 */

SoftmaxWithLossLayer::SoftmaxWithLossLayer(int u_num) {
  unit_num = u_num;
}

SoftmaxWithLossLayer::~SoftmaxWithLossLayer() {
}

/** 入出力計算をする関数
 *
 *  @param  *in クラスPoolLayer型のポインタ
 *  @param  *teach_one_hot_vector int型の教師データ配列
 */
std::vector<double> SoftmaxWithLossLayer::forward(std::vector<double> in, std::vector<int> teach_one_hot_vector) {
  // comment
  std::cout << "ans" << test(teach_one_hot_vector) << std::endl;
  // comment

  std::vector<double> class_results = softmax(in);
  cross_entropy_error(class_results, teach_one_hot_vector);
  return class_results;
}

std::vector<double> SoftmaxWithLossLayer::softmax(std::vector<double> in){
  std::vector<double> class_results;

  double max = *max_element( in.begin(), in.end() );
  for (auto data: in) {
    // comment
    // std::cout << data << std::endl;
    // comment
    if (max == data)
      class_results.push_back(1);
    if (max != data)
      class_results.push_back(0);
  }

  // comment
  std::cout << "res" << testd(class_results) << std::endl;
  // comment

  return class_results;
}

// comment  ------------------------------------------------------------------
int SoftmaxWithLossLayer::test(std::vector<int> in){
  int n;
  for (int i = 0; i < in.size(); i++) {
    if (in[i] == 1) {
      n = i;
    }
  }
  return n;
}

int SoftmaxWithLossLayer::testd(std::vector<double> in){
  int n;
  double max = *std::max_element(in.begin(), in.end());
  for (int i = 0; i < in.size(); i++) {
    if (in[i] == max) {
      n = i;
    }
  }
  return n;
}
// comment  ------------------------------------------------------------------

double SoftmaxWithLossLayer::cross_entropy_error(std::vector<double> class_results, std::vector<int> teach_one_hot_vector){
  double delta = 1e-7;

  double loss;
  for (int i = 0; i < 10; i++) {
    loss += teach_one_hot_vector[i] * std::log(class_results[i] + delta);
  }

  return -loss;
}

std::vector<double> SoftmaxWithLossLayer::backward(std::vector<int> teach_one_hot_vector, std::vector<double> class_results) {
  std::vector<double> dx;
  for (int i = 0; i < unit_num; i++)
    dx.push_back( class_results[i] - teach_one_hot_vector[i]);

  return dx;
}



/** パラメータ保存のための関数
 * 
 * SoftmaxWithLossLayerのパラメータおよびw、x、y、b
 * dC_dyの値をバイナリ形式で保存する関数。パラメータと値は
 * 別々のファイルに保存することにしたので、引数で指定する
 * 文字列はディレクトリ名とベースファイル名にしているので注意すること。
 * また、x、y、bについてはパラメータファイルに保存することにする。
 *
 *  @param dir_name 保存先ディレクトリ名
 *  @param base_name ファイル名の先頭に付けるベース名
 *  @param layer_num base_nameの後ろにつける数字(層番号など)
 */
void SoftmaxWithLossLayer::save_param(const char *dir_name, const char *base_name, int layer_num) {
  std::ofstream fout;                            // 出力ファイル用
  std::ofstream fout_t;                          // text出力ファイル用
  std::string file_name;                         // 出力ファイル用文字列
  std::string txt_file_name;                     // text出力ファイル用文字列
  std::string base_name_t;                       // ベースファイル名用
  std::string filename_para = "_parameter";      // パラメータ用文字列
  std::string filename_w = "_w";                 // w用文字列
  std::string filename_dC_dy = "_dC_dy";         // ∂C/∂y用文字列
  char *file_name_char;
  bool flag;
  int i,j;

  std::cout << "Save parameter=" << base_name << '\n';
  // ファイル名のベースとなる文字列の設定
  base_name_t = dir_name;
  base_name_t += "/";
  base_name_t += base_name;
  base_name_t += "_";
  base_name_t += std::to_string(layer_num);

  // パラメータの保存
  // ファイルのオープン　失敗すると処理せずリターン
  file_name = base_name_t + filename_para + ".dat"; 
  fout.open(file_name.c_str(), std::ios_base::binary);
  if(!fout){
    std::cout << "Can not open " << file_name << '\n';
    return;
  }
  txt_file_name = base_name_t + filename_para + ".txt"; 
  fout_t.open(txt_file_name.c_str());
  if(!fout_t){
    std::cout << "Can not open " << txt_file_name << '\n';
    return;
  }
  fout_t << "Parameter of " << base_name << '\n';
  // 基本パラメータの保存
  fout.write((char*)&unit_num, sizeof(int)); fout_t << "unit_num=" << unit_num << '\n';
  // 学習に関するパラメータ
  fout_t << '\n';
  fout.close();
  fout_t.close();
  std::cout << "Output file=" << txt_file_name << '\n';
  std::cout << "            " << file_name << '\n';

}

/** パラメータ読み込みのための関数
 * 
 * save_paraで保存したSoftmaxWithLossLayerのパラメータおよびw、x、y、b、
 * dC_dyの値をの値を読み込む関数。
 * 当たり前だがこの関数を利用すると初期化する必要がない。
 *
 *  @param dir_name 保存先ディレクトリ名
 *  @param base_name ファイル名の先頭に付けたベース名
 */
void SoftmaxWithLossLayer::load_param(const char *dir_name, const char *base_name, int layer_num) {
  std::ifstream fin;                            // 入力ファイル用
  std::string file_name;                        // 入力ファイル用文字列
  std::string base_name_t;                      // ベースファイル名用
  std::string filename_para = "_parameter";     // パラメータ用文字列
  std::string filename_w = "_w";                // w用文字列
  std::string filename_dC_dy = "_dC_dy";        // ∂C/∂y用文字列
  char *file_name_char;
  bool flag;
  int i,j;

  std::cout << "Load parameter=" << base_name << '\n';
  // ファイル名のベースとなる文字列の設定
  base_name_t = dir_name;
  base_name_t += "/";
  base_name_t += base_name;
  base_name_t += "_";
  base_name_t += std::to_string(layer_num);

  // パラメータの読み込み
  // ファイルのオープン　失敗すると処理せずリターン
  file_name = base_name_t + filename_para + ".dat"; 
  fin.open(file_name.c_str(), std::ios_base::binary);
  if(!fin){
    std::cout << "Can not open " << file_name << '\n';
    return;
  }
  // 基本パラメータ
  fin.read((char*)&unit_num, sizeof(int));
}
