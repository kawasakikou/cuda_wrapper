#ifndef CONV_LAYER
#define CONV_LAYER

#include <nistk.h>
#include "layer_params.hpp"

using sda = Nistk::SimDataArray;

/** 畳み込みニューラルネットワーク(CNN)の畳み込み層のためのクラス
 *
 * CNNの畳み込み層のためのクラス。<br>
 * ・フィルタはSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦を畳み込み層のチャネル、横を入力チャネル数とする。<br>
 * ・畳み込み結果と出力格納用はSimDataArrayを使い、縦を学習用サンプル数として
 *   横を畳み込み層のチャネルとする。<br>
 * ・バイアス用はSimDataArrayを使い、縦を1として
 *   横を畳み込み層のチャネルとする。<br>
 * ・学習でのプーリング層の計算で使う∂C/∂x格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 * ・関数funcは容易に入出力計算用関数を変更できるように別関数とする。
 **/

class ConvLayer
{
  public:
    // 基本
    int conv_ch_num;                   ///< 畳み込み層のチャネル数
    int in_ch_num;                     ///< 入力チャネル数
    int w_width;                       ///< フィルタの幅
    int w_height;                      ///< フィルタの高さ
    int width;                         ///< 畳み込み結果及び出力の幅
    int height;                        ///< 畳み込み結果及び出力の高さ
    int stride;                        ///< 畳み込み計算のストライド
    // 学習
    int minibatch_size;                ///< ミニバッチ用サンプル数
    int minibatch_count;               ///< ミニバッチ用サンプル数用カウンタ
    double learn_ratio;                ///< 学習率
    double alpha;                      ///< 慣性項用係数
    double lambda;                     ///< 減衰項用係数
    sda X;             ///< 入力用SimDataArray
    sda W;             ///< フィルタ用SimDataArray
    sda preactive;     ///< 畳み込み結果x 格納用SimDataArray
    sda bias;          ///< バイアスb 格納用SimDataArray
    sda out;           ///< 出力y 格納用SimDataArray
    // REVIEW: バイアスは学習しない方向で

    ConvLayer();
    ~ConvLayer();
    void init(ConvLayerParams p);
    sda forward( sda in);
    sda backward(sda in);

    void save_param(const char *dir_name, const char *base_name, int layer_num);
    void load_param(const char *dir_name, const char *base_name, int layer_num);

  private:
    sda update_dX(sda dY);
    sda update_dW(sda dY);
    void learn(sda dW);
    void ReLu_activate();
    void input_single_weight(Nistk::SimData *in, Nistk::SimData *weight, Nistk::SimData *out);
    double weighted_sum(Nistk::SimData *in, Nistk::SimData *weight, int x, int y);
};

#endif // CONV_LAYER
