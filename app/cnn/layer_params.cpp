#include <gtkmm.h>
#include <nistk.h>
#include "parameter.h"
#include "layer_params.hpp"


BaseLayerParams::BaseLayerParams(){
  minibatch_size  = 1;
}

BaseLayerParams::~BaseLayerParams(){}

InLayerParams::InLayerParams(){
  in_ch_num  = 1;   // 入力層のチャネル数
  img_width  = 28;  // 入力画像の幅
  img_height = 28;  // 入力画像の高さ
}

InLayerParams::~InLayerParams(){}

ConvLayerParams::ConvLayerParams(int layer_num){
  learn_ratio  = 0.001;
  alpha        = 0.3;
  lambda       = 0.1;
  init_with_layer_num(layer_num);
}

ConvLayerParams::~ConvLayerParams(){}

void ConvLayerParams::init_with_layer_num(int layer_num){
  if (layer_num == 0) {
    conv_ch_num     = 16;
    in_ch_num       = 1 ;
    w_width         = 11;
    w_height        = 11;
    width           = 26;
    height          = 26;
    stride          = 1 ;
  }

  if (layer_num == 1) {
    conv_ch_num     = 16;
    in_ch_num       = 16;
    w_width         = 5 ;
    w_height        = 5 ;
    width           = 13;
    height          = 13;
    stride          = 1 ;
  }
}

PoolLayerParams::PoolLayerParams(int layer_num) {
  flag_max = true;
  init_with_layer_num(layer_num);
}

PoolLayerParams::~PoolLayerParams(){}

void PoolLayerParams::init_with_layer_num(int layer_num){
  if (layer_num == 0) {
    in_pool_ch_num = 16;
    pool_width     = 2;
    pool_height    = 2;
    width          = 13;
    height         = 13;
    stride         = 2;
  }

  if (layer_num == 1) {
    in_pool_ch_num = 16;
    pool_width     = 3;
    pool_height    = 3;
    width          = 6;
    height         = 6;
    stride         = 2;
  }
}

AffineLayerParams::AffineLayerParams(){
  unit_num        = OL_UNIT_NUM;
  in_ch_num       = OL_CH_NUM;
  w_width         = OL_WIDTH;
  w_height        = OL_HEIGHT;
  minibatch_size  = MINIBATCH_SIZE;
  learn_ratio     = LEARN_RATIO;
  alpha           = LEARN_ALPHA;
  lambda          = LEARN_LAMBDA;
}

AffineLayerParams::~AffineLayerParams(){}
