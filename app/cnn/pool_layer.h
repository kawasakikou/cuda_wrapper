#ifndef POOL_LAYER
#define POOL_LAYER

#include <nistk.h>
#include "layer_params.hpp"

/** 畳み込みニューラルネットワーク(CNN)のプーリング層のためのクラス
 *
 * CNNのプーリング層のためのクラス。<br>
 * ・この層のチャネル数は入力チャネル数と同じになるので
 *   channel_numは入力チャネル数とプーリング層共通となる。<br>
 * ・出力格納用はSimDataArrayを使い、縦を学習用サンプル数として
 *   横をプーリング層のチャネルとする。<br>
 * ・出力が最大プーリングを使う際の入力の縦横位置格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横をプーリング層のチャネルとする。
 *   ただし、原点は左上であり、格納値はdoubleなどの利用の際はキャストが必要。<br>
 * ・学習での畳み込み層の計算で使う∂C/∂y格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 **/
class PoolLayer {
 public:
  int in_pool_ch_num;              ///< チャネル数
  int pool_width;               ///< 集約の局所領域の幅
  int pool_height;              ///< 集約の局所領域の高さ
  int width;                    ///< 出力の幅
  int height;                   ///< 出力の高さ
  int stride;                   ///< 計算のストライド
  bool flag_max;                ///< 最大プーリングを使うかのフラグ
  int minibatch_size;               ///< ミニバッチ用サンプル数

  Nistk::SimDataArray X;        ///< 入力x 格納用SimDataArray
  Nistk::SimDataArray out;  ///< 出力y 格納用SimDataArray

  PoolLayer();
  ~PoolLayer();
  void init(PoolLayerParams p);
  void max_pooling(Nistk::SimData *in, Nistk::SimData *out);
  double single_max_pooling(Nistk::SimData *in, int start_x, int start_y);

  Nistk::SimDataArray forward(Nistk::SimDataArray in_val);
  Nistk::SimDataArray backward(Nistk::SimDataArray dx);
};

#endif // POOL_LAYER
