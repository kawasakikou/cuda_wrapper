#ifndef SOFTMAX_WITH_LOSS_LAYER
#define SOFTMAX_WITH_LOSS_LAYER

#include<nistk.h>

/** 畳み込みニューラルネットワーク(CNN)の出力層のためのクラス
 *
 * CNNの出力層のためのクラス。<br>
 * ・重みはSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦をユニット数、横を入力チャネル数とする。<br>
 * ・入力の総和と出力格納用はdouble型のポインタのポインタを使い、
 *   縦を学習用サンプル数として横を畳み込み層の出力ユニット数とする。<br>
 * ・学習での畳み込み層の計算で使う∂C/∂y格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 * ・関数funcは容易に入出力計算用関数を変更できるように別関数とする。
 *
 **/

class SoftmaxWithLossLayer 
{
 public:
  int unit_num;                           ///< 出力ユニットの数(今回は10)
  
  SoftmaxWithLossLayer(int u_num);
  ~SoftmaxWithLossLayer();

  std::vector<double> forward(std::vector<double> in, std::vector<int> label);
  std::vector<double> backward(std::vector<int> teach_data, std::vector<double> class_results);

  std::vector<double> softmax(std::vector<double> in);
  double cross_entropy_error(std::vector<double> class_results, std::vector<int> teach_one_hot_vector);
  int   test(std::vector<int> in);
  int   testd(std::vector<double> in);

  void save_param(const char *dir_name, const char *base_name, int layer_num);
  void load_param(const char *dir_name, const char *base_name, int layer_num);
};

#endif
