#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <nistk.h>
#include "in_layer.h"
#include "layer_params.hpp"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>

/** 初期化を行う関数
 * ・入力はSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦をサンプル数、横を入力チャネル数とする。<br> 
 *
 *  @param  ch_num チャネル数
 *  @param  w 入力画像の幅
 *  @param  h 入力画像の高さ
 *  @param  s_num ミニバッチ用サンプル数
 */

InLayer::InLayer(InLayerParams p) : 
  in_ch_num(     p.in_ch_num), 
  minibatch_size(p.minibatch_size), 
  img_width(     p.img_width),
  img_height(    p.img_height) {

  out.create_data_size(in_ch_num, minibatch_size, img_width, img_height);
}

InLayer::~InLayer() { out.delete_array(); }

Nistk::SimDataArray InLayer::forward() {
  return out; 
}

/** 画像をセットする関数
 *
 * TODO: ほぼ機能させてない、不必要っぽいから。
 *
 * 入力画像をセットすると、チャネルのカウンタが更新されるようにしてある。
 * また、画像が入力チャネル数分各のされているときに、次の画像がセットされると
 * サンプル数のカウンタが更新されるようになってあり、さらにサンプル数が
 * 設定値と同じになるとサンプル数のカウンタが0にリセットされるように
 * してある。
 *
 *  @param  m_pixbuf 入力画像用Gdk::Pixbuf型スマートポインタ
 */
void InLayer::set_image_from_simdata(Nistk::SimData *sim_data) {
  int sample_count = 0;
  int channel_count = 0;

  // 入力画像のセット
  // TODO:これ大丈夫?for
  out.set_simdata(sim_data, channel_count, sample_count);

  // 入力画像の格納数がサンプル数になっているかチェックしてカウンタをセット
  if((sample_count == minibatch_size) &&  (channel_count == in_ch_num)){
       sample_count = 0;
       channel_count = 0;
  }
  else if(channel_count == in_ch_num){
    sample_count++;
    channel_count = 0;
  }
  else channel_count++;
}
