#include <iostream>
#include <gtkmm.h>
#include <nistk.h>
#include <vector>
#include <cassert>
#include <random>
#include "read_mnist.hpp" 
#include "sim_data_factory.hpp" 

#define repeat(i,n) for (int i = 0; (i) < (n); (i)++)

std::vector<Nistk::SimData> SimDataFactory::create_mnist_data(bool train_flag){
  read_mnist c1;
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  std::vector<Nistk::SimData> mnist_data;

  if (train_flag) {
    mnist_data.reserve(60000);
    assert( c1.read_images("data/train-images-idx3-ubyte"));
    assert( c1.read_labels("data/train-labels-idx1-ubyte"));
    c1.read_images("data/train-images-idx3-ubyte");
    c1.read_labels("data/train-labels-idx1-ubyte");
  }

  if (!train_flag) {
    mnist_data.reserve(10000);
    assert( c1.read_images("data/t10k-images-idx3-ubyte"));
    assert( c1.read_labels("data/t10k-labels-idx1-ubyte"));
    c1.read_images("data/t10k-images-idx3-ubyte");
    c1.read_labels("data/t10k-labels-idx1-ubyte");
  }
  

  //images() returns image files
  //example: image[0][0] is 1st pixel datum of 1st image
  //example: image[1][20] is 20th pixel datum of 2nd image
  std::vector<std::vector<unsigned char> >image = c1.images();
  std::vector<unsigned char>label = c1.labels();

  int w = 28;
  int h = 28;
  double normalization = 255;

  for(int i = 0; i < label.size(); i++){
    mnist_data.push_back(Nistk::SimData(w,h));
  }

  for(int i = 0; i < label.size(); i++){
    for (int j = 0; j < w * h; j++) {
      mnist_data[i].put_data_1d(j, image[i][j] / normalization);
    }
  }

  std::cout << "aaaaaa" << std::endl;
  return mnist_data;
}

std::vector<int> SimDataFactory::create_mnist_label(bool train_flag){
  read_mnist c;

  if (train_flag) {
    assert( c.read_labels("data/train-labels-idx1-ubyte"));
    c.read_labels("data/train-labels-idx1-ubyte");
  }

  if (!train_flag) {
    assert( c.read_labels("data/t10k-labels-idx1-ubyte"));
    c.read_labels("data/t10k-labels-idx1-ubyte");
  }

  std::vector<unsigned char>label = c.labels();
  std::vector<int>int_label;
  for (auto x : label) {
    int_label.push_back((int)x);
  }

  return int_label;
}

std::vector<int> SimDataFactory::create_teach_data_for_mnist(int ans_num){

  std::vector<int> teach_data(10, 0);
  teach_data[ans_num] = 1;

  return teach_data;
}

void SimDataFactory::replace_random_filter(Nistk::SimDataArray *sda){
  std::random_device rnd;
  std::mt19937 generator(rnd());
  std::uniform_real_distribution<double> dis(0.0, 1.0);

  repeat(sda_h, sda->get_height())
    repeat(sda_w, sda->get_width())
      repeat(h, sda->get_array_height(0,0))
        repeat(w, sda->get_array_width(0,0))
          sda->put_data(sda_w, sda_h, w, h, dis(generator));
}
