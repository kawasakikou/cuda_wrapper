#ifndef IN_LAYER
#define IN_LAYER

#include <nistk.h>
#include "layer_params.hpp"

/** 畳み込みニューラルネットワーク(CNN)の入力層のためのクラス
 *
 * CNNの入力層のためのクラス。<br>
 * ・入力はSimData型を2次元配列として確保できるSimDataArray
 *   を使い、縦をサンプル数、横を入力チャネル数とする。<br>
 **/

class InLayer {
  public:
    InLayer(InLayerParams in_layer_params);
    ~InLayer();

    Nistk::SimDataArray forward();

    void set_image_from_simdata(Nistk::SimData *sim_data);

  private:
    const int in_ch_num;
    const int minibatch_size;
    const int img_width;
    const int img_height;
    Nistk::SimDataArray out;
};

#endif
