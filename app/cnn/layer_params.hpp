#ifndef LayerParams
#define LayerParams

#include <nistk.h>


class BaseLayerParams {
  public:
    int minibatch_size;

    BaseLayerParams();
    ~BaseLayerParams();
};


class InLayerParams : public BaseLayerParams{
  public:
    int in_ch_num;
    int img_width;
    int img_height;

    InLayerParams();
    ~InLayerParams();
};

class ConvLayerParams : public BaseLayerParams{
  public:
    int conv_ch_num;
    int in_ch_num;
    int w_width;
    int w_height;
    int width;
    int height;
    int stride;
    double learn_ratio;
    double alpha;
    double lambda;

    ConvLayerParams(int layer_num);
    ~ConvLayerParams();
    void init_with_layer_num(int layer_num);
};


class PoolLayerParams : public BaseLayerParams{
  public:
    int in_pool_ch_num;
    int pool_width;
    int pool_height;
    int width;
    int height;
    int stride;
    bool flag_max;

    PoolLayerParams(int layer_num);
    ~PoolLayerParams();
    void init_with_layer_num(int layer_num);
};

class AffineLayerParams {
  public:
    int unit_num;                       ///< 出力ユニットの数
    int in_ch_num;                      ///< 入力チャネル数
    int w_width;                        ///< 重みの幅
    int w_height;                       ///< 重みの高さ
    int minibatch_size;                 ///< ミニバッチ用サンプル数
    double learn_ratio;
    double alpha;
    double lambda;

    AffineLayerParams();
    ~AffineLayerParams();
};

#endif 
