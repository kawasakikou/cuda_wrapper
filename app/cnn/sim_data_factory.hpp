#ifndef SIM_DATA_FACTORY
#define SIM_DATA_FACTORY

#include <nistk.h>
#include <vector>

class SimDataFactory
{
 public:
   static std::vector<Nistk::SimData> create_mnist_data(bool train_flag);
   static std::vector<int>            create_mnist_label(bool train_flag);
   static std::vector<int>            create_teach_data_for_mnist(int ans_num);
   static void replace_random_filter(Nistk::SimDataArray *sda);
};

#endif 
