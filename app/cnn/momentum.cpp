#include <gtkmm.h>
#include <nistk.h>
#include "momentum.hpp"


Momentum::Momentum(){
  learn_raito   = 0;
  learn_alpha   = 0;
  learn_lambda  = 0; 
  before_moment = 0;
  v             = 1;            
}

Momentum::~Momentum(){}

void Momentum::init(double lr, double alpha, double lambda){
  learn_raito  = lr;
  learn_alpha  = alpha;
  learn_lambda = lambda;
}

double Momentum::update(double param, double grad){
  v = (learn_alpha * v) - (learn_raito * grad);
  return param + v - before_moment * learn_raito * learn_lambda;
}
