#include <nistk.h>
#include "pool_layer.h"

#define repeat(i,n) for (int i = 0; (i) < (n); (i)++)
using sda = Nistk::SimDataArray;


PoolLayer::PoolLayer() {
  in_pool_ch_num = 0;
  pool_width = 0;
  pool_height = 0;
  width = 0;
  height = 0;
  stride = 1;
  //
  minibatch_size = 0;
}

PoolLayer::~PoolLayer() {
  if(width > 0){
    out.delete_array();
  }
}

/** 初期化を行う関数
 *
  * ・この層のチャネル数は入力チャネル数と同じになるので
 *   in_pool_ch_numは入力チャネル数とプーリング層共通となる。<br>
 * ・出力格納用はSimDataArrayを使い、縦を学習用サンプル数として
 *   横をプーリング層のチャネルとする。<br>
 * ・出力が最大プーリングを使う際の入力の縦横位置格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横をプーリング層のチャネルとする。
 *   ただし、原点は左上であり、格納値はdoubleなどの利用の際はキャストが必要。<br>
 * ・学習での畳み込み層の計算で使う∂C/∂y格納用はSimDataArrayを使い、
 *   縦を学習用サンプル数として横を畳み込み層のチャネルとする。<br>
 *
 *  @param  ch_num チャネル数
 *  @param  p_w 集約の局所領域の幅
 *  @param  p_h 集約の局所領域の高さ
 *  @param  w 出力の幅
 *  @param  h 出力の高さ
 *  @param  strd 計算のストライド
 *  @param  s_num ミニバッチ用サンプル数
 *  @param  flag 最大プーリングを使うかのフラグ(true:使用 false:不使用)
 */
void PoolLayer::init(PoolLayerParams p) {
  // パラメータのメンバへの格納
  in_pool_ch_num = p.in_pool_ch_num;
  pool_width         = p.pool_width;
  pool_height        = p.pool_height;
  width              = p.width;
  height             = p.height;
  stride             = p.stride;
  flag_max           = p.flag_max;
  minibatch_size     = p.minibatch_size;

  out.create_data_size(in_pool_ch_num, minibatch_size, width, height);
}

sda PoolLayer::forward(sda in) {
  X = in;

  repeat(y, in.get_height())
    repeat(x, in.get_width())
      PoolLayer::max_pooling(in.get_simdata_ptr(x, y), out.get_simdata_ptr(x, y));

  return out;
}


void PoolLayer::max_pooling(Nistk::SimData *in, Nistk::SimData *out) {
  repeat(out_y, out->get_height())
    repeat(out_x, out->get_width())
      out->put_data(out_x, out_y, PoolLayer::single_max_pooling(in, stride * out_x, stride * out_y));
}

double PoolLayer::single_max_pooling(Nistk::SimData *in, int start_x, int start_y) {
  double max = 0;
  for (int h = 0; h < pool_height; h++) {
    for (int w = 0; w < pool_width; w++) {
      // REVIEW: はみ出した分は考えずにmaxを算出(無視)
      if (max < in->get_data(w + start_x, h + start_y)&&
          w + start_x < in->get_width() &&
          h + start_y < in->get_height() ) {
        max = in->get_data(w + start_x, h + start_y);
      }
    }
  }
  return max;
}

sda PoolLayer::backward(sda dy) {
  sda dX(X.get_width(), X.get_height(), X.get_array_width(0, 0), X.get_array_height(0,0));

  repeat(minibatch_size, out.get_height()){
    repeat(ch, out.get_width()){
      repeat(out_y, out.get_array_height(0,0)){
        repeat(out_x, out.get_array_width(0,0)){
          repeat(h, pool_height){
            repeat(w, pool_width){
              // そのほかはもう0なので無視すると0になってる
              if (out.get_data(ch, minibatch_size, out_x, out_y) == X.get_data(ch, minibatch_size, stride * out_x + w, stride * out_y + h)) {
                double d = dy.get_data(ch, minibatch_size, out_x, out_y);
                dX.put_data(ch, minibatch_size, stride * out_x + w, stride * out_y + h, d); 
              }
            }
          }
        }
      }
    }
  }
  return dX;
}
