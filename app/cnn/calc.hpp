#ifndef CALC
#define CALC

#include <nistk.h>

class Calc 
{
 public:
   static double dot(Nistk::SimData *a, Nistk::SimData *b);
   static void   input_single_weight(Nistk::SimData *in, Nistk::SimData *weight, Nistk::SimData *out, int stride);
   static double weighted_sum(Nistk::SimData *in, Nistk::SimData *weight, int x, int y);
};

#endif 
