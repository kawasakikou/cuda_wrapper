#include <gtkmm.h>
#include <nistk.h>
#include "calc.hpp"


#define repeat(i,n) for (int i = 0; (i) < (n); (i)++)

double Calc::dot(Nistk::SimData *a, Nistk::SimData *b){
  double summation;
  for (int i = 0; i < a->get_height(); i++) {
    for (int j = 0; j < a->get_width() ; j++) {
      summation += a->get_data(i, j) * b->get_data(i, j);
    }
  }
  return summation;
}

void Calc::input_single_weight(Nistk::SimData *in, Nistk::SimData *weight, Nistk::SimData *out, int stride) {
  repeat(y, out->get_height())
    repeat(x, out->get_width())
      out->put_data(x,y, weighted_sum(in, weight, stride * x, stride * y));
}

double Calc::weighted_sum(Nistk::SimData *in, Nistk::SimData *weight, int x, int y) {
  int w_height = weight->get_height();
  int w_width = weight->get_width();
  int wc_x = w_width/2;
  int wc_y = w_height/2;

  double w_sum = 0.0;
  for(int i = -1*(int)(w_height/2); i <= (int)(w_height/2); i++){
    if(((y+i) >= 0) && ((y+i) < in->get_height() ) && ((wc_y+i) >= 0) && ((wc_y+i) < w_height)){
      for(int j = -1*(int)(w_width/2); j <= (int)(w_width/2); j++){
        if(((x+j) >= 0) && ((x+j) < in->get_width()) && ((wc_x+j) >= 0) && ((wc_x+j) < w_width)){
          w_sum += weight->get_data(wc_x+j,wc_y+i) * in->get_data(x+j,y+i);
        }
      }
    }
  }
  return w_sum;
}

