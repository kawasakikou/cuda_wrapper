#include <iostream>
#include <chrono>
#include <fstream>
#include <gtkmm.h>
#include <nistk.h>
#include "in_layer.h"
#include "conv_layer.h"
#include "pool_layer.h"
#include "affine_layer.hpp"
#include "softmax_with_loss_layer.hpp"
#include "parameter.h"
#include "sim_data_factory.hpp"


using sda = Nistk::SimDataArray;
using dbv = std::vector<double>;
using inv = std::vector<int>;
using sdv = std::vector<Nistk::SimData>;

void print_arr(Nistk::SimDataArray * data, char* in_name) {
  Nistk::ImageArray b;
  b.array_from_simdataarray(data, 10, 3, data->get_length());
  Glib::RefPtr<Gdk::Pixbuf> in_pixbuf;
  b.set_name(in_name);
  Gtk::Main::run(b);
}

int main(int argc,char *argv[]) {
  Gtk::Main kit(argc,argv); // gtkmmを使う際には必ず必要

  InLayerParams in_params;
  InLayer in_layer(in_params);

  std::vector<ConvLayer> c_layer(CL_NUM);
  ConvLayerParams c0_params(0);// 1層目
  ConvLayerParams c1_params(1);// 3層目

  std::vector<PoolLayer> p_layer(PL_NUM);
  PoolLayerParams p0_params(0);// 2層目
  PoolLayerParams p1_params(1);// 4層目

  AffineLayerParams affine_params;
  AffineLayer affine_layer(affine_params);

  SoftmaxWithLossLayer soft_loss_layer(OL_UNIT_NUM);

  c_layer[0].init(c0_params); 
  c_layer[1].init(c1_params); 
  p_layer[0].init(p0_params);
  p_layer[1].init(p1_params);

  std::ifstream ifs("./output_parameter/c_layer_0_W.dat");
  if (ifs.is_open()) {
    c_layer[0].load_param("output_parameter", "c_layer", 0);
    c_layer[1].load_param("output_parameter", "c_layer", 1);
    affine_layer.load_param("output_parameter", "affine_layer", 0);
    soft_loss_layer.load_param("output_parameter", "soft_loss_layer", 0);
  }
  
  sdv mnist_data = SimDataFactory::create_mnist_data(true);
  inv mnist_label = SimDataFactory::create_mnist_label(true);

  for (int i = 0; i < 60000; i++) {
    inv teach_one_hot_vector = SimDataFactory::create_teach_data_for_mnist(mnist_label[i]);
    std::cout << "Forward Processing "<< i << "th data is" << mnist_label[i] << std::endl;

    in_layer.set_image_from_simdata(&mnist_data[i]);
    sda in_data = in_layer.forward();

    auto c0start = std::chrono::system_clock::now();
    sda c0_data = c_layer[0].forward(in_data); //print_arr(&c0_data, "fc0");
    auto c0end = std::chrono::system_clock::now();

    sda p0_data = p_layer[0].forward(c0_data); //print_arr(&p0_data, "fp0");
 
    auto c1start = std::chrono::system_clock::now();
    sda c1_data = c_layer[1].forward(p0_data); //print_arr(&c1_data, "fc1");
    auto c1end = std::chrono::system_clock::now();

    sda p1_data = p_layer[1].forward(c1_data); //print_arr(&p1_data, "fp1");

    dbv out_affine = affine_layer.forward(p1_data);
    dbv class_res  = soft_loss_layer.forward(out_affine, teach_one_hot_vector);

    auto c0diff = c0end - c0start;
    auto c1diff = c1end - c1start;
    std::cout << "elapsed time = "
        << std::chrono::duration_cast<std::chrono::milliseconds>(c0diff).count() << " msec."
        << std::chrono::duration_cast<std::chrono::milliseconds>(c1diff).count() << " msec."
        << std::endl;

    std::cout << "backward Processing "<< i << "th data is" << mnist_label[i] << std::endl;

    dbv dout_soft_loss = soft_loss_layer.backward(teach_one_hot_vector, class_res);
    sda dx = affine_layer.backward(dout_soft_loss);

    sda dp1 = p_layer[1].backward(dx);

    auto bc1start = std::chrono::system_clock::now();
    sda dc1 = c_layer[1].backward(dp1);
    auto bc1end = std::chrono::system_clock::now();

    sda dp0 = p_layer[0].backward(dc1);

    auto bc0start = std::chrono::system_clock::now();
    c_layer[0].backward(dp0);
    auto bc0end = std::chrono::system_clock::now();

    auto bc0diff = bc0end - bc0start;
    auto bc1diff = bc1end - bc1start;
    std::cout << "elapsed time = "
        << std::chrono::duration_cast<std::chrono::milliseconds>(bc0diff).count() << " msec."
        << std::chrono::duration_cast<std::chrono::milliseconds>(bc1diff).count() << " msec."
        << std::endl;
  }

  // save parameter
  c_layer[0].save_param("output_parameter", "c_layer", 0);
  c_layer[1].save_param("output_parameter", "c_layer", 1);
  affine_layer.save_param("output_parameter", "affine_layer", 0);
  soft_loss_layer.save_param("output_parameter", "soft_loss_layer", 0);

  sdv mnist_test_data = SimDataFactory::create_mnist_data(false);
  inv mnist_test_label = SimDataFactory::create_mnist_label(false);

  // 認識率のためのテスト入力による処理
  int correct_count = 0;
  int test_num = 10000;
  for(int t = 0; t < test_num; t++){
  
    inv test_one_hot_vector = SimDataFactory::create_teach_data_for_mnist(mnist_test_label[t]);
  
    std::cout << "Test Processing "<< t << "th data is" << mnist_test_label[t] << std::endl;
    in_layer.set_image_from_simdata(&mnist_test_data[t]);
    sda in_data = in_layer.forward();
    sda c0_data = c_layer[0].forward(in_data); 
    sda p0_data = p_layer[0].forward(c0_data); 
    sda c1_data = c_layer[1].forward(p0_data); 
    sda p1_data = p_layer[1].forward(c1_data); 
    dbv out_affine = affine_layer.forward(p1_data);
    dbv class_res  = soft_loss_layer.forward(out_affine, test_one_hot_vector);
  
    for (int a = 0; a < class_res.size(); a++) {
      if (class_res[a] == 1 && a == mnist_test_label[t]) {
        correct_count++;
      }
    }
  }

  std::cout << "correct_count   is" << correct_count << std::endl;
  std::cout << "correct_percent is" << (double)correct_count / test_num << std::endl;

  return 0;
}
