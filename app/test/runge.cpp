#include <iostream>
#include <cmath>
#include <nistk.h>
#include <user_runge_exec.h>
#include <stdio.h>



int main(int argc, char **argv) {

  double t  = 0;
  double x[4] = {0.0, 0.1, 0.2, 0.3};
  double dt = 0.01;

  Gnistk::Runge::Si(t, x, 0.0, 0.0, dt);

  for (int i = 0; i < 4; i++)
    printf("args change? %d %lf \n", i, x[i]);

  return 0;
}
