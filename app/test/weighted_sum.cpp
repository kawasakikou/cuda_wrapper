#include<iostream>
#include<gtkmm.h>
#include<nistk.h>
#include<gnistk.h>

#define W_WIDTH       51
#define W_HEIGHT      51
#define OUT_WIDTH     87
#define OUT_HEIGHT    91
#define CALC_STEP     2
#define SIGMOID_THETA 2.0
#define SIGMOID_GAIN  3.0

int main(int argc,char *argv[])
{
  Gtk::Main kit(argc,argv);
  Nistk::ImageView window;
  Glib::RefPtr<Gdk::Pixbuf> in_image;
  Glib::RefPtr<Gdk::Pixbuf> image_buf;
  Nistk::SimData in_data;
  Nistk::SimData weight_data;
  Nistk::SimData out_data;
  int x,y;
  char win_name[][15] = {"input data", "weight data", "out data"};
  double tmp;

  in_image = Gdk::Pixbuf::create_from_file("image/test-fig1.png");
  in_data.create_data_size(in_image->get_width(),in_image->get_height());
  Nistk::ImageData::simdata_from_pixbuf(&in_data,in_image,2);
  image_buf = Nistk::ImageData::create_pixbuf_new(in_data.get_width(), in_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&in_data,image_buf);
  window.set_image(image_buf);
  window.set_name(win_name[0]);
  Gtk::Main::run(window);
  std::cout << "pass 1 \n";

  weight_data.create_data_size(W_WIDTH,W_HEIGHT);
  Nistk::MathTools::simdata_from_DoG(&weight_data, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 3.0, 6.0, true);

  image_buf = Nistk::ImageData::resize_pixbuf(image_buf, W_WIDTH,W_HEIGHT);
  Nistk::ImageData::pixbuf_from_simdata(&weight_data, image_buf);
  window.set_image(image_buf);
  window.set_name(win_name[1]);
  Gtk::Main::run(window);
  std::cout << "pass 2 \n";

  out_data.create_data_size(OUT_WIDTH,OUT_HEIGHT);
  for(int i = 0; i < out_data.get_height(); i++){
    for(int j = 0; j < out_data.get_width(); j++){
      x = CALC_STEP * j + (int)(W_WIDTH/2);
      y = CALC_STEP * i + (int)(W_HEIGHT/2);
      tmp = Gnistk::Calc::weighted_sum(&in_data, &weight_data, x, y);
      tmp = Nistk::MathTools::sigmoid_func(tmp-SIGMOID_THETA,SIGMOID_GAIN);
      out_data.put_data(j,i,tmp);
    }
  }

  image_buf = Nistk::ImageData::resize_pixbuf(image_buf, OUT_WIDTH,OUT_HEIGHT);
  Nistk::ImageData::pixbuf_from_simdata(&out_data, image_buf);
  window.set_image(image_buf);
  window.set_name(win_name[2]);
  Gtk::Main::run(window);

  return 0;
}
