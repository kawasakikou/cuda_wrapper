#include <unistd.h>
#include <stdio.h>
#include <gnistk.h>
#include <nistk.h>
#include <user.h>
#include <chrono>
#include <iostream>


void random(int N);
void random_range(int N);
void random_0to1(int N);
void random_exp(int N);
void random_poisson(int N);
void random_erlang(int N);
void random_gaussian(int N);

int main(int argc, char const* argv[])
{
    int N = 1000000;
    random(N);
    random_range(N);
    random_0to1(N);
    random_exp(N);
    random_poisson(N);
    random_erlang(N);
    random_gaussian(N);

    // double x[11] = {0.0, 0.1,  0.2,  0.3,  0.4,  0.5,  0.6,  0.7,  0.8,  0.9,  1.0};
    // double p[11] = {0.0, 0.02, 0.36, 0.34, 0.13, 0.07, 0.03, 0.02, 0.01, 0.01, 0.01};
    // rd = user_hist(x, p, 240);
    // printf("rand_hist %d \n", r);
    return 0;
}

void random(int N) {
  int grarr[N];
  user_random(grarr, N);

  int crarr[N];
  auto cpu_start = std::chrono::system_clock::now();
  Nistk::RandGene m_rand;
  for (int i = 0; i < N; i++)
      crarr[i] = m_rand.get_rand();
  auto cpu_end = std::chrono::system_clock::now();

  auto cpu_diff = cpu_end - cpu_start;
  std::cout << "cpu random  : "
    << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count() 
    << " ms \n"
    << std::endl;
}


void random_range(int N) {
  int start = 0;
  int end   = 10;
  int shift = 0;

  int grarr[N];
  user_rand_range(grarr, N, start,end,shift);

  int crarr[N];
  auto cpu_start = std::chrono::system_clock::now();
  Nistk::RandGene m_rand;
  for (int i = 0; i < N; i++)
      crarr[i] = m_rand.get_rand_i(start,end,shift);
  auto cpu_end = std::chrono::system_clock::now();

  auto cpu_diff = cpu_end - cpu_start;
  std::cout << "cpu range"
    << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count()
    << " msec. \n"
    << std::endl;
}

void random_0to1(int N) {
  double grarr[N];
  user_rand_0to1(grarr, N);

  double crarr[N];
  // auto cpu_start = std::chrono::system_clock::now();
  // Nistk::RandGene m_rand;
  // for (int i = 0; i < N; i++)
  //     crarr[i] = m_rand.get_rand_d();
  // auto cpu_end = std::chrono::system_clock::now();
  //
  // auto cpu_diff = cpu_end - cpu_start;
  // std::cout << "cpu 0to1"
  //   << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count()
  //   << " msec. \n"
  //   << std::endl;
}

void random_exp(int N) {
  double alpha = 1.0;
  double grarr[N];
  user_exp(grarr, N, alpha);

  // int crarr[N];
  // auto cpu_start = std::chrono::system_clock::now();
  // Nistk::RandGene2 m_rand;
  // for (int i = 0; i < N; i++)
  //     crarr[i] = m_rand.get_rand_exp(alpha);
  // auto cpu_end = std::chrono::system_clock::now();
  //
  // auto cpu_diff = cpu_end - cpu_start;
  // std::cout << "cpu exp"
  //   << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count()
  //   << " msec. \n"
  //   << std::endl;
}

void random_poisson(int N) {
  double M = 1.0;
  double grarr[N];
  user_poisson(grarr, N, M);

  // int crarr[N];
  // auto cpu_start = std::chrono::system_clock::now();
  // Nistk::RandGene2 m_rand;
  // for (int i = 0; i < N; i++)
  //     crarr[i] = m_rand.get_rand_poisson(M);
  // auto cpu_end = std::chrono::system_clock::now();
  //
  // auto cpu_diff = cpu_end - cpu_start;
  // std::cout << "cpu poisson"
  //   << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count()
  //   << " msec. \n"
  //   << std::endl;
}

void random_erlang(int N) {
  double lambda = 1.0;
  int n = 8;

  int grarr[N];
  user_erlang(grarr, N, lambda, n);

  int crarr[N];
  auto cpu_start = std::chrono::system_clock::now();
  Nistk::RandGene2 m_rand;
  for (int i = 0; i < N; i++)
      crarr[i] = m_rand.get_rand_erlang(lambda, n);
  auto cpu_end = std::chrono::system_clock::now();

  auto cpu_diff = cpu_end - cpu_start;
  std::cout << "cpu erlang"
    << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count()
    << " msec. \n"
    << std::endl;
}

void random_gaussian(int N) {
  double ex = 10.0;
  double vx = 2.0;

  double grarr[N];
  user_gaussian(grarr, N, ex, vx);

  // int crarr[N];
  // auto cpu_start = std::chrono::system_clock::now();
  // Nistk::RandGene2 m_rand;
  // for (int i = 0; i < N; i++)
  //     crarr[i] = m_rand.get_rand_gaussian(ex, vx);
  // auto cpu_end = std::chrono::system_clock::now();
  //
  // auto cpu_diff = cpu_end - cpu_start;
  // std::cout << "cpu gaussian"
  //   << std::chrono::duration_cast<std::chrono::milliseconds>(cpu_diff).count()
  //   << " msec. \n"
  //   << std::endl;
}
