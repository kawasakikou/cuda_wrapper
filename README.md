CUDA_Wrapper
====

Overview

## Description


## Spec

####Nistkライブラリと同じ入出力関係を持つこと
研究時に、現在CPUでのみ記述されているプログラムがあるとする。
そのプログラムをできるだけ少ない変更でCUDA上で動かし、高速化するライブラリを作成する。
さらに、CUDA用に書き換える際に、ほぼCUDAや、GPU内部に対するアプローチを知らなくても活用できるようにする。
つまり、グローバルメモリや、コンスタントメモリなどへの転送や、効率のいいグリッドブロックスレッド数なども意識せずに

ユーザはホストコードとデバイスコードを書かなければならないが、 デバイスコードではCUDA特有の書き方をしなくてはならない。

Nistkに付属する処理のGPGPUバージョンは前もって別ライブラリに含む。


####ライブラリヘッダをインクルードするだけで使用可能であること

####CUDAを扱える環境なら環境に依存せず使用可能であること

####ニューラルネットワークシミュレーションを高速、高効率で行えること

## 実装するもの
- データ型のCUDAラップ
- 下記関数群

## 実装するデータ型
- SimData型
- SimDataArray型

## 実装する関数群
- Rand
  - get_rand_i()
  - get_rand_d()
  - get_rand_exp()
  - get_rand_poisson()
  - get_rand_erlang()
  - get_rand_gaussian()
  - get_rand_hist()
- Calc
  - weighted_sum()
  - weighted_sum2()
  - input_single_weight()
  - input_array_weight()
- runge 
  - Runge4();
  - Runge4Si()
- HHnewron
  - convert_mhn_to_MSAV();
  - calc();
  - calc_v();
  - calc_m_si();
  - calc_h_si();
  - calc_n_si();
  - calc_m();
  - calc_h();
  - calc_n();
  - calc_alpha_m();
  - calc_beta_m();
  - calc_alpha_h();
  - calc_beta_h();
  - calc_alpha_n();
  - calc_beta_n();
- IVnewron
  - calc();
  - calc_v();
  - calc_u();
