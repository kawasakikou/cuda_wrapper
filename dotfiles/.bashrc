[ -f ~/.path ] && . ~/.path

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ht='ls -a'
alias htt='ls -lta'
alias htg='ls|grep'
