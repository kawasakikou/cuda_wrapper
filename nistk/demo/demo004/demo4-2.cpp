#include<iostream>
#include<gtkmm.h>
#include<nistk.h>
#include<mathtools.h>


int main(int argc,char *argv[])
{
// gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  Nistk::ImageView window;
  Nistk::SimData s_data;
  // 変数の宣言
  double x,y,val,total1,total2,sum,xd,yd;
  char win_name[] = "DoG-image";

   // DoG 3DをSimData型に格納してして表示
  s_data.create_data_size(81,81); // わざと大きくしてある
  total1 = Nistk::MathTools::gaussian_3D_sum(0.0, 0.0, -5.0, 5.0, 
					 -5.0, 5.0, 0.1, 0.1, 1.0, true);
  total2 = Nistk::MathTools::gaussian_3D_sum(0.0, 0.0, -5.0, 5.0, 
					 -5.0, 5.0, 0.1, 0.1, 3.0, true);
  // わざとgainを変更している。これをすると全体の加算が0に
  // ならないので注意
  Nistk::MathTools::simdata_from_DoG(&s_data, 0.0, 0.0, 
				     0.1, 0.1, 1.0, 5.0, 1.0, 3.0, true);
  // SimData型の配列の左上角の座標の計算
  xd = -((double)(s_data.get_width()/2))*0.5; 
  yd = -((double)(s_data.get_height()/2))*0.5;
  m_pixbuf = Nistk::ImageData::create_pixbuf_new(s_data.get_width(),
						      s_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&s_data, m_pixbuf);
  window.set_name(win_name);
  window.set_image(m_pixbuf);

  Gtk::Main::run(window);
 
  return 0;
}
