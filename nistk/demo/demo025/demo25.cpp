#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

#define IMAGE_VIEW_NUM 1
#define I_WIDTH  101       // 荷重の幅
#define I_HEIGHT 101       // 荷重の高さ
#define STEP 0.1           // 計算の刻み幅
#define G_NUM 2            // ガウシアンの数
#define PAI 3.141592

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  // PlotView用名前の文字列の配列
  char image_name[IMAGE_VIEW_NUM][15] = {"input_test"};
  Glib::RefPtr<Gdk::Pixbuf> in_image;   // 画像用pixbuf
  Glib::RefPtr<Gdk::Pixbuf> in_image2;   // 画像用pixbuf
  Nistk::SimData in_data;               // simdat
  Nistk::SimData image_data;            // simdat
  double x_c[G_NUM] = {-2.0, 2.0};
  double y_c[G_NUM] = {2.0, -1.0};
  double gain[G_NUM] = {1.0, 1.0};
  double sigma1[G_NUM] = {2.0, 3.0};
  double sigma2[G_NUM] = {0.5, 0.5};
  double angle[G_NUM] = {(PAI / 180) * 45, (PAI / 180) * 90};
  double x, y;
  int i;

  main_window.set_size_request(200,150);
  // ImageViewの生成と名前のセット
  main_window.create_imageview(IMAGE_VIEW_NUM);
  for(i = 0; i < IMAGE_VIEW_NUM; i++) 
    main_window.set_imageview_name(i,image_name[i]);
  in_data.create_data_size(I_WIDTH,I_HEIGHT);

  /*** 
   * get_e_gaussianを計算してin_dataに格納してそれを表示
  ***/
  // 計算して格納
  Nistk::Input::get_e_gaussian(&in_data, x_c, y_c, STEP, STEP, 
			       gain, sigma1, sigma2, angle, G_NUM);
  // simdataの内容を表示
  in_image = Nistk::ImageData::create_pixbuf_new(in_data.get_width(),
						  in_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 1\n";

  /*** 
   * 入力画像イメージの配置
  ***/
  // 前回の画像に上書きして表示
  in_image2 = Gdk::Pixbuf::create_from_file("../../fig/square.png");
  image_data.create_data_size(in_image2->get_width(),in_image2->get_height());
  Nistk::ImageData::simdata_from_pixbuf(&image_data,in_image2,2);
  Nistk::Input::set_input_image(&in_data, &image_data, 20, 20, 0.0, "max");
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 2\n";
  // 初期化して回転して表示
  Nistk::MathTools::init_simdata(&in_data, 0.0);
  Nistk::Input::set_input_image(&in_data, &image_data, 20, 20, 
				(PAI / 180) * 30.0, "none");
  Nistk::ImageData::pixbuf_from_simdata(&in_data,in_image);
  main_window.get_imageview_ptr(0)->set_image(in_image);
  Gtk::Main::run(main_window);
  std::cout << "pass 3\n";

  return 0;
}

