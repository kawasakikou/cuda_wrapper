#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

#define W_WIDTH  51       // 荷重の幅
#define W_HEIGHT 51       // 荷重の高さ
#define OUT_WIDTH  47     // Arrayの幅
#define OUT_HEIGHT 47     // Arrayの高さ

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクト及び変数宣言
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;   // テンポラリ用pixbuf
  Nistk::SimData s_data;                // 入力画像用SimData
  Nistk::SimDataArray weight_data;      // 荷重用simdata
  Nistk::ImageLoader i_loader;          // イメージローダ
  Nistk::NistkMainWin main_window;      // Nistkメインウィンドウ
  Nistk::SaveArray s_array;
  char file_name1[] = "test_out";        // 出力ファイル名1
  char file_name2[] = "test_out_array";  // 出力ファイル名2
  char file_name3[] = "test_out_array2";  // 出力ファイル名2
  char file_name4[] = "test_out_loader"; // 出力ファイル名2
  char i_file_name[] = "i_file3.txt";    // イメージローダ用ファイル
  int i,j;

  // 画像をSimDataに読み込んでファイルに保存
  m_pixbuf=Gdk::Pixbuf::create_from_file("../image/AVG.png");
  s_data.create_data_size(m_pixbuf->get_width(),m_pixbuf->get_height());
  Nistk::ImageData::simdata_from_pixbuf(&s_data, m_pixbuf, 2);
  Nistk::ImageData::imagefile_from_simdata(&s_data, file_name1, 
					   "png", 1, 23.4567, 4, 2);
  std::cout << "pass1 \n";  

  /*** 
   * 荷重データを作成してそれを表示 ON-CENTER
  ***/
  weight_data.create_data_size(OUT_WIDTH, OUT_HEIGHT, W_WIDTH, W_HEIGHT);
  // 荷重データ作成
  for(i = 0; i < OUT_HEIGHT; i++){  // y
    for(j = 0; j < OUT_WIDTH; j++){  // x
      Nistk::MathTools::simdata_from_DoG(weight_data.get_simdata_ptr(j,i), 
					 0.0, 0.0,              // 中心
					 1.0, 1.0,              // x,yのステップ
					 1.0, 1.0,              // ゲイン
					 3.0, 6.0, true);       // シグマ
      // std::cout << "create weight i=" << i << " j=" << j << '\n';
    }
  }
  // 位置x=3,y=2の場所のデータだけ変更しておく
  for(i = 0; i < W_HEIGHT; i++){  // y
    for(j = 0; j < W_WIDTH; j++){  // x
      weight_data.put_data(3, 2, j, i, 1.0); 
    }
  }
  weight_data.put_data(3, 2, 10, 10, 0.0); 
  std::cout << "pass2 \n";
  // ImageArrayの生成と名前のセット
  main_window.create_imagearray(1);
  main_window.set_imagearray_name(0,"DOG ON-CENTER");
  // SimDataArrayをImagearrayにセット
  main_window.get_imagearray_ptr(0)->array_from_simdataarray(&weight_data, 5,
    				                 1.0, weight_data.get_width());
  std::cout << "pass3 \n";
  s_array.array_from_simdataarray(&weight_data, 5, 1.0, 
				  weight_data.get_width());
  std::cout << "pass4 \n";
  s_array.save_image(file_name2, "png", 1, 12.35, 4, 1);
  // メインウィンドウを起動
  Gtk::Main::run(main_window);

  // set_simdata_to_arrayを使う（場所はx=4,y=2）
  // データ作成
  s_data.recreate_data_size(W_WIDTH,W_HEIGHT);
  for(i = 0; i < W_HEIGHT; i++){  // y
    for(j = 0; j < W_WIDTH; j++){  // x
      s_data.put_data(j, i, 1.0); 
    }
  }
  s_data.put_data(10, 10, 0.0); 
  // x=4,y=2に貼り付けてセーブ
  main_window.get_imagearray_ptr(0)->set_simdata_to_array(&s_data, 4, 2);
  s_array.set_simdata_to_array(&s_data, 4, 2);
  s_array.save_image(file_name3, "png", 1, 12.35, 4, 1);
  std::cout << "pass5 \n";
  // メインウィンドウを起動
  Gtk::Main::run(main_window);

  // イメージローダを利用
  i_loader.set_input_file(i_file_name);
  main_window.get_imagearray_ptr(0)->array_from_file(i_loader,10,0.1,6);
  s_array.array_from_file(i_loader,10,0.1,6);
  s_array.save_image(file_name4, "png", 1, 12.35, 4, 1);
  Gtk::Main::run(main_window);
  std::cout << "pass6 \n";


  return 0;
}
