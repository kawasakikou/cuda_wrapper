#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

// 画像サイズは幅225,高さ233
#define W_WIDTH  51       // 荷重の幅
#define W_HEIGHT 51       // 荷重の高さ
#define OUT_WIDTH  87     // 出力の幅
#define OUT_HEIGHT 91     // 出力の高さ
#define CALC_STEP 2       // 荷重和計算の間隔
#define SIGMOID_THETA 2.0 // シグモイド関数の閾値
#define SIGMOID_GAIN  3.0 // シグモイド関数のゲイン

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::ImageView window;              // 表示用ウィンドウ 
  Glib::RefPtr<Gdk::Pixbuf> in_image;   // 入力画像用pixbuf
  Glib::RefPtr<Gdk::Pixbuf> image_buf;  // テンポラリ用pixbuf
  Nistk::SimData in_data;               // 入力画像用simdat
  Nistk::SimDataArray weight_data;      // 荷重用simdata
  Nistk::ImageArray array_window;       // 荷重表示用ImageArray
  Nistk::SimData out_data;              // 計算結果用simdata
  int x,y;                              // 入力上の位置
  char win_name[][25] = {"input data", "out data(on-center)"};
  char array_name[] = "weight_data first and end";
  double tmp;
  int i,j;

  /*** 
   * 画像を入力してin_dataに格納してそれを表示
  ***/
  // 入力画像をpixbufに格納
  in_image = Gdk::Pixbuf::create_from_file("../image/test-fig2.png");
  // pixbufから入力画像をsimdataに格納
  in_data.create_data_size(in_image->get_width(),in_image->get_height());
  Nistk::ImageData::simdata_from_pixbuf(&in_data,in_image,2);
  std::cout << "input data  ";
  std::cout << "width=" << in_data.get_width() << ' '
	    << "height=" <<in_data.get_height() << '\n';
  // simdataの内容を表示
  image_buf = Nistk::ImageData::create_pixbuf_new(in_data.get_width(),
						  in_data.get_height());
  Nistk::ImageData::pixbuf_from_simdata(&in_data,image_buf);
  window.set_image(image_buf);
  window.set_name(win_name[0]);
  Gtk::Main::run(window);
  std::cout << "pass 1 \n";

  /*** 
   * 荷重データを作成してそれを表示 ON-CENTER
  ***/
  weight_data.create_data_size(OUT_WIDTH, OUT_HEIGHT, W_WIDTH, W_HEIGHT);
  // 荷重データ作成
  for(i = 0; i < OUT_HEIGHT; i++){  // y
    for(j = 0; j < OUT_WIDTH; j++){  // x
      Nistk::MathTools::simdata_from_DoG(weight_data.get_simdata_ptr(j,i), 
					 0.0, 0.0,              // 中心
					 1.0, 1.0,              // x,yのステップ
					 1.0, 1.0,              // ゲイン
					 3.0, 6.0, true);       // シグマ
      std::cout << "create weight i=" << i << " j=" << j << '\n';
    }
  }
  // 荷重データを表示
  std::cout << "weight data  ";
  std::cout << "width=" << weight_data.get_width() << ' '
	    << "height=" <<weight_data.get_height() << '\n';
  /* std::cout << "create weight array image \n";
  array_window.array_from_simdataarray(&weight_data,10,1.0,OUT_WIDTH);
  */
  // 全部の表示をすると多過ぎなので最初と最後のを表示
  array_window.create_array_area(W_WIDTH, W_HEIGHT, 10, 2, 1);
  array_window.set_simdata_to_array(weight_data.get_simdata_ptr(0,0),0,0);
  array_window.set_simdata_to_array(weight_data.get_simdata_ptr(
  				   OUT_WIDTH-1,OUT_HEIGHT-1),1,0);
  array_window.set_array_name(array_name);
  Gtk::Main::run(array_window);
  
  std::cout << "pass 2 \n";

  /*** 
   * 荷重和を計算してそれを表示
  ***/
  // 領域確保
  out_data.create_data_size(OUT_WIDTH,OUT_HEIGHT);
  // 荷重和の計算
  Nistk::Calc::input_array_weight(&in_data, &weight_data, &out_data, 
		                        W_WIDTH/2, W_HEIGHT/2, CALC_STEP);
  // 荷重和から出力値を計算
  for(i = 0; i < out_data.get_height(); i++){  // y
    for(j = 0; j < out_data.get_width(); j++){  // x
      tmp = Nistk::MathTools::sigmoid_func(out_data.get_data(j,i)-SIGMOID_THETA,
					                          SIGMOID_GAIN);
      out_data.put_data(j,i,tmp);
    }
  }
  // 計算結果を表示
  std::cout << "out data  ";
  std::cout << "width=" << out_data.get_width() << ' '
	    << "height=" << out_data.get_height() << '\n';
  image_buf = Nistk::ImageData::resize_pixbuf(image_buf, OUT_WIDTH,OUT_HEIGHT);
  Nistk::ImageData::pixbuf_from_simdata(&out_data, image_buf);
  window.set_image(image_buf);
  window.set_name(win_name[1]);
  Gtk::Main::run(window);
  std::cout << "pass 3 \n";

  return 0;
}

