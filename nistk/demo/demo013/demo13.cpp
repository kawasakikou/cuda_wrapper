#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

#define IMAGE_VIEW_NUM 12
#define IMAGE_ARRAY_NUM 12

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  // ImageView用名前の文字列の配列
  char view_name[IMAGE_VIEW_NUM][15] 
    = {"image view1","image view2","image view3",
       "image view4","image view5","image view6",
       "image view7","image view8","image view9",
       "image view10","image view11","image view12"};
  // ImageArray用名前の文字列の配列
  char array_name[IMAGE_ARRAY_NUM][15] 
    = {"image array1","image array2","image array3",
       "image array4","image array5","image array6",
       "image array7","image array8","image array9",
       "image array10","image array11","image array12"};
  int height,width;
  int i,j;
  
  // ImageViewの生成と名前のセット
  main_window.create_imageview(IMAGE_VIEW_NUM);
  for(i = 0; i < IMAGE_VIEW_NUM; i++) 
    main_window.set_imageview_name(i,view_name[i]);

  // ImageArrayの生成と名前のセット
  main_window.create_imagearray(IMAGE_ARRAY_NUM);
  for(i = 0; i < IMAGE_ARRAY_NUM; i++) 
    main_window.set_imagearray_name(i,array_name[i]);

  // ImageViewの0番目に画像をセット
  m_pixbuf=Gdk::Pixbuf::create_from_file("../image/AVG.png");
  main_window.get_imageview_ptr(0)->set_image(m_pixbuf);

  // ImageArrayの0番目に2×2の領域を生成して画像をセット
  height = m_pixbuf->get_height();
  width = m_pixbuf->get_width();
  main_window.get_imagearray_ptr(0)->create_array_area(width,height,10,2,2);
  for(i = 0; i < 2; i++){
    for(j =0; j < 2; j++){
      main_window.get_imagearray_ptr(0)->set_pixbuf_to_array(m_pixbuf,j,i);
    }
  }

  // メインウィンドウを起動
  Gtk::Main::run(main_window);
  // ImageViewのフレームを取り除いてメインウィンドウを起動
  main_window.remove_imageview();
  Gtk::Main::run(main_window);
  // ImageViewのフレーム再度追加してメインウィンドウを起動
  main_window.add_imageview();
  Gtk::Main::run(main_window);
  // ImageArrayのフレームを取り除いてメインウィンドウを起動
  main_window.remove_imagearray();
  Gtk::Main::run(main_window);
  // ImageArrayのフレーム再度追加してメインウィンドウを起動
  main_window.add_imagearray();
  Gtk::Main::run(main_window);
  // ImageViewのボタン数を変更してメインウィンドウを起動
  // ボタン数変更の場合はいったボタンをdeleteするので
  // 再度画像をセット
  main_window.create_imageview(10);
  for(i = 0; i < 10; i++) 
    main_window.set_imageview_name(i,view_name[i]);
  m_pixbuf=Gdk::Pixbuf::create_from_file("../image/AVG.png");
  main_window.get_imageview_ptr(0)->set_image(m_pixbuf);
  Gtk::Main::run(main_window);


  return 0;
}
