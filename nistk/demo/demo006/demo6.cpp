// SimDataクラスのテストデモ
#include<iostream>
#include<gtkmm.h>
#include<nistk.h>
#include"imagearray.h"
#include"simdataarray.h"

void test_copy_constructer(Nistk::SimData in);

int main(int argc,char *argv[])
{
  Nistk::SimData t_data,t_data2;
  double value,out_data;

  // データを2Dデータとして確保して,2Dデータとして
  // 値を格納して表示
  t_data.create_data_size(5,2);

  std::cout << "pass 1 \n";
  std::cout << "    set 2D;out 2D \n";
  value = 0.0;
  for(int i=0; i<2; i++){
    for(int j=0; j<5; j++){
      t_data.put_data(j,i,value);
      out_data = t_data.get_data(j,i);
      std::cout << out_data << ' ';
      value += 1.0;
    }
    std::cout << '\n';
  }
  // データを2Dとして格納したのを1Dとして扱って表示
  std::cout << "     set 2D;out 1D \n";
  for(int i=0; i<10; i++){
          out_data = t_data.get_data_1d(i);
	  std::cout << out_data << ' ';
  }
  std::cout << '\n';

  // データ領域のサイズを変更して,1Dデータとして
  // 値を代入して2Dとして表示
  t_data.recreate_data_size(3,2);
  std::cout << "pass 2 \n";
  std::cout << "    set 1D;out 2D \n";
  value = 0.0;
  for(int i=0; i<6; i++){
    t_data.put_data_1d(i,value);
    value += 1.0;
  }
  for(int i=0; i<2; i++){
    for(int j=0; j<3; j++){
      out_data = t_data.get_data(j,i);
      std::cout << out_data << ' ';
    }
    std::cout << '\n';
  }
  // 1Dとして表示
  std::cout << "    set 1D;out 1D \n";
  for(int i=0; i<6; i++){
          out_data = t_data.get_data_1d(i);
	  std::cout << out_data << ' ';
  }
  std::cout << '\n';

  // 代入演算子のオーバーロードのテスト
  std::cout << "pass 3 \n";
  t_data2 = t_data;
  std::cout << "    test of t_data2=t_data \n";
  for(int i=0; i<6; i++){
          out_data = t_data2.get_data_1d(i);
	  std::cout << out_data << ' ';
  }
  std::cout << '\n';

  // コピーコンストラクタのテスト
  // ポインタではなくてオブジェクトそのものを関数に渡す
  std::cout << "pass 4 \n";
  std::cout << "    test of copy constructer \n";
  test_copy_constructer(t_data);

  return 0;
}

void test_copy_constructer(Nistk::SimData in)
{
  double out_data;
  
  for(int i=0; i<6; i++){
          out_data = in.get_data_1d(i);
	  std::cout << out_data << ' ';
  }
  std::cout << '\n';

  return;
}
