#include<iostream>
#include<gtkmm.h>
#include<nistk.h>

#define PLOT_VIEW_NUM 12

int main(int argc,char *argv[])
{
  // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  // PlotView用名前の文字列の配列
  char plot_name[PLOT_VIEW_NUM][15] 
    = {"plot view1","plot view2","plot view3",
       "plot view4","plot view5","plot view6",
       "plot view7","plot view8","plot view9",
       "plot view10","plot view11","plot view12"};
  double a,x,y;
  int i,j;
  
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

  // グラフを横2個、縦3個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(2,3,200,100,80,30);
  }
  // グラフの縦横のスケールを設定(一部あとで変更）
  for(i=0; i < 3; i++){
    for(j=0; j < 2; j++){
      main_window.pd_ptr(0,j,i)->set_data_range(-0.00005,0.00005,0.0,0.25);
    }
  }
  // 0番目のボタンの一番左上(x,y)=(0,0)のグラフのスケールを再設定
  main_window.pd_ptr(0,0,0)->set_data_range(-1.0, 1.0, -1.4, 1.4);
  // 0番目のボタンの一番左上(x,y)=(0,0)のグラフ用のデータ領域を5個確保
  main_window.pd_ptr(0,0,0)->create_data_region(5);
  // 5個のデータ領域に傾きを変えた直線のデータを格納
  a = 2.0;
  for(i = 0; i < 5; i++){
    for(x = -1.2; x <=1.2; x+=0.01){
      y = a * x;
      main_window.pd_ptr(0,0,0)->push_back_data(i, x, y);
    }
    a -= 1.0;
  }
  // 0番目のボタンの位置(x,y)=(1,0)のグラフのスケールを再設定
  main_window.pd_ptr(0,1,0)->set_data_range(0.0, 5.0, -2.0, 2.0);
  // 0番目のボタンの位置(x,y)=(1,0)のグラフ用のデータ領域を1個確保
  main_window.pd_ptr(0,1,0)->create_data_region(1);
  // 0番目のボタンの位置(x,y)=(1,0)のグラフに対数のデータを格納
  for(x = 0.1; x <=5.2; x+=0.01){
    y = log(x);
    main_window.pd_ptr(0,1,0)->push_back_data(0, x, y);
  }

  // メインウィンドウを起動
  Gtk::Main::run(main_window);
  // PlotViewのフレームを取り除いてメインウィンドウを起動
  main_window.remove_plotview();
  Gtk::Main::run(main_window);
  // PlotViewのフレーム再度追加してメインウィンドウを起動  
  main_window.add_plotview();
  Gtk::Main::run(main_window);

  return 0;
}
