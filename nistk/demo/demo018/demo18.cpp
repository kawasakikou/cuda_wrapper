#include<iostream>
#include<cmath>
#include<nistk.h>

#define PLOT_VIEW_NUM 1
#define DX 0.001
#define FROM_X 0.1
#define END_X 3.0

// テスト用関数のプロトタイプ宣言
double func1(double x);
double func2(double x, double y, double p1, double p2);

int main(int argc, char **argv)
{
 // gtkmmを使う際には必ず必要。まず、最初にやる。
  Gtk::Main kit(argc,argv);
  // オブジェクトの宣言
  Nistk::NistkMainWin main_window;
  // PlotView用名前の文字列の配列
  char plot_name[PLOT_VIEW_NUM][15] = {"runge test"};
  char file_name[][15] = {"test1.dat","test2.dat","test3.dat","test4.dat"};

  double x,y,dx;
  double buf_x, buf_y, buf_xb, buf_yb, diff;
  int i;

  main_window.set_size_request(200,150);
  // PlotViewの生成と名前のセット
  main_window.create_plotview(PLOT_VIEW_NUM);
  for(i = 0; i < PLOT_VIEW_NUM; i++) 
    main_window.set_plotview_name(i,plot_name[i]);

 // グラフを横2個、縦2個として縦横の大きさとマージンを設定
  for(i = 0; i < PLOT_VIEW_NUM; i++){
    main_window.get_plotview_ptr(i)->set_graph_area(2,2,200,100,80,30);
    main_window.pd_ptr(i,0,0)->create_data_region(1);
    main_window.pd_ptr(i,1,0)->create_data_region(1);
    main_window.pd_ptr(i,0,1)->create_data_region(1);
    main_window.pd_ptr(i,1,1)->create_data_region(1);
  }

  // dxの設定
  dx = DX;
  // 原関数の計算をして格納
  for(x = FROM_X; x <= END_X; x += dx){
    y = func1(x);
    main_window.pd_ptr(0,0,0)->push_back_data(0, x, y);
  }
  // 4次のルンゲクッタで計算して格納
  for(x = FROM_X; x <= END_X; x += dx){
    // 初期値を計算
    if(x == 0.1){
	y = func1(x);
    }
    main_window.pd_ptr(0,1,0)->push_back_data(0, x, y);
    y = Nistk::Temp::Runge4<double,double>(func2,x,y,0.0,0.0,dx);
  }  
  // 元関数の値とルンゲクッタでの計算値の差を求めて格納
  // ついでにオイラー法で計算して差を取って格納
  for(i = 0; i < main_window.pd_ptr(0,0,0)->get_size(0); i++){
    buf_x = main_window.pd_ptr(0,0,0)->get_x(0,i);
    buf_y = main_window.pd_ptr(0,0,0)->get_y(0,i);
    diff = buf_y - main_window.pd_ptr(0,1,0)->get_y(0,i);
    main_window.pd_ptr(0,0,1)->push_back_data(0, buf_x, diff);
    // オイラー法での差を計算
    if(i == 0){
      main_window.pd_ptr(0,1,1)->push_back_data(0, buf_x, diff);
      buf_yb = buf_y;  // 一つ前の値として保存
      buf_xb = buf_x;  // 一つ前の値として保存
    } else {
      buf_yb = buf_yb + dx * func2(buf_xb, buf_yb,0.0,0.0);
      buf_xb = buf_x;
      diff = buf_y - buf_yb; 
      main_window.pd_ptr(0,1,1)->push_back_data(0, buf_x, diff);
    }
  }

  // データを保存
  main_window.pd_ptr(0,0,0)->save_data(0,file_name[0]);
  main_window.pd_ptr(0,1,0)->save_data(0,file_name[1]);
  main_window.pd_ptr(0,0,1)->save_data(0,file_name[2]);
  main_window.pd_ptr(0,1,1)->save_data(0,file_name[3]);

 // メインウィンドウを起動して表示
  Gtk::Main::run(main_window);

  return 0;
}

// テスト用関数の定義
double func1(double x)
{
  double ret;

  ret = x * log(x);
  
  return ret;
}

double func2(double x, double y, double p1, double p2)
{
  double ret;

  ret = 1.0 + (y/x);
  
  return ret;
}
