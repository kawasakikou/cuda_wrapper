#include<nistk/plotview.h>
#include<gtkmm.h>
#include<cmath>
#define X_NUM 2
#define Y_NUM 3

int main(int argc, char **argv)
{
  Gtk::Main kit(argc, argv);

  Nistk::PlotView win;
  char save_file_name[] = "test.dat";
  double x,y,a;
  int i,j,k;
  
  // グラフを横X_NUM個、縦Y_NUM個として縦横の大きさとマージンを設定
  win.set_graph_area(X_NUM,Y_NUM,200,100,80,30);
  // グラフの縦横のスケールを設定(一部あとで変更）
  for(i=0; i < Y_NUM; i++){
    for(j=0; j < X_NUM; j++){
      win.pd_ptr(j,i)->set_data_range(-0.00005,0.00005,0.0,0.25);
    }
  }

  // 一番左上(x,y)=(0,0)のグラフのスケールを再設定
  win.pd_ptr(0,0)->set_data_range(-1.0, 1.0, -1.4, 1.4);
  // 一番左上(x,y)=(0,0)のグラフ用のデータ領域を5個確保
  win.pd_ptr(0,0)->create_data_region(5);
  // 5個のデータ領域に傾きを変えた直線のデータを格納
  a = 2.0;
  for(i = 0; i < 5; i++){
    for(x = -1.2; x <=1.2; x+=0.01){
      y = a * x;
      win.pd_ptr(0,0)->push_back_data(i, x, y);
    }
    a -= 1.0;
  }

  // 位置(x,y)=(1,0)のグラフのスケールを再設定
  win.pd_ptr(1,0)->set_data_range(0.0, 5.0, -2.0, 2.0);
  // 位置(x,y)=(1,0)のグラフ用のデータ領域を1個確保
  win.pd_ptr(1,0)->create_data_region(1);
  // 位置(x,y)=(1,0)のグラフに対数のデータを格納
  for(x = 0.1; x <=5.2; x+=0.01){
    y = log(x);
    win.pd_ptr(1,0)->push_back_data(0, x, y);
  }

  // 位置(x,y)=(0,1)のグラフのスケールを再設定
  win.pd_ptr(0,1)->set_data_range(0.0, 4.0, 0.0, 1.0);
  // 位置(x,y)=(0,1)のグラフ用のデータ領域を1個確保
  win.pd_ptr(0,1)->create_data_region(1);
  // 位置(x,y)=(0,1)のグラフにテント型の関数のデータを格納
  for(x = 0.0; x <=4.0; x+=0.01){
    if(x < 2.0) y = 1.0 * x;
    else y = -1.0 * x + 4.0;
    win.pd_ptr(0,1)->push_back_data(0, x, y);
  }
  // 位置(x,y)=(0,1)のグラフのテント型の関数のデータをファイルに出力
  win.pd_ptr(0,1)->save_data(0, save_file_name);

  // 位置(x,y)=(1,1)のグラフのスケールを再設定
  win.pd_ptr(1,1)->set_data_range(0.0, 4.0, 0.0, 1.0);
  // 位置(x,y)=(1,1)のグラフ用のデータ領域を1個確保
  win.pd_ptr(1,1)->create_data_region(1);
  // オートレンジを設定
  win.pd_ptr(1,1)->set_auto_range();
  // 位置(x,y)=(1,1)のグラフにテント型の関数のデータを格納
  for(x = 0.0; x <=4.0; x+=0.01){
    if(x < 2.0) y = 1.0 * x;
    else y = -1.0 * x + 4.0;
    win.pd_ptr(1,1)->push_back_data(0, x, y);
  }

  Gtk::Main::run(win);
  return 0;
}
