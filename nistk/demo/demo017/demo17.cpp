#include<iostream>
#include<nistk/fsocket.h>

// テスト用の適当な関数のプロトタイプ宣言
int func1(int x);
int func2(int x, int y);
int func3(int x, int y, int a);
int func4(int *x, int *y, int *a, int *b);

int main(int argc, char **argv)
{
  // FSocketを4種類宣言
  Nistk::Temp::FSocket1<int, int>                   m_socket1;
  Nistk::Temp::FSocket2<int, int, int>              m_socket2;
  Nistk::Temp::FSocket3<int, int, int, int>         m_socket3;
  Nistk::Temp::FSocket4<int, int *, int *, int *, int *>    m_socket4;
  int res,x,y,off_set,scale;
  
  // 関数をセット
  m_socket1.set_function(func1);
  m_socket2.set_function(func2);
  m_socket3.set_function(func3);
  m_socket4.set_function(func4);

  // FSocketを使って関数呼び出しのテスト
  // FSocket1
  std::cout << "FSocket1 Test \n";
  for(x = 0; x < 3; x++){
    m_socket1.fs_arg1 = x;
    res = m_socket1.calc_function();
    std::cout << "x=" << x << " res=" << res << '\n';
  }

  // FSocket2
  std::cout << "\n FSocket2 Test \n";
  for(y = 0; y < 3; y++){
    m_socket2.fs_arg2 = y;
    for(x = 0; x < 3; x++){
      m_socket2.fs_arg1 = x;
      res = m_socket2.calc_function();
      std::cout << "x=" << x << " y=" << y << " res=" << res << '\n';
    }
    std::cout << '\n';
  }

  // FSocket3
  std::cout << "\n FSocket3 Test \n";
  off_set=1000;
  m_socket3.fs_arg3 = off_set;
  for(y = 0; y < 3; y++){
    m_socket3.fs_arg2 = y;
    for(x = 0; x < 3; x++){
      m_socket3.fs_arg1 = x;
      res = m_socket3.calc_function();
      std::cout << "x=" << x << " y=" << y << " res=" << res << '\n';
    }
    std::cout << '\n';
  }
  
  // FSocket4 ポインタを使う場合
  std::cout << "\n FSocket4 Test \n";
  off_set=1000;
  scale = 2;
  m_socket4.fs_arg1 = &x;
  m_socket4.fs_arg2 = &y;
  m_socket4.fs_arg3 = &off_set;
  m_socket4.fs_arg4 = &scale;
  for(y = 0; y < 3; y++){
    for(x = 0; x < 3; x++){
      res = m_socket4.calc_function();
      std::cout << "x=" << x << " y=" << y << " res=" << res << '\n';
    }
    std::cout << '\n';
  }

  return 0;
}

// テスト用関数の定義
int func1(int x)
{
  int ret;

  ret = x * 10 ;
  
  return ret;
}

int func2(int x, int y)
{
  int ret;

  ret = (x * 10) + y;
  
  return ret;
}

int func3(int x, int y, int a)
{
  int ret;

  ret = x + y + a;
  
  return ret;
}

int func4(int *x, int *y, int *a, int *b)
{
  int ret;

  ret = *b * (*x + *y + *a);
  
  return ret;
}
