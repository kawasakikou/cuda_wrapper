○導入
 
NISTKはニューラルネットワークシミュレーションのための
クラスライブラリである。画像を使ったシミュレーションが
可能となるように,いくつかのクラスを準備している。
ImageData,MathToolsクラスは,分類のためにクラスとしており,
オブジェクトとして使う必要はない。画像処理とウィンドウ関係
はGTK+のpixbuf構造体を使っており、実際には gtkmmを使って
組んである。画像表示関係は全てpixbufを使うことになるが,
実際にはpixbufの扱いは全てGlib::RefPtr<Gdk::Pixbuf>型
スマートポインタを介して行うことになる。詳しくはgtkmmの
HPを参照のこと。

使用方法はdoc/htmlディレクトリのindex.htmlとdomo
ディレクトリのデモプログラムを参照のこと。

					2016年3月22日
					    小森 雅和

○インストール
1. includeディレクトリにあるimagefiles.hファイルを以下のように編集

   #define NOIMAGE "PATH/nistk/image/no_image.png"

   のPATH部分をnistkを展開しているディレクトリに書き換える

2. srcディレクトリにいき以下のコマンドを実行

   make

3. 次に以下のコマンドによりライブラリのインストールをする

   make install

4. 最後に以下のコマンドを実行しクリーンナップをする

   make clean
