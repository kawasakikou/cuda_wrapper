/**
 * @file  imagearray_button.h
 * @brief class for imagearray button used by nistk main window
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imagearray_button.h,v.20110809 $
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

#ifndef IMAGEARRAY_BUTTON_H
#define IMAGEARRAY_BUTTON_H

#include<gtkmm/window.h>
#include<gtkmm/button.h>
#include<imagearray.h>

namespace Nistk{
/** メインウィンドウのためのImageArray用ボタン
 *
 * NistkMainWinでImageArrayのボタンを配置し、ボタンがクリックされた際に
 * ImageArrayのウィンドウが表示されるようにするためのクラス。
 * なので、ユーザが使用することはない。
 * 再度ボタンがクリックすると消えるようになっている。<br>
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

class ImageArrayButton :public Gtk::Button  // Gtk::Buttonクラスを継承
{
 protected:
  Nistk::ImageArray window;          ///< ImageArrayウィジット
  virtual void on_clicked();         ///< クリックされた時の処理(オーバーライド)

 public:
  ImageArrayButton();                ///< コンストラクタ
  ~ImageArrayButton();               ///< デストラクタ
  void set_name(const char *name);   ///< ボタンとウィンドウの名前をセットする関数
  Nistk::ImageArray* get_imagearray_ptr();///< ImageArrayのポインタを取得する関数
};

}

#endif // IMAGEARRAY_BUTTON_H

