/**
 * @file  graydata.h
 * @brief class for graydata from pixbuf
 *
 * @author Masakazu Komori
 * @date 2017-09-01
 * @version $Id: graydata.h,v.20170901 $
 *
 * Copyright (C) 2008-2017 Masakazu Komori
 */


#ifndef NISTK_GRAYDATA_H
#define NISTK_GRAYDATA_H

namespace Nistk{
/** グレイデータを扱うクラス
 * 
 * シミュレーション用の画像データのためのクラス。
 * pixbufのRGBデータからグレイデータ(0〜255)を
 * を得て、二次元データのようにして扱えるように
 * してあるクラス。実際に中身ではデータは1次元で扱っている。
 * このクラスのデータからpixbufのデータを作ることもできる。<br>
 *
 * Copyright (C) 2008-2017 Masakazu Komori
 */
class GrayData
{
 protected:
  int height; ///< 二次元データの高さ
  int width;  ///< 二次元データの幅
  int length; ///< 一次元での長さ
  int **data2d;  ///< データを二次元配列として扱うためのポインタ

 public:
  GrayData();                ///< コンストラクタ 引数無し
  GrayData(int w,int h);     ///< コンストラクタ 配列の確保もする
  GrayData(const Nistk::GrayData &obj); ///< コピーコンストラクタ
  ~GrayData(); ///< デストラクタ
  /// 代入演算子=のオーバーロード
  Nistk::GrayData &operator=(Nistk::GrayData &obj);
  void create_data_size(int w,int h);      ///< 配列の確保をする関数
  void recreate_data_size(int w,int h);    ///< 配列の大きさを変更する関数
  int get_height();                        ///< 配列の高さを取得する関数
  int get_width();                         ///< 配列の幅を取得する関数
  int get_length();                        ///< 配列の長さを取得する関数
  int get_data(int x, int y);              ///< 2Dデータの読み出しを行う関数
  void put_data(int x, int y, int i_data); ///< 2Dデータの格納を行う関数
  int get_data_1d(int x);                  ///< 1Dデータの読み出しを行う関数
  void put_data_1d(int x, int i_data);     ///< 1Dデータの格納を行う関数
  void delete_data();                      ///< 配列を消去する関数
  /// 連続領域確保されたデータ配列のポインタを取得する関数
  int* get_data_ptr();  
};

}

#endif // NISTK_GRAYDATA_H
