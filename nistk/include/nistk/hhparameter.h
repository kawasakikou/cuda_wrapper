/**
 * @file  hhparameter.h
 * @brief Class for parameter of Hodgikin-Huxley model
 *
 * @author Masakazu Komori
 * @date 2011-08-01
 * @version $Id: hhparameter.h,v.20110801 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef HHPARAMETER_H
#define HHPARAMETER_H

namespace Nistk{
namespace Neuron{
/** Hodgikin-Huxley modelのためのパラメータ用クラス
 *
 * Hodgikin-Huxley modelのためのパラメータ用クラス。
 * Hodgikin-Huxley modelにおける定数パラメータを集めたクラス。
 * このクラスのメンバ変数は、手法としては良くないが操作の煩雑さを
 * 減らすために全てpublicとする。つまり、アクセッサなどを使わない。
 * 計算によって初期化が必要なパラメータがあるので、使用の際には
 * パラメータの設定後に必ず関数
 * Nistk::Neuron::HHNeuron::init()を使用すること。
 * パラメータの詳しい説明はクラス
 * Nistk::Neuron::HHNeuronを参照のこと。<br>
 *   イオンチャネルの電圧依存係数\f$\alpha\f$、\f$\beta\f$の式に
 * 関する定数も以下のように変数としている。<br>
 \f[
      \alpha _m(V) = \frac{am_1-V}{am_2(e^{(am_3-V)/am_4}-am_5)}
 \f]
 \f[
      \beta _m(V) = bm_1e^{-V/bm_2}
 \f]
 \f[
      \alpha _h(V) = ah_1e^{-V/ah_2}
 \f]
 \f[
      \beta _h(V) = \frac{bh_1}{e^{(bh_2-V)/bh_3}+bh_4}
 \f]
 \f[
      \alpha _n(V) = \frac{an_1-V}{an_2(e^{(an_3-V)/an_4}-an_5)}
 \f]
 \f[
      \beta _n(V) = bn_1e^{-V/bn_2}
 \f]
 * 初期値:\f$am_1=25\f$,\f$am_2=10\f$,\f$am_3=25\f$,\f$am_4=10\f$,\f$am_5=1\f$
 * ,\f$bm_1=4\f$,\f$bm_2=18\f$,\f$ah_1=0.07\f$,\f$ah_2=20\f$
 * ,\f$bh_1=1\f$,\f$bh_2=30\f$,\f$bh_3=10\f$,\f$bh_4=1\f$
 * ,\f$an_1=10\f$,\f$an_2=100\f$,\f$an_3=10\f$,\f$an_4=10\f$,\f$an_5=1\f$
 * ,\f$bn_1=0.125\f$,\f$bn_2=80\f$ <br>
 * \f$m\f$,\f$h\f$,\f$n\f$の式も次のようにしている。<br>
 \f[
      \frac{dm}{dt} = (\alpha _m(V)(1-m) - \beta _m(V)m) * mhn\_scale
 \f]
 \f[
      \frac{dh}{dt} = (\alpha _h(V)(1-h) - \beta _h(V)h) * mhn\_scale
 \f]
 \f[
      \frac{dn}{dt} = (\alpha _n(V)(1-n) - \beta _n(V)n) * mhn\_scale
 \f]
 * mhn_scaleの初期値は1である。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class HHParameter
{
 public:
  double C_m_bar;        ///< 単位あたりの膜容量(初期値1[uF/cm^2])
  double G_m_bar;        ///< 単位あたりの最大膜コンダクタンス(初期値0.3[mS/cm^2])
  /// 単位あたりのNaイオンチャネル最大コンダクタンス(初期値120[mS/cm^2])
  double G_Na_bar;
  /// 単位あたりのKイオンチャネル最大コンダクタンス(初期値36[mS/cm^2])
  double G_K_bar;
  double V_rest;         ///< 静止膜電位(初期値0[mV])
  double E_Na;           ///< Naイオンの反転電位(初期値115[mV])
  double E_K;            ///< Kイオンの反転電位(初期値-12[mV])
  double V_L;            ///< Leakの反転電位(計算により決定)
  double T;              ///< 設定温度(初期値6.3[℃])
  double area_size;      ///< ニューロンの面積(初期値2.827*10^-5[cm^2])
  double T_scale;        ///< 設定温度によるα、βの係数(計算により決定)
  double C_m;            ///< 計算対象の膜容量(計算により決定)
  double G_m;            ///< 計算対象の最大膜コンダクタンス(計算により決定)
  /// 計算対象のNaイオンチャネル最大コンダクタンス(計算により決定)
  double G_Na;
  /// 計算対象のKイオンチャネル最大コンダクタンス(計算により決定)
  double G_K;
  double mhn_scale;      ///< イオンチャネルの開口状態m,h,nの時間係数(初期値1)
  double am_1;           ///< イオンチャネルの電圧依存係数α_mの係数1(初期値25)
  double am_2;           ///< イオンチャネルの電圧依存係数α_mの係数2(初期値10)
  double am_3;           ///< イオンチャネルの電圧依存係数α_mの係数3(初期値25)
  double am_4;           ///< イオンチャネルの電圧依存係数α_mの係数4(初期値10)
  double am_5;           ///< イオンチャネルの電圧依存係数α_mの係数5(初期値1)
  double bm_1;           ///< イオンチャネルの電圧依存係数β_mの係数1(初期値4)
  double bm_2;           ///< イオンチャネルの電圧依存係数β_mの係数2(初期値18)
  double ah_1;           ///< イオンチャネルの電圧依存係数α_hの係数1(初期値0.07)
  double ah_2;           ///< イオンチャネルの電圧依存係数α_hの係数2(初期値20)
  double bh_1;           ///< イオンチャネルの電圧依存係数β_hの係数1(初期値1)
  double bh_2;           ///< イオンチャネルの電圧依存係数β_hの係数2(初期値30)
  double bh_3;           ///< イオンチャネルの電圧依存係数β_hの係数3(初期値10)
  double bh_4;           ///< イオンチャネルの電圧依存係数β_hの係数4(初期値1)
  double an_1;           ///< イオンチャネルの電圧依存係数α_nの係数1(初期値10)
  double an_2;           ///< イオンチャネルの電圧依存係数α_nの係数2(初期値100)
  double an_3;           ///< イオンチャネルの電圧依存係数α_nの係数3(初期値10)
  double an_4;           ///< イオンチャネルの電圧依存係数α_nの係数4(初期値10)
  double an_5;           ///< イオンチャネルの電圧依存係数α_nの係数5(初期値1)
  double bn_1;           ///< イオンチャネルの電圧依存係数β_nの係数1(初期値0.125)
  double bn_2;           ///< イオンチャネルの電圧依存係数β_nの係数2(初期値80)
  HHParameter();         ///< コンストラクタ
  ~HHParameter();        ///< デストラクタ
};

}
}

#endif //HHPARAMETER_H
