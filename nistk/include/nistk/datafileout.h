/**
 * @file  datafileout.h
 * @brief class for data output to file
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: datafileou.h,v.20110809 $
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */

#ifndef NISTK_DATAFILEOUT_H
#define NISTK_DATAFILEOUT_H

#include<fstream>
#include<simdata.h>

namespace Nistk{
/** シミュレーションデータをファイルに書き出すためのクラス
 *
 * シミュレーションでの計算データ(2D,3D)をGNU PLOT形式でファイル
 * に書き出すためのクラス。実際は無くても良いクラスであるが,
 * 一応作っておく。2D,3Dのファイル形式とGNU PLOTでのコマンドは
 * 次のとおりである。<br>
 * <br>
 * 2D ファイル形式 (x軸とy軸の組合せの列挙) <br>
 * x0 y0 <br>
 * x1 y1 <br>
 * .  .  <br>
 * <br>
 * 2D グラフの表示(いくつかバリエーションがあるが一番簡単なもの) <br>
 * plot "ファイル名" with line <br>
 * <br>
 * 3D ファイル形式(x軸とy軸とz軸の組合せ。ただし,x軸の終わりに空行が必要) <br>
 * x0 y0 z0 <br>
 * x1 y0 z1 <br>
 * .  .  .  <-y軸を変化させていないことに注意 <br>
 * xn y0 zn <br>
 *          <-y軸を変化させるときに空行が必要 <br>
 * x0 y1 zn+1 <br>
 * x1 y1 zn+2 <br>
 * 3Dのサーフェースを表示させる場合には,y軸の値を固定させて
 * x軸を変化させ範囲の最後までいったときには1行空行をいれる必要がある。
 * そして,y軸の値を変えて同じ事するといったことを繰り返す必要がある。
 * z軸の値はx、yによる計算データであるので気にしなくても良い。
 * ちなみに2行の空行をいれると別のサーフェースの始まりになる。
 * <br>
 * 3D グラフの表示(いくつかバリエーションがあるが一番簡単なもの) <br>
 * set parametric <br>
 * splot "ファイル名" with line <br>
 * <br>
 * 注意:このクラスは,関数間などでやりとりする場合は
 * 必ずポインタでやりとりすること。
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */
class DataFileOut
{
 protected: 
  std::ofstream o_file; ///< データ出力のファイル(ofstream型)
  char *o_file_name;    ///< 出力ファイル名
  bool mode;            ///< 出力モード(true:3D false:2D)
  bool check;           ///< 状態(true:open false:not_open)

 public:
  DataFileOut();                  ///< コンストラクタ 引数無し
  DataFileOut(const char *f, bool i_m); ///< コンストラクタ 引数(ファイル名,mode)
  ~DataFileOut();                 ///< デストラクタ
  void open(const char *f, bool i_m);   ///< ファイルのオープン
  void close();                   ///< ファイルのクローズ
  void write(double x, double y); ///< データ書き込み関数(2D)
  void write(double x, double y, double z);  ///< データ書き込み関数(3D)
  void insert_line();             ///< 空行を書き込む関数
  void insert_comment(const char *comment); ///< コメントを書き込む関数
  /// SimData型のデータを書き出す関数 
  void data_from_simdata(Nistk::SimData *i_data, double x_start=0.0, 
 	      double x_step=1.0, double y_start=0.0, double y_step=1.0);
  bool is_open();                 ///< ファイルが開いてるかチェックする関数
  bool is_3D();                   ///< modeが3Dかチェックする関数
  char *get_file_name();     ///< 開いているファイルのファイル名を取得する関数
};

}

#endif // NISTK_DATAFILEOUT_H
