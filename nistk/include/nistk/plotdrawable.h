/**
 * @file plotdrawable.h
 * @brief class for plotdrawable used window
 *
 * @author Masakazu Komori
 * @date 2011-08-15
 * @version $Id: plotdrawable.h,v.20110815 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef NISTK_POLT_DRAWABLE
#define NISTK_POLT_DRAWABLE

#include<gtkmm/drawingarea.h>
#include<plotdata.h>


namespace Nistk{
/** PlotDataクラスよりグラフのDrawableを生成するクラス
 *
 * Nistk::PlotDataとGtk::DrawingAreaを使ってグラフ描画をするクラス。
 * 一つのグラフに複数のグラフを描画することが可能となっている。
 * よって、描画するグラフの個数分だけPlotDataクラスを動的確保
 * するので、あまりたくさんになるとメモリに注意する必要がある。
 * また、データの個数があまりにも多くなると描画に時間がかかる
 * 可能性があるので注意が必要である。
 * <br>
 *
 * Copyright (C) 2011 Masakazu Komori
 */

class PlotDrawable : public Gtk::DrawingArea
{
 protected:
  Nistk::PlotData **p_data2d; ///< プロット用データのクラスの2次元配列として扱うため
  int p_data_x_num;    ///< プロット用データのクラスの数（列）
  int p_data_y_num;    ///< プロット用データのクラスの数（行）
  double **data_min_x, **data_min_y; ///< グラフの最小値x,y
  double **data_max_x, **data_max_y; ///< グラフの最大値x,y
  int    **data_org_x;               ///< 各グラフの仮想原点x
  int    **data_org_y;               ///< 各グラフの仮想原点y
  double **data_diff_x;              ///< 各グラフの１ドットの大きさx
  double **data_diff_y;              ///< 各グラフの１ドットの大きさy
  int **x_org,**y_org;               ///< 各グラフ四隅の座標１
  int **x_end,**y_end;               ///< 各グラフ四隅の座標２
  Glib::RefPtr<Pango::Layout> m_layout; ///< Pango用スマートポインタ
  bool show_flag;                    ///< 画面が表示されているかのフラグ
  /// 自動描画の関数でユーザーが使うことはない
  virtual bool on_expose_event(GdkEventExpose *event);
  void make_array(int x_num, int y_num); ///< プロット関係の配列確保の関数
  /// 目盛用文字列生成関数
  void make_str(char *o_str, int str_len, double o_data, double diff_data);
  /// 範囲外の時のプロット点計算関数
  void calc_p_point(int x_num, int y_num, double *a_data,
		    double *b_data, int *p_point);

 public:
  PlotDrawable();                     ///< コンストラクタ
  ~PlotDrawable();                    ///< デストラクタ
  /// プロットするグラフの領域を確保する関数
  void set_graph_area(int x_num, int y_num, int x_range, int y_range, 
		      int x_margin, int y_margin);
  /// 位置x,yのグラフのPlotDataのポインタを取得する関数
  Nistk::PlotData* pd_ptr(int x, int y);
  bool is_show();                     ///< showフラグの値を取得する関数
  void set_show_flag(bool sflag);     ///< showフラグをセットする関数
  void redraw();                      ///< 強制再描画用関数
};

} 

#endif //NISTK_POLT_DRAWABLE
