/**
 * @file  ivneuron.h
 * @brief Class for Izhikevich's Simple Spiking Neuron
 *
 * @author Masakazu Komori
 * @date 2011-08-05
 * @version $Id: ivneuron.h,v.20110805 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef IVNEURON_H
#define IVNEURON_H

#include<nistk/ivvariable.h>
#include<nistk/ivparameter.h>

namespace Nistk{
namespace Neuron{
/** IzhikevichのSimple Spiking Neuron modelの計算用関数を集めたクラス
 *
 * このクラスのメンバ関数は全て静的関数となっているのでオブジェクトを
 * 生成する必要はない。また、個別の関数を利用することは可能であるが、
 * 膜電位の計算をする際には、関数calcを利用するだけで良い。<br>
 *   Hodgikin-Huxleyよりも簡単で、2元連立微分方程式
 \f[
        \frac{dv}{dt} = 0.04 * v^2 + 5 * v + 140 - u + I
 \f]
 \f[
         \frac{du}{dt} = a * (b * v - u)
 \f]
 \f[
  if \,\,\,\,v \geq 30[mV] \,\,\,\,then \left\{
                                  \begin{array}{ll}
                                     v & \leftarrow c\\
                                     u & \leftarrow u + d
                                  \end{array}
                                 \right.
 \f]
 * で、4つのパラメータa,b,c,dの設定次第で皮質ニューロンや視床皮質ニューロンの
 * スパイク特性を再現できる。(Eugene M. Izhikevich, "Simple Model of
 * Spiking Neurons", IEEE Trans. Neural Networks, Vol.14, No.6, 
 * pp.1569-1572, 2003)vは膜電位を表しており，uは膜リカバリ変数
 * と呼ばれるものである。膜リカバリ変数は、膜電位vの式に負のフィードバックが
 * かかる形で入っており、\f$K^+\f$の活性化、\f$Na^+\f$の不活性化
 * を意味する、つまり、明確な膜時定数やしきい値はない。(モデルには閾値がないが、
 * これによって発火の状況次第で、発火しやすくなったり、しにくくなる。)
 * aは、時定数ようなものであり、uの時間変化が決定される。bは、vに対する
 * 感度であり、ある意味追随性が決まる。cは、発火後期の\f$K^+\f$による
 * 過分極の動作を表している。要は、膜電位のリセット。よって、
 * cは静止膜電位を決定する。(cの値になるとは限らない)dは、発火後の
 * しきい値の一時的上昇(\f$K^+\f$,\f$Na^+\f$コンダクタンスによる)
 * を表している。要は、uは、vに負のフィードバックがかかっているので、
 * 膜電位が上昇しにくくなる。(どうも、このdによるしきい値上昇と
 * aによる時定数によりスパイク間隔などが変わる)また、Iは入力である。<br>
 *   v,u,a,b,c,d,I,tはdimensionlessであるが、想定としては、
 * v[mV],c[mV],t[ms]である。その他は当然単位なしである。基本となる
 * パラメータ値は次の通りである。<br>
 * <br>
 * デフォルトパラメータ:<br>
 * a = 0.02 <br>
 * b = 0.2 <br>
 * c = -65[mV] <br>
 * d = 2 <br>
 * 初期値:<br>
 * v = c <br>
 * u = b * v <br>
 * <br>
 * これを基本として、パラメータa,b,c,dを変えることによって、様々な
 * タイプの神経細胞の発火パターンを再現出来る。以下に、Izhikevichの
 * 論文を参考に、このクラスを用いてシミュレーションしたシミュレーション例を示す。
 * \image html ivneuron-sim.png
 * \image latex ivneuron-sim.eps "Izhikevichのmodelのシミュレーション例"
 *   ただ、IzhikevichのHP(www.Izhikevich.com)を見ると上式と少し異なる
 * 形のものがのっている。
 \f[
        100 * \frac{dv}{dt} = 0.07 * (v + 60) * (v + 40) - u + I
 \f]
 \f[
         \frac{du}{dt} = 0.03 * (-2 * (v + 60) - u)
 \f]
 \f[
  if v \,\,\,\,\geq 35[mV] \,\,\,\,then \left\{
                                  \begin{array}{ll}
                                     v & \leftarrow -50[mV]\\
                                     u & \leftarrow u + 100
                                  \end{array}
                                 \right.
 \f]
 * vに関する式については、変形すれば元の式と同じような2次形式になるが、
 * uに関する式については、vのところが少し異なっている。<br>
 *   よって、このクラスではモデルの式を次のような形でパラメータを
 * 設定して使う形式とする。
 \f[
        \frac{dv}{dt} = (vp\_1 * v^2 + vp\_2 * v + vp\_3 
                            - vp\_4 * u + vp\_5 + I) / vp\_6
 \f]
 \f[
        \frac{du}{dt} = a * (b * (v + up\_1) - up\_2 * u)
 \f]
 \f[
  if\,\,\, v \geq v\_rest[mV] \,\,\,then \,\,\,\left\{
                                  \begin{array}{ll}
                                     v & \leftarrow vp\_7 * v + c\\
                                     u & \leftarrow up\_3 * u + d
                                  \end{array}
                                 \right.
 \f]
 * 初期値は、次の通りである。
 \f[
         a = 0.02, b = 0.2, c = -65[mV], d = 2, v\_rest = 30[mV]
 \f]
 \f[
         vp\_1 = 0.04, vp\_2 = 5, vp\_3 = 140, vp\_4 = 1.0
 \f]
 \f[
         vp\_5 = 0.0, vp\_6 = 1.0, vp\_7 = 0.0
 \f]
 \f[
         up\_1 = 0.0, vp\_2 = 1.0, vp\_3 = 1.0
 \f]
 * 使用の際には必ず初期化のための
 * Nistk::Neuron::IVNeuron::init関数を利用すること。<br>
 *   また、活動電位の発生時は上昇の様子が急であるので、
 * dtが大きい時(例えばdt=0.1)は電位のピークにかなりの
 * ばらつきが発生する。それがまずい場合は、dtを小さくするか
 * Nistk::Neuron::IVNeuron::calc関数の実行後に<br>
 * <br>
 * // 活動電位の最大値をv_resetで揃える場合 <br>
 * if(ivv.v >= ivp.v_rest) ivv.v = ivp.v_reset;<br>
 * <br>
 * といれてやれば良い。クラスの方ではどちらが良いかわからないので
 * そのままの値を返すことにしている。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class IVNeuron
{
 public:
  /// Nistk::Neuron::IVVariableの初期化用関数
  static void init(Nistk::Neuron::IVVariable *ivv, 
		   Nistk::Neuron::IVParameter *ivp);
  /// Nistk::Temp::Runge4SiによるSimple Spiking Neuron modelを計算する関数
  static void calc(double dt, Nistk::Neuron::IVVariable *ivv, 
		   Nistk::Neuron::IVParameter *ivp);
  /// 膜電位vの微分値計算用関数
  static double calc_v(double dt, double *val, 
		       Nistk::Neuron::IVVariable *ivv, 
		       Nistk::Neuron::IVParameter *ivp);
  /// リカバリ変数uの微分値計算用関数
  static double calc_u(double dt, double *val, 
		       Nistk::Neuron::IVVariable *ivv, 
		       Nistk::Neuron::IVParameter *ivp);
};

}
}

#endif //IVNEURON_H
