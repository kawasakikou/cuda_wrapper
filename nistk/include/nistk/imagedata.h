/**
 * @file imagedata.h
 * @brief class for operate pixbuf
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: imagedata.h,v.20130315 $
 *
 * Copyright (C) 2008-2013 Masakazu Komori
 */

#ifndef NISTK_IMAGEDATA_H
#define NISTK_IMAGEDATA_H
#include<string>
#include<gtkmm.h>
#include<simdata.h>
#include<graydata.h>

namespace Nistk{
/** pixbuf関連の操作をするクラス
 *
 * pixbufを扱うための関数を集めたクラス。
 * このクラスは分類のために作っただけで
 * 実際にはオブジェクト化する必要はない。
 * pixbufからgraydata,simdataを得る
 * 関数やその逆,さらにRGBのsimdataを
 * 得たり、pixbufの書き出しも可能となっている。
 *
 * Copyright (C) 2008-2013 Masakazu Komori
 */
class ImageData
{
 public:
  /// pixbufの画像の幅を得る関数
  static int pixbuf_get_width(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);
  /// pixbufの画像の高さを得る関数
  static int pixbuf_get_height(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);

  /// 幅と高さを指定して空のpixbufを生成する関数
  static Glib::RefPtr<Gdk::Pixbuf> create_pixbuf_new(int width,int height);

  /// pixbufのサイズを変更する関数
  static Glib::RefPtr<Gdk::Pixbuf> resize_pixbuf(Glib::RefPtr<Gdk::Pixbuf> 
					      i_pixbuf, int width,int height);

  /// pixbufデータからグレースケールの０から1の値をとるデータを取得する関数
  static void simdata_from_pixbuf(SimData *s_data,
			     Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,int mode=2);

  /// pixbufデータから0から1の値をとるRGBデータを取得する関数
  static void simdata_from_pixbuf_RGB(SimData *r_data, SimData *g_data,
	               SimData *b_data, Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);

  /// pixbufデータからグレースケールの０から255の値をとる関数
  static void graydata_from_pixbuf(GrayData *s_data,
			     Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,int mode=2);

  /// SimDataからpixbufのグレースケールの画像データを格納する関数
  static void pixbuf_from_simdata(SimData *s_data,
			     Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);

  /// SimData型のデータからpixbufの各RGBの画像データを格納する関数
  static void pixbuf_from_simdata_RGB(SimData *s_data,
	  	            Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,int mode=0);

  /// SimData型のデータからpixbufのRGBカラーの画像データを格納する関数
  static void pixbuf_from_simdata_RGB_full(SimData *r_data, SimData *g_data,
	               SimData *b_data, Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);

  /// GrayDataからpixbufのグレースケールの画像データを格納する関数
  static void pixbuf_from_graydata(GrayData *s_data,
			     Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);

  /// pixbufデータをファイルに保存する関数
  static void save_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,
			  const char *i_file_name="out-fig", 
			  const char *i_file_type="png");
  /// SimDataのデータを画像ファイルとして保存する関数
  static void imagefile_from_simdata(Nistk::SimData *in, 
			    const char *i_file_name="out-fig",
			    const char *i_file_type="png",
			    int mode=0, double num=0, 
			    int major=0, int minor=0);	  

};

}

#endif // NISTK_IMAGEDATA_H
