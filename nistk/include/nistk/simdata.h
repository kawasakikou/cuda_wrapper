/**
 * @file  simdata.h
 * @brief class for simulation data
 *
 * @author Masakazu Komori
 * @date 2017-09-01
 * @version $Id: simdata.h,v.20170901 $
 *
 * Copyright (C) 2009-2017 Masakazu Komori
 */

#ifndef NISTK_SIMDATA_H
#define NISTK_SIMDATA_H

namespace Nistk{
/** シミュレーション用データのためのクラス 
 *
 * シミュレーション用データのためのクラス。
 * pixbufのRGBデータからグレイデータの実数値(double 0〜1)を
 * を得て、二次元データのようにして扱えるように
 * してあるクラスでもある。実際に中身ではデータは一次元で扱っている。
 * このクラスのデータからpixbufのデータを作ることもできる。
 * 使いかたいろいろ可能となっていると思う。二次元データといって
 * いるが、一次元データを複数持つ使いかたも可能と言える。
 * ただし、一次元データの大きさはみんな同じになってしまう。
 * また、一次元配列として扱うことが可能となるようにもしてある。
 * これを使うことによって、画像を二次元データとして取得して、
 * 一次元データとして取り出すことも可能となっている。<br>
 *
 * Copyright (C) 2009-2017 Masakazu Komori
 */
class SimData
{
 protected:
  int height;    ///< 二次元データの高さ
  int width;     ///< 二次元データの幅
  int length;    ///< 一次元での長さ
  double **data2d;  ///< データを二次元配列として扱うためのポインタ

 public:
  SimData();                             ///< コンストラクタ 引数無し
  SimData(int w,int h);                  ///< コンストラクタ 配列の確保もする
  SimData(const Nistk::SimData &obj);    ///< コピーコンストラクタ
  ~SimData();                            ///< デストラクタ
  /// 代入演算子=のオーバーロード
  Nistk::SimData &operator=(Nistk::SimData &obj);
  void create_data_size(int w,int h);    ///< 配列の確保をする関数
  void recreate_data_size(int w,int h);  ///< 配列の大きさを変更する関数
  int get_height();                      ///< 配列の高さを取得する関数 
  int get_width();                       ///< 配列の幅を取得する関数
  int get_length();                      ///< 配列の長さを取得する関数
  double get_data(int x, int y);         ///< 2Dデータの読み出しを行う関数
  void put_data(int x, int y, double i_data); ///< 2Dデータの格納を行う関数
  double get_data_1d(int x);             ///< 1Dデータの読み出しを行う関数
  void put_data_1d(int x, double i_data); ///< 1Dデータの格納を行う関数
  void delete_data();                    ///< 配列を消去する関数
  /// 連続領域確保されたデータ配列のポインタを取得する関数
  double* get_data_ptr();                 
};

}

#endif // NISTK_SIMDATA_H
