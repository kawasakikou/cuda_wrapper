/**
 * @file  imageloader.h
 * @brief class for imageloader of simulation 
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imageloader.h,v.20110809 $
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */

#ifndef NISTK_IMAGELOADER_H
#define NISTK_IMAGELOADER_H

#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include<gtkmm.h>

namespace Nistk{
/** 複数のシミュレーション用画像を逐次で読み込むクラス
 *
 * リストファイルを読み込んでそれを基に関数を使って
 * 一つずつファイルからpixbufに読み込む。
 * 全て読むとまた最初から読み込むようになっている。
 * また、コメントを書くことができる。サンプルを参照のこと。<br>
 *
 * リストファイルの書式 <br>
 *
 *     読み込むファイル数 <br>
 *     画像ファイル名1 <br>
 *     画像ファイル名2 <br>
 *         : <br>
 * <br>
 *
 * Copyright (C) 2008-20011 Masakazu Komori
 */
class ImageLoader
{
 protected:
  int f_num;                       ///< 読み込むファイル数
  int f_count;                     ///< 何番目のファイルかのカウンタ
  std::string i_file;              ///< リストファイル名
  std::vector<std::string> f_name; ///< ファイル名の配列
  char *lf_name;                   ///< char型 リストファイル名
  char *if_name;                   ///< char型 現在の入力ファイル名

 public:
  ImageLoader();                ///< コンストラクタ 引数無し
  ImageLoader(const char *f);         ///< コンストラクタ 引数(リストファイル名)
  ImageLoader(const Nistk::ImageLoader &obj);  ///< コピーコンストラクタ
  ~ImageLoader();               ///< デストラクタ
  /// 代入演算子=のオーバーロード
  Nistk::ImageLoader &operator=(Nistk::ImageLoader &obj);
  void set_input_file(const char *f); ///< 入力ファイルリストをセットする関数
  char* get_list_file_name();   ///< リストファイル名を返す関数
  char* get_input_file_name();  ///< 読み込みファイル名を返す関数
  int get_input_file_num();     ///< 読み込みファイル数を返す関数
   /// 今、何番目のファイルか(0から始まる)を返す関数
  int get_input_file_count();
  void count_reset();           ///< 読み込みカウンタをリセットする関数
  void skip_input_file();       ///< 読み込みファイルを一つスキップする関数
  /// 次のイメージを読む関数
  Glib::RefPtr<Gdk::Pixbuf> load_image_next();
};

}

#endif // NISTK_IMAGELOADER_H
