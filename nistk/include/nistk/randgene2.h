/**
 * @file  randgene2.h
 * @brief class for random generator follow various distribution
 *
 * @author Masakazu Komori
 * @date 2016-03-22
 * @version $Id: randgene2.h,v.20160322 $
 *
 * Copyright (C) 2016 Masakazu Komori
 */

#ifndef NISTK_RANDGENE2_H
#define NISTK_RANDGENE2_H
#include<cstdlib>
#include<ctime>

namespace Nistk{
/** ランダムジェネレータ(一様乱数[0~1]を使って様々な確率分布の乱数列を生成)
 *
 * ランダムジェネレータ。(一様乱数[0~1]を使って様々な確率分布の乱数列を生成)
 * このクラスのオブジェクトを生成すれば,オブジェクト一つでいろいろな
 * 確率分布に従った乱数を得ることができる。
 * ただし,C/C++標準のrand関数を使っているので乱数としては
 * よくないので,メルセンヌツイスタ法による乱数の方がよい。
 *
 * Copyright (C) 2016 Masakazu Komori
 */
class RandGene2
{
 public:
  RandGene2();                           ///< コンストラクタ
  ~RandGene2();                          ///< デストラクタ
  double get_rand_d();                   ///< 0から1までの一様乱数の発生
  double get_rand_exp(double alpha);     ///< 指数分布による乱数の発生
  int get_rand_poisson(double m);        ///< ポアソン分布による乱数の発生
  /// アーラン分布による乱数の発生
  int get_rand_erlang(double lambda, int n);     
  /// 正規分布による乱数の発生
  double get_rand_gaussian(double ex, double vx, int n=240);     
  /// 度数分布による乱数の発生
  double get_rand_hist(double *x, double *p, int num);     
};

}
#endif // NISTK_RANDGENE2_H
