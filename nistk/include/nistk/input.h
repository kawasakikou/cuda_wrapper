/**
 * @file  input.h
 * @brief Class for Tools of Generating Simulation Input
 *
 * @author Masakazu Komori
 * @date 2011-08-30
 * @version $Id: input.h,v.20110830 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef INPUT_H
#define INPUT_H

#include<nistk/simdata.h>

namespace Nistk{
/** シミュレーション用の入力を作成する関数を集めた関数 
 *
 * このクラスは、シミュレーション用の入力を作成する関数を集めたクラスである。
 * よって、このクラスのメンバはすべてstaticであるので、オブジェクトを
 * 生成する必要はない。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class Input
{
 public:
  /// 楕円型のガウシアンの入力を得る関数
  static void get_e_gaussian(Nistk::SimData *i_data,
			     double *x_c, double *y_c,
			     double x_step, double y_step,
			     double *gain, double *sigma1, double *sigma2, 
			     double *angle, int num);
  /// 入力イメージを入力空間にセットする関数
  static void set_input_image(Nistk::SimData *i_data, Nistk::SimData *i_image,
			      int x_c, int y_c, double angle, const char *op);
};

}

#endif //INPUT_H
