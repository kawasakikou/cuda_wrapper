/**
 * @file  runge.h
 * @brief class for runge kutta
 *
 * @author Masakazu Komori
 * @date 2011-07-22
 * @version $Id: runge4.h,v.20110722 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */
#ifndef RUNGE4_H
#define RUNGE4_H

#include<nistk/fsocket.h>

namespace Nistk{
namespace Temp{
/** 4次のルンゲクッタ法で微分方程式の計算をする関数
 *
 * 4次のルンゲクッタ法で微分方程式の計算をするテンプレート関数。<br>
 * <br>
 \f[
      \frac{dy}{dx} = f(x,y)
 \f]
 * <br>
 * 形式の微分方程式の1ステップを4次のルンゲクッタ法<br>
 * <br>
 \f[
      k1 = dx * f(x_i, y_i) 
 \f]
 \f[
      k2 = dx * f(x_i+dx/2, y_i+k1/2)
 \f]
 \f[
      k3 = dx * f(x_i+dx/2, y_i+k2/2)
 \f]
 \f[
      k4 = dx * f(x_i+dx, y_i+k3) 
 \f]
 \f[ 
     y_{i+1} = y_i + \frac{1}{6}(k1 + 2k2 + 2k3 +k4)
 \f]
 * で計算する。誤差は\f$dx^5\f$のオーダであるのでそこを忘れないように。
 * 右辺の関数\f$f(x,y)\f$は、2変数となっているが、この関数では
 * 関数用パラメータも引数として指定するために<br>
 \f[
      \frac{dy}{dx} = f(x, y, para1, para2)
 \f]
 * という形の関数\f$f(x,y,para1,para2)\f$のポインタを渡すことになっている。
 * この形は必ず守る必要がある。この関数ポインタを使ってこの関数内から
 * 関数呼び出しをするからである。また、当然、関数\f$f(x,y,para1,para2)\f$
 * は自前で準備が必要である。
 * よって、関数は次のような形になっている必要がある。<br>
 * <br>
 * double 関数(double x, double y, ARG1 para1, ARG2 para2) <br>
 * <br>
 * 3,4番目の引数の型はARG1,ARG2というテンプレートとなっており、
 * 使用の際に指定可能となっている。
 * (1つを変数パラメータ、もう1つを固定パラメータにするなど)
 * よって、これを使うことによって自由度はある程度確保されている。
 * 引数の一部が必要ない時は、ダミーを使ってこの形にすること。
 * また、計算速度的に関数ポインタを使うとどうなのかわからないが、
 * もしこれ以上の速度が必要な時は、自作するのが良いと思われる。
 *
 * @param *func 関数のポインタ 
 * @param x 関数のxの値
 * @param y 関数のxの値
 * @param para1 関数のパラメータ1
 * @param para2 関数のパラメータ2
 * @param dx 刻み幅
 * @return 次のステップのyの値
 */
template <class ARG1, class ARG2>
double Runge4(double (*func)(double fx, double fy, 
				   ARG1 para1, ARG2 para2),
		    double x, double y, ARG1 para1, 
		    ARG2 para2, double dx)
{
  double k1,k2,k3,k4,dx2;
  double y_next;

  dx2 = dx / 2.0;
  k1 = dx * func(x, y, para1, para2);
  k2 = dx * func(x + dx2, y + (k1/2.0), para1, para2);
  k3 = dx * func(x + dx2, y + (k2/2.0), para1, para2);
  k4 = dx * func(x + dx, y + k3, para1, para2);
  y_next = y + (k1 + 2.0*k2 + 2.0*k3 +k4) / 6.0;  
  
  return y_next;
}

/** 4次のルンゲクッタ法で微分方程式の計算をする関数(FSocket)
 *
 * 4次のルンゲクッタ法で微分方程式の計算をするテンプレート関数の
 * FSoket4を使ったバージョン。<br>
 * <br>
 \f[
      \frac{dy}{dx} = f(x,y)
 \f]
 * <br>
 * 形式の微分方程式の1ステップを4次のルンゲクッタ法<br>
 * <br>
 \f[
      k1 = dx * f(x_i, y_i) 
 \f]
 \f[
      k2 = dx * f(x_i+dx/2, y_i+k1/2) 
 \f]
 \f[
      k3 = dx * f(x_i+dx/2, y_i+k2/2) 
 \f]
 \f[
      k4 = dx * f(x_i+dx, y_i+k3) 
 \f]
 \f[
      y_{i+1} = y_i + \frac{1}{6}(k1 + 2k2 + 2k3 +k4)
 \f]
 * で計算する。
 * で計算する。誤差は\f$dx^5\f$のオーダであるのでそこを忘れないように。
 * 右辺の関数\f$f(x,y)\f$は、2変数となっているが、この関数では
 * 関数用パラメータも引数として指定するために<br>
 \f[
      \frac{dy}{dx} = f(x, y, para1, para2)
 \f]
 * という形の関数\f$f(x,y,para1,para2)\f$のポインタを渡すことになっている。
 * この形は必ず守る必要がある。この関数ポインタを使ってこの関数内から
 * 関数呼び出しをするからである。また、当然、関数\f$f(x,y,para1,para2)\f$
 * は自前で準備が必要である。
 * 関数の引数は次のような形になっている必要がある。<br>
 * <br>
 * double 関数(double x, double y, ARG1 para1, ARG2 para2) <br>
 * <br>
 * 3,4番目の引数の型はARG1,ARG2というテンプレートとなっており
 * 使用の際に指定可能となっている。
 * (1つを変数パラメータ、もう1つを固定パラメータにするなど)
 * よって、これを使うことによって自由度はある程度確保されている。
 * 引数の一部が必要ない時は、ダミーを使ってこの形にすること。
 * この関数をFSocket4のオブジェクトにセットしてそのポインタを
 * 渡すことによって計算することが可能となっている。
 * また、計算速度的に関数ポインタを使うとどうなのかわからないが、
 * もしこれ以上の速度が必要な時は、自作するのが良いと思われる。
 *
 * @param dx 刻み幅
 * @param *func FSocket4のオブジェクトのポインタ
 * @return 次のステップのyの値
 */
template <class ARG1, class ARG2>
double Runge4Fs(double dx, 
		Nistk::Temp::FSocket4<double, double, double, ARG1, ARG2> *func)
{
  double k1,k2,k3,k4;
  double x_org, y_org;
  double y_next;

  x_org = func->fs_arg1;
  y_org = func->fs_arg2;
  k1 = dx * func->calc_function();

  func->fs_arg1 = x_org + (dx/2.0);
  func->fs_arg2 = y_org + (k1/2.0);
  k2 = dx * func->calc_function();

  func->fs_arg1 = x_org + (dx/2.0);
  func->fs_arg2 = y_org + (k2/2.0);
  k3 = dx * func->calc_function();

  func->fs_arg1 = x_org + dx;
  func->fs_arg2 = y_org + k3;
  k4 = dx * func->calc_function();

  y_next = y_org + (1 / 6.0) * (k1 + 2.0*k2 + 2.0*k3 +k4);  
  func->fs_arg2 = y_next;  

  return y_next;
}

/** 4次のルンゲクッタ法で連立微分方程式の計算用関数テンプレート
 *
 * 4次のルンゲクッタ法で連立微分方程式(Simultaneous differential 
 * equation)の計算をするテンプレート関数。<br>
 * <br>
 \f[
      \frac{dy^{(1)}}{dx} = f_1(x,y^{(1)},\cdots,y^{(n)})
 \f]
 \f[
      \vdots
 \f]
 \f[
      \frac{dy^{(n)}}{dx} = f_n(x,y^{(1)},\cdots,y^{(n)})
 \f]
 * <br>
 * 形式の連立微分方程式の1ステップを4次のルンゲクッタ法<br>
 * <br>
 \f[
      k1^{(1)} = dx * f_1(x_i, y^{(1)}_i,\cdots,y^{(n)}_i) 
 \f]
 \f[
      \vdots
 \f]
 \f[
      k1^{(n)} = dx * f_n(x_i, y^{(1)}_i,\cdots,y^{(n)}_i) 
 \f]
 \f[
k2^{(1)} = dx * f_1(x_i+dx/2, y^{(1)}_i+k1^{(1)}/2,\cdots,y^{(n)}_i+k1^{(n)}/2) 
 \f]
 \f[
      \vdots
 \f]
 \f[
k2^{(n)} = dx * f_n(x_i+dx/2, y^{(1)}_i+k1^{(1)}/2,\cdots,y^{(n)}_i+k1^{(n)}/2) 
 \f]
 \f[
k3^{(1)} = dx * f_1(x_i+dx/2, y^{(1)}_i+k2^{(1)}/2,\cdots,y^{(n)}_i+k2^{(n)}/2) 
 \f]
 \f[
      \vdots
 \f]
 \f[
k3^{(n)} = dx * f_n(x_i+dx/2, y^{(1)}_i+k2^{(1)}/2,\cdots,y^{(n)}_i+k2^{(n)}/2) 
 \f]
 \f[
    k4^{(1)} = dx * f_1(x_i+dx, y^{(1)}_i+k3^{(1)},\cdots,y^{(n)}_i+k3^{(n)}) 
 \f]
 \f[
      \vdots
 \f]
 \f[
    k4^{(n)} = dx * f_n(x_i+dx, y^{(1)}_i+k3^{(1)},\cdots,y^{(n)}_i+k3^{(n)}) 
 \f]
 \f[ 
    y^{(1)}_{i+1} = y^{(1)}_i + \frac{1}{6}(k1^{(1)} + 2k2^{(1)} + 2k3^{(1)} +k4^{(1)
})
 \f]
 \f[
      \vdots
 \f]
 \f[ 
    y^{(n)}_{i+1} = y^{(n)}_i + \frac{1}{6}(k1^{(n)} + 2k2^{(n)} + 2k3^{(n)} +k4^{(n)
})
 \f]
* で計算する。誤差は\f$dx^5\f$のオーダであるのでそこを忘れないように。
 * 右辺の関数\f$f_j(x,y^{(1)},\cdots,y^{(n)})\f$は、n+1変数となっているが、
 * この関数では関数用パラメータも引数として指定するために<br>
 \f[
      \frac{dy^{(j)}}{dx} = f_j(x, y^{(1)},\cdots,y^{(n)}, para1, para2)
 \f]
 * <br>
 * として、関数\f$f_j(x,y^{(1)},\cdots,y^{(n)},para1,para2)\f$の
 * \f$y^{(1)},\cdots,y^{(n)}\f$を配列としてそのポインタを利用することによって、
 * n元連立微分方程式の計算をすることが可能となっている。
 * よって、関数は次のような形になっている必要がある。<br>
 * <br>
 * double 関数(double x, double *y, ARG1 para1, ARG2 para2) <br>
 * <br>
 * この形は必ず守る必要がある。この関数ポインタを使ってこの関数内から
 * 関数呼び出しをするからである。関数\f$f(x,*y,para1,para2)\f$
 * は自前で準備が必要である。
 * 3,4番目の引数の型はARG1,ARG2というテンプレートとなっており、
 * 使用の際に指定可能となっている。
 * (1つを変数パラメータ、もう1つを固定パラメータにするなど)
 * よって、これを使うことによって自由度はある程度確保されている。
 * 引数の一部が必要ない時は、ダミーを使ってこの形にすること。
 * この関数を使用する際は、その性質上、連立微分方程式の関数ポインタの
 * 配列のポインタを渡す必要があるのも注意すること。
 * 計算速度的に関数ポインタを使うとどうなのかわからないが、
 * もしこれ以上の速度が必要な時は、自作するのが良いと思われる。
 *
 *
 * @param *func[] 関数ポインタ配列のポインタ 
 * @param x 連立微分方程式のxの値
 * @param y 連立微分方程式のyの値の配列ポインタ
 * @param para1 関数のパラメータ1
 * @param para2 関数のパラメータ2
 * @param dx 刻み幅
 * @param num 連立微分方程式の数
 */
template <class ARG1, class ARG2>
void Runge4Si(double (*func[])(double fx, double *fy, 
				   ARG1 para1, ARG2 para2),
		    double x, double *y, ARG1 para1, 
		ARG2 para2, double dx, int num)
{
  double x_org;               // yの値を保存するための変数
  double *y_org;              // yの値を保存するための配列用ポインタ
  double *k1,*k2,*k3,*k4;     // 各値を保存するための配列用ポインタ
  double dx2;
  int i;

  // 設定と配列確保してx,yの値をコピー
  dx2 = dx / 2.0;
  y_org = new double[num];
  k1 = new double[num];
  k2 = new double[num];
  k3 = new double[num];
  k4 = new double[num];
  x_org = x;
  for(i = 0; i < num; i++){
    y_org[i] = y[i];
  }

  // k1の計算
  for(i = 0; i < num; i++){
    k1[i] = dx * func[i](x, y, para1, para2);
  }

  // x,yを更新して、k2を計算
  x = x_org + dx2;
  for(i = 0; i < num; i++){
      y[i] = y_org[i] + (k1[i]/2.0);
  }
  for(i = 0; i < num; i++){
    k2[i] = dx * func[i](x, y, para1, para2);
  }

  // x,yを更新して、k3を計算
  x = x_org + dx2;
  for(i = 0; i < num; i++){
      y[i] = y_org[i] + (k2[i]/2.0);
  }
  for(i = 0; i < num; i++){
    k3[i] = dx * func[i](x, y, para1, para2);
  }

  // x,yを更新して、k3を計算
  x = x_org + dx;
  for(i = 0; i < num; i++){
      y[i] = y_org[i] + k3[i];
  }
  for(i = 0; i < num; i++){
    k4[i] = dx * func[i](x, y, para1, para2);
  }

  // 最終的なy[i](x+dx)を計算して格納
  for(i = 0; i < num; i++){
    y[i] = y_org[i] + (k1[i] + 2.0*k2[i] + 2.0*k3[i] +k4[i]) / 6.0;  
  }
  
  // 配列の解放
  delete[] y_org;
  delete[] k1;
  delete[] k2;
  delete[] k3;
  delete[] k4;

  return;
}

}
}


#endif // RUNGE4_H
