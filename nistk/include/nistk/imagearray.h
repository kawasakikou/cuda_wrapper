/**
 * @file  imagearray.h
 * @brief class for array view of image
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: imagearray.h,v.20130315 $
 *
 * Copyright (C) 2010-2013 Masakazu Komori
 */

#ifndef NISTK_IMAGEARRAY_H
#define NISTK_IMAGEARRAY_H
#include<gtkmm.h>
#include<imageview.h>
#include<simdataarray.h>
#include<imageloader.h>

namespace Nistk{
/** 複数のイメージを並べて一つのpixbufに格納して表示するクラス 
 *
 * 複数のイメージを並べて一つのpixbufに格納して
 * その画像をウィンドウに表示するクラス。
 * Nistk::ImageViewクラスを継承している。
 * 表示できるイメージはImageLoader,SimDataArrayといった
 * 複数のイメージを一括して扱えるものからと,個別のSimData,画像
 * を並べることもできる。ただし、個別のSimData,画像を並べる場合,
 * 幅と高さが揃っていないといけない。
 * 表示を保存できるが,そのときのファイル名はウィンドウ名
 * となる。ファイル形式はpngである。<br>
 *
 * Copyright (C) 2010-2013 Masakazu Komori
 */
class ImageArray : public Nistk::ImageView
{
 public:
  ImageArray();                           ///< コンストラクタ 引数無し
  virtual ~ImageArray();                  ///< デストラクタ
  /// arrayのためのpixbuf領域を確保する関数
  void create_array_area(int im_width, int im_height, int im_margin,
			                             int w_num, int h_num);
  /// 1つのpixbufをarrayの位置x_num,y_numに書き込む関数
  void set_pixbuf_to_array(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf, 
			                             int x_num, int y_num);
  /// 1つのpixbufをarray_pixbufの位置x_num,y_numに書き込む関数
  void set_pixbuf_to_array_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf, 
			                             int x_num, int y_num);
  /// 1つのSimDataをarrayの位置x_num,y_numに書き込む関数
  void set_simdata_to_array(Nistk::SimData *i_simdata, 
			                             int x_num, int y_num);
  /// Nistk::SimDataArrayから画像のアレイを作る関数
  void array_from_simdataarray(Nistk::SimDataArray *i_array, int i_margin, 
		                        double i_scale=1, int num_width=5);
  /// Nistk::ImageLoaderから画像のアレイを作る関数
  void array_from_file(Nistk::ImageLoader i_loader, int i_margin, 
		                        double i_scale=1, int num_width=5);
  /// arrayのpixbufを取得する関数
  Glib::RefPtr<Gdk::Pixbuf> get_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf);
  void set_array_name(const char *i_name); ///< 名前をセットする関数
  int get_array_width();                 ///< 画像アレイの幅を取得する関数
  int get_array_height();                ///< 画像アレイの高さを取得する関数
  int get_array_w_num();                 ///< 横の画像の数を取得する関数
  int get_array_h_num();                 ///< 縦の画像のを取得する関数
  int get_image_height();                ///< 1つの画像の高さを取得する関数
  int get_image_width();                 ///< 1つの画像の幅を取得する関数
  int get_image_margin();                ///< 画像間の幅を取得する関数
 protected: 
  Glib::RefPtr<Gdk::Pixbuf> array_pixbuf;  ///< 画像アレイの収納用
  int array_width;                         ///< 画像アレイの幅
  int array_height;                        ///< 画像アレイの高さ
  int array_w_num;                         ///< 横の画像の数
  int array_h_num;                         ///< 縦の画像の数
  int image_width;                         ///< 1つの画像の幅
  int image_height;                        ///< 1つの画像の高さ
  int image_margin;                        ///< 画像間のマージン
  bool is_alloc;                           ///< pixbufを確保しているか

  guint8 *array_p_data;                    ///< RGBデータの開始位置
  int array_channels;                      ///< 1pixel当たりのデータ数
  int array_stride;                        ///< 1行当たりのデータ数
};

}

#endif // NISTK_IMAGEARRAY_H
