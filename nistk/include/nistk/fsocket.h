/**
 * @file  fsocket.h
 * @brief class for function socket
 *
 * @author Masakazu Komori
 * @date 2011-06-23
 * @version $Id: fsocket.h,v.20110623 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */
#ifndef FSOCKET_H
#define FSOCKET_H

namespace Nistk{
namespace Temp{
  /** 関数自体(引数1)をクラスもしくは関数に渡すためのソケットクラス
   *
   * 引数が1つの関数自体をクラスもしくは関数に渡すためのソケットの
   * ような役割をするクラス。勝手にソケットといってるが正式にこのような
   * クラスを何と言うかはしらない。利用するクラスは、関数ポインタを使って
   * 呼び出すことになるので、速度的どうなのかという問題がある。
   * そこは考えて使うように。また、このクラスはテンプレートクラスなので
   * そこはよく理解しておくように。関数の形としては <br>
   * <br>
   * RET function(ARG1 farg1) <br>
   * RET 関数の返り値 <br>
   * ARG1 引数1の型 <br>
   * <br>
   * という形のものを扱う。
   * セットする関数の引数に関しては、本当は良くないが利用形態を考えて、
   * パブリックメンバとしている。よって、アクセッサはないので注意すること。
   * 
   * Copyright (C) 2011 Masakazu Komori
   */
  template <class RET, class ARG1>
  class FSocket1{
    protected:
      RET (*fs_function)(ARG1 farg1);   ///< 関数ポインタ格納用関数ポイン
    public:
      ARG1 fs_arg1;                     ///< 関数の1番目の引数
      FSocket1();                     ///< コンストラクタ
      ~FSocket1();                    ///< デストラクタ
      /// 関数をセットする関数
      void set_function(RET (*func)(ARG1 farg1));
      /// セットした関数を呼び出す関数
      RET calc_function();
  };

  /** 関数自体(引数2)をクラスもしくは関数に渡すためのソケットクラス
   *
   * 引数が2つの関数自体をクラスもしくは関数に渡すためのソケットの
   * ような役割をするクラス。勝手にソケットといってるが正式にこのような
   * クラスを何と言うかはしらない。利用するクラスは、関数ポインタを使って
   * 呼び出すことになるので、速度的どうなのかという問題がある。
   * そこは考えて使うように。また、このクラスはテンプレートクラスなので
   * そこはよく理解しておくように。関数の形としては <br>
   * <br>
   * RET function(ARG1 farg1, ARG2 farg2) <br>
   * RET 関数の返り値 <br>
   * ARG1 引数1の型 <br>
   * ARG2 引数2の型 <br>
   * <br>
   * という形のものを扱う。
   * セットする関数の引数に関しては、本当は良くないが利用形態を考えて、
   * パブリックメンバとしている。よって、アクセッサはないので注意すること。
   * 
   * Copyright (C) 2011 Masakazu Komori
   */
  template <class RET, class ARG1, class ARG2>
  class FSocket2{
    protected:
      /// 関数ポインタ格納用関数ポインタ
      RET (*fs_function)(ARG1 farg1, ARG2 farg2); 
    public:
      ARG1 fs_arg1;                   ///< 関数の1番目の引数
      ARG2 fs_arg2;                   ///< 関数の2番目の引数
      FSocket2();                     ///< コンストラクタ
      ~FSocket2();                    ///< デストラクタ
      /// 関数をセットする関数
      void set_function(RET (*func)(ARG1 farg1, ARG2 farg2));
      /// セットした関数を呼び出す関数
      RET calc_function();
  };

  /** 関数自体(引数3)をクラスもしくは関数に渡すためのソケットクラス
   *
   * 引数が3つの関数自体をクラスもしくは関数に渡すためのソケットの
   * ような役割をするクラス。勝手にソケットといってるが正式にこのような
   * クラスを何と言うかはしらない。利用するクラスは、関数ポインタを使って
   * 呼び出すことになるので、速度的どうなのかという問題がある。
   * そこは考えて使うように。また、このクラスはテンプレートクラスなので
   * そこはよく理解しておくように。関数の形としては <br>
   * <br>
   * RET function(ARG1 farg1, ARG2 farg2, AGR3 farg3) <br>
   * RET 関数の返り値 <br>
   * ARG1 引数1の型 <br>
   * ARG2 引数2の型 <br>
   * ARG3 引数3の型 <br>
   * <br>
   * という形のものを扱う。
   * セットする関数の引数に関しては、本当は良くないが利用形態を考えて、
   * パブリックメンバとしている。よって、アクセッサはないので注意すること。
   * 
   * Copyright (C) 2011 Masakazu Komori
   */
  template <class RET, class ARG1, class ARG2, class ARG3>
  class FSocket3{
    protected:
      /// 関数ポインタ格納用関数ポインタ
      RET (*fs_function)(ARG1 farg1, ARG2 farg2, ARG3 farg3);
    public:
      ARG1 fs_arg1;                   ///< 関数の1番目の引数
      ARG2 fs_arg2;                   ///< 関数の2番目の引数
      ARG3 fs_arg3;                   ///< 関数の3番目の引数
      FSocket3();                     ///< コンストラクタ
      ~FSocket3();                    ///< デストラクタ
      /// 関数をセットする関数
      void set_function(RET (*func)(ARG1 farg1, ARG2 farg2, ARG3 farg3));
      /// セットした関数を呼び出す関数
      RET calc_function();
  };

 /** 関数自体(引数4)をクラスもしくは関数に渡すためのソケットクラス
   *
   * 引数が4つの関数自体をクラスもしくは関数に渡すためのソケットの
   * ような役割をするクラス。勝手にソケットといってるが正式にこのような
   * クラスを何と言うかはしらない。利用するクラスは、関数ポインタを使って
   * 呼び出すことになるので、速度的どうなのかという問題がある。
   * そこは考えて使うように。また、このクラスはテンプレートクラスなので
   * そこはよく理解しておくように。関数の形としては <br>
   * <br>
   * RET function(ARG1 farg1, ARG2 farg2, AGR3 farg3, ARG4 farg4) <br>
   * RET 関数の返り値 <br>
   * ARG1 引数1の型 <br>
   * ARG2 引数2の型 <br>
   * ARG3 引数3の型 <br>
   * ARG4 引数4の型 <br>
   * <br>
   * という形のものを扱う。
   * セットする関数の引数に関しては、本当は良くないが利用形態を考えて、
   * パブリックメンバとしている。よって、アクセッサはないので注意すること。
   * 
   * Copyright (C) 2011 Masakazu Komori
   */
  template <class RET, class ARG1, class ARG2, class ARG3, class ARG4>
  class FSocket4{
    protected:
      /// 関数ポインタ格納用関数ポインタ
      RET (*fs_function)(ARG1 farg1, ARG2 farg2, ARG3 farg3, ARG4 farg4);
    public:
      ARG1 fs_arg1;                   ///< 関数の1番目の引数
      ARG2 fs_arg2;                   ///< 関数の2番目の引数
      ARG3 fs_arg3;                   ///< 関数の3番目の引数
      ARG3 fs_arg4;                   ///< 関数の4番目の引数
      FSocket4();                     ///< コンストラクタ
      ~FSocket4();                    ///< デストラクタ
      /// 関数をセットする関数
      void set_function(RET (*func)(ARG1 farg1, ARG2 farg2, 
				    ARG3 farg3, ARG4 farg4));
      /// セットした関数を呼び出す関数
      RET calc_function();
  };

}
}

//////////////////////
//FSocket1
//////////////////////

/** コンストラクタ
 */
template <class RET, class ARG1>
Nistk::Temp::FSocket1<RET, ARG1>::FSocket1()
{
}

/** デストラクタ
 */
template <class RET, class ARG1>
Nistk::Temp::FSocket1<RET, ARG1>::~FSocket1()
{
}

/** 関数をセットする関数
 *
 * FSocket1に関数をセットする関数。
 *
 * @param *func セットする関数のポインタ 
 *
 */
template <class RET, class ARG1>
void Nistk::Temp::FSocket1<RET, ARG1>::
set_function(RET (*func)(ARG1 farg1))
{
  fs_function = func;

  return;
}

/** セットした関数を呼び出す関数
 *
 * セットした関数を呼び出す関数。
 *
 * @return セットした関数の返り値
 */
template <class RET, class ARG1>
RET Nistk::Temp::FSocket1<RET, ARG1>::calc_function()
{
  return (*fs_function)(fs_arg1);
}

///////////////
// FSocket2
///////////////

/** コンストラクタ
 */
template <class RET, class ARG1, class ARG2>
Nistk::Temp::FSocket2<RET, ARG1, ARG2>::FSocket2()
{
}

/** デストラクタ
 */
template <class RET, class ARG1, class ARG2>
Nistk::Temp::FSocket2<RET, ARG1, ARG2>::~FSocket2()
{
}

/** 関数をセットする関数
 *
 * FSocket2に関数をセットする関数。
 *
 * @param *func セットする関数のポインタ 
 *
 */
template <class RET, class ARG1, class ARG2>
void Nistk::Temp::FSocket2<RET, ARG1, ARG2>::
set_function(RET (*func)(ARG1 farg1, ARG2 farg2))
{
  fs_function = func;

  return;
}

/** セットした関数を呼び出す関数
 *
 * セットした関数を呼び出す関数。
 *
 * @return セットした関数の返り値
 */
template <class RET, class ARG1, class ARG2>
RET Nistk::Temp::FSocket2<RET, ARG1, ARG2>::calc_function()
{
  return (*fs_function)(fs_arg1, fs_arg2);
}

///////////////
// FSocket3
//////////////

/** コンストラクタ
 */
template <class RET, class ARG1, class ARG2, class ARG3>
Nistk::Temp::FSocket3<RET, ARG1, ARG2, ARG3>::FSocket3()
{
}

/** デストラクタ
 */
template <class RET, class ARG1, class ARG2, class ARG3>
Nistk::Temp::FSocket3<RET, ARG1, ARG2, ARG3>::~FSocket3()
{
}

/** 関数をセットする関数
 *
 * FSocket3に関数をセットする関数。
 *
 * @param *func セットする関数のポインタ 
 */
template <class RET, class ARG1, class ARG2, class ARG3>
void Nistk::Temp::FSocket3<RET, ARG1, ARG2, ARG3>::
set_function(RET (*func)(ARG1 farg1, ARG2 farg2, ARG3 farg3))
{
  fs_function = func;

  return;
}

/** セットした関数を呼び出す関数
 *
 * セットした関数を呼び出す関数。
 *
 * @return セットした関数の返り値
 */
template <class RET, class ARG1, class ARG2, class ARG3>
RET Nistk::Temp::FSocket3<RET, ARG1, ARG2, ARG3>::calc_function()
{
  return (*fs_function)(fs_arg1, fs_arg2, fs_arg3);
}

///////////////
// FSocket4
//////////////

/** コンストラクタ
 */
template <class RET, class ARG1, class ARG2, class ARG3, class ARG4>
Nistk::Temp::FSocket4<RET, ARG1, ARG2, ARG3, ARG4>::FSocket4()
{
}

/** デストラクタ
 */
template <class RET, class ARG1, class ARG2, class ARG3, class ARG4>
Nistk::Temp::FSocket4<RET, ARG1, ARG2, ARG3, ARG4>::~FSocket4()
{
}

/** 関数をセットする関数
 *
 * FSocket3に関数をセットする関数。
 *
 * @param *func セットする関数のポインタ 
 */
template <class RET, class ARG1, class ARG2, class ARG3, class ARG4>
void Nistk::Temp::FSocket4<RET, ARG1, ARG2, ARG3, ARG4>::
  set_function(RET (*func)(ARG1 farg1, ARG2 farg2, ARG3 farg3, ARG4 farg4))
{
  fs_function = func;

  return;
}

/** セットした関数を呼び出す関数
 *
 * セットした関数を呼び出す関数。
 *
 * @return セットした関数の返り値
 */
template <class RET, class ARG1, class ARG2, class ARG3, class ARG4>
RET Nistk::Temp::FSocket4<RET, ARG1, ARG2, ARG3, ARG4>::calc_function()
{
  return (*fs_function)(fs_arg1, fs_arg2, fs_arg3, fs_arg4);
}

#endif // FSOCKET_H
