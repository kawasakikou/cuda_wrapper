/**
 * @file  mathtools.h
 * @brief class for math tools
 *
 * @author Masakazu Komori
 * @date 2013-04-01
 * @version $Id: dmathtools.h,v.20130401 $
 *
 * Copyright (C) 2008-2013 Masakazu Komori
 */

#ifndef NISTK_MATHTOOLS_H
#define NISTK_MATHTOOLS_H
#include<cmath>
#include<simdata.h>

namespace Nistk{
/** シミュレーションでよく使いそうな数学関係の関数等を集めたクラス
 *
 * ニューラルネットワークのシミュレーションでプログラムを組む際に
 * よく使いそうな関数や処理を集めたクラス。このクラスは便宜上
 * クラスにしているだけで、オブジェクトを生成する必要はないクラス
 * である。また、このクラスで準備される関数は必要に応じて増やしていく
 * つもりなので,最終的にどうなるかは考えていない。準備している関数の
 * 中には引数が非常に多いものがあり,プログラムとしては美しくない
 * ので将来変更するかも。。
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */
class MathTools
{
 public:
  static double step_func(double x);            ///< しきい値関数(ステップ関数)
  static double sigmoid_func(double x, double gain);   ///< シグモイド関数
  static double gaussian_2D(double x, double x_c=0.0,double gain=1.0, 
			           double sigma=1.0);  ///< ガウス関数(2D)
  static double gaussian_3D(double x, double y, double x_c=0.0, double y_c=0.0,
		  double gain=1.0, double sigma=1.0);  ///< ガウス関数(3D)
  /// ガウス関数の和を計算する関数(2D)
  static double gaussian_2D_sum(double x_c, double x_start, double x_end, 
				double step, double sigma, bool mode=false);  
  /// ガウス関数の和を計算する関数(3D)
  static double gaussian_3D_sum(double x_c, double y_c, 
                 double x_start, double x_end, double y_start, double y_end, 
		double x_step, double y_step, double sigma, bool mode=false);
  /// DoG(Differntial of Gaussian)を計算する関数(2D)
  static double diff_of_gaussian_2D(double x, double x_c,
		    double gain1, double gain2, double sigma1, double sigma2);
  /// DoG(Differential of Gaussian)を計算する関数(3D)
  static double diff_of_gaussian_3D(double x, double y, 
			 double x_c, double y_c, double gain1, double gain2, 
				                double sigma1, double sigma2);
  /// DoG(Differential of Gaussian)を計算しSimData型に格納する関数(3D)
  static void simdata_from_DoG(Nistk::SimData *i_data, 
			 double x_c, double y_c, double x_step, double y_step,
                   double gain1, double gain2, double sigma1, double sigma2, 
				                             bool mode=false);
  /// DoG(Differential of Gaussian)を計算しSimData型に格納する関数2(3D)
  static void simdata_from_DoG2(Nistk::SimData *i_data, 
			 double x_c, double y_c, double x_start, double x_end,
		   double y_start, double y_end, double x_step, double y_step,
                   double gain1, double gain2, double sigma1, double sigma2, 
				                             bool mode=false);
  /// 楕円形ガウス関数(3D)
  static double e_gaussian(double x, double y, 
		    double x_c, double y_c, double gain, double sigma1,
		    double simga2, double angle);
  /// 楕円形Gaussianを計算しSimData型に格納する関数(3D)
  static void simdata_from_e_gaussian(Nistk::SimData *i_data,
			       double x_c, double y_c,
			       double x_step, double y_step,
			       double gain, double sigma1, double sigma2, 
			       double angle, const char *op);
  /// SimDataに格納されている値を初期化する関数
  static void init_simdata(Nistk::SimData *i_data, double i_val);
  /// SimDataに格納されている値を正規化する関数
  static void normalize_simdata(Nistk::SimData *i_data, bool mode=false);
  /// SimData型のデータ同士を演算してSimData型に格納する関数
  static void calc_simdata(Nistk::SimData *o_data, Nistk::SimData *i_data1,
		    Nistk::SimData *i_data2, const char *op);
  /// 半径rのDoG(Differential of Gaussian)を計算しSimData型に格納する関数(3D)
  static void simdata_from_DoG_r(Nistk::SimData *i_data,
				 double x_c, double y_c, 
				 double x_step, double y_step,
				 double gain1, double gain2, 
				 double sigma1, double sigma2,
				 double r, bool mode);

  /// 半径rのガウス関数の和を計算する関数(3D)
  static double gaussian_3D_sum_r(double x_c, double y_c,
				  double x_start, double x_end, 
				  double y_start, double y_end,
				  double x_step, double y_step, 
				  double sigma, double r,
				  bool mode);
};

}

#endif // NISTK_MATHTOOLS_H
