/**
 * @file  hhneuron.h
 * @brief Class for Hodgikin-Huxley model
 *
 * @author Masakazu Komori
 * @date 2011-08-01
 * @version $Id: hhneuron.h,v.20110801 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef HHNEURON_H
#define HHNEURON_H

#include<nistk/hhparameter.h>
#include<nistk/hhvariable.h>

namespace Nistk{
namespace Neuron{
/** Hodgikin-Huxley modelの計算用関数を集めたクラス
 *
 * Hodgikin-Huxley modelの計算用関数を集めたクラス。
 * このクラスのメンバ関数は全て静的関数となっているのでオブジェクトを
 * 生成する必要はない。また、個別の関数を利用することは可能であるが、
 * 膜電位の計算をする際には、関数calcを利用するだけで良い。
 * また、新たなイオン電流の項を加える場合は、このクラスと関連クラスを継承し新たに
 * にクラスを作成するか、追加の電流の計算用関数を書き、関数calc,calc_vを
 * 新しく作成すればよいと思われる。<br>
 * <br>
 * \image html HHmodelFig.png
 * \image latex HHmodelFig.eps "Hodgikin-Huxley model"
 *   まずは、Hodgikin-Huxley modelの一般的説明について述べる。<br>
 * 元はHodgikin-Huxleyによるヤリイカの巨大軸索の軸索上丘での活動
 * 電位の発生をモデル化したもの。実際の神経細胞のモデルとしてよく
 * 用いられる。その式を以下に示す。<br>
 * 膜電位の計算式:
 \f[
      C_m\frac{dV}{dt} = \bar{G}_{Na}m^3h(E_{Na} -V) 
                       + \bar{G}_Kn^4(E_K -V) + \bar{G}_m(V_L -V) + I_{inj}(t) 
 \f]
 * 各パラメータの単位(V_{rest}は静止膜電位)
 \f[
      C_m[\mu F/cm^2],\bar{G}_{Na}[mS/cm^2],\bar{G}_K[mS/cm^2],\bar{G}_m[mS/cm^2]
 \f]
 \f[
      V[mV],E_{Na}[mV],E_K[mV],V_L=(-G_{Na}(0)E_{Na}-G_K(0)E_K)/G_m
 \f]
 * 注入電流(実際の使用の際は単位面積あたりという形では使わない)
 \f[
      I_{inj}(t)[\mu A / cm^2]
 \f]
 * イオンチャネルの開口状態(開口確率)を表す成分(単位なし):
 \f[
      \frac{dm}{dt} = \alpha _m(V)(1-m) - \beta _m(V)m
 \f]
 \f[
      \frac{dh}{dt} = \alpha _h(V)(1-h) - \beta _h(V)h
 \f]
 \f[
      \frac{dn}{dt} = \alpha _n(V)(1-n) - \beta _n(V)n
 \f]
 * イオンチャネルの電圧依存係数(定数,単位:1/msec)
 \f[
      \alpha _m(V) = \frac{25-V}{10(e^{(25-V)/10}-1)}
 \f]
 \f[
      \beta _m(V) = 4e^{-V/18}
 \f]
 \f[
      \alpha _h(V) = 0.07e^{-V/20}
 \f]
 \f[
      \beta _h(V) = \frac{1}{e^{(30-V)/10}+1}
 \f]
 \f[
      \alpha _n(V) = \frac{10-V}{100(e^{(10-V)/10}-1)}
 \f]
 \f[
      \beta _n(V) = 0.125e^{-V/80}
 \f]
 * ただし、上式は軸索の細胞膜のパッチ(単位面積あたり)の電流を表す式であることに
 * に注意する必要がある。実際に使用する際には、各コンダクタンスやキャパシタンスを
 * 細胞膜の面積にあわせてやる必要がある。膜電位、\f$Na^+\f$、\f$K^+\f$の
 * 反転電位やリーク電流の反転電位は静止膜電位が\f$0[mV]\f$の場合となっている。
 * よって、静止膜電位が\f$0[mV]\f$でない場合はそれぞれの電位を\f$+V_{rest}\f$
 * してやる必要がある。ただし、\f$\alpha_m(V)\f$,\f$\beta_m(V)\f$,\f$\alpha_h(V)\f$,
 * \f$\beta_h(V)\f$,\f$\alpha_n(V)\f$,\f$\beta_n(V)\f$は、静止膜電位からの
 * 相対電位(つまり、静止膜電位を0[mV]としてかんがえる)でないといけないので注意が
 * 必要となる。それぞれの、\f$\alpha \f$,\f$\beta \f$は、単位が元々
 * msecであるので、\f$m\f$,\f$h\f$,\f$n\f$が単位なしになるためには、
 * \f$dt\f$の単位はmsecであるので、注意が必要である。<br>
 *   また、\f$\alpha\f$、\f$\beta\f$は6.3[℃]の時のものであり、
 * これらは温度に依存し、10[℃]上昇するごとに3倍の値を取る。よって、任意の
 * 温度\f$T\f$において、全ての\f$\alpha\f$、\f$\beta\f$に
 \f[
       3^{(T-6.3)/10}
 \f]
 * を乗じてやる必要がある。<br>
 * <br>
 *   次に、このクラスでの設定について述べておく。いろいろなパラメータに
 * 単位系変更等による変更に対する拡張性を持たせるために、
 * このクラスでは以下のようにすることにする。
 * まず、イオンチャネルの電圧依存係数\f$\alpha\f$、\f$\beta\f$の式は、
 * 次のように定数も変数で表すことにする。<br>
  \f[
      \alpha _m(V) = \frac{am_1-V}{am_2(e^{(am_3-V)/am_4}-am_5)}
 \f]
 \f[
      \beta _m(V) = bm_1e^{-V/bm_2}
 \f]
 \f[
      \alpha _h(V) = ah_1e^{-V/ah_2}
 \f]
 \f[
      \beta _h(V) = \frac{bh_1}{e^{(bh_2-V)/bh_3}+bh_4}
 \f]
 \f[
      \alpha _n(V) = \frac{an_1-V}{an_2(e^{(an_3-V)/an_4}-an_5)}
 \f]
 \f[
      \beta _n(V) = bn_1e^{-V/bn_2}
 \f]
 * 変数値:\f$am_1=25\f$,\f$am_2=10\f$,\f$am_3=25\f$,\f$am_4=10\f$,\f$am_5=1\f$
 * ,\f$bm_1=4\f$,\f$bm_2=18\f$,\f$ah_1=0.07\f$,\f$ah_2=20\f$
 * ,\f$bh_1=1\f$,\f$bh_2=30\f$,\f$bh_3=10\f$,\f$bh_4=1\f$
 * ,\f$an_1=10\f$,\f$an_2=100\f$,\f$an_3=10\f$,\f$an_4=10\f$,\f$an_5=1\f$
 * ,\f$bn_1=0.125\f$,\f$bn_2=80\f$ <br>
 * 先に述べたように、上式で計算される値は6.3[℃]の時のものであるので、
 * このクラスでは設定温度Tにより計算されたT_scaleを掛け合わせることにする。<br>
 *   これも先に述べたように、それぞれの、\f$\alpha \f$,\f$\beta \f$は、単位が元々
 * msecであるので、\f$m\f$,\f$h\f$,\f$n\f$が単位なしになるためには、
 * \f$dt\f$の単位はmsecであるので、単位をsecに変更が可能となるように
 * \f$m\f$,\f$h\f$,\f$n\f$の式を次のようにする。<br>
 \f[
      \frac{dm}{dt} = (\alpha _m(V)(1-m) - \beta _m(V)m) * mhn\_scale
 \f]
 \f[
      \frac{dh}{dt} = (\alpha _h(V)(1-h) - \beta _h(V)h) * mhn\_scale
 \f]
 \f[
      \frac{dn}{dt} = (\alpha _n(V)(1-n) - \beta _n(V)n) * mhn\_scale
 \f]
 * mhn_scaleの初期値は1である。
 *   次に、それぞれのイオンチャネル等の最大コンダクタンスは、単位あたりの
 * の値とシミュレーションしようとするニューロンの面積等をかける形で考える。<br>
 * 単位あたりの最大コンダクタンス:<br>
 \f[
      C_m[\mu F/cm^2],\bar{G}_{Na}[mS/cm^2],\bar{G}_K[mS/cm^2],\bar{G}_m[mS/cm^2]
 \f]
 * これにarea_size(単位[\f$cm^2\f$])をかけて使うことになる。<br>
 *   以上より、電圧電流および時間の単位は、電圧[mV],電流[uA],時間[ms]となる。
 * 各パラメータの単位はそのように定義してあるだけなので、MKS単位系にするには、
 * 各数値に対して単位変換してやる必要がある。モデルを電圧[V],電流[A],長さ[m]
 * ,時間[s],容量[F],コンダクタンス[S]に変換してやるには、次のような
 * 単位変換が必要である。まず、パラメータについては、まず
 * area_sizeを元の値の\f$10^{-4}\f$倍に変更し，<br>
 \f[
      C_m[\mu F/cm^2] \rightarrow C_m \times 10^{-2}[F/m^2]
 \f]
 \f[
      \bar{G}_{Na}[mS/cm^2] \rightarrow \bar{G}_{Na} \times 10[S/m^2]
 \f]
 \f[ 
      \bar{G}_K[mS/cm^2] \rightarrow \bar{G}_{K} \times 10[S/m^2]
 \f]
 \f[
      \bar{G}_m[mS/cm^2] \rightarrow \bar{G}_m \times 10[S/m^2]
 \f]
 * と元のパラメータを変換する必要がある。反転電位\f$E_{Na}\f$,\f$E_{K}\f$,
 * \f$V_L\f$に関しては、元の値を\f$10^{-3}\f$倍する必要がある。
 * 次に，それぞれの、\f$\alpha \f$,\f$\beta \f$,\f$m\f$,\f$h\f$,\f$n\f$
 * に関しては、mhn_scaleを1000に変更し、\f$am_1\f$,\f$am_2\f$,\f$am_3\f$
 * \f$am_4\f$,\f$bm_2\f$,\f$ah_2\f$,\f$bh_2\f$,\f$bh_3\f$,\f$an_1\f$,
 * \f$an_2\f$,\f$an_3\f$,\f$an_4\f$,\f$bn_2\f$をそれぞれ\f$10^{-3}\f$
 * 倍してやる必要がある。\f$\alpha \f$,\f$\beta \f$,\f$m\f$,\f$h\f$,\f$n\f$
 * の変換に関しては、
 * Nistk::Neuron::HHNeuron::convert_mhn_to_MSAV関数
 * を利用することにより変換が可能となっている。この関数を利用する際には
 * Nistk::Neuron::HHNeuron::init関数の前に利用する必要がある。<br>
 *   また、このクラスの膜電位計算に関しては、静止膜電位(\f$V_{rest}\f$)に
 * 対する相対電位(つまり静止膜電位を0mV
 * とみなす)で計算し、\f$+V_{rest}\f$して元に戻すことにする。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class HHNeuron
{
 public:
  /// Nistk::Neuron::HHVariable,Nistk::Neuron::HHParameterの初期化用関数
  static void init(Nistk::Neuron::HHVariable *hhv, 
		   Nistk::Neuron::HHParameter *hhp);

  /// Nistk::Neuron::HHParameterのm,h,n,α,βのMSAV単位への変換
  static void convert_mhn_to_MSAV(Nistk::Neuron::HHParameter *hhp);

  /// Nistk::Temp::Runge4SiによるHodgikin-Huxley modelを計算する関数
  static void calc(double dt, Nistk::Neuron::HHVariable *hhv, 
		   Nistk::Neuron::HHParameter *hhp);

  /// 膜電位Vの微分値計算用関数
  static double calc_v(double dt, double *val, 
		       Nistk::Neuron::HHVariable *hhv, 
		       Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの開口状態(開口確率)を表す成分mの微分値Si用関数
  static double calc_m_si(double dt, double *val, 
			  Nistk::Neuron::HHVariable *hhv, 
			  Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの開口状態(開口確率)を表す成分hの微分値Si用関数
  static double calc_h_si(double dt, double *val, 
			  Nistk::Neuron::HHVariable *hhv, 
			  Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの開口状態(開口確率)を表す成分nの微分値Si用関数
  static double calc_n_si(double dt, double *val, 
			  Nistk::Neuron::HHVariable *hhv, 
			  Nistk::Neuron::HHParameter *hhp);

  /// イオンチャネルの開口状態(開口確率)を表す成分mの微分値計算用関数
  static double calc_m(double dt, double m, double v, 
		       Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの開口状態(開口確率)を表す成分hの微分値計算用関数
  static double calc_h(double dt, double h, double v, 
		       Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの開口状態(開口確率)を表す成分nの微分値計算用関数
  static double calc_n(double dt, double n, double v, 
		       Nistk::Neuron::HHParameter *hhp);

  /// イオンチャネルの電圧依存係数α_mを計算する関数
  static double calc_alpha_m(double v, Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの電圧依存係数β_mを計算する関数
  static double calc_beta_m(double v, Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの電圧依存係数α_hを計算する関数
  static double calc_alpha_h(double v, Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの電圧依存係数β_hを計算する関数
  static double calc_beta_h(double v, Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの電圧依存係数α_nを計算する関数
  static double calc_alpha_n(double v, Nistk::Neuron::HHParameter *hhp);
  /// イオンチャネルの電圧依存係数β_nを計算する関数
  static double calc_beta_n(double v, Nistk::Neuron::HHParameter *hhp);
};

}
}

#endif //HHNEURON_H
