/**
 * @file  data_io.h
 * @brief class for data input-output of simulation
 *
 * @author Masakazu Komori
 * @date 2012-09-07
 * @version $Id: data_io.h,v.20120907 $
 *
 * Copyright (C) 2008-2012 Masakazu Komori
 */

#ifndef NISTK_DATA_IO_H
#define NISTK_DATA_IO_H

#include<fstream>
#include<simdata.h>

namespace Nistk{
/** シミュレーションにおけるデータの入出力に関するクラス
 *
 * このクラスは、gnuplotのプロット用データを出力するクラス
 * DataFileOutやシミュレーション用の入力を作成する関数を集めたクラスInput
 * とは異なり、シミュレーションで使用しているSimDataのオブジェクト等
 * のデータの保存、読み込みなどのデータの入出力に関する関数を集めた
 * クラスである。って、このクラスのメンバはすべてstaticであるので、
 * オブジェクトを 生成する必要はない。
 *
 * Copyright (C) 2012 Masakazu Komori
 */
class DataIO
{
 public:
  /// SimData型のオブジェクトに格納されているデータをファイルに出力する関数
  static bool save_simdata(Nistk::SimData *src, char *filename);
  /// 関数save_simdataで保存したデータをSimData型のオブジェクトに格納する関数
  static bool load_simdata(Nistk::SimData *dest, char *filename);
  /// SimDataArray型のオブジェクトに格納されているデータをファイルに出力する関数
  static bool save_array(Nistk::SimDataArray *src, char *filename);
  /// 関数save_arrayで保存したデータをSimDataArray型のオブジェクトに格納する関数
  static bool load_array(Nistk::SimDataArray *dest, char *filename);
};

}

#endif // NISTK_DATA_IO_H
