/**
 * @file  ivparameter.h
 * @brief Class for Parameter of Izhikevich's Simple Spiking Neuron
 *
 * @author Masakazu Komori
 * @date 2011-08-05
 * @version $Id: ivparameter.h,v.20110805 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#ifndef IVPARAMETER_H
#define IVPARAMETER_H

namespace Nistk{
namespace Neuron{
/** IzhikevichのSimple Spiking Neuron modelのためのパラメータクラス
 *
 * IzhikevichのSimple Spiking Neuron modelのためのパラメータクラスで、
 * モデルを次のようなパラメータで表した際のメンバをもっている。
 \f[
        \frac{dv}{dt} = (vp\_1 * v^2 + vp\_2 * v + vp\_3 
                            - vp\_4 * u + vp\_5 + I) / vp\_6
 \f]
 \f[
        \frac{du}{dt} = a * (b * (v + up\_1) - up\_2 * u)
 \f]
 \f[
  if\,\,\, v \geq v\_rest[mV] \,\,\,then \,\,\,\left\{
                                  \begin{array}{ll}
                                     v & \leftarrow vp\_7 * v + c\\
                                     u & \leftarrow up\_3 * u + d
                                  \end{array}
                                 \right.
 \f]
 * 初期値は、次の通りである。
 \f[
         a = 0.02, b = 0.2, c = -65[mV], d = 2, v\_rest = 30[mV]
 \f]
 \f[
         vp\_1 = 0.04, vp\_2 = 5, vp\_3 = 140, vp\_4 = 1.0
 \f]
 \f[
         vp\_5 = 0.0, vp\_6 = 1.0, vp\_7 = 0.0
 \f]
 \f[
         up\_1 = 0.0, up\_2 = 1.0, up\_3 = 1.0
 \f]
 * このクラスのメンバ変数は、手法としては良くないが操作の煩雑さを
 * 減らすために全てpublicとする。つまり、アクセッサなどを使わない。詳しいことは、
 * Nistk::Neuron::IVNeuronを参照のこと。
 *
 * Copyright (C) 2011 Masakazu Komori
 */
class IVParameter
{
 public:
  double a;         ///< Simple Spiking Neuron modelのパラメータa (初期値:0.02)
  double b;         ///< Simple Spiking Neuron modelのパラメータb (初期値:0.2)
  double c;         ///< Simple Spiking Neuron modelのパラメータc (初期値:-65[mV])
  double d;         ///< Simple Spiking Neuron modelのパラメータd (初期値:2)
  double v_rest;    ///< 膜電位リセットがかかる電位v_rest (初期値:30[mV])
  double vp_1;      ///< 膜電位vに関する式のパラメータvp_1 (初期値:0.04)
  double vp_2;      ///< 膜電位vに関する式のパラメータvp_2 (初期値:5.0)
  double vp_3;      ///< 膜電位vに関する式のパラメータvp_3 (初期値:140)
  double vp_4;      ///< 膜電位vに関する式のパラメータvp_4 (初期値:1.0)
  double vp_5;      ///< 膜電位vに関する式のパラメータvp_5 (初期値:0.0)
  double vp_6;      ///< 膜電位vに関する式のパラメータvp_6 (初期値:1.0)
  double vp_7;      ///< 膜電位vに関する式のパラメータvp_7 (初期値:0.0)
  double up_1;      ///< リカバリ変数に関する式のパラメータup_1 (初期値:0.0)
  double up_2;      ///< リカバリ変数に関する式のパラメータup_2 (初期値:1.0)
  double up_3;      ///< リカバリ変数に関する式のパラメータup_3 (初期値:1.0)
  IVParameter();    ///< コンストラクタ
  ~IVParameter();    ///< デストラクタ
};

}
}

#endif //IVPARAMETER_H
