/**
 * @file  randgene.h
 * @brief class for random generator using stdlib
 *
 * @author Masakazu Komori
 * @date 2009-01-06
 * @version $Id: randgene.h,v.20090106 $
 *
 * Copyright (C) 2009 Masakazu Komori
 */

#ifndef NISTK_RANDGENE_H
#define NISTK_RANDGENE_H
#include<cstdlib>
#include<ctime>

namespace Nistk{
/** ランダムジェネレータ(stdlib版)
 *
 * ランダムジェネレータ。このクラスのオブジェクトを生成
 * すれば,オブジェクト一つでいろいろな乱数を得ることができる。
 * ただし,C/C++標準のrand関数を使っているので乱数としては
 * よくないので,メルセンヌツイスタ法による乱数の方がよい。
 *
 * Copyright (C) 2009 Masakazu Komori
 */
class RandGene
{
 public:
  RandGene();          ///< コンストラクタ
  ~RandGene();         ///< デストラクタ
  int get_rand();      ///< ノーマルの乱数(範囲はコンパイラ次第)
   /// startからendまでの範囲の乱数を発生させる
  int get_rand_i(int start, int end, int shift);
  double get_rand_d(); ///< 0から1までの実数型乱数の発生
};

}
#endif // NISTK_RANDGENE_H
