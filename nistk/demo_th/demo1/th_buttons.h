#ifndef TH_BUTTONS_H
#define TH_BUTTONS_H

#include<gtkmm.h>
#include"th_buttons_base.h"
#include<nistk_thread.h>

/*
  ThButtonBaseクラスをメンバとして持ち、NistkThreadクラスを
  継承してウィンドウをthreadとして走らせることが出来るように
  するクラス
 */
class ThButtons :public Nistk::NistkThread
{
 protected:
  ThButtonsBase m_buttons;        // ThButtonBaseクラスのオブジェクト

 public:
  ThButtons();                    // コンストラクタ
  ~ThButtons();                   // デストラクタ
  void nistk_slot_main();         // スレッドのスロット関数でのmain関数
  void send_th_end();             // ウィンドウ終了フラグをセットする関数
  void set_title(char *m_title);  // ボタンウィンドウのタイトルをセットする関数
  void set_num(int num);          // ウィンドウ番号をセットする関数
};

#endif // TH_BUTTONS_H
