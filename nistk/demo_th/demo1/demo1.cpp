#include<gtkmm.h>
#include"th_buttons.h"
#include<iostream>

int main(int argc, char *argv[])
{
  // スレッドシステムの初期化
  Glib::thread_init();
  // Gtk::Mainの初期化
  Gtk::Main kit(argc,argv);
  // thread対応ウィンドウオブジェクトを生成
  ThButtons m_buttons1,m_buttons2;

  // オブジェクトの初期設定
  m_buttons1.set_thread_id(1);
  m_buttons1.set_num(1);
  m_buttons2.set_thread_id(2);
  m_buttons2.set_num(2);
  m_buttons1.set_title("button 1");
  m_buttons2.set_title("button 2");

  // threadを実行 ボタンウィンドウ２つが現れる
  m_buttons1.thread_run();
  m_buttons2.thread_run();

  // threadと並行して以下を実行
  for(int i=0;i<10;i++){
    std::cout << " i =  " << i  << std::endl;
    sleep(1);
  }   

  // ウィンドウ終了メッセージを送ってボタンウィンドウ２つを閉じる
  sleep(1);
  m_buttons2.send_th_end();
  std::cout << "m_buttons2 end \n";
  m_buttons1.send_th_end();
  std::cout << "m_buttons1 end \n";
  m_buttons2.thread_join();           // joinは必ず必要
  m_buttons1.thread_join();           // joinは必ず必要

  // もう一度threadを実行
  m_buttons1.thread_run();
  m_buttons2.thread_run();

  // 今度はボタンウィンドウがマウスで閉じられるのを待つ
  m_buttons2.thread_join();
  m_buttons1.thread_join();

  return 0;
}
