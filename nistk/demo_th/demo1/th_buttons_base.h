#ifndef TH_BUTTONS_BASE_H
#define TH_BUTTONS_BASE_H

#include<gtkmm.h>
#include"buttons.h"
#include<th_window_base.h>

/*
  ベースとなるボタンウィンドウクラスをthreadに対応できるベースクラスを
  作る為のクラス。よって、Buttons,Nistk::ThWindowBaseの２つのクラス
  を継承する。
 */
class ThButtonsBase : public Buttons, public Nistk::ThWindowBase
{
 public:
  ThButtonsBase();                     // コンストラクタ 
  ~ThButtonsBase();                    // デストラクタ
  bool on_delete_event(GdkEventAny*);  // xボタンが押されたときの処理関数
  void time_out_win_func();            // タイムアウト処理用関数
  void close_window();                 // ウィンドウ終了関数
};

#endif // TH_BUTTONS_BASE_H
