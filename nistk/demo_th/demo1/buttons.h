#ifndef GTKMM_EXAMPLE_BUTTONS_H
#define GTKMM_EXAMPLE_BUTTONS_H

#include<gtkmm/window.h>
#include<gtkmm/button.h>
#include<gtkmm/box.h>

/*
  ベースとなるボタン２つで構成されるウィンドウクラス
 */

class Buttons :public Gtk::Window      // Gtk::Windowクラスを継承
{
 public:
  Buttons();                           // コンストラクタ
  virtual ~Buttons();                  // デストラクタ
  void button_close();                 // ボタンウィンドウ終了関数
  void set_button_num(int num);        // ボタンウィンドウに番号を付ける関数

 protected:
  int button_num;                      // ボタンウィンドウ番号用変数
  // Signal handlers:
  virtual void on_button_clicked();    // もう一つのボタンが押されたときの処理関数
  virtual void on_button_close();      // closeボタンが押されたときの処理関数

  // Child widgets:
  Gtk::Button m_button,m_button_close; // ボタンウィジット
  Gtk::HBox m_hbox;                    // ボタン収納用ボックスウィジット
};

#endif // GTKMM_EXAMPLE_BUTTONS_H

