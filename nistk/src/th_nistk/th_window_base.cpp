/**
 * @file  th_window_base.cpp
 * @brief class for make thread-based window class（実装）
 *
 * @author Masakazu Komori
 * @date 2010-08-12
 * @version $Id: th_window_base.cpp,v.20100812 $
 *
 * Copyright (C) 2010 Masakazu Komori
 */

#include<th_window_base.h>

/** コンストラクタ
 */
Nistk::ThWindowBase::ThWindowBase()
{
  is_window_close = false;
  // タイムアウトコネクタと接続
  conn_close = Glib::signal_timeout().connect(sigc::mem_fun(*this,
	       &Nistk::ThWindowBase::slot_window_func), NISTK_THREAD_TIMEOUT);
}

/** デストラクタ
 */
Nistk::ThWindowBase::~ThWindowBase()
{
}

/** タイムアウト用スロット関数
 * 
 * 一定の時間毎に処理が行われる関数。ウィンドウ関連はイベント処理型となるので
 * 強引に処理をするのではなくてこのような形態を取った。まず、ウィンドウ終了用
 * フラグのチェックを行い、trueであれば純粋仮想関数close_windowを呼出し、
 * ウィンドウを閉じる。そうでない場合は、純粋仮想関数time_out_win_func()
 * を呼出している。<br>
 * タイムアウト処理とのコネクションを維持するには
 * 常にtureを返す必要がある。逆にコネクションを切る
 * にはfalseを返せばよいので常にtureを返している。
 *
 * @return true
 */
bool Nistk::ThWindowBase::slot_window_func()
{
  if(is_window_close == true){
    is_window_close = false;
    close_window();
  }
  time_out_win_func();
  
  return true;
}

/** ウィンドウ終了フラグセット用関数
 *
 * ウィンドウ終了フラグセット用関数。あくまでフラグセットのみ
 * であるので、この時点ではウィンドウ終了はしない。
 */
void Nistk::ThWindowBase::set_window_close()
{
  is_window_close = true;

  return;
}



