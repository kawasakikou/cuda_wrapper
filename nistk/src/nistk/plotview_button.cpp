/**
 * @file  plotview_button.cpp
 * @brief class for plotview buttonused by nistk main window(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: plotview_button.cpp,v.20110809 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<nistk/plotview_button.h>
#include<gtkmm.h>

/** コンストラクタ
 */
Nistk::PlotViewButton::PlotViewButton()
{

}

/** デストラクタ
 */
Nistk::PlotViewButton::~PlotViewButton()
{
  window.hide();
}

/** クリックされた時の処理(オーバーライド)
 *
 * ボタンがクリックされたときの処理をする関数。親クラスである
 * Gtk::Buttonのものをオーバーライドしている。これにより、
 * ボタンがクリックされたときにPlotViewを表示することが可能に
 * なっている。
 * 
 */
void Nistk::PlotViewButton::on_clicked()
{
  Gtk::Button::on_clicked(); // 親クラスのメソッドの呼び出し
  if(window.is_window_show() == false){
    window.show();
    window.set_show_window(true);
  } else {
    window.hide();
    window.set_show_window(false);
  }
}

/** ボタンとウィンドウの名前をセットする関数
 *
 * ボタンとボタンと連動しているPlotViewウィンドウの名前をセットする関数。
 * 
 * @param name セットする名前のchar型ポインタ
 */
void Nistk::PlotViewButton::set_name(const char *name)
{
  window.set_name(name);
  set_label(name);
}

/** PlotViewのポインタを取得する関数
 *
 * このボタンが持っているPlotViewのポインタを取得する関数。
 * これにより、このボタンが持っているPlotViewを通常
 * のものと同じように扱うことが出来る。
 *
 * @return PlotView型のポインタ
 */
Nistk::PlotView* Nistk::PlotViewButton::get_plotview_ptr()
{
  return &window;
}
