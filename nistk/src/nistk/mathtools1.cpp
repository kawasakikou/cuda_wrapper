/**
 * @file  mathtools1.cpp
 * @brief class for math tools(実装)
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: mathtools1.cpp,v.20130315 $
 *
 * Copyright (C) 2008-2013 Masakazu Komori
 */
#include<cmath>
#include<simdata.h>
#include<mathtools.h>

/** しきい値関数(ステップ関数)
 *
 * しきい値関数(ステップ関数)である以下の式を計算して返す関数。
 * 値は,0.0もしくは1.0になるので注意すること。<br>
 \f[
    f(x)=\left\{
      \begin{array}{ll}
        0 & x<0 \\
        1 & x\geq 0
      \end{array}
      \right.
 \f] 
 *
 * @param x 入力
 * @return 関数値
 */
double Nistk::MathTools::step_func(double x)
{
  if(x < 0) return 0.0;
  else return 1.0;
}

/** シグモイド関数
 *
 * シグモイド関数である以下の式を計算して返す関数。
 * 値は,0.0から1.0の間になる。<br>
 \f[
    f(x)=\frac{1}{1+exp^{-\alpha x}}
 \f] 
 *
 * @param x 入力
 * @param gain ゲイン(\f$\alpha \f$)
 * @return 関数値
 */
double Nistk::MathTools::sigmoid_func(double x, double gain)
{
  return 1/(1+exp(-1.0*gain*x));
}

/** ガウス関数(2D)
 *
 * １変数のガウス関数(グラフを書くと2D)である以下の式を計算して返す関数。
 * ゲインであるaを指定できるようにしてある。<br>
 \f[
    f(x)=a\, exp^{-\frac{(x-x_c)^2}{\sigma ^2}}
 \f] 
 *
 * @param x 入力
 * @param x_c ガウス関数の中心位置
 * @param gain ゲイン(a)
 * @param sigma 幅(\f$\sigma \f$)
 * @return 関数値
 */
double Nistk::MathTools::gaussian_2D(double x, double x_c,
					    double gain, double sigma)
{
  return gain*exp(-1.0*(((x-x_c)*(x-x_c))/(sigma*sigma)));
}

/** ガウス関数(3D)
 *
 * 2変数のガウス関数(グラフを書くと3D)である以下の式を計算して返す関数。
 * ゲインであるaを指定できるようにしてある。<br>
 \f[
    f(x)=a\, exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma ^2}}
 \f] 
 *
 * @param x 入力(x軸)
 * @param y 入力(y軸)
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param gain ゲイン(a)
 * @param sigma 幅(\f$\sigma \f$)
 * @return 関数値
 */
double Nistk::MathTools::gaussian_3D(double x, double y, 
                      double x_c, double y_c, double gain, double sigma)
{
  return gain*exp(-1.0*(((x-x_c)*(x-x_c)+(y-y_c)*(y-y_c))/(sigma*sigma)));
}

/** ガウス関数の和を計算する関数(2D)
 *
 * 以下の様な１変数のガウス関数(グラフを書くと2D)の和を計算して返す関数。
 * 和はx_startからx_endまでをstep刻みでガウス関数を計算した値となる。
 * 中心位置は必要ないが一応付けておく。
 * この和(total)計算し、gaussian_2Dのゲインに1/totalを指定することによって,
 * 正規化されたガウス関数の値を得ることができる。on中心,off中心の受容野
 * 計算に必要となる。<br>
 \f[
    total=\sum _{u=x\_ start}^{x\_ end}exp^{-\frac{(u-x_c)^2}{\sigma ^2}}
 \f] 
 * ただし、u(n+1)=u(n)+step <br>
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param x_c ガウス関数の中心位置
 * @param x_start 計算の開始位置
 * @param x_end 計算終了位置
 * @param step 刻み幅
 * @param sigma 幅(\f$\sigma \f$)
 * @param mode 計算モード
 * @return 合計値
 */
double Nistk::MathTools::gaussian_2D_sum(double x_c, double x_start, 
		         double x_end, double step, double sigma, bool mode)
{
  double total;   // ガウス関数の和
  double counter; // ループカウンタ
  double val;     // ガウス関数の計算値
  double diff;    // 積み残し
  double buf;     // 一時変数

  total = 0.0;
  diff = 0.0;
  for(counter = x_start; counter <= x_end; counter += step){
    val = gaussian_2D(counter, x_c, 1.0, sigma);
    if(mode == true){    // 情報落ちを補正する場合
      diff += val;         // 積み残し+加えたい数
      buf = total;         // 前回までの和をbufへ代入
      total += diff;       // 和を更新
      buf -= total;        // 実際に加算された数の符号を変えたもの
      diff += buf;         // 積み残し
    }
    else total += val;
  }

  return total;
}

/** ガウス関数の和を計算する関数(3D)
 *
 * 以下の様な2変数のガウス関数(グラフを書くと3D)の和を計算して返す関数。
 * 和はx_start,y_startからx_end,y_endまでをそれぞれx_step,y_step刻みで
 * ガウス関数を計算した値となる。中心位置は必要ないが一応付けておく。
 * この和(total)計算し、gaussian_3Dのゲインに1/totalを指定することによって,
 * 正規化されたガウス関数の値を得ることができる。on中心,off中心の受容野
 * 計算に必要となる。<br>
 \f[
    total=\sum _{v=y\_ start}^{y\_ end} \sum _{u=x\_ start}^{x\_ end} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma ^2}}
 \f] 
 * ただし、u(n+1)=u(n)+x_step,v(n+1)=v(n)+y_step <br>
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param x_start 計算の開始位置(x軸)
 * @param x_end 計算終了位置(x軸)
 * @param y_start 計算の開始位置(y軸)
 * @param y_end 計算終了位置(y軸)
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param sigma 幅(\f$\sigma \f$)
 * @param mode 計算モード
 * @return 合計値
 */
double Nistk::MathTools::gaussian_3D_sum(double x_c, double y_c,
                 double x_start, double x_end, double y_start, double y_end,
                       double x_step, double y_step, double sigma, bool mode)
{
  double total;   // ガウス関数の和
  double x_counter; // ループカウンタ(x軸）
  double y_counter; // ループカウンタ(y軸)
  double val;     // ガウス関数の計算値
  double diff;    // 積み残し
  double buf;     // 一時変数

  total = 0.0;
  diff = 0.0;
  for(y_counter = y_start; y_counter <= y_end; y_counter += y_step){
    for(x_counter = x_start; x_counter <= x_end; x_counter += x_step){
      val = gaussian_3D(x_counter, y_counter, x_c, y_c, 1.0, sigma);
      if(mode == true){    // 情報落ちを補正する場合
	diff += val;         // 積み残し+加えたい数
	buf = total;         // 前回までの和をbufへ代入
	total += diff;       // 和を更新
	buf -= total;        // 実際に加算された数の符号を変えたもの
	diff += buf;         // 積み残し
      }
      else total += val;
    }
  }

  return total;
}

/** DoG(Differntial of Gaussian)を計算する関数(2D)
 *
 * １変数のDoG(Differntial of Gaussian)(グラフを書くと2D)である
 * 以下の式を計算して返す関数。ゲインである\f$a_1\f$,\f$a_2\f$を指定
 *できるようにしてある。このゲインに２つのガウス関数の和
 * (\f$total_1\f$,\f$total_2\f$)の逆数である\f$1/total_1\f$,
 * \f$1/total_2\f$とそれぞれ指定すれば正規化されたDoGを得ることができる。
 * on,off中心の受容野をつくり出すために必要となる。<br>
 \f[
     f(x)=a1\, exp^{-\frac{(x-x_c)^2}{\sigma_1 ^2}} - a2\, exp^{-\frac{(x-x_c)^2}{\sigma_2 ^2}}
 \f] 
 *
 * @param x 入力
 * @param x_c ガウス関数の中心位置
 * @param gain1 ゲイン(\f$a_1\f$)
 * @param gain2 ゲイン(\f$a_2\f$)
 * @param sigma1 幅(\f$\sigma_1 \f$)
 * @param sigma2 幅(\f$\sigma_2 \f$)
 * @return 関数値
 */
double Nistk::MathTools::diff_of_gaussian_2D(double x, double x_c,
                    double gain1, double gain2, double sigma1, double sigma2)
{
  double val; // 記述が長くなりそうなので。。

  val = gaussian_2D(x, x_c, gain1, sigma1)-gaussian_2D(x, x_c, gain2, sigma2);
  return val;
}

/** DoG(Differential of Gaussian)を計算する関数(3D)
 *
 * 2変数のDoG(Differntial of Gaussian)(グラフを書くと3D)である
 * 以下の式を計算して返す関数。ゲインである\f$a1\f$,\f$a2\f$を指定できる
 * ようにしてある。このゲインに２つのガウス関数の和(\f$total_1\f$,
 * \f$total_2\f$)の逆数である\f$1/total_1\f$,\f$1/total_2\f$とそれぞれ
 * 指定すれば正規化されたDoGを得ることができる。
 * on,off中心の受容野をつくり出すために必要となる。<br>
 \f[
    f(x)=a_1\, exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_1 ^2}}-a_2\, exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_2 ^2}}
 \f] 
 *
 * @param x 入力(x軸)
 * @param y 入力(y軸)
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param gain1 ゲイン(\f$a_1\f$)
 * @param gain2 ゲイン(\f$a_2\f$)
 * @param sigma1 幅(\f$\sigma_1 \f$)
 * @param sigma2 幅(\f$\sigma_2 \f$)
 * @return 関数値
 */
double Nistk::MathTools::diff_of_gaussian_3D(double x, double y,
                         double x_c, double y_c, double gain1, double gain2,
                                                double sigma1, double sigma2)
{
  double val; // 記述が長くなりそうなので。。

  val = gaussian_3D(x, y, x_c, y_c, gain1, sigma1)
          - gaussian_3D(x, y, x_c, y_c, gain2, sigma2);
  return val;  
}

/** DoG(Differential of Gaussian)を計算しSimData型に格納する関数(3D)
 *
 * 2変数のDoG(Differntial of Gaussian)(グラフを書くと3D)である
 * 以下の式を計算してSimData型に格納する関数。ゲインであるa1,a2を
 * 指定できるようにしてある。原点は,SimData型の二次元配列の中心になる。
 * 単純に縦と横を2で割る(整数演算)だけなので,偶数のときは原点が
 * ずれることになるので注意が必要である。(例えば,7だと3,10だと5となる。
 * 配列は0からなので,奇数は中心,偶数は+1ずれる。)x_step,y_stepを1以外
 * の値にすると,計算では要素間の距離が1以外になる。(配列の添字は整数値で
 * 扱うことになるが。。）通常は,1で良いかと思う。計算範囲はSimData型の範囲
 * となる。
 * 
 * <br>
 \f[
    f(x)=a_1\, \frac{exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_1 ^2}}}{\sum _{v,u} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma_1 ^2}}} -a_2\, \frac{exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_2 ^2}}}{\sum _{v,u} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma_2 ^2}}}
 \f] 
 * ただし、u(n+1)=u(n)+x_step,v(n+1)=v(n)+y_step <br>
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param gain1 ゲイン(a1)
 * @param gain2 ゲイン(a2)
 * @param sigma1 幅(\f$\sigma 1 \f$)
 * @param sigma2 幅(\f$\sigma 2 \f$)
 * @param mode 計算モード
 */
void Nistk::MathTools::simdata_from_DoG(Nistk::SimData *i_data,
                         double x_c, double y_c, double x_step, double y_step,
		       double gain1, double gain2, double sigma1, double sigma2,
                                                                     bool mode)
{
  int col,row;             // 縦横のカウンタ
  int x_origin,y_origin;   // SimData型上での原点
  int width,height;        // SimData型上の幅と高さ
  double xd_start,yd_start;  // SimData型配列起点のでのx,y値
  double x_val,y_val;      // 計算上のx,yの値
  double total1,total2;    // ガウス関数の和
  double DoG_val;          // DoGの値

  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // SimData型の2次元配列上での原点を計算
  x_origin = width/2;
  y_origin = height/2;
  // 原点から見た左上角のx,y値
  xd_start = -(double)x_origin*x_step;
  yd_start = -(double)y_origin*y_step;
 
  // ２つのガウス関数の和を計算(SimDataのサイズを考慮して計算)
  total1 = gaussian_3D_sum(x_c, y_c, xd_start, xd_start+((width-1)*x_step), 
			   yd_start, yd_start+((height-1)*y_step),
			                  x_step, y_step, sigma1, mode);
  total2 = gaussian_3D_sum(x_c, y_c, xd_start, xd_start+((width-1)*x_step), 
			   yd_start, yd_start+((height-1)*y_step),
			                  x_step, y_step, sigma2, mode);

  // DoGを計算してSimData型に格納
  x_val = xd_start; // 計算開始点のセット
  y_val = yd_start;
  for(row=0; row<height; row++){ // 縦
    for(col=0; col<width; col++){ // 横
      DoG_val = diff_of_gaussian_3D(x_val, y_val, x_c, y_c, gain1/total1, 
		   	                      gain2/total2, sigma1, sigma2);
      i_data->put_data(col, row, DoG_val);
      x_val += x_step;
    }
    x_val = xd_start; // 計算点のセット
    y_val += y_step;
  }

  return;
}

/** DoG(Differential of Gaussian)を計算しSimData型に格納する関数2(3D)
 *
 * 2変数のDoG(Differntial of Gaussian)(グラフを書くと3D)である
 * 以下の式を計算してSimData型に格納する関数。
 * simdata_from_DoGとの違いは、計算上は,そのまま式を計算
 * するが配列の範囲外の値は無視される。つまり、それぞれのガウシアンを
 * 正規化する際に、式の通り計算する。SimDataの範囲を考慮せずに
 * 和をとったもので正規化されることになる。よって、SimData型の範囲を
 * 超えているとSimDataに格納された値の和をとってもほぼ0とならない。
 * 
 * <br>
 \f[
    f(x)=a_1\, \frac{exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_1 ^2}}}{\sum _{v=y\_ star
t}^{y\_ end} \sum _{u=x\_ start}^{x\_ end} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma_1 ^
2}}} -a_2\, \frac{exp^{-\frac{(x-x_c)^2+(y-y_c)^2}{\sigma_2 ^2}}}{\sum _{v=y\_ start}^
{y\_ end} \sum _{u=x\_ start}^{x\_ end} exp^{-\frac{(u-x_c)^2+(v-y_c)^2}{\sigma_2 ^2}}
}
 \f] 
 * ただし、u(n+1)=u(n)+x_step,v(n+1)=v(n)+y_step <br>
 * <br>
 * 計算モードはおまけで付けたもので,計算における情報落ちをある程度
 * 補正するものである。ここで使う計算ではたいした効果はないと思われる。
 * sを和,rを積み残し,\f$a_i\f$を加えたい数とすると,以下の様な計算をしている
 * だけである。<br>
 * r += \f$a_i\f$ 積み残し+加えたい数 <br>
 * t = s    前回までの和をtへ代入<br>
 * s += r   和を更新 <br>
 * t -= s   実際に加算された数の符号を変えたもの <br>
 * r += t   積み残し
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param x_c ガウス関数の中心位置(x軸)
 * @param y_c ガウス関数の中心位置(y軸)
 * @param x_start 計算の開始位置(x軸)
 * @param x_end 計算終了位置(x軸)
 * @param y_start 計算の開始位置(y軸)
 * @param y_end 計算終了位置(y軸)
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param gain1 ゲイン(a1)
 * @param gain2 ゲイン(a2)
 * @param sigma1 幅(\f$\sigma 1 \f$)
 * @param sigma2 幅(\f$\sigma 2 \f$)
 * @param mode 計算モード
 */
void Nistk::MathTools::simdata_from_DoG2(Nistk::SimData *i_data,
                         double x_c, double y_c, double x_start, double x_end,
                   double y_start, double y_end, double x_step, double y_step,
                   double gain1, double gain2, double sigma1, double sigma2,
                                                                     bool mode)
{
  int col,row;             // 縦横のカウンタ
  int x_origin,y_origin;   // SimData型上での原点
  int width,height;        // SimData型上の幅と高さ
  double xd_start,yd_start;  // SimData型配列起点のでのx,y値
  double x_val,y_val;      // 計算上のx,yの値
  double total1,total2;    // ガウス関数の和
  double DoG_val;          // DoGの値

  // ２つのガウス関数の和を計算
  total1 = gaussian_3D_sum(x_c, y_c, x_start, x_end, y_start, y_end,
			                  x_step, y_step, sigma1, mode);
  total2 = gaussian_3D_sum(x_c, y_c, x_start, x_end, y_start, y_end,
			                  x_step, y_step, sigma2, mode);
  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // SimData型の2次元配列上での原点を計算
  x_origin = width/2;
  y_origin = height/2;
  // 原点から見た左上角のx,y値
  xd_start = -(double)x_origin*x_step;
  yd_start = -(double)y_origin*y_step;

  // DoGを計算してSimData型に格納
  x_val = xd_start; // 計算開始点のセット
  y_val = yd_start;
  for(row=0; row<height; row++){ // 縦
    for(col=0; col<width; col++){ // 横
      if((x_val >= x_start) && (x_val <= x_end)
	 && (y_val >= y_start) && (y_val <= y_end)){
	DoG_val = diff_of_gaussian_3D(x_val, y_val, x_c, y_c, gain1/total1, 
				      gain2/total2, sigma1, sigma2);
	i_data->put_data(col, row, DoG_val);
      } else i_data->put_data(col, row, 0.0);
      x_val += x_step;
    }
    x_val = xd_start; // 計算点のセット
    y_val += y_step;
  }

  return;
}
