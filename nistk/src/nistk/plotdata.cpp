/**
 * @file  plotdata.cpp
 * @brief class for manage plotdata(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-10
 * @version $Id: plotdata.cpp,v.20110810 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<iostream>
#include<fstream>
#include<nistk/plotdata.h>

/** コンストラクタ
 */
Nistk::PlotData::PlotData()
{
  data_num = 0;
  increment_capacity = 100;
  x_data_max = 150.0;
  x_data_min = -150.0;
  y_data_max = 100.0;
  y_data_min = -100.0;
  auto_range = true;

}

/** デストラクタ
 */
Nistk::PlotData::~PlotData()
{
  delete_data_region();
}

/** プロット用データ領域の作成関数
 *
 * プロット用データ領域の作成をする関数。データ領域のサイズでないので
 * 注意すること。実際には、std::vectorクラスの確保を行っている。
 *
 * @param num 作成するデータ領域の数
 */
void Nistk::PlotData::create_data_region(int num)
{
  int i;

  if(data_num != 0) delete_data_region();
  data_num  = num;
  x_data = new std::vector<double>[num];
  y_data = new std::vector<double>[num];
  raster = new bool[num];
  raster_scale = new double[num];
  
  for(i=0; i < num; i++){
    x_data[i].reserve(10);
    y_data[i].reserve(10);
    raster[i] = false;
    raster_scale[i] = 0.005;
  }
  return;
}

/** プロット用データ領域の削除関数
 *
 * プロット用データ領域の削除関数。
 *
 */
void Nistk::PlotData::delete_data_region()
{
  if(data_num != 0){
    delete[] x_data;
    delete[] y_data;
    delete[] raster;
    delete[] raster_scale;
  }
}

/** プロット用データ領域の数を取得する関数
 *
 * プロット用データ領域の数を取得する関数
 *
 * @return プロット用データ領域の数
 */
int Nistk::PlotData::get_data_num()
{
  return data_num;
}

/** x,y軸の最大値、最小値をセットする関数
 *
 * x,y軸の最大値、最小値をセットする関数。
 * セットされるとオートレンジにならない。
 *
 * @param x_min x軸の最小値
 * @param x_max x軸の最大値
 * @param y_min y軸の最小値
 * @param y_max y軸の最大値
 */
void Nistk::PlotData::set_data_range(double x_min, double x_max, 
				     double y_min, double y_max)
{
  x_data_min = x_min;
  x_data_max = x_max;
  y_data_min = y_min;
  y_data_max = y_max;
  auto_range = false;

  return;
}

/** プロット領域のメモリ領域の増分をセットする関数
 *
 * プロット領域のメモリ領域の増分をセットする関数。
 * 関数push_back_dataでデータを末尾にセットする際に、確保している
 * データ領域が足りない場合、新たに再確保するがその際にどれだけ
 * 領域を増やすかを設定する。デフォルトは100となっている。
 * 値が大きいと再確保の回数が減りプログラムの実行時間が
 * 短くなると思われるが，その分メモリを大量に消費することになる。
 *
 * @param size 設定するメモリ領域の増分
 */
void Nistk::PlotData::set_increment_capacity(int size)
{
  increment_capacity = size;
  return;
}

/** オートレンジをセットする関数
 *
 * プロットの際のオートレンジをセットする関数。
 * デフォルトではオートレンジになっているが、
 * set_data_range関数を使うとレンジが固定になるので、
 * 再びオートレンジにするための関数。ただし、
 * オートレンジセット後のデータがオートレンジにする前のデータ
 * の範囲に入っている場合はプロットの範囲は変わらない。
 *
 */
void Nistk::PlotData::set_auto_range()
{
  auto_range = true;
  return;
}

/** num番目のデータ領域の末尾にデータをセットする関数
 *
 * num番目のデータ領域の末尾にデータをセットする関数。
 * セットする際に、確保しているデータ領域が足りない場合、
 * 新たに再確保する。その際にどれだけ領域を増やすか
 * はincrement_capacityによって決定される。この値に
 * よってプログラム全体の実行時間に関わる場合もある。
 * 関数set_increment_capacityを参照のこと。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param x x軸のデータ
 * @param y y軸のデータ
 */
void Nistk::PlotData::push_back_data(int num, double x, double y)
{
  // 格納領域のチェック 足りないと増加
  if(x_data[num].size() == x_data[num].capacity())
    x_data[num].reserve(x_data[num].size()+increment_capacity);
  if(y_data[num].size() == y_data[num].capacity())
    y_data[num].reserve(y_data[num].size()+increment_capacity);
  // auto_rangeがtrueの時、最初のデータの際に最大値と最小値をセット
  if((auto_range == true) && (x_data[num].size() == 0)){
      x_data_max = x;
      x_data_min = x;
      y_data_max = y;
      y_data_min = y;
  }
  // auto_rangeがtrueの時、x,yの最大値と最小値をチェック
  if((auto_range == true) && (data_num != 0)){
      if(x > x_data_max) x_data_max = x;
      if(x < x_data_min) x_data_min = x;
      if(y > y_data_max) y_data_max = y;
      if(y < y_data_min) y_data_min = y;
  }
  // dataの格納
  x_data[num].push_back(x);
  y_data[num].push_back(y);
  return;
}

/** num番目のデータ領域のindex番目のxのデータを取得する関数
 *
 * num番目のデータ領域のindex番目のxのデータを取得する関数。
 * 現在のデータが格納されているサイズ(キャパシティではない)を
 * こえることはできないので注意すること。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param index num番目のデータ領域のx軸データのindex番目
 * @return num番目のデータ領域のx軸データ
 */
double Nistk::PlotData::get_x(int num, int index)
{
  return x_data[num][index];
}

/** num番目のデータ領域のindex番目のyのデータを取得する関数
 *
 * num番目のデータ領域のindex番目のyのデータを取得する関数。
 * 現在のデータが格納されているサイズ(キャパシティではない)を
 * こえることはできないので注意すること。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param index num番目のデータ領域のy軸データのindex番目
 * @return num番目のデータ領域のy軸データ
 */
double Nistk::PlotData::get_y(int num, int index)
{
  return y_data[num][index];
}

/** num番目のデータ領域のindex番目のxにデータをセットする関数
 *
 * num番目のデータ領域のindex番目のxにデータをセットする関数。
 * 現在のデータが格納されているサイズ(キャパシティではない)を
 * 超えることはできないので注意すること。超えた時は何もしない。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param index num番目のデータ領域のx軸データのindex番目
 * @param data num番目のデータ領域のx軸データのindex番目のデータ
 */
void Nistk::PlotData::set_x(int num, int index, double data)
{
  if(index < x_data[num].size()) x_data[num][index]=data;

  return;
}

/** num番目のデータ領域のindex番目のyにデータをセットする関数
 *
 * num番目のデータ領域のindex番目のyにデータをセットする関数。
 * 現在のデータが格納されているサイズ(キャパシティではない)を
 * 超えることはできないので注意すること。超えた時は何もしない。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param index num番目のデータ領域のy軸データのindex番目
 * @param data num番目のデータ領域のy軸データのindex番目のデータ
 */
void Nistk::PlotData::set_y(int num, int index, double data)
{
  if(index < y_data[num].size()) y_data[num][index]=data;

  return;
}

/** num番目のデータ領域のサイズを取得する関数
 *
 * num番目のデータ領域のサイズを取得する関数。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @return num番目のデータ領域のサイズ
 */
int Nistk::PlotData::get_size(int num)
{
  return x_data[num].size();
}

/** num番目のデータ領域のデータを消去する関数
 *
 * num番目のデータ領域のデータを消去する関数。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 */
void Nistk::PlotData::clear(int num)
{
  x_data[num].clear();
  y_data[num].clear();

  return; 
}

/** x_data_maxを取得する関数
 *
 * x_data_maxを取得する関数
 *
 * @return x_data_maxの値
 */
double Nistk::PlotData::get_x_data_max()
{
  return x_data_max;
}

/** x_data_minを取得する関数
 *
 * x_data_minを取得する関数
 *
 * @return x_data_minの値
 */
double Nistk::PlotData::get_x_data_min()
{
  return x_data_min;
}

/** y_data_maxを取得する関数
 *
 * y_data_maxを取得する関数
 *
 * @return y_data_maxの値
 */
double Nistk::PlotData::get_y_data_max()
{
  return y_data_max;
}

/** y_data_minを取得する関数
 *
 * y_data_minを取得する関数
 *
 * @return y_data_minの値
 */
double Nistk::PlotData::get_y_data_min()
{
  return y_data_min;
}
 
/** num番目のデータ領域のデータをファイルに出力する関数
 *
 * num番目のデータ領域のデータをファイルに出力する関数。
 * file_nameで指定したファイル名でファイルを開き、
 * x0 y0 <br>
 * x1 y1 <br>
 * ..... <br>
 * 形式で出力する。<br>
 * フラグrasterがtureの場合のファイル出力は、
 * ファイルの出力は、GNUPLOTで描画出来る形式、つまり、
 * x0 y0 <br>
 * x0+raster_scale*(x_data_max-x_data_min) y0 <br>
 * <br>
 * x1 y1<br>
 * x1+raster_scale*(x_data_max-x_data_min) y1 <br>
 * <br>
 * ..... <br>
 * という形で、各データの間に空行を入れ、x軸に各点から
 * raster_scale*x_data_maxの長さの線を引く形に
 * 出力する。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param *file_name 出力ファイル名の文字列ポインタ
 */
void Nistk::PlotData::save_data(int num, const char *file_name)
{
  std::ofstream fout;      // 出力ファイルストリーム
  int max_size;            // num番目のデータ領域の最大サイズ
  double raster_size;      // ラスター出力の際の線の幅
  int i;

  raster_size = raster_scale[num] * (x_data_max - x_data_min);
  fout.open(file_name);
  if((!fout) || (num >= data_num)){
    std::cout << file_name << ":File can not open or no such data region! \n";
    return;
  }
  max_size = x_data[num].size();
  for(i = 0; i < max_size; i++){
    if( raster[num] == false){
      fout << x_data[num][i] << ' ' << y_data[num][i] << '\n';
    } else {
      fout << x_data[num][i] << ' ' << y_data[num][i] << '\n';
      fout << x_data[num][i] + raster_size << ' ' << y_data[num][i] << '\n';
      fout << '\n';
    }
  }
  fout.close();

  return;
}

/** num番目のデータ領域のデータをラスター図とするかどうかのフラグをセットする関数
 *
 * プロットの際およびファイル出力時にラスター図として
 * 保存及び出力するかどうかのフラグをセットする関数。
 * trueでラスター図になる。デフォルトはfalseである。
 * tureの場合のファイル出力は、
 * ファイルの出力は、GNUPLOTで描画出来る形式、つまり、
 * x0 y0 <br>
 * x0+raster_scale*(x_data_max-x_data_min) y0 <br>
 * <br>
 * x1 y1<br>
 * x1+raster_scale*(x_data_max-x_data_min) y1 <br>
 * <br>
 * ..... <br>
 * という形で、各データの間に空行を入れ、x軸に各点から
 * raster_scale*x_data_maxの長さの線を引く形に
 * 出力する。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param flag セットするフラグ値
 *
 */
void Nistk::PlotData::set_raster(int num, bool flag)
{
  raster[num] = flag;
  return;
}

/** num番目のデータ領域のデータをラスター図とするかどうかのフラグを取得する関数
 *
 * プロットの際およびファイル出力時にラスター図として
 * 保存及び出力するかどうかのフラグを取得する関数。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 *
 */
bool Nistk::PlotData::get_raster(int num)
{
  return raster[num];
}

/** num番目のデータ領域のデータのraster_scaleをセットする関数
 *
 * データをラスター図としてファイルに出力する際の線の
 * 幅を決定するraster_scaleの値をセットする関数。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 * @param val セットする値
 *
 */
void Nistk::PlotData::set_raster_scale(int num, double val)
{
  raster_scale[num] = val;
  return;
}

/** num番目のデータ領域のデータのraster_scaleを取得する関数
 *
 * データをラスター図としてファイルに出力する際の線の
 * 幅を決定するraster_scaleの値をセットする関数。
 *
 * @param num 格納するデータ領域の番号(0から始まる）
 *
 */
double Nistk::PlotData::get_raster_scale(int num)
{
  return raster_scale[num];
}
