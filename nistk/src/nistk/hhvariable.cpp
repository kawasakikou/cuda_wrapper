/**
 * @file  hhvariable.cpp
 * @brief class for variable of Hodgikin-Huxley model(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-01
 * @version $Id: hhvariable.cpp,v.20110801 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<nistk/hhvariable.h>

/** コンストラクタ
 */
Nistk::Neuron::HHVariable::HHVariable()
{
}

/** デストラクタ
 */
Nistk::Neuron::HHVariable::~HHVariable()
{
}
