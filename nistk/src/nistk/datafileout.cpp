/**
 * @file  datafileout.cpp
 * @brief class for output to file(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: datafileout.cpp,v.20110809 $
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */

#include<datafileout.h>
#include<iostream>
#include<fstream>
#include<string>

/** コンストラクタ 引数無し
 */
Nistk::DataFileOut::DataFileOut()
{
  mode = false;
  check = false;
}

/** コンストラクタ 引数(ファイル名,mode)
 *
 * 生成と同時にファイルをオープンする。出力モードは,
 * true:3D,false:2Dとなる。
 *
 * @param f オープンするファイルのファイル名
 * @param i_m 出力モード(true:3D false:2D)
 */
Nistk::DataFileOut::DataFileOut(const char *f, bool i_m)
{
  mode = false;
  check = false; 
  open(f,i_m);
}

/** デストラクタ
 */
Nistk::DataFileOut::~DataFileOut()
{
  if(check == true) o_file.close();
}

/** ファイルのオープン
 *
 * ファイルをオープンする。出力モードは,
 * true:3D,false:2Dとなる。既にファイルが
 * 開かれているときには,なにもせずにreturn
 * となるので注意。いったん,closeされていれば
 * 問題無く再度オープンすることができる。
 *
 * @param f オープンするファイルのファイル名
 * @param i_m 出力モード(true:3D false:2D)
 */
void Nistk::DataFileOut::open(const char *f, bool i_m)
{
  std::string str_buf;
  int f_length,i;

  str_buf=f; // ファイル名をコピー
  // ファイルを開き,正常に開いているかチェックし,
  // ちゃんと開いていないときメッセージを表示して戻る。
  o_file.open(str_buf.c_str());
  if(!o_file){
    std::cout << "Can not open file=" << str_buf.c_str() << '\n';
    return;
  }

  // モード,状態をセット
  mode = i_m;
  check = true;

  // ファイル名を文字列にセット
  // stringでは都合が悪いので。。
  f_length = str_buf.length();
  o_file_name = new char[f_length+1];
  for(i=0; i< f_length; i++) o_file_name[i] = str_buf[i];
  str_buf[f_length] = '\0';

  return;
}

/** ファイルのクローズ
 * 
 * 現在開かれているファイルをクローズする関数。
 * 開かれていない状態で呼ばれると,なにもせずに
 * returnされる。
 */
void Nistk::DataFileOut::close()
{
  if(check == true) {
    o_file.close();
    mode = false;
    check = false;
    delete[] o_file_name;
  }  
  
  return;
}

/** データ書き込み関数(2D)
 *
 * ファイルにデータを2Dモードで出力する関数。当然ながら
 * 書き込まれるのは１つのデータ点。ちなみに、ファイルが
 * 開かれていない状態,または3Dモードで開かれているときに,
 * 呼ばれるとなにもせずにreturnする。
 *
 * @param x 書き込みデータのx軸の値
 * @param y 書き込みデータのy軸の値
 */
void Nistk::DataFileOut::write(double x, double y)
{
  if((check == true) && (mode ==false))
               o_file << x << ' ' << y << '\n';
  return;
}

/** データ書き込み関数(3D)
 *
 * ファイルにデータを3Dモードで出力する関数。当然ながら
 * 書き込まれるのは１つのデータ点。ちなみに、ファイルが
 * 開かれていない状態,または2Dモードで開かれているときに,
 * 呼ばれるとなにもせずにreturnする。
 *
 * @param x 書き込みデータのx軸の値
 * @param y 書き込みデータのy軸の値
 * @param z 書き込みデータのy軸の値
 */
void Nistk::DataFileOut::write(double x, double y, double z)
{
  if((check == true) && (mode ==true))
       o_file << x << ' ' << y << ' ' << z <<'\n';
  return;
}

/** 空行を書き込む関数
 *
 * ファイルに空行を書き込む関数。これがないと,
 * 3Dサーフェースのファイルを作成することができない。
 * まあ、他の用途でも使えるが。。。ちなみに、ファイルが
 * 開かれていない状態で呼ばれるとなにもせずにreturnする。
 *
 */
void Nistk::DataFileOut::insert_line()
{
  if(check == true) o_file <<'\n';
  return;
}

/** コメントを書き込む関数
 *
 * ファイルにGNU PLOT形式のコメントを書き込む関数。ちなみに,
 * 開かれていない状態で呼ばれるとなにもせずにreturnする。
 *
 * @param comment コメントとして書き込む文字列のchar型ポインタ
 */
void Nistk::DataFileOut::insert_comment(const char *comment)
{
  std::string buf;

  buf = comment; // とりあえずこうしておく

  if(check == true) o_file << "# " << buf.c_str() << '\n';
  return;
}

/** SimData型のデータを書き出す関数
 *
 * ファイルにSimData型のデータを書き込む関数。当然ながら
 * 3D形式になる。ファイルが開かれていない,もしくは3D形式で
 * 開かれていない状態で呼ばれるとなにもせずにreturnする。
 * また、x軸とy軸の値の開始位置と増分を指定することができるように
 * してあり、それぞれの軸が0から始まる整数倍にしなくても良いように
 * することが可能となっている。
 *
 * @param i_data SimData型オブジェクトのポインタ
 * @param x_start xの値のスタート値
 * @param x_step  xの値の増分
 * @param y_start yの値のスタート値
 * @param y_step  yの値の増分
 */
void Nistk::DataFileOut::data_from_simdata(Nistk::SimData *i_data, 
                double x_start, double x_step, double y_start, double y_step)
{
  int hight,width;
  double x,y;

  if((check == true) && (mode ==true)){
    hight = i_data->get_height();  // SimDataの高さと幅を取得
    width = i_data->get_width();
    // データを取得して書き込み
    y = y_start;
    for(int i=0; i<hight; i++){ // 縦 y
      x = x_start;
      for(int j=0; j<width; j++){ // 横 x
	o_file << x << ' ' << y << ' ' << i_data->get_data(j,i) <<'\n';
	x += x_step;
      }
      y += y_step;
      o_file << '\n';
    }
  }
  return;
}

/** ファイルが開いてるかチェックする関数
 *
 * ファイルが開いた状態かどうかをbool型で返す関数。<br>
 * (true:open false:not_open)
 *
 * @return ファイルが開いているかどうか(bool型)
 */
bool Nistk::DataFileOut::is_open()
{
  return check;
}

/** modeが3Dかチェックする関数
 *
 * 開いているファイルが3Dかどうかをbool型で返す関数。<br>
 * (true:3D false:2D)
 *
 * @return 開いているファイルが3Dかどうか(bool型)
 */
bool Nistk::DataFileOut::is_3D()
{
  return mode;
}

/** 開いているファイルのファイル名を取得する関数
 *
 * 開いているファイルのファイル名の文字型ポインタを返す関数。
 *
 * @return 開いているファイルのファイル名の文字型ポインタ
 */
char* Nistk::DataFileOut::get_file_name()
{
  return o_file_name;
}
