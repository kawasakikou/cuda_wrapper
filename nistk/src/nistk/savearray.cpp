/**
 * @file  savearray.cpp
 * @brief class for save SimDataArray image file(実装)
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: savearray.cpp,v.20130315 $
 *
 * Copyright (C) 2013 Masakazu Komori
 */
#include<string>
#include<cmath>
#include<imageloader.h>
#include<imagedata.h>
#include<simdataarray.h>
#include<iostream>
#include<savearray.h>

/** コンストラクタ 引数無し
 */
Nistk::SaveArray::SaveArray()
{
  array_width=0;
  array_height=0;
  array_w_num=0;
  array_h_num=0;
  image_width=0;
  image_height=0;
  image_margin=0;
  is_alloc=false;
}

/** デストラクタ
 */
Nistk::SaveArray::~SaveArray()
{
}

/** arrayのためのpixbuf領域を確保する関数
 *
 * 画像アレイのためのpixbuf領域を確保する関数。array_from_file関数
 * やarray_from_simdata_array関数を使って,一気に画像アレイを
 * 生成する場合はこの関数をユーザーが使う必要がない。
 * この関数は,領域が既に確保されていたとしても
 * resizeという形で新たに領域を確保するようになっている。
 * 
 * @param im_width 1つの画像の幅
 * @param im_height 1つの画像の高さ
 * @param im_margin 画像間のマージン
 * @param w_num 画像アレイの横の画像の数
 * @param h_num 画像アレイの縦の画像の数
 */
void Nistk::SaveArray::create_array_area(int im_width, int im_height, 
					  int im_margin, int w_num, int h_num)
{
  // パラメータセット
  array_w_num = w_num;
  array_h_num = h_num;
  image_width = im_width;
  image_height = im_height;
  image_margin = im_margin;
  array_width = w_num*im_width+(im_margin+1)*w_num;
  array_height = h_num*im_height+(im_margin+1)*h_num;  

  // pixbufの領域確保
  if(is_alloc == false){
    array_pixbuf = 
       Nistk::ImageData::create_pixbuf_new(array_width, array_height);
    is_alloc = true;
  }
  else array_pixbuf = 
       Nistk::ImageData::create_pixbuf_new(array_width, array_height);


  // pixbufのパラメータを取得
  array_p_data = array_pixbuf->get_pixels();        // RGBデータの開始位置
  array_channels = array_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  array_stride = array_pixbuf->get_rowstride();     // 1行当たりのデータ数

  return;
}

/** 1つのpixbufをarrayの位置x_num,y_numに書き込む関数
 *
 * 1つのpixbufをarrayの位置x_num,y_num(ピクセル値では無く
 * ,縦横何番目の画像か)に書き込む関数。
 * この関数を実行する前には,当然ながらarrayのpixbufの
 * 領域確保がされていないといけない。また、書き込むpixbufの
 * 縦横のサイズは領域確保のときに指定した大きさになっていないと
 * いけないので注意すること。
 * 
 * @param i_pixbuf 書き込む画像のGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @param x_num 横の位置(画像の枚数で考えて)
 * @param y_num 縦の位置(画像の枚数で考えて)
 */
void Nistk::SaveArray::set_pixbuf_to_array(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,
			                                  int x_num, int y_num)
{
  guint8 *i_p_data;                // RGBデータの開始位置
  int i_channels;                  // 1pixel当たりのデータ数
  int i_stride;                    // 1行当たりのデータ数
  int i_w,i_h;                      // 画像の幅と高さ
  int image_offset_w;                // array中の画像の横のオフセット
  int image_offset_h;                // array中の画像の縦のオフセット
  int array_offset,i_offset;

  i_p_data = i_pixbuf->get_pixels();        // RGBデータの開始位置
  i_channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  i_w = i_pixbuf->get_width();              // 画像データの幅
  i_h = i_pixbuf->get_height();             // 画像データの高さ
  i_stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数

  // 画像のオフセットの計算
  image_offset_w = (image_width+image_margin)*x_num+image_margin;
  image_offset_h = (image_height+image_margin)*y_num+image_margin;

  // tmp_bufからデータを読み込みつつarray_pixbufに書き込み
  for(int row=0; row<i_h; row++){ // 行 y
    for(int col=0; col<i_w; col++){ // 列 x
      // 1ピクセルあたりのデータ数から位置を計算し,
      // RGBデータを取得し書き込み
      i_offset = row*i_stride+col*i_channels;
      array_offset= (image_offset_h + row) * array_stride
      	             + (image_offset_w + col) * array_channels;
      array_p_data[array_offset] = i_p_data[i_offset];
      array_p_data[array_offset+1] = i_p_data[i_offset+1];
      array_p_data[array_offset+2] = i_p_data[i_offset+2];
    }
  }

  return;
}

/** 1つのSimDataをarrayの位置x_num,y_numに書き込む関数
 *
 * 1つのsimdataを画像に変換してarrayの位置x_num,y_num(ピクセル値では無く
 * ,縦横何番目の画像か)に書き込む関数。
 * この関数を実行する前には,当然ながらarrayのpixbufの
 * 領域確保がされていないといけない。また、書き込むpixbufの
 * 縦横のサイズは領域確保のときに指定した大きさになっていないと
 * いけないので注意すること。
 * 
 * @param i_simdata 書き込む画像のNistk::SimData型ポインタ
 * @param x_num 横の位置(画像の枚数で考えて)
 * @param y_num 縦の位置(画像の枚数で考えて)
 */
void Nistk::SaveArray::set_simdata_to_array(Nistk::SimData *i_simdata, 
			                             int x_num, int y_num)
{
    Glib::RefPtr<Gdk::Pixbuf> tmp_buf; // テンポラリ用pixbuf
    int simdata_w,simdata_h;           // SimDataの幅と高さ

    simdata_w=i_simdata->get_width();
    simdata_h=i_simdata->get_height();
    tmp_buf =Nistk::ImageData::create_pixbuf_new(simdata_w,simdata_h);
    Nistk::ImageData::pixbuf_from_simdata(i_simdata,tmp_buf);
    set_pixbuf_to_array(tmp_buf,x_num,y_num);

    return;
}

/** Nistk::ImageLoaderから画像のアレイを作る関数
 *
 * ImageLoaderから画像アレイを作り、表示する関数。
 * ImageLoaderはポインタではなくコピーという形を
 * とっている。描画領域はそれぞれの画像に対してでは
 * なく,アレイ全体で一つの描画領域になっている。
 * よって、セーブすると一つの画像とすることができる。
 * 
 * @param i_loader 表示させる画像を読み込むイメージローダ
 * @param i_margin イメージとイメージの間の幅
 * @param i_scale 表示させるイメージの倍率
 * @param num_width 横に並べるイメージの数
 */
void Nistk::SaveArray::array_from_file(Nistk::ImageLoader i_loader, 
				int i_margin, double i_scale, int num_width)
{
  Glib::RefPtr<Gdk::Pixbuf> tmp_buf; // テンポラリ用pixbuf
  int file_num;                      // 読み込みファイル数
  int file_count;                    // 読み込みファイルカウント
  int w_max, h_max;
  int tmp_w,tmp_h,w_num,h_num;

  // ImageLoaderの読み込みファイル数を取得し,0の場合はなにもせずに
  // 戻る。スケールが０のときも。そうでない場合は,カウンタを0にリセット
  file_num = i_loader.get_input_file_num();
  if((file_num == 0) || (i_scale == 0)) return;
  i_loader.count_reset();

  // 読み込み画像の縦と横の最大値を取得
  w_max=0;
  h_max=0;
  for(int i=0; i<file_num; i++){
    tmp_buf = i_loader.load_image_next();
    tmp_w = tmp_buf->get_width();
    tmp_h = tmp_buf->get_height();
    if(w_max < (int)(i_scale*tmp_w)) w_max = (int)(i_scale*tmp_w);
    if(h_max < (int)(i_scale*tmp_h)) h_max = (int)(i_scale*tmp_h);
  }

  // 縦横の最大値と横の画像の数,マージンでpixbufのサイズを決定
  // pixbufを生成
  if(file_num < num_width){ // 縦横の画像の数を決定
    w_num = file_num;
    h_num = 1;
  }
  else{
    w_num = num_width;
    if((file_num % w_num) == 0) h_num = file_num/w_num;
    else h_num = file_num/w_num + 1;
  }
  create_array_area(w_max, h_max, i_margin, w_num, h_num);

  // ImageLoaderを使って読み込みながらpixbufにセット
  // わかりやすいように二重ループを使う
  i_loader.count_reset(); // カウンタをリセット
  file_count = 0;
  for(int i=0; i<h_num; i++){        // 縦
    for(int j=0; j<w_num; j++){      // 横
      if(file_count == file_num) break;  // 全てのファイルを呼んだら抜ける
      tmp_buf = i_loader.load_image_next();
      if(i_scale != 1){
	tmp_w = tmp_buf->get_width();
	tmp_h = tmp_buf->get_height();
	tmp_buf = Nistk::ImageData::resize_pixbuf(tmp_buf,
			       (int)(i_scale*tmp_w), (int)(i_scale*tmp_h));
      }
      // 画像をarrayにセット
      set_pixbuf_to_array(tmp_buf, j, i);
      file_count++;
    }
    if(file_count == file_num) break; // 全てのファイルを呼んだら抜ける
  }

  return;
}

/** Nistk::SimDataArrayから画像のアレイを作る関数
 *
 * SimDataArrayから画像アレイを作り、表示する関数。
 * 描画領域はそれぞれの画像に対してでは
 * なく,アレイ全体で一つの描画領域になっている。
 * よって、セーブすると一つの画像とすることができる。
 * 
 * @param i_array 表示させるSimDataArrayのポインタ
 * @param i_margin イメージとイメージの間の幅
 * @param i_scale 表示させるイメージの倍率
 * @param num_width 横に並べるイメージの数
 */
void Nistk::SaveArray::array_from_simdataarray(Nistk::SimDataArray *i_array, 
			           int i_margin, double i_scale, int num_width)
{
  Glib::RefPtr<Gdk::Pixbuf> tmp_buf; // テンポラリ用pixbuf
  Nistk::SimData *tmp_simdata;       // テンポラリ用simdata型ポインタ  
  int simdata_num_w;                      // SimDataの数(横)
  int simdata_num_h;                      // SimDataの数(縦)
  int simdata_total;                      // SimDataのトータル数
  int w_max, h_max;                     // 縦横の最大値
  int simdata_count;                    
  int tmp_w,tmp_h,w_num,h_num;
  int simdata_w,simdata_h;

  // SimDataArrayの幅と高さを取得し,0の場合はなにもせずに
  // 戻る。スケールが０のときも。
  simdata_num_w = i_array->get_width();
  simdata_num_h = i_array->get_height();
  if((simdata_num_w == 0) || (simdata_num_w == 0) || (i_scale == 0)) return;
  simdata_total = simdata_num_w * simdata_num_h;

  // 読み込み画像の縦と横の最大値を取得
  w_max=0;
  h_max=0;
  for(int i=0; i<simdata_num_h; i++){ // 縦
    for(int j=0; j<simdata_num_w; j++){ // 横
      tmp_w = i_array->get_array_width(j,i);
      tmp_h = i_array->get_array_height(j,i);
      if(w_max < (int)(i_scale*tmp_w)) w_max = (int)(i_scale*tmp_w);
      if(h_max < (int)(i_scale*tmp_h)) h_max = (int)(i_scale*tmp_h);
    }
  }

  // 縦横の最大値と横の画像の数,マージンでpixbufのサイズを決定
  // pixbufを生成
  if( simdata_total < num_width){ // 縦横の画像の数を決定
    w_num = simdata_total;
    h_num = 1;
  }
  else{
    w_num = num_width;
    if((simdata_total % w_num) == 0) h_num = simdata_total/w_num;
    else h_num = simdata_total/w_num + 1;
  }
  create_array_area(w_max, h_max, i_margin, w_num, h_num);

  // データをテンポラリのpixbufに変換してpixbufにセット
  simdata_count = 0;
  for(int i=0; i<h_num; i++){        // 縦
    for(int j=0; j<w_num; j++){      // 横
      if(simdata_count == simdata_total) break;// 全てのsimDataが終われば抜ける
      // チェック
      //tmp_simdata = i_array->get_simdata_ptr(j,i); 
      tmp_simdata = i_array->get_simdata_ptr1d(i * w_num + j); 
      simdata_w=tmp_simdata->get_width();
      simdata_h=tmp_simdata->get_height();
      tmp_buf =Nistk::ImageData::create_pixbuf_new(simdata_w,simdata_h);
      Nistk::ImageData::pixbuf_from_simdata(tmp_simdata,tmp_buf);
      if(i_scale != 1){
	tmp_w = tmp_buf->get_width();
	tmp_h = tmp_buf->get_height();
	tmp_buf = Nistk::ImageData::resize_pixbuf(tmp_buf,
			       (int)(i_scale*tmp_w), (int)(i_scale*tmp_h));
      }
      // 画像をarrayにセット
      set_pixbuf_to_array(tmp_buf, j, i);
      simdata_count++;
    }
    if(simdata_count == simdata_total) break;  // 全てのsimDataが終われば抜ける
  }

  return;
}

/** 格納された画像を画像ファイルとして保存する関数
 *
 * 格納された画像を画像ファイルとして保存する関数。
 * 引数のファイル名は拡張子を付けなくても良いようになっている。
 * 使えるファイル形式はgtkmmのpixbufが対応しているファイル形式になる。
 * 例えば,png,jpeg,bmpなど。また、シミュレーションでの途中経過を
 * 保存したい場合にも使いやすいように、ファイル名の前もしくは後に
 * 数値を付けれるようにしてある。(例：test0023.45.png,0010test.png) 
 * ただし、汎用性を考えて引数はdouble型としている。実数を文字に変換
 * する際にたまに値がずれるので注意すること。(例:23.45→23.449)
 * 整数と少数の桁数指定も必要となっている。モードの指定は以下の
 * とおりである。<br><br>
 *
 * mode: 0:数字を付けない 
 *       1:ファイル名の後に数字をつける 
 *       2:ファイル名の前に数字をつける 
 *
 * @param *i_file_name 保存ファイル名
 * @param *i_file_type ファイルの保存形式
 * @param mode ファイル名に数字を付けるどうかのモード
 * @param num ファイル名に付ける数字
 * @param major 整数部の桁数
 * @param minor 小数部の桁数
 */
void Nistk::SaveArray::save_image(const char *i_file_name, 
				  const char *i_file_type,
			          int mode, double num, int major, int minor)
{
  std::string i_file;    // 保存ファイル名格納用
  std::string tmp;       // 数字の文字列一時格納用
  double tmp_num1;
  int tmp_num2;
  int i;

  // ファイル名をいったんstringに代入
  i_file = i_file_name;
  // ファイル名に数字をつける時の処理(doubleの関係で少数が少し変になる時あり)
  // 例 23.45が23.4499となる
  if(mode !=0){
    // 整数部の処理
    tmp_num1 = pow(10,major-1);
    for(i = 0; i < major; i++){
      tmp_num2 = (int)(num / tmp_num1);
      num -= (double)(tmp_num2*tmp_num1);
      switch(tmp_num2){
        case 0: tmp += '0'; break;
        case 1: tmp += '1'; break;
        case 2: tmp += '2'; break;
        case 3: tmp += '3'; break;
        case 4: tmp += '4'; break;
        case 5: tmp += '5'; break;
        case 6: tmp += '6'; break;
        case 7: tmp += '7'; break;
        case 8: tmp += '8'; break;
        case 9: tmp += '9'; break;
      }
      tmp_num1 /= 10.0;
    }
    // 小数部の処理
    if(minor != 0) tmp +='.';
    for(i = 0; i < minor; i++){
      tmp_num2 = (int)(num * 10.0);
      num = (num * 10.0) - (double)(tmp_num2);
      switch(tmp_num2){
        case 0: tmp += '0'; break;
        case 1: tmp += '1'; break;
        case 2: tmp += '2'; break;
        case 3: tmp += '3'; break;
        case 4: tmp += '4'; break;
        case 5: tmp += '5'; break;
        case 6: tmp += '6'; break;
        case 7: tmp += '7'; break;
        case 8: tmp += '8'; break;
        case 9: tmp += '9'; break;
      }
    }
    if(mode == 1) i_file += tmp;
    else if(mode == 2) i_file = tmp + i_file;
  }
 
  // pixbufをイメージファイルとして保存
  Nistk::ImageData::save_pixbuf(array_pixbuf, i_file.c_str(), i_file_type);

  return;
}



/**  arrayのpixbufを取得する関数
 *
 * SaveArrayのpixbufを引数として渡された
 * pixbufのポインタを使ってコピーしてそれを返り値として戻す関数
 * スマートポインタを使っているのでこのような形になる。
 * 
 * @param i_pixbuf 取得するpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @return pixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
Glib::RefPtr<Gdk::Pixbuf> Nistk::SaveArray::get_pixbuf(
                                          Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  i_pixbuf=array_pixbuf->copy();

  return i_pixbuf;
 }

/**  pixbufの幅を取得する関数
 *
 * pixbufの幅を取得する関数。
 * 
 * @return pixbufの幅
 */
int Nistk::SaveArray::get_array_width()
{
  return array_width;
}

/**  pixbufの高さを取得する関数
 *
 * pixbufの高さを取得する関数。
 * 
 * @return pixbufの高さ
 */
int Nistk::SaveArray::get_array_height()
{
  return array_height;
}

/**  横の画像の数を取得する関数
 *
 * pixbufの横の画像の数を取得する関数。
 * 
 * @return 横の画像の数
 */
int Nistk::SaveArray::get_array_w_num()
{
  return array_w_num;
}

/**  縦の画像のを取得する関数
 *
 * アレイの縦の画像の数を取得する関数。
 * 
 * @return 縦の画像の数
 */
int Nistk::SaveArray::get_array_h_num()
{
  return array_h_num;
}

/**  1つの画像の高さを取得する関数
 *
 * 1つの画像の高さを取得する関数
 * 
 * @return 1つの画像の高さ
 */
int Nistk::SaveArray::get_image_height()
{
  return image_height;
}

/**  1つの画像の幅を取得する関数
 *
 * 1つの画像の幅を取得する関数
 * 
 * @return 1つの画像の幅
 */
int Nistk::SaveArray::get_image_width()
{
  return image_width;
}

/**  画像間の幅を取得する関数
 *
 * アレイの画像間の幅を取得する関数
 * 
 * @return 画像間の幅
 */
int Nistk::SaveArray::get_image_margin()
{
  return image_margin;
}








