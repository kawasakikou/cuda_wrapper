/**
 * @file  imageview_button.cpp
 * @brief class for imageview buttonused by nistk main window(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imageview_button.cpp,v.20110809 $
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

#include<imageview_button.h>
#include<imagefiles.h>
#include<gtkmm.h>
#include<imagedata.h>

/** コンストラクタ
 */
Nistk::ImageViewButton::ImageViewButton()
{
  // 一時使用の為の変数
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
   // ImageViewの初期設定
  m_pixbuf=Gdk::Pixbuf::create_from_file(NOIMAGE);
  window.set_image(m_pixbuf);
}

/** デストラクタ
 */
Nistk::ImageViewButton::~ImageViewButton()
{
  window.hide();
}

/** クリックされた時の処理(オーバーライド)
 *
 * ボタンがクリックされたときの処理をする関数。親クラスである
 * Gtk::Buttonのものをオーバーライドしている。これにより、
 * ボタンがクリックされたときにImageViewを表示することが可能に
 * なっている。
 * 
 */
void Nistk::ImageViewButton::on_clicked()
{
  Gtk::Button::on_clicked(); // 親クラスのメソッドの呼び出し
  if(window.is_window_show() == false){
    window.show();
    window.set_show_window(true);
  } else {
    window.hide();
    window.set_show_window(false);
  }
}

/** ボタンとウィンドウの名前をセットする関数
 *
 * ボタンとボタンと連動しているImageViewウィンドウの名前をセットする関数。
 * 
 * @param name セットする名前のchar型ポインタ
 */
void Nistk::ImageViewButton::set_name(const char *name)
{
  window.set_name(name);
  set_label(name);
}

/** ImageViewのポインタを取得する関数
 *
 * このボタンが持っているImageViewのポインタを取得する関数。
 * これにより、このボタンが持っているImageViewを通常
 * のものと同じように扱うことが出来る。
 *
 * @return ImageView型のポインタ
 */
Nistk::ImageView* Nistk::ImageViewButton::get_imageview_ptr()
{
  return &window;
}
