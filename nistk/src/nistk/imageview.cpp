/**
 * @file  imageview.cpp
 * @brief class for image drawable(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imageview.cpp,v.20110809 $
 *
 * Copyright (C) 2008-2011 Masakazu Komori
 */

#include<imageview.h>
#include<imagedata.h>

/** コンストラクタ 引数無し
 */
Nistk::ImageView::ImageView() 
  : m_Button_Close("Close"),
    m_Button_Save("Save")
{
  // ウィンドウの初期化
  set_up_window();
  // 実体化
  show_all_children();
}

/** コンストラクタ 引数(Glib::RefPtr<Gdk::Pixbuf>) 描画までしてしまう
 */
Nistk::ImageView::ImageView(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf) 
  : m_Button_Close("Close"),
    m_Button_Save("Save")
{
  // ウィンドウの初期化
  set_up_window();
  // イメージの張り付け
  m_draw.set_pixbuf(i_pixbuf);
  // 実体化
  show_all_children();  
}

/** デストラクタ
 */
Nistk::ImageView::~ImageView()
{
}

/** コンストラクタで呼ばれる関数 ユーザは使う必要はない
 */
void Nistk::ImageView::set_up_window()
{
  char test_name[] = "test";

  // 大きさや外枠のマージンを設定する
  set_border_width(0);
  set_size_request(200,200);
  m_ScrolledWindow.set_border_width(10);
  set_name(test_name);
  // スクロールバーのポリシーを設定
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC);
  // 子ウィジットをパックする
  get_vbox()->pack_start(m_ScrolledWindow);
  m_ScrolledWindow.add(m_draw);

  // ボタンのイベントと動作させる関数を結合し,配置する
  m_Button_Save.signal_clicked().connect(sigc::mem_fun(*this,
					   &ImageView::on_button_save));
  m_Button_Close.signal_clicked().connect(sigc::mem_fun(*this,
					   &ImageView::on_button_close));
  m_Button_Close.set_flags(Gtk::CAN_DEFAULT);
  m_Hbox.pack_start(m_Button_Save);
  m_Hbox.pack_start(m_Button_Close);
  Gtk::Box *pBox=get_action_area();
  if(pBox) pBox->pack_start(m_Hbox);

  return;
}

/** 名前をセットする関数
 *
 * Drawableを表示しているウィンドウに名前を
 * 付ける関数。現在何を表示しているか分かる
 * ようにするためにも付けるべきである。
 * また、表示しているDrawableを保存する
 * 際にもファイル名として使用される。
 * 名前を付けないと、名前はtestになる。
 *
 * @param i_name ウィンドウに付ける名前
 */
void Nistk::ImageView::set_name(const char *i_name)
{
  m_name = i_name;
  set_title(m_name.c_str());
  return;
}

/** 画像をセットする関数
 *
 * pixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * を渡すことにより、pixbufにあるイメージを
 * ウィンドウに表示する関数。
 *
 * @param i_pixbuf 表示させるpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
void Nistk::ImageView::set_image(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  m_draw.set_pixbuf(i_pixbuf);
}

/** ボタンが押されると現在描画しているイメージをファイルにセーブする関数
 *
 * ボタンが押されると現在描画しているイメージを
 * ファイルにセーブする関数。ファイル名は
 * ウィンドウに付けているファイル名.pngになる。
 * つまり、ファイル形式はpngとなる。
 *
 */
void Nistk::ImageView::on_button_save()
{
  Glib::RefPtr<Gdk::Pixbuf> s_pixbuf;
  int f_length,i;
  char *f_name;
  char file_type[] = "png";

  // セーブファイル名を文字列にセット
  // stringではうまいこといかないので
  f_length = m_name.length();
  f_name = new char[f_length+1];
  for(i=0; i< f_length; i++) f_name[i] = m_name[i];
  f_name[f_length] = '\0';

  // drawableからpixbufを取得してファイルにセーブ
  s_pixbuf = m_draw.get_pixbuf(s_pixbuf);
  Nistk::ImageData::save_pixbuf(s_pixbuf,f_name,file_type);

  delete[] f_name;
}

/** ボタンが押されるとウィンドウを閉じる関数 ユーザは使う必要はない
 */
void Nistk::ImageView::on_button_close()
{
  m_draw.set_show_flag(false);
  hide();
}

/** windowの表示フラグの取得関数
 *
 * windowの表示フラグの取得関数
 *
 * @return 表示されているかどうかのフラグ
 */
bool Nistk::ImageView::is_window_show()
{
  return m_draw.is_show();
}

/** windowのshowフラグをセットする関数
 *
 * windowのshowフラグをセットする関数
 *
 * @param sflag showフラグの値
 */
void Nistk::ImageView::set_show_window(bool sflag)
{
  m_draw.set_show_flag(sflag);

  return ;
}

/** 再描画関数 
 * スレッドプログラミングの際に必要になる
 */
void Nistk::ImageView::redraw_window()
{
  m_draw.redraw();
}

/** xボタンが押されたときの処理関数
 *  xボタンが押されたときの処理関数。押されても閉じないようにする。
 */
bool Nistk::ImageView::on_delete_event(GdkEventAny*)
{
  return true;
}
