/**
 * @file  data_io.cpp
 * @brief class for data input-output of simulation(実装)
 *
 * @author Masakazu Komori
 * @date 2012-09-07
 * @version $Id: data_io.cpp,v.20120907 $
 *
 * Copyright (C) 2012 Masakazu Komori
 */

#include<iostream>
#include<fstream>
#include<simdata.h>
#include<simdataarray.h>
#include<data_io.h>

/** SimData型のオブジェクトに格納されているデータをファイルに出力する関数
 *
 * SimData型のオブジェクトに格納されているデータをファイルに出力する関数。
 * SimData型のオブジェクトに格納されているデータを指定したファイル名の
 * ファイルに保存する。指定されたファイルが存在する場合はそのファイルに
 * 上書きされる。ファイル出力の際には、バイナリモードで書き込みをするため、
 * 基本、内容を確認することはできない。
 *
 * @param *src Nistk::SimData型ポインタ
 * @param *filename 出力ファイル名
 * @return ファイルオープンの成否
 */
bool Nistk::DataIO::save_simdata(Nistk::SimData *src, char *filename)
{
  std::ofstream fout;   // 出力ファイル用
  int height;           // SimDataの高さ
  int width;            // SimDataの幅
  double data;          // データ読み出し用
  int x,y;

  // ファイルのオープン　失敗すると処理せずfalseをリターン
  fout.open(filename,std::ios_base::binary);
  if(!fout){
    std::cout << "Can not open " << filename << '\n';
    return false;
  }
  // SimData型のファイルへの格納
  width = src->get_width();
  height = src->get_height();
  fout.write((char*)&height, sizeof(int));  // 高さを出力
  fout.write((char*)&width, sizeof(int));   // 幅を出力
  // データを出力
  for(y = 0; y < height; y++){   // 縦
    for(x = 0; x < width; x++){  // 横
      data = src->get_data(x,y);
      fout.write((char*)&data, sizeof(double));
    }
  }
  fout.close();

  return true;
}

/** 関数save_simdataで保存したデータをSimData型のオブジェクトに格納する関数
 *
 * 関数save_simdataで保存したデータをSimData型のオブジェクトに格納する関数。
 * 指定された関数save_simdataで保存したデータファイルから、データを読み込み
 * SimData型のオブジェクトに格納する。ただし、指定したデータファイルが、
 * 本当に、関数save_simdataで保存したデータファイルかはチェックしないので
 * 注意すること。戻り値は、ファイルが開けたかどうかなので注意。
 * 
 * @param *dest Nistk::SimData型ポインタ
 * @param *filename 入力ファイル名
 * @return ファイルオープンの成否
 */
bool Nistk::DataIO::load_simdata(Nistk::SimData *dest, char *filename)
{
  std::ifstream fin;    // 入力ファイル用
  int height;           // SimDataの高さ
  int width;            // SimDataの幅
  double data;          // データ読み出し用
  int x,y;

  // ファイルのオープン　失敗すると処理せずfalseをリターン
  fin.open(filename,std::ios_base::binary);
  if(!fin){
    std::cout << "Can not open " << filename << '\n';
    return false;
  }
  // SimData型のデータの削除 データがない場合は何もしない
  dest->delete_data();
  // ファイルからSimData型への格納
  fin.read((char*)&height, sizeof(int));  // 高さを入力
  fin.read((char*)&width, sizeof(int));   // 幅を入力
  dest->create_data_size(width, height);  // SimDataのデータ領域確保
  // データを入力
  for(y = 0; y < height; y++){            // 縦
    for(x = 0; x < width; x++){           // 横
      fin.read((char*)&data, sizeof(double));
      dest->put_data(x,y,data);
    }
  }
  fin.close();

  return true;
}

/** SimDataArray型のオブジェクトに格納されているデータをファイルに出力する関数
 *
 * SimDataArray型のオブジェクトに格納されているデータをファイルに出力する関数。
 * SimDataArray型のオブジェクトに格納されているデータを指定したファイル名の
 * ファイルに保存する。指定されたファイルが存在する場合はそのファイルに
 * 上書きされる。ファイル出力の際には、バイナリモードで書き込みをするため、
 * 基本、内容を確認することはできない。
 * 
 * @param *src Nistk::SimDataArray型ポインタ
 * @param *filename 出力ファイル名
 * @return ファイルオープンの成否
 */
bool Nistk::DataIO::save_array(Nistk::SimDataArray *src, char *filename)
{
  std::ofstream fout;   // 出力ファイル用
  int array_h;          // SimDataArrayの高さ
  int array_w;          // SimDataArrayの幅
  int height;           // SimDataの高さ
  int width;            // SimDataの幅
  double data;          // データ読み出し用
  int a_x,a_y,x,y;

  // ファイルのオープン　失敗すると処理せずfalseをリターン
  fout.open(filename,std::ios_base::binary);
  if(!fout){
    std::cout << "Can not open " << filename << '\n';
    return false;
  }
  // SimDataArray型のファイルへの格納
  array_w = src->get_width();
  array_h = src->get_height();
  fout.write((char*)&array_h, sizeof(int));   // arrayの高さを出力
  fout.write((char*)&array_w, sizeof(int));   // arrayの幅を出力
  // 各SimDataのデータ出力
  for(a_y = 0; a_y < array_h; a_y++){     // arrayの縦
    for(a_x = 0; a_x < array_w; a_x++){   // arrayの横
      width = src->get_array_width(a_x, a_y);
      height = src->get_array_height(a_x, a_y);
      fout.write((char*)&height, sizeof(int));  // SimDataの高さを出力
      fout.write((char*)&width, sizeof(int));   // SimDataの幅を出力
      // データを出力
      for(y = 0; y < height; y++){   // 縦
	for(x = 0; x < width; x++){  // 横
	  data = src->get_data(a_x, a_y, x,y);
	  fout.write((char*)&data, sizeof(double));
	}
      }
    }
  }
  fout.close();

  return true;
}

/** 関数save_arrayで保存したデータをSimDataArray型のオブジェクトに格納する関数
 *
 * 関数save_arrayで保存したデータをSimDataArray型のオブジェクトに格納する関数。
 * 指定された関数save_arrayで保存したデータファイルから、データを読み込み
 * SimDataArray型のオブジェクトに格納する。ただし、指定したデータファイルが、
 * 本当に、関数save_arrayで保存したデータファイルかはチェックしないので
 * 注意すること。戻り値は、ファイルが開けたかどうかなので注意。
 * 
 * @param *dest Nistk::SimDataArray型ポインタ
 * @param *filename 入力ファイル名
 * @return ファイルオープンの成否
 */
bool Nistk::DataIO::load_array(Nistk::SimDataArray *dest, char *filename)
{
  std::ifstream fin;    // 出力ファイル用
  int array_h;          // SimDataArrayの高さ
  int array_w;          // SimDataArrayの幅
  int height;           // SimDataの高さ
  int width;            // SimDataの幅
  double data;          // データ読み出し用
  int a_x,a_y,x,y;

  // ファイルのオープン　失敗すると処理せずfalseをリターン
  fin.open(filename,std::ios_base::binary);
  if(!fin){
    std::cout << "Can not open " << filename << '\n';
    return false;
  }

  // SimDataArray型のデータの削除 データがない場合は何もしない
  dest->delete_array();
  // ファイルからSimDataArray型への格納
  fin.read((char*)&array_h, sizeof(int));   // arrayの高さを入力
  fin.read((char*)&array_w, sizeof(int));   // arrayの幅を入力
  // arrayの確保のためにとりあえず
  dest->create_data_size(array_w, array_h, 1, 1);
  // 各SimDataのデータの入力
  for(a_y = 0; a_y < array_h; a_y++){     // arrayの縦
    for(a_x = 0; a_x < array_w; a_x++){   // arrayの横
      fin.read((char*)&height, sizeof(int));      // SimDataの高さを入力
      fin.read((char*)&width, sizeof(int));       // SimDataの幅を入力
      dest->set_simdata(a_x, a_y, width, height); // SimDataのデータ領域確保
      // データを出力
      for(y = 0; y < height; y++){   // 縦
	for(x = 0; x < width; x++){  // 横
	  fin.read((char*)&data, sizeof(double));
	  dest->put_data(a_x, a_y, x, y, data);
	}
      }
    }
  }
  fin.close();

  return true;
}
