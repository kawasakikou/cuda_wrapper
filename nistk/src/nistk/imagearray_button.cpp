/**
 * @file  imagearray_button.cpp
 * @brief class for imagearray button used by nistk main window(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: imagearray_button.cpp,v.20110809 $
 *
 * Copyright (C) 2010-2011 Masakazu Komori
 */

#include<imagearray_button.h>
#include<imagefiles.h>
#include<gtkmm.h>
#include<imagedata.h>

/** コンストラクタ
 */
Nistk::ImageArrayButton::ImageArrayButton()
{
  // 一時使用の為の変数
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  int i,j;
   // ImageArrayの初期設定
  m_pixbuf=Gdk::Pixbuf::create_from_file(NOIMAGE);
  window.create_array_area(m_pixbuf->get_width(),m_pixbuf->get_height(),10,2,2);
  for(i = 0; i < 2; i++){
    for(j =0; j < 2; j++){
      window.set_pixbuf_to_array(m_pixbuf,j,i);
    }
  }
}

/** デストラクタ
 */
Nistk::ImageArrayButton::~ImageArrayButton()
{
  window.hide();
}

/** クリックされた時の処理(オーバーライド)
 *
 * ボタンがクリックされたときの処理をする関数。親クラスである
 * Gtk::Buttonのものをオーバーライドしている。これにより、
 * ボタンがクリックされたときにImageArrayを表示することが可能に
 * なっている。
 * 
 */
void Nistk::ImageArrayButton::on_clicked()
{
  Gtk::Button::on_clicked(); // 親クラスのメソッドの呼び出し
  if(window.is_window_show() == false){
    window.show();
    window.set_show_window(true);
  } else {
    window.hide();
    window.set_show_window(false);
  }
}

/** ボタンとウィンドウの名前をセットする関数
 *
 * ボタンとボタンと連動しているImageArrayウィンドウの名前をセットする関数。
 * 
 * @param name セットする名前のchar型ポインタ
 */
void Nistk::ImageArrayButton::set_name(const char *name)
{
  window.set_array_name(name);
  set_label(name);
}

/** ImageArrayのポインタを取得する関数
 *
 * このボタンが持っているImageArrayのポインタを取得する関数。
 * これにより、このボタンが持っているImageArrayを通常
 * のものと同じように扱うことが出来る。
 *
 * @return ImageArray型のポインタ
 */
Nistk::ImageArray* Nistk::ImageArrayButton::get_imagearray_ptr()
{
  return &window;
}
