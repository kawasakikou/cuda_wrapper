/**
 * @file  tools.cpp
 * @brief class for tools(実装)
 *
 * @author Masakazu Komori
 * @date 2011-05-04
 * @version $Id: tools.cpp,v.20110504 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */
#include<cmath>
#include<tools.h>

/** データの最大次数を求める関数
 *
 * データの最大次数を求める関数。例えば、引数data
 * の値が128.5であれば、次数は2となる。-0.025
 * であれば、次数は-2となる。0の場合は次数は0と
 * なる。
 *
 * @param data 調べるデータ
 * @return dataの最大次数
 */
int Nistk::Tools::calc_order(double data)
{
  double tmp_data;   // dataが小数の時のテンポラリ
  int order;         // データの次数
  int tmp,tmp_b;     // dataのテンポラリ

  // オーダのチェック
  if(data == 0.0){                                 // dataが0の時
     order = 0;
    return order;
  } else if(data >= 1.0 || data <= -1.0){         // dataが1以上または-1以下の時
    tmp = (int)data;
    order =0;
    for(;;){
      tmp_b = tmp;
      tmp = tmp/10;
      if(tmp == 0) return order;
      order++;
    }
  } else {                                         // dataが小数の時 
    tmp_data = data;
    order =-1;
    for(;;){
      tmp_data = tmp_data * 10.0;
      tmp = (int)tmp_data;
      if(tmp != 0) return order;
      order--;
    }
  }
}

/** orderで指定した次数からdataの下位の桁を0にする関数
 *
 * orderで指定した次数以下の桁を0にする関数。小数の場合は、
 * 指定した次数以下を取ってしまう。dataが1234で次数が1だと
 * 1200となる。0.1234で次数が-2の時は、0.1となる。
 *
 * @param data 切捨て対象のデータ
 * @param order 次数
 * @return 切り捨て後のデータ
 */
double Nistk::Tools::round_off_data(double data, int order)
{
  double r_data;
  int tmp_data;
  double tmp;

  tmp = data * pow(10.0, -1 * order);
  tmp_data = (int)tmp;
  if((double)(tmp_data+1) == tmp) tmp_data++; // intに変換した際の誤差補正 
  r_data = (double)(tmp_data * pow(10.0, order));

  return r_data;
}

/** dataの値を文字列に変換する関数
 *
 * dataの値を文字列に変換する関数。引数dataの値を文字列に変換して
 * 引数である文字列ポo_strに格納する関数。変換に際しては、引数
 * min_order+1もしくはlenght-1で指定したところまで行う。指数形式の歳は
 * min_order+1が仮数部の最小次数になる。形式は○.○E○○となる。また、
 * min_orderが負の時の丸め誤差対策として、dataを-min_order*10倍して
 * 0.005を加算して、元に戻すということをしている。よって、値によっては
 * 少しずれることがある。0.00019999999999の時とかは、0.0002となる場合もある。
 *
 * @param data 変換対象のデータ
 * @param o_str 変換結果を格納する文字列のポインタ
 * @param max_order 引数dataの最大次数
 * @param min_order 変換の最小次数
 * @param length 文字列長さ
 * @param mode 指数形式にするかどうか(trueで指数形式)
 */
void Nistk::Tools::num_to_str(double data, char *o_str, int max_order, 
			      int min_order, int length, bool mode)
{
  double buf;              // 変換に使うデータ用バッファ
  int i_buf;               // 変換対象となる数字用バッファ
  int ch_num;              // 文字列のカウンタ
  int order;               // 変換している文字のオーダ
  double e_order;          // 指数部のオーダ
  int buf2;

  // 変換前の処理
  ch_num = 0;
  e_order = (double)max_order;  // 指数モードの時に利用
  if(data < 0.0){      // dataが負の時
    o_str[ch_num] = '-';
    data = -1.0 * data;
    ch_num++;
  }
  // mode=falseでdataが小数の場合 
  if((mode == false) && (data > -1.0) && (data < 1.0)){ 
        o_str[ch_num] = '0';
	ch_num++;
	buf = data * 10;
	order = -1;
	max_order = -1;
  } else{  // それ以外
    // dataの値を-order乗して1の位からの値にする
    buf = data * pow(10, -1 * max_order);
    if(mode == true) order = 0;        // mode=true 指数モード
    else order = max_order;            // それ以外
  }

  // 数値を文字列に変換
  for(;;){
    if(order == -1){ // 小数部に入る前にピリオドを入れる
	o_str[ch_num] = '.';
	ch_num++;
    }
    // 指数モードのときの指数部分の変換のための処理
    if((mode == true) && (max_order == min_order)){
      // 仮数部の文字がピリオドで終わっていた時
      if(order == -1){
	o_str[ch_num] = '0';
	ch_num++;
      }
      o_str[ch_num] = 'E';
      ch_num++;
      // 指数部を変換用バッファに格納
      buf = e_order;
      if(buf < 0){  // 指数部が負の時
	o_str[ch_num] = '-';
	buf = -1.0 * buf;
	ch_num++;
      }
      max_order = calc_order(buf);
      buf = buf * pow(10, -1 * max_order);
      min_order = 0;
      order = max_order;
      mode = false;  // if文にかからないように。。
    }
    // 1の位の数値を取り、bufを小数の値にする
    // 丸め誤差対策 min_order分桁移動、四捨五入して元に戻す
    if(min_order < 0){
      i_buf = (int)((int)(buf*(-1*min_order)*10+0.005))/(10*(-1*min_order));
    } else {
      i_buf = (int)buf;
    }
    buf = buf - (double)i_buf;
    // 取り出した数値を文字に変換して文字列に格納
    switch(i_buf){
      case 0: o_str[ch_num] = '0';
	      break;
      case 1: o_str[ch_num] = '1';
	      break;
      case 2: o_str[ch_num] = '2';
	      break;
      case 3: o_str[ch_num] = '3';
	      break;
      case 4: o_str[ch_num] = '4';
	      break;
      case 5: o_str[ch_num] = '5';
	      break;
      case 6: o_str[ch_num] = '6';
	      break;
      case 7: o_str[ch_num] = '7';
	      break;
      case 8: o_str[ch_num] = '8';
	      break;
      case 9: o_str[ch_num] = '9';
	      break;
    }
    ch_num++;
    // 変換の終了判定
    if((ch_num == length-2) || (max_order == min_order)){
      // 整数部が途中で切れた(0が足りない)時の処置
      if((max_order == min_order) && (max_order > 0)){ 
	for(;;){
	  if((ch_num == length-2) || (max_order == 0)) break;
	  o_str[ch_num] = '0';
	  ch_num++;
	  max_order--;
	}
	break;
      } else break;
    }
    // 次の処理の為の処置
    order--;
    max_order--;
    buf = buf * 10.0;
  }
  o_str[ch_num] = '\0'; // 文字列最後に終端記号を挿入

  return;
}
