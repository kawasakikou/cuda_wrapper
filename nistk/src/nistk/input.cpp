/**
 * @file  input.cpp
 * @brief class for Tools of Generating Simulation Input(実装)
 *
 * @author Masakazu Komori
 * @date 2012-05-14
 * @version $Id: input.cpp,v.20120514 $
 *
 * Copyright (C) 2011-2012 Masakazu Komori
 */

#include<cmath>
#include<cstring>
#include<nistk/mathtools.h>
#include<input.h>

/** 楕円型のガウシアンの入力を得る関数
 *
 * 楕円形のガウス関数の入力を得る関数である。
 * 以下の式を計算してSimData型に格納する関数。ゲインであるaを
 * 指定できるようにしてある。原点は,SimData型の二次元配列の中心になる。
 * 単純に縦と横を2で割る(整数演算)だけなので,偶数のときは原点が
 * ずれることになるので注意が必要である。(例えば,7だと3,10だと5となる。
 * 配列は0からなので,奇数は中心,偶数は+1ずれる。)x_step,y_stepを1以外
 * の値にすると,計算では要素間の距離が1以外になる。(配列の添字は整数値で
 * 扱うことになるが。。）通常は,1で良いかと思う。計算上は,そのまま式を計算
 * するが配列の範囲外の値は無視される。
 * 
 * <br>
 \f[
    f(x)=\max_k\, a\,exp^{-\frac{[(x-x_{ck})cos(\phi_k)-(y-y_{ck})sin(\phi_k)]^2}{\sigma_{1k}^2}
                  -\frac{[(x-x_{ck})sin(\phi_k)+(y-y_{ck})cos(\phi_k)]^2}{\sigma_{2k}^2}}
 \f]
 * 注意：上式をよく見ればわかるが、計算は、計算点を回転させて、角度が0の時の
 * 楕円に対して計算しているので、元の計算点から見ると、時計回りに回転させた
 * 事になるので注意が必要である。
 *
 *
 * @param i_data データを格納するNistk::SimData型のポインタ
 * @param x_c ガウス関数の中心位置(x軸)のポインタ
 * @param y_c ガウス関数の中心位置(y軸)のポインタ
 * @param x_step 刻み幅(x軸)
 * @param y_step 刻み幅(y軸)
 * @param gain ゲイン(a)のポインタ
 * @param sigma1 幅(\f$\sigma 1 \f$)のポインタ
 * @param sigma2 幅(\f$\sigma 2 \f$)のポインタ
 * @param angle 角度(単位：ラジアン　\f$0 \leq \phi <\pi \f$)のポインタ
 * @param num 楕円形ガウシアンの個数
 */
void Nistk::Input::get_e_gaussian(Nistk::SimData *i_data,
				  double *x_c, double *y_c,
				  double x_step, double y_step,
				  double *gain, double *sigma1, double *sigma2, 
				  double *angle, int num)
{
  int col,row;             // 縦横のカウンタ
  int x_origin,y_origin;   // SimData型上での原点
  int width,height;        // SimData型上の幅と高さ
  double xd_start,yd_start;  // SimData型配列起点のでのx,y値
  double x_val,y_val;      // 計算上のx,yの値
  double val;              // Gaussian2の値
  double s_val;            // SimDataの値
  double *v_sin;           // sinの値
  double *v_cos;           // cosの値
  double f1, f2;
  int i;
  
  // sin,cosの計算
  v_sin = new double[num];
  v_cos = new double[num];
  for(i = 0; i < num; i++){
    v_sin[i] = sin(angle[i]);
    v_cos[i] = cos(angle[i]);
  }
  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  // SimData型の2次元配列上での原点を計算
  x_origin = width/2;
  y_origin = height/2;
  // 原点から見た左上角のx,y値
  xd_start = -(double)x_origin * x_step;
  yd_start = (double)y_origin * y_step;

  // DoGを計算してSimData型に格納
  x_val = xd_start; // 計算開始点のセット
  y_val = yd_start;
  for(row = 0; row < height; row++){ // 縦
    for(col = 0; col < width; col++){ // 横
      // gaussianを計算して最大値を格納(速度重視)
      f1 = ((x_val - x_c[0]) * v_cos[0]) - ((y_val - y_c[0]) * v_sin[0]);
      f1 = (f1 * f1) / (sigma1[0] * sigma1[0]);
      f2 = ((x_val - x_c[0]) * v_sin[0]) + ((y_val - y_c[0]) * v_cos[0]);
      f2 = (f2 * f2) / (sigma2[0] * sigma2[0]);
      s_val = gain[0] * exp(-1.0 * (f1 + f2));
      for(i = 1; i < num; i++){
	f1 = ((x_val - x_c[i]) * v_cos[i]) - ((y_val - y_c[i]) * v_sin[i]);
	f1 = (f1 * f1) / (sigma1[i] * sigma1[i]);
	f2 = ((x_val - x_c[i]) * v_sin[i]) + ((y_val - y_c[i]) * v_cos[i]);
	f2 = (f2 * f2) / (sigma2[i] * sigma2[i]);
	val = gain[i] * exp(-1.0 * (f1 + f2));
	if(val > s_val) s_val = val;
      }
      i_data->put_data(col, row, s_val);
      x_val += x_step;
    }
    x_val = xd_start; // 計算点のセット
    y_val -= y_step;
  }

  delete[] v_sin;
  delete[] v_cos;

  return;
}


/** 入力イメージを入力空間にセットする関数
 *
 * \image html create_input_image-fig.png
 * \image latex create_input_image-fig.eps "calculation image of set_input_image"
 * <br>
 * 　入力イメージを入力空間に配置する関数である。入力イメージ、入力空間
 * としているが、双方ともSimData型である。入力イメージは、
 * 画像を一旦SimData型に取り込み、それを配置するといった
 * 方法を取ることが出来る。また、この関数では、入力空間に関しては、
 * 配置する前に初期化は行わないので注意すること。逆にそうすることにより、
 * 複数のイメージを配置することが可能となっている。配置の際に関して、
 * 計算モードの指定の仕方によって得られる画像も変わってくるようになっている。
 * 原点は,SimData型の二次元配列の中心になる。
 * 単純に縦と横を2で割る(整数演算)だけなので,偶数のときは原点が
 * ずれることになるので注意が必要である。(例えば,7だと3,10だと5となる。
 * 配列は0からなので,奇数は中心,偶数は+1ずれる。)
 * <br>
 * 　入力イメージの配置後の点を計算して配置をすると、各点は整数でないと
 * いけないという性質から、配置後のイメージに穴空きのような写像されていない
 * 点ができるので、次のような手法を用いる。(参考http://homepage2.nifty.com
 * /tsugu/sotuken/rotation)<br>
 * 　要は、逆に配置後の点に対応する入力イメージの点を求めるという手法を使う。<br>
 * 通常の回転<br>
 * x2 = x1 * cos(A) - y1 * sin(A)<br>
 * y2 = x1 * sin(A) + y1 * cos(A)<br>
 * を<br>
 * x1 = x2 * cos(-A) - y2 * sin(-A)<br>
 * y1 = x2 * sin(-A) + y2 * cos(-A)<br>
 * この際の回転後のイメージの高さと幅は<br>
 * width_new = fabs(width * cos(A)) + fabs(height * sin(A))<br>
 * height_new = fabs(width * sin(A)) + fabs(height * cos(A))<br>
 * となる。
 * <br>
 * 計算モードopは<br>
 * "none": 入力イメージの値をSimDataにそのまま格納<br> 
 * "max" : 入力イメージの値とSimDataの値を比較した時の最大値を格納<br>
 * "min" : 入力イメージの値とSimDataの値を比較した時の最小値を格納<br>
 *
 * @param i_data 入力空間用Nistk::SimData型のポインタ
 * @param i_image 入力イメージ用Nistk::SimData型のポインタ
 * @param x_c 入力空間でのイメージの配置の中心位置(x軸)
 * @param y_c 入力空間でのイメージの配置の中心位置(y軸)
 * @param angle 角度(単位：ラジアン　\f$0 \leq \phi <\pi \f$)のポインタ
 * @param op 計算モード
 */
void Nistk::Input::set_input_image(Nistk::SimData *i_data, 
				   Nistk::SimData *i_image,
				   int x_c, int y_c, 
				   double angle, const char *op)
{
  int col,row;             // 縦横のカウンタ
  int x_origin,y_origin;   // 入力空間上での入力イメージの原点
  int xi_origin,yi_origin; // 入力イメージ内での原点
  int width,height;        // 入力空間用SimData型上の幅と高さ
  int i_width,i_height;    // イメージ用SimData型上の幅と高さ
  int i_width2,i_height2;  // イメージ配置後の幅と高さの幅
  int x,y;                 // イメージの配置前のx,y値
  int x_next,y_next;       // イメージの配置後のx,y値
  double dx,dy;            // イメージ中心からの配置前の相対位置
  double val;              // 入力イメージの値
  double v_sin;            // sinの値
  double v_cos;            // cosの値
  double s_val;              
  int mode;
  
  if(strcmp(op, "none") == 0) mode = 0;
  else if(strcmp(op,"max") == 0) mode = 1;
  else if(strcmp(op,"min") == 0) mode = 2;

  v_sin = sin(-1.0 * angle);
  v_cos = cos(-1.0 * angle);

  // SimData型の幅と高さを取得
  width = i_data->get_width();
  height = i_data->get_height();
  i_width = i_image->get_width();
  i_height = i_image->get_height();
  // 入力空間上での入力イメージの配置後の原点を計算
  // まずは入力空間の原点を求める
  x_origin = width/2;
  y_origin = height/2;
  // これより配置後の入力イメージの原点を求める
  x_origin += x_c;
  y_origin -= y_c;
  // 入力イメージ内での原点を求める
  xi_origin = i_width/2;
  yi_origin = i_height/2;
  // 入力イメージの配置後の幅と高さの半分の計算(切り上げにしてる)
  i_width2 = (int)(fabs(i_width * cos(angle)) 
		   + fabs(i_height * sin(angle)) + 1);
  i_width2 = i_width2 / 2;
  i_height2 = (int)(fabs(i_width * sin(angle)) 
		    + fabs(i_height * cos(angle)) + 1);
  i_height2 = i_height2 / 2;

  // 回転を用いて入力イメージを配置
  for(row = -1.0 * i_height2; row <= i_height2; row++){ // 縦
    for(col = -1.0 * i_width2; col <= i_width2; col++){ // 横
      // 入力空間内で座標計算
      x_next = x_origin + col;
      y_next = y_origin - row;
      // 入力イメージを中心に対して対応点を計算
      dx = col * v_cos - row * v_sin;
      dy = col * v_sin + row * v_cos;
      x = xi_origin + (int)dx;
      y = yi_origin - (int)dy;

      if((x >= 0) && (x < i_width) && (y >= 0) && (y < i_height)
	 && (x_next >= 0) && (x_next < width)
	 && (y_next >= 0) && (y_next < height)){
	val = i_image->get_data(x, y);
	// 配置位置の処置
	if(mode == 0){  
	  i_data->put_data(x_next, y_next, val);
	} else if(mode == 1){
	  s_val = i_data->get_data(x_next, y_next);
	  if(s_val <= val) i_data->put_data(x_next, y_next, val);
	} else if(mode == 2){
	  s_val = i_data->get_data(x_next, y_next);
	  if(s_val >= val) i_data->put_data(x_next, y_next, val);
	}
      } 
    }
  }

  return;
}
