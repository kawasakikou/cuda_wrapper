/**
 * @file  imagedata.cpp
 * @brief class for operate pixbuf(実装)
 *
 * @author Masakazu Komori
 * @date 2013-03-15
 * @version $Id: imagedata.cpp,v.20130315 $
 *
 * Copyright (C) 2008-2013 Masakazu Komori
 */

#include<string>
#include<cmath>
#include<gtkmm.h>
#include<imagedata.h>

/** pixbufの画像の幅を得る関数
 * 
 * @param i_pixbuf 幅を知りたいpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @return 画像の幅
 */
int Nistk::ImageData::pixbuf_get_width(
					Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  int width = i_pixbuf->get_width();
  return width;
}

/** pixbufの画像の高さを得る関数
 * 
 * @param i_pixbuf 高さを知りたいpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @return 画像の高さ
 */
int Nistk::ImageData::pixbuf_get_height(
					Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  int height = i_pixbuf->get_height();
  return height;
}

/** 幅と高さを指定して空のpixbufを生成する関数
 *
 * 幅と高さを指定して空のpixbufを生成し、RGBそれぞれ255
 * で初期化する。幅と高さは画像であれば,画像の
 * 幅と高さになる。
 *
 * @param width 生成するpixbufの幅
 * @param height 生成するpixbufの高さ
 * @return pixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
Glib::RefPtr<Gdk::Pixbuf> Nistk::ImageData::create_pixbuf_new(
						  int width, int height)
{
  Glib::RefPtr<Gdk::Pixbuf> i_pixbuf;
  i_pixbuf = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, 
				 false, 8, width, height);
  // ここからはピクセルデータの初期化
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset,gray_data;
  //  std::cout << "stride=" << stride << '\n';

  gray_data = 255;
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      offset = row*stride+col*channels;
      p_data[offset] = gray_data;
      p_data[offset+1] = gray_data;
      p_data[offset+2] = gray_data;
    }
  }

  return i_pixbuf;
}

/** pixbufのサイズを変更する関数
 *
 * 画像等を保持しているpixbufの大きさを変更する。 
 * 変更の際の画素間の補間は一番よいものを使っているので
 * 処理速度はそれなりに遅くなる。幅と高さは画像であれば,画像の
 * 幅と高さになる。
 *
 * @param i_pixbuf 変更対象となるpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @param width 変更後のpixbufの幅
 * @param height 変更後のpixbufの高さ
 * @return 変更後のpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
Glib::RefPtr<Gdk::Pixbuf> Nistk::ImageData::resize_pixbuf(
                 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf, int width, int height)
{
  i_pixbuf = i_pixbuf->scale_simple(width, height, Gdk::INTERP_HYPER);
  return i_pixbuf;
}

/** pixbufデータからグレースケールの０から1の値をとるデータを取得する関数
 *
 * pixbufが持っているRGBデータをグレースケール(0〜255)に変換し,
 * 0から１までのdouble型の値にして,SimData型変数に格納する関数。
 * pixbufの幅と高さとSimData型の幅と高さが同じでないといけない。
 * 変換モードは <br>
 * mode 0:CHROMA 1:AVG 2:YIQ <br>
 * となっている。通常使う場合はYIQが良いようである。
 *
 * @param *s_data pixbufからのデータを格納するSimData型変数のポインタ
 * @param i_pixbuf pixbufデータのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @param mode RGBデータからグレースケールへの変換モード
 */
void Nistk::ImageData::simdata_from_pixbuf(SimData *s_data,
				 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,int mode)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset,r_data,g_data,b_data,v_max,v_min,med;


  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
        // 1ピクセルあたりのデータ数から位置を計算し,RGBデータを取得
	offset = row*stride+col*channels;
	r_data = p_data[offset];
	g_data = p_data[offset+1];
	b_data = p_data[offset+2];
	if(mode==0){ // CHROMA 最大値と最小値の平均を使う
	  v_max=r_data;
	  if(v_max < g_data) v_max=g_data;
	  if(v_max < b_data) v_max=b_data;
	  v_min=r_data;
	  if(v_min > g_data) v_min=g_data;
	  if(v_min > b_data) v_min=b_data;
	  med = (int)(v_max+v_min)/2;
	  s_data->put_data(col, row,  ((double)med)/255.0);
	}
	else if(mode==1){ // AVG RGBの平均を使う
	  med = (int)(r_data + g_data + b_data)/3;
	  s_data->put_data(col, row,  ((double)med)/255.0);
	}
	else if(mode>=2){ // YIQ 重みつき計算
	  med = (int)(0.299*r_data + 0.587*g_data + 0.114*b_data);
	  s_data->put_data(col, row,  ((double)med)/255.0);
	}
    }
  }
  return;
}

/** pixbufデータから０から1の値をとるRGBデータを取得する関数
 *
 * pixbufが持っているRGBデータを0〜1のdouble型の値に変換し、
 * それぞれSimData型の変数へ格納する関数
 *
 * @param *r_data pixbufからのRデータを格納するSimData型変数のポインタ
 * @param *g_data pixbufからのGデータを格納するSimData型変数のポインタ
 * @param *b_data pixbufからのBデータを格納するSimData型変数のポインタ
 * @param i_pixbuf pixbufデータのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
void Nistk::ImageData::simdata_from_pixbuf_RGB(SimData *r_data, 
				 SimData *g_data, SimData *b_data,
				 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset;


  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
        // 1ピクセルあたりのデータ数から位置を計算し,RGBデータを取得
	offset = row*stride+col*channels;
	r_data->put_data(col, row,  (double)p_data[offset]/255.0);
	g_data->put_data(col, row,  (double)p_data[offset+1]/255.0);
	b_data->put_data(col, row,  (double)p_data[offset+2]/255.0);
    }
  }
  return;
}


/** pixbufデータからグレースケールの０から255の値をとる関数
 *
 * pixbufが持っているRGBデータをグレースケール(0〜255)に変換し,
 * GrayData型変数に格納する関数。
 * pixbufの幅と高さとGrayData型の幅と高さが同じでないといけない。
 * 変換モードは <br>
 * mode 0:CHROMA 1:AVG 2:YIQ <br>
 * となっている。通常使う場合はYIQが良いようである。
 *
 * @param *s_data pixbufからのデータを格納するGrayData型変数のポインタ
 * @param i_pixbuf pixbufデータのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @param mode RGBデータからグレースケールへの変換モード
 */
void Nistk::ImageData::graydata_from_pixbuf(GrayData *s_data,
				 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,int mode)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset,r_data,g_data,b_data,v_max,v_min,med;


  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
        // 1ピクセルあたりのデータ数から位置を計算し,RGBデータを取得
	offset = row*stride+col*channels;
	r_data = p_data[offset];
	g_data = p_data[offset+1];
	b_data = p_data[offset+2];
	if(mode==0){ // CHROMA 最大値と最小値の平均を使う
	  v_max=r_data;
	  if(v_max < g_data) v_max=g_data;
	  if(v_max < b_data) v_max=b_data;
	  v_min=r_data;
	  if(v_min > g_data) v_min=g_data;
	  if(v_min > b_data) v_min=b_data;
	  med = (int)(v_max+v_min)/2;
	  s_data->put_data(col, row, med);
	}
	else if(mode==1){ // AVG RGBの平均を使う
	  med = (int)(r_data + g_data + b_data)/3;
	  s_data->put_data(col, row, med);
	}
	else if(mode>=2){ // YIQ 重みつき計算
	  med = (int)(0.299*r_data + 0.587*g_data + 0.114*b_data);
	  s_data->put_data(col, row, med);
	}
    }
  }
  return;
}

/** SimDataからpixbufのグレースケールの画像データを格納する関数
 *
 * SimDataにあるデータを0〜255のグレースケールのデータに
 * 変換し,pixbufに格納する関数。
 * pixbufの幅と高さとSimData型の幅と高さが同じでないといけない。
 *
 * @param *s_data pixbufへデータを格納するSimData型変数のポインタ
 * @param i_pixbuf データを格納するpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
void Nistk::ImageData::pixbuf_from_simdata(SimData *s_data,
				 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset,gray_data;
  double v_max,v_min,diff,buf;

  // SimDataの最大値と最小値の検出
  v_max = s_data->get_data(0, 0);
  v_min = s_data->get_data(0, 0);
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      if(v_max < s_data->get_data(col, row)) 
	             v_max = s_data->get_data(col, row);
      if(v_min > s_data->get_data(col, row)) 
	             v_min = s_data->get_data(col, row);
    }
  }
  // 最大値と最小値の差を出す
  // if(v_max >=0) 
  //  else diff = -1.0*(v_max-v_min);  
  diff = v_max-v_min;

  // 0から255の値に変換して代入
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      // 1ピクセルあたりのデータを正規化してからグレーデータに
      // 変換して入力
      if(v_min < 0) buf = s_data->get_data(col, row) + (-1.0)*v_min;
      else buf = s_data->get_data(col, row) - v_min;
      gray_data = (int)((buf/diff)*255.0);
      offset = row*stride+col*channels;
      p_data[offset] = gray_data;
      p_data[offset+1] = gray_data;
      p_data[offset+2] = gray_data;
    }
  }

  return;
}

/** SimData型のデータからpixbufの各RGBの画像データを格納する関数
 *
 * SimDataにあるデータを0〜255のデータに換し,RGBを指定してpixbuf
 * に格納する関数。つまり、色を指定して格納することになる。
 * pixbufの幅と高さとSimData型の幅と高さが同じでないといけない。
 *
 * @param *s_data pixbufへデータを格納するSimData型変数のポインタ
 * @param i_pixbuf データを格納するpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @param mode RGB指定 0:R 1:G B:2
 */
void Nistk::ImageData::pixbuf_from_simdata_RGB(SimData *s_data,
				 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf, int mode)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset,out_data;
  double v_max,v_min,diff,buf;

  // SimDataの最大値と最小値の検出
  v_max = s_data->get_data(0, 0);
  v_min = s_data->get_data(0, 0);
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      if(v_max < s_data->get_data(col, row)) 
	             v_max = s_data->get_data(col, row);
      if(v_min > s_data->get_data(col, row)) 
	             v_min = s_data->get_data(col, row);
    }
  }
  // 最大値と最小値の差を出す
  // if(v_max >=0) 
  //  else diff = -1.0*(v_max-v_min);  
  diff = v_max-v_min;

  // 0から255の値に変換して代入
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      // 1ピクセルあたりのデータを正規化してからグレーデータに
      // 変換して入力
      if(v_min < 0) buf = s_data->get_data(col, row) + (-1.0)*v_min;
      else buf = s_data->get_data(col, row) - v_min;
      out_data = (int)((buf/diff)*255.0);
      offset = row*stride+col*channels;
      if(mode==0){
	p_data[offset] = out_data;
	p_data[offset+1] = 0;
	p_data[offset+2] = 0;
      }
      else if(mode==1){
	p_data[offset] = 0;
	p_data[offset+1] = out_data;
	p_data[offset+2] = 0;
      }
	else{
	  p_data[offset] = 0;
	  p_data[offset+1] = 0;
	  p_data[offset+2] = out_data;
	}
    }
  }

  return;
}

/** SimData型のデータからpixbufのRGBカラーの画像データを格納する関数
 *
 * SimDataにあるデータを0〜255のデータに換し,RGBカラー画像として
 * pixbufに格納する関数。
 * pixbufの幅と高さとSimData型の幅と高さが同じでないといけない。
 *
 * @param *r_data pixbufへデータを格納するRのSimData型変数のポインタ
 * @param *g_data pixbufへデータを格納するGのSimData型変数のポインタ
 * @param *b_data pixbufへデータを格納するBのSimData型変数のポインタ
 * @param i_pixbuf データを格納するpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
void Nistk::ImageData::pixbuf_from_simdata_RGB_full(SimData *r_data, 
    SimData *g_data, SimData *b_data, Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset;
  double rv_max,rv_min,r_diff,r_buf;
  double gv_max,gv_min,g_diff,g_buf;
  double bv_max,bv_min,b_diff,b_buf;

  // SimDataの最大値と最小値の検出
  rv_max = r_data->get_data(0, 0);
  gv_max = g_data->get_data(0, 0);
  bv_max = b_data->get_data(0, 0);
  rv_min = r_data->get_data(0, 0);
  gv_min = g_data->get_data(0, 0);
  bv_min = b_data->get_data(0, 0);
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      // R
      if(rv_max < r_data->get_data(col, row)) 
	             rv_max = r_data->get_data(col, row);
      if(rv_min > r_data->get_data(col, row)) 
	             rv_min = r_data->get_data(col, row);
      // G
      if(gv_max < g_data->get_data(col, row)) 
	             gv_max = g_data->get_data(col, row);
      if(gv_min > g_data->get_data(col, row)) 
	             gv_min = g_data->get_data(col, row);
      // B
      if(bv_max < b_data->get_data(col, row)) 
	             bv_max = b_data->get_data(col, row);
      if(bv_min > b_data->get_data(col, row)) 
	             bv_min = b_data->get_data(col, row);
    }
  }
  // 最大値と最小値の差を出す
  // if(v_max >=0) 
  //  else diff = -1.0*(v_max-v_min);  
  r_diff = rv_max-rv_min;
  g_diff = gv_max-gv_min;
  b_diff = bv_max-bv_min;

  // 0から255の値に変換して代入
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      // 1ピクセルあたりのデータを正規化してからグレーデータに
      // 変換して入力
      // R
      if(rv_min < 0) r_buf = r_data->get_data(col, row) + (-1.0)*rv_min;
      else r_buf = r_data->get_data(col, row) - rv_min;
      // G
      if(gv_min < 0) g_buf = g_data->get_data(col, row) + (-1.0)*gv_min;
      else g_buf = g_data->get_data(col, row) - gv_min;
      // B
      if(bv_min < 0) b_buf = b_data->get_data(col, row) + (-1.0)*bv_min;
      else b_buf = b_data->get_data(col, row) - bv_min;
      // データのセット
      offset = row*stride+col*channels;
      p_data[offset] =(int)((r_buf/r_diff)*255.0);
      p_data[offset+1] =(int)((g_buf/g_diff)*255.0);
      p_data[offset+2] =(int)((b_buf/b_diff)*255.0);
    }
  }

  return;
}

/** GrayDataからpixbufのグレースケールの画像データを格納する関数
 *
 * GrayDataにあるデータを0〜255のグレースケールのデータに
 * 変換し,pixbufに格納する関数。
 * pixbufの幅と高さとGrayData型の幅と高さが同じでないといけない。
 *
 * @param *s_data pixbufへデータを格納するGrayData型変数のポインタ
 * @param i_pixbuf データを格納するpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 */
void Nistk::ImageData::pixbuf_from_graydata(GrayData *s_data,
				 Glib::RefPtr<Gdk::Pixbuf> i_pixbuf)
{
  guint8 *p_data = i_pixbuf->get_pixels();    // RGBデータの開始位置
  int channels = i_pixbuf->get_n_channels();  // 1pixel当たりのデータ数
  int width = i_pixbuf->get_width();          // 画像データの幅
  int height = i_pixbuf->get_height();        // 画像データの高さ
  int stride = i_pixbuf->get_rowstride();     // 1行当たりのデータ数
  int offset,gray_data;

  // グレーデータを代入
  for(int row=0; row<height; row++){ // 行 y
    for(int col=0; col<width; col++){ // 列 x
      gray_data = s_data->get_data(col, row);
      offset = row*stride+col*channels;
      p_data[offset] = gray_data;
      p_data[offset+1] = gray_data;
      p_data[offset+2] = gray_data;
    }
  }
  return;
}

/** pixbufデータをファイルに保存する関数
 *
 * pixbufにあるデータをファイルに保存する関数。ファイル名に
 * 拡張子がなくても自動で付ける様になっている。保存ファイル形式
 * を省略して使うこともできる。その場合は png形式になる。
 * 使えるファイル形式はgtkmmのpixbufが対応しているファイル形式になる。
 * 例えば,png,jpeg,bmpなど
 *
 * @param i_pixbuf 保存したいpixbufのGlib::RefPtr<Gdk::Pixbuf>型ポインタ
 * @param *i_file_name 保存ファイル名
 * @param *i_file_type ファイルの保存形式
 */
void Nistk::ImageData::save_pixbuf(Glib::RefPtr<Gdk::Pixbuf> i_pixbuf,
				   const char *i_file_name, 
				   const char *i_file_type)
{
  int t_pos;
  std::string i_file,i_type;

  // 文字列をstringクラスにセット
  // こうした方があとが楽なので。。
  i_file=i_file_name;
  i_type=i_file_type;

  // ファイル名の拡張子となるべき開始位置
  // を決めて,ファイルタイプと比較
  // 拡張子がない場合にはそれを追加
  t_pos = i_file.length()-i_type.length();
  for(int i=0; i < i_type.length(); i++){
    // i_fileが拡張子+2より小さいときはチェックせずに
    // 拡張子を付ける
    if(t_pos < 2){
      i_file += '.';
      i_file += i_type;
      break;
    }
    if(i_file[t_pos+i] != i_type[i]){
      i_file += '.';
      i_file += i_type;
      break;
    }
  }

  // pixbufをセーブ
  i_pixbuf->save(i_file.c_str(), i_type.c_str());

  return;
}

/** SimDataのデータを画像ファイルとして保存する関数
 *
 * SimDataに格納されている二次元平面上のデータを0〜255の
 * グレースケールのデータに変換し画像として保存する関数。
 * 引数のファイル名は拡張子を付けなくても良いようになっている。
 * 使えるファイル形式はgtkmmのpixbufが対応しているファイル形式になる。
 * 例えば,png,jpeg,bmpなど。また、シミュレーションでの途中経過を
 * 保存したい場合にも使いやすいように、ファイル名の前もしくは後に
 * 数値を付けれるようにしてある。(例：test0023.45.png,0010test.png) 
 * ただし、汎用性を考えて引数はdouble型としている。実数を文字に変換
 * する際にたまに値がずれるので注意すること。(例:23.45→23.449)
 * 整数と少数の桁数指定も必要となっている。モードの指定は以下の
 * とおりである。<br><br>
 *
 * mode: 0:数字を付けない 
 *       1:ファイル名の後に数字をつける 
 *       2:ファイル名の前に数字をつける 
 *
 * @param *in 保存したいNistk::SimData型のポインタ
 * @param *i_file_name 保存ファイル名
 * @param *i_file_type ファイルの保存形式
 * @param mode ファイル名に数字を付けるどうかのモード
 * @param num ファイル名に付ける数字
 * @param major 整数部の桁数
 * @param minor 小数部の桁数
 */
void Nistk::ImageData::imagefile_from_simdata(Nistk::SimData *in, 
			    const char *i_file_name,
			    const char *i_file_type,
			    int mode, double num, int major, int minor)
{
  Glib::RefPtr<Gdk::Pixbuf> m_pixbuf;
  std::string i_file;    // 保存ファイル名格納用
  std::string tmp;       // 数字の文字列一時格納用
  double tmp_num1;
  int tmp_num2;
  int i;

  // ファイル名をいったんstringに代入
  i_file = i_file_name;
  // ファイル名に数字をつける時の処理(doubleの関係で少数が少し変になる時あり)
  // 例 23.45が23.4499となる
  if(mode !=0){
    // 整数部の処理
    tmp_num1 = pow(10,major-1);
    for(i = 0; i < major; i++){
      tmp_num2 = (int)(num / tmp_num1);
      num -= (double)(tmp_num2*tmp_num1);
      switch(tmp_num2){
        case 0: tmp += '0'; break;
        case 1: tmp += '1'; break;
        case 2: tmp += '2'; break;
        case 3: tmp += '3'; break;
        case 4: tmp += '4'; break;
        case 5: tmp += '5'; break;
        case 6: tmp += '6'; break;
        case 7: tmp += '7'; break;
        case 8: tmp += '8'; break;
        case 9: tmp += '9'; break;
      }
      tmp_num1 /= 10.0;
    }
    // 小数部の処理
    if(minor != 0) tmp +='.';
    for(i = 0; i < minor; i++){
      tmp_num2 = (int)(num * 10.0);
      num = (num * 10.0) - (double)(tmp_num2);
      switch(tmp_num2){
        case 0: tmp += '0'; break;
        case 1: tmp += '1'; break;
        case 2: tmp += '2'; break;
        case 3: tmp += '3'; break;
        case 4: tmp += '4'; break;
        case 5: tmp += '5'; break;
        case 6: tmp += '6'; break;
        case 7: tmp += '7'; break;
        case 8: tmp += '8'; break;
        case 9: tmp += '9'; break;
      }
    }
    if(mode == 1) i_file += tmp;
    else if(mode == 2) i_file = tmp + i_file;
  }
 
  // pixbufにSimDataを格納してイメージファイルとして保存
  m_pixbuf = Nistk::ImageData::create_pixbuf_new(in->get_width(),
						  in->get_height());
  Nistk::ImageData::pixbuf_from_simdata(in, m_pixbuf);
  Nistk::ImageData::save_pixbuf(m_pixbuf, i_file.c_str(), i_file_type);

  return;
}
