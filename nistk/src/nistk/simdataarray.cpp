/**
 * @file  simdataarray.cpp
 * @brief class for SimData array(実装)
 *
 * @author Masakazu Komori
 * @date 2017-09-01
 * @version $Id: simdataarray.cpp,v.20170901 $
 *
 * Copyright (C) 2009-2017 Masakazu Komori
 */
#include<simdata.h>
#include<simdataarray.h>

/** コンストラクタ 引数無し
 */
Nistk::SimDataArray::SimDataArray()
{
  height = 0;
  width = 0;
  length = 0;
}

/** コンストラクタ 配列の確保もする SimData配列の大きさは皆同じになる
 */
Nistk::SimDataArray::SimDataArray(int w,int h, int w_w, int w_h)
{
  height = 0;
  width = 0;
  length = 0;
  create_data_size(w, h, w_w, w_h);
}

/** コピーコンストラクタ 基本使わないこと! 処理が遅くなる
 */
Nistk::SimDataArray::SimDataArray(const Nistk::SimDataArray &obj)
{
  int i,j,k,l;
  int w_height,w_width;

  height = obj.height;     // 高さのコピー
  width = obj.width;       // 幅のコピー
  length = obj.length;     // 長さのコピー
  // w_array2d = new Nistk::SimData*[height];   // 配列の確保
  // for(i = 0; i < height; i++) w_array2d[i] = new Nistk::SimData[width];
  w_array2d = new Nistk::SimData*[height];    // 配列の確保
  w_array2d[0] = new Nistk::SimData[height * width];
  for(i = 1; i < height; i++) w_array2d[i] = w_array2d[0] + (i * width);

  // 配列要素の格納
  for(i=0; i < height; i++){  // 縦軸 y
    for(j=0; j < width; j++){  // 横軸 x
      w_height = obj.w_array2d[i][j].get_height();
      w_width = obj.w_array2d[i][j].get_width();
      w_array2d[i][j].create_data_size(w_width, w_height);
      for(k=0; k < w_height; k++){ // 重みの縦
	for(l=0; l < w_width; l++){ // 重みの横
	  w_array2d[i][j].put_data(l, k, 
				      obj.w_array2d[i][j].get_data(l,k));
	}
      }
    }
  }
}

/** デストラクタ
 */
Nistk::SimDataArray::~SimDataArray()
{
  delete_array();
}

/** 代入演算子=のオーバーロード 基本使わないこと!使うと処理が遅くなる
 */
Nistk::SimDataArray &Nistk::SimDataArray::operator=(Nistk::SimDataArray &obj)
{
  int i,j,k,l;
  int w_height,w_width;

  // 代入先にデータがあるときにデータをdeleteしておかないと
  // まずいのでまずデータを消去する。
  this->delete_array();

  this->height = obj.height;     // 高さのコピー
  this->width = obj.width;       // 幅のコピー
  this->length = obj.length;     // 長さのコピー
  // this->w_array2d = new Nistk::SimData*[this->height];   // 配列の確保
  // for(i = 0; i < this->height; i++) 
  //   this->w_array2d[i] = new Nistk::SimData[this->width];
  this->w_array2d = new Nistk::SimData*[this->height];   // 配列の確保
  this->w_array2d[0] = new Nistk::SimData[this->height * this->width];
  for(i = 1; i < this->height; i++) 
    this->w_array2d[i] = this->w_array2d[0] + (i * this->width);

  // 配列要素の格納
  for(i=0; i<this->height; i++){  // 縦軸 y
    for(j=0; j<this->width; j++){  // 横軸 x
      w_height = obj.w_array2d[i][j].get_height();
      w_width = obj.w_array2d[i][j].get_width();
      this->w_array2d[i][j].create_data_size(w_width, w_height);
      for(k=0; k < w_height; k++){ // 重みの縦
	for(l=0; l < w_width; l++){ // 重みの横
	  this->w_array2d[i][j].put_data(l, k,
			     obj.w_array2d[i][j].get_data(l,k));
	}
      }
    }
  }

  return *this;
}

/** SimDataArrayにあるデータを消去する関数
 *
 * 現在持っているデータを全て消去する関数。
 * データそのものを消去するだけでなく,確保している
 * データ領域そのものをdeleteしてしまう。
 * 次に使うときにはデータ領域の再確保をする必要がある。
 */
void Nistk::SimDataArray::delete_array()
{
  int i,j;

  // 配列要素の削除
  if(length != 0){
    for(i = 0; i < height; i++){  // 縦軸 y
	for(j = 0; j < width; j++){  // 横軸 x
	    w_array2d[i][j].delete_data();
	}
    }
    // for(i = 0; i < height; i++){
    //  delete[] w_array2d[i];
    // }
    // delete[] w_array2d;
    delete[] w_array2d[0];
    delete[] w_array2d;
    width = 0;
    height = 0;
    length = 0;
  }

  return;
}

/** 配列の確保をする関数  SimData配列の大きさは皆同じになる
 *
 * SimDataArrayのデータ領域の確保をする関数。SimDataArray
 * はSimData型のオブジェクトを配列(二次元配列)として扱って
 * いるだけなので,その配列と各SimData型のデータ領域を確保している。
 * ただし、データの初期化をしてはいないので注意すること。 
 *
 * @param w SimData型二次元配列の幅
 * @param h SimData型二次元配列の高さ
 * @param w_w 各SimData型の幅
 * @param w_h 各SimData型の幅
 */
void Nistk::SimDataArray::create_data_size(int w,int h, int w_w, int w_h)
{
  int i,j;
  int w_height,w_width;
  
  delete_array();
  height = h;
  width = w;
  length = h * w;
  w_height = w_h;
  w_width = w_w;
  
  //  w_array2d = new Nistk::SimData*[height];
  // for(i = 0; i < height; i++) w_array2d[i] = new Nistk::SimData[width];
  w_array2d = new Nistk::SimData*[height];    // 配列の確保
  w_array2d[0] = new Nistk::SimData[height * width];
  for(i = 1; i < height; i++) w_array2d[i] = w_array2d[0] + (i * width);

  for(i=0; i < h; i++){ //縦
    for(j=0; j < w; j++){ // 横
      w_array2d[i][j].create_data_size(w_width, w_height);
    }
  }
}

/** 配列の大きさを変更する関数 データはきえてしまい,大きさは皆同じになる
 *
 * SimDataArrayのデータ領域をいったんdeleteして,再確保をする関数。
 * SimDataArrayはSimData型のオブジェクトを配列(二次元配列)として扱って
 * いるだけなので,その配列と各SimData型のデータ領域を確保している。
 * ただし、データの初期化をしてはいないので注意すること。 
 *
 * @param w SimData型二次元配列の幅
 * @param h SimData型二次元配列の高さ
 * @param w_w 各SimData型の幅
 * @param w_h 各SimData型の幅
 */
void Nistk::SimDataArray::recreate_data_size(int w,int h, int w_w, int w_h)
{
  create_data_size(w, h, w_w, w_h);
}

/**  SimData型配列の高さを取得する関数
 *
 * 現在のSimDataArray(SimData型の二次元配列)の高さを取得する関数。
 *
 * @return SimDataArrayの高さ
 */
int Nistk::SimDataArray::get_height()
{
  return height;
}

/**  SimData型配列の幅を取得する関数
 *
 * 現在のSimDataArray(SimData型の二次元配列)の幅を取得する関数。
 *
 * @return SimDataArrayの幅
 */
int Nistk::SimDataArray::get_width()
{
  return width;
}

/** SimData型配列の長さを取得する関数
 *
 * 現在のSimDataArray(SimData型の二次元配列)を1次元配列として
 * 見た時の配列の長さを取得する関数。
 *
 *  @return SimData型配列を一次元配列とした時の長さ
 */
int Nistk::SimDataArray::get_length()
{
  return length;
}

/**  位置x,yのSimData型オブジェクトの幅を取得する関数
 *
 * 現在のSimDataArray(SimData型の二次元配列)の位置x,yの
 * SimData型オブジェクトの幅を取得する関数。
 * 位置指定は左上が原点となる。
 *
 * @param x SimDataArray(SimData型の二次元配列)の横位置
 * @param y SimDataArray(SimData型の二次元配列)の縦位置
 * @return SimData型オブジェクトの幅
 */
int Nistk::SimDataArray::get_array_width(int x, int y)
{
  return w_array2d[y][x].get_width();
}

/**  位置x,yのSimData型オブジェクトの高さを取得する関数
 *
 * 現在のSimDataArray(SimData型の二次元配列)の位置x,yの
 * SimData型オブジェクトの高さを取得する関数。
 * 位置指定は左上が原点となる。
 *
 * @param x SimDataArray(SimData型の二次元配列)の横位置
 * @param y SimDataArray(SimData型の二次元配列)の縦位置
 * @return SimData型オブジェクトの高さ
 */
int Nistk::SimDataArray::get_array_height(int x, int y)
{
  return w_array2d[y][x].get_height();
}

/**  位置x,yのSimData型オブジェクト内の位置w_x,w_yの値を取得する関数
 *
 * 位置x,yのSimData型オブジェクト内の位置w_x,w_yの値を取得する関数。
 * 位置指定は左上が原点となる。
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 * @param w_x SimData型オブジェクト内の横の位置
 * @param w_y SimData型オブジェクト内の縦の位置
 * @return 位置x,yのSimData型オブジェクト内の位置w_x,w_yの値
 */
double Nistk::SimDataArray::get_data(int x, int y, int w_x, int w_y)
{
  return w_array2d[y][x].get_data(w_x,w_y);
}

/**  位置x,yのSimData型オブジェクト内の位置w_x,w_yにデータを格納する関数
 *
 * 位置x,yのSimData型オブジェクト内の位置w_x,w_yに値を格納する関数。
 * 位置指定は左上が原点となる。
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 * @param w_x SimData型オブジェクト内の横の位置
 * @param w_y SimData型オブジェクト内の縦の位置
 * @param i_data 格納するデータ
 */
void Nistk::SimDataArray::put_data(int x,int y,int w_x,int w_y,double i_data)
{
  w_array2d[y][x].put_data(w_x, w_y, i_data);

  return;
}

/**  位置x,yのSimData型オブジェクトのポインタを取得する関数
 *
 * 位置x,yのSimData型オブジェクト内のポインタを取得する関数。
 * 位置指定は左上が原点となる。返り値はあくまでポインタで
 * あるのでそこは注意すること。
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 * @return 位置x,yのSimData型オブジェクトのポインタ
 */
Nistk::SimData* Nistk::SimDataArray::get_simdata_ptr(int x, int y)
{
  return &w_array2d[y][x];
}

/**  位置x,yのSimData型オブジェクトのデータを消去する関数
 *
 * 位置x,yのSimData型オブジェクト内のデータを消去する関数。
 * 位置指定は左上が原点となる。データ領域をdeleteするだけなので,
 * SimDataそのものはなくならない
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 */
void Nistk::SimDataArray::delete_simdata(int x, int y)
{
  w_array2d[y][x].delete_data();

  return;
}

/**  位置x,yのSimData型オブジェクトのデータをセットする関数  0で初期化
 *
 * 位置x,yのSimData型オブジェクト内のデータをセットする関数。
 * 位置指定は左上が原点となる。指定した大きさで位置x,yのSimData型
 * オブジェクトのデータ領域を確保し、確保したデータ領域の値は0で初期化
 * される。
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 * @param w_w 確保するSimData型オブジェクトのデータ領域の幅
 * @param w_h 確保するSimData型オブジェクトのデータ領域の幅
 */
void Nistk::SimDataArray::set_simdata(int x, int y, int w_w, int w_h)
{
  int i,j,h,w;

  h = w_array2d[y][x].get_height();
  w = w_array2d[y][x].get_width();
  if((h == 0) && (w == 0)) w_array2d[y][x].create_data_size(w_w,w_h);
  else w_array2d[y][x].recreate_data_size(w_w,w_h);

  for(i=0; i<w_h; i++){ // 縦
    for(j=0; j<w_w; j++){ // 横
      w_array2d[y][x].put_data(j,i,0.0);
    }
  }

  return;
}

/**  位置x,yのSimData型オブジェクトのデータをセットする関数  i_SimDataで初期化
 *
 * 位置x,yのSimData型オブジェクト内のデータをセットする関数。
 * 位置指定は左上が原点となる。指定した大きさで位置x,yのSimData型
 * オブジェクトのデータ領域を確保し、確保したデータ領域の値は
 * 引数で指定されたSimData型のデータがコピーされる。
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 * @param i_SimData コピーするSimData型オブジェクトのポインタ
 */
void Nistk::SimDataArray::set_simdata(Nistk::SimData *i_SimData, int x, int y)
{
  int i,j,h,w,i_h,i_w;

  h = w_array2d[y][x].get_height();
  w = w_array2d[y][x].get_width();
  i_h = i_SimData->get_height();
  i_w = i_SimData->get_width();
  if((h == 0) && (w == 0)) w_array2d[y][x].create_data_size(i_w,i_h);
  else w_array2d[y][x].recreate_data_size(i_w,i_h);

  for(i=0; i<i_h; i++){ // 縦
    for(j=0; j<i_w; j++){ // 横
      w_array2d[y][x].put_data(j,i,i_SimData->get_data(j,i));
    }
  }

  return;
}

/**  位置x,yのSimData型オブジェクトのデータをコピーする関数 
 *
 * 位置x,yのSimData型オブジェクトのデータをコピーする関数。
 * 位置指定は左上が原点となる。指定した位置x,yのSimData型
 * オブジェクトのデータが引数で指定されたSimData型オブジェクト
 * にコピーされる。
 *
 * @param x SimDataArray内の横の位置
 * @param y SimDataArray内の縦の位置
 * @param i_SimData コピーするデータを格納するSimData型オブジェクトのポインタ
 */
void Nistk::SimDataArray::get_simdata(int x, int y, Nistk::SimData *i_SimData)
{
  int i,j,h,w,i_h,i_w;

  h = w_array2d[y][x].get_height();
  w = w_array2d[y][x].get_width();
  i_h = i_SimData->get_height();
  i_w = i_SimData->get_width();
  if((i_h == 0) && (i_w == 0)) i_SimData->create_data_size(w,h);
  else i_SimData->recreate_data_size(w,h);

  for(i=0; i<h; i++){ // 縦
    for(j=0; j<w; j++){ // 横
      i_SimData->put_data(j,i,w_array2d[y][x].get_data(j,i));
    }
  }

  return;
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトの幅を取得する関数
 *
 * 現在のSimDataArrayを１次元配列として見た時の位置xの
 * SimData型オブジェクトの幅を取得する関数
 *
 * @param x 一次元配列での位置
 * @return SimData型オブジェクトの幅
 */
int Nistk::SimDataArray::get_array_width1d(int x)
{
  int h,w;

  h = x / width;
  w = x % width;

  return w_array2d[h][w].get_width();
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトの高さを取得する関数
 *
 * 現在のSimDataArrayを１次元配列として見た時の位置xの
 * SimData型オブジェクトの高さを取得する関数
 *
 * @param x 一次元配列での位置
 * @return SimData型オブジェクトの高さ
 */
int Nistk::SimDataArray::get_array_height1d(int x)
{
  int h,w;

  h = x / width;
  w = x % width;

  return w_array2d[h][w].get_height();
}

/** 一次元配列のとした時の位置xのSimData型オブジェクト内の位置w_x,w_yの値を取得する関数
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクト内の位置w_x,w_yの値を取得する関数。
 * 位置指定は左上が原点となる。
 *
 * @param x 一次元配列での位置
 * @param w_x SimData型オブジェクト内の横の位置
 * @param w_y SimData型オブジェクト内の縦の位置
 * @return 位置xのSimData型オブジェクト内の位置w_x,w_yの値
 */
double Nistk::SimDataArray::get_data1d(int x, int w_x, int w_y)
{
  int h,w;

  h = x / width;
  w = x % width;

  return w_array2d[h][w].get_data(w_x,w_y);
}

/** 一次元配列のとした時の位置xのSimData型オブジェクト内の位置w_x,w_yにデータを格納する関数
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクト内の位置w_x,w_yに値を格納する関数。
 * 位置指定は左上が原点となる。
 *
 * @param x 一次元配列での位置
 * @param w_x SimData型オブジェクト内の横の位置
 * @param w_y SimData型オブジェクト内の縦の位置
 * @param i_data 格納するデータ
 */
void Nistk::SimDataArray::put_data1d(int x, int w_x, int w_y, double i_data)
{
  int h,w;

  h = x / width;
  w = x % width;

  w_array2d[h][w].put_data(w_x, w_y, i_data);

  return;
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトのポインタを取得する関数
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクト内のポインタを取得する関数。
 * 位置指定は左上が原点となる。返り値はあくまでポインタで
 * あるのでそこは注意すること。
 *
 * @param x 一次元配列での位置
 * @return 位置xのSimData型オブジェクトのポインタ
 */
Nistk::SimData* Nistk::SimDataArray::get_simdata_ptr1d(int x)
{
  int h,w;

  h = x / width;
  w = x % width;

  return &w_array2d[h][w];
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトのデータを消去する関数
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクト内のデータを消去する関数。
 * 位置指定は左上が原点となる。データ領域をdeleteするだけなので,
 * SimDataそのものはなくならない
 *
 * @param x 一次元配列での位置
 */
void Nistk::SimDataArray::delete_simdata1d(int x)
{
  int h,w;

  h = x / width;
  w = x % width;

  w_array2d[h][w].delete_data();

  return;
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトのデータをセットする関数  0で初期化
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクト内のデータをセットする関数。
 * 位置指定は左上が原点となる。指定した大きさで位置x,yのSimData型
 * オブジェクトのデータ領域を確保し、確保したデータ領域の値は0で初期化
 * される。
 *
 * @param x 一次元配列での位置
 * @param w_w 確保するSimData型オブジェクトのデータ領域の幅
 * @param w_h 確保するSimData型オブジェクトのデータ領域の幅
 */
void Nistk::SimDataArray::set_simdata1d(int x, int w_w, int w_h)
{
  int i,j,h,w;
  int ah,aw;

  ah = x / width;
  aw = x % width;

  h = w_array2d[ah][aw].get_height();
  w = w_array2d[ah][aw].get_width();
  if((h == 0) && (w == 0)) w_array2d[ah][aw].create_data_size(w_w,w_h);
  else w_array2d[ah][aw].recreate_data_size(w_w,w_h);

  for(i=0; i<w_h; i++){ // 縦
    for(j=0; j<w_w; j++){ // 横
      w_array2d[ah][aw].put_data(j,i,0.0);
    }
  }

  return;
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトのデータをセットする関数  i_SimDataで初期化
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクト内のデータをセットする関数。
 * 位置指定は左上が原点となる。指定した大きさで位置x,yのSimData型
 * オブジェクトのデータ領域を確保し、確保したデータ領域の値は
 * 引数で指定されたSimData型のデータがコピーされる。
 *
 * @param i_SimData コピーするSimData型オブジェクトのポインタ
 * @param x 一次元配列での位置
 */
void Nistk::SimDataArray::set_simdata1d(Nistk::SimData *i_SimData, int x)
{
  int i,j,h,w,i_h,i_w;
  int ah,aw;

  ah = x / width;
  aw = x % width;

  h = w_array2d[ah][aw].get_height();
  w = w_array2d[ah][aw].get_width();
  i_h = i_SimData->get_height();
  i_w = i_SimData->get_width();
  if((h == 0) && (w == 0)) w_array2d[ah][aw].create_data_size(i_w,i_h);
  else w_array2d[ah][aw].recreate_data_size(i_w,i_h);

  for(i=0; i<i_h; i++){ // 縦
    for(j=0; j<i_w; j++){ // 横
      w_array2d[ah][aw].put_data(j,i,i_SimData->get_data(j,i));
    }
  }

  return;
}

/** 一次元配列のとした時の位置xのSimData型オブジェクトのデータをコピーする関数
 *
 * 一次元配列のとした時の位置xのSimData型オブジェクトのデータをコピーする関数。
 * 位置指定は左上が原点となる。指定した位置x,yのSimData型
 * オブジェクトのデータが引数で指定されたSimData型オブジェクト
 * にコピーされる。
 *
 * @param x 一次元配列での位置
 * @param i_SimData コピーするデータを格納するSimData型オブジェクトのポインタ
 */
void Nistk::SimDataArray::get_simdata1d(int x, Nistk::SimData *i_SimData)
{
  int i,j,h,w,i_h,i_w;
  int ah,aw;

  ah = x / width;
  aw = x % width;

  h = w_array2d[ah][aw].get_height();
  w = w_array2d[ah][aw].get_width();
  i_h = i_SimData->get_height();
  i_w = i_SimData->get_width();
  if((i_h == 0) && (i_w == 0)) i_SimData->create_data_size(w,h);
  else i_SimData->recreate_data_size(w,h);

  for(i=0; i<h; i++){ // 縦
    for(j=0; j<w; j++){ // 横
      i_SimData->put_data(j,i,w_array2d[ah][aw].get_data(j,i));
    }
  }

  return;
}
