/**
 * @file  ivneuron.cpp
 * @brief class for Izhikevich's Simple Spiking Neuron(実装)
 *
 * @author Masakazu Komori
 * @date 2011-09-10
 * @version $Id: ivneuron.cpp,v.20110910 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<nistk/runge.h>
#include<nistk/ivneuron.h>
#include<nistk/ivvariable.h>
#include<nistk/ivparameter.h>

/** Nistk::Neuron::IVVariableの初期化用関数
 *
 * Nistk::Neuron::IVVariableを利用する場合には、
 * 必ずこの関数で初期化する必要がある。この関数では、
 * Nistk::Neuron::IVParameterを使い、
 * Nistk::Neuron::IVVariableのメンバv,uを次のように初期化する。<br>
 * v = c <br>
 * u = b * v <br>
 * I = 0.0 <br>
 *
 * @param *ivv Simple Spiking Neuron modelの変数クラスのポインタ
 * @param *ivp Simple Spiking Neuron modelのパラメータクラスのポインタ
 */
void Nistk::Neuron::IVNeuron::init(Nistk::Neuron::IVVariable *ivv, 
				   Nistk::Neuron::IVParameter *ivp)
{
  ivv->v = ivp->c;
  ivv->u = ivp->b * ivv->v;
  ivv->I = 0.0;
}

/** Nistk::Temp::Runge4SiによるSimple Spiking Neuron modelを計算する関数
 *
 * 4次のルンゲクッタ法を用いてSimple Spiking Neuron modelの1ステップ
 * (dt)を計算する。計算は2元連立微分方程式になるので、ルンゲクッタ法
 * の計算には
 * Nistk::Temp::Runge4Siを用いる。
 *
 * @param dt 時間刻み幅
 * @param *ivv Simple Spiking Neuron model modelの変数クラスのポインタ
 * @param *ivp Simple Spiking Neuron model modelのパラメータクラスのポインタ
 */
void Nistk::Neuron::IVNeuron::calc(double dt, Nistk::Neuron::IVVariable *ivv, 
				   Nistk::Neuron::IVParameter *ivp)
{
  double val[2];     // ルンゲクッタ法用に変数v,uを格納する変数
  // ルンゲクッタ用関数ポインタの配列
  double (*func[2])(double dt, double *val, 
		    Nistk::Neuron::IVVariable *ivv, 
		    Nistk::Neuron::IVParameter *ivp) = {
                                &Nistk::Neuron::IVNeuron::calc_v,
				&Nistk::Neuron::IVNeuron::calc_u};
  // HHVariableの変数V,m,h,nを配列valに格納
  val[0] = ivv->v;      // 膜電位
  val[1] = ivv->u;      // リカバリ変数
  // 膜電位vの判定とリセット
  if(val[0] >= ivp->v_rest){
    val[0] = ivp->vp_7 * val[0] + ivp->c;
    val[1] = ivp->up_3 * val[1] + ivp->d;
  }
  // ルンゲクッタ法を用いて計算(2番目の引数dtはダミー)
  Nistk::Temp::Runge4Si<Nistk::Neuron::IVVariable*, 
               Nistk::Neuron::IVParameter*>(func, dt, val, ivv, ivp, dt, 2);

  // IVVariableの変数の値を格納
  ivv->v = val[0]; 
  ivv->u = val[1];

  return;
}


/** 膜電位vの微分値計算用関数
 *
 * 膜電位vの微分値
 \f[
        \frac{dv}{dt} = (vp\_1 * v^2 + vp\_2 * v + vp\_3 
                            - vp\_4 * u + vp\_5 + I) / vp\_6
 \f]
 * を計算する。
 * この関数は、膜電位vの微分値計算を
 * Nistk::Neuron::IVNeuron::calc等で利用できるようにしたものである。
 * 2番目の引数*valは<br>
 * <br>
 * val[0]:v   (膜電位)<br>
 * val[1]:u   (リカバリ変数)<br>
 * <br>
 * となっているので注意すること。要は、
 * Nistk::Temp::Runge4Si使用するために引数等はこのようになっている。
 *
 * @param dt ダミー(使用しない) 
 * @param *val 変数v,uを格納する変数のポインタ
 * @param *ivv Simple Spiking Neuron modelの変数クラスのポインタ(ダミー使用しない) 
 * @param *ivp Simple Spiking Neuron modelのパラメータクラスのポインタ
 * @return 膜電位の微分値
 */
double Nistk::Neuron::IVNeuron::calc_v(double dt, double *val, 
				       Nistk::Neuron::IVVariable *ivv, 
				       Nistk::Neuron::IVParameter *ivp)
{
  double diff;       // 微分値用

  diff = (ivp->vp_1 * val[0] * val[0] + ivp->vp_2 * val[0] + ivp->vp_3
	  - ivp->vp_4 * val[1] + ivp->vp_5 + ivv->I) / ivp->vp_6;

  return diff;
}

/** リカバリ変数uの微分値計算用関数
 *
 * リカバリ変数uの微分値
 \f[
        \frac{du}{dt} = a * (b * (v + up\_1) - up\_2 * u)
 \f]
 * を計算する。
 * この関数は、リカバリ変数uの微分値計算を
 * Nistk::Neuron::IVNeuron::calc等で利用できるようにしたものである。
 * 2番目の引数*valは<br>
 * <br>
 * val[0]:v   (膜電位)<br>
 * val[1]:u   (リカバリ変数)<br>
 * <br>
 * となっているので注意すること。要は、
 * Nistk::Temp::Runge4Si使用するために引数等はこのようになっている。
 *
 * @param dt ダミー(使用しない) 
 * @param *val 変数v,uを格納する変数のポインタ
 * @param *ivv Simple Spiking Neuron modelの変数クラスのポインタ(ダミー使用しない) 
 * @param *ivp Simple Spiking Neuron modelのパラメータクラスのポインタ
 * @return リカバリ変数の微分値
 */
double Nistk::Neuron::IVNeuron::calc_u(double dt, double *val, 
				       Nistk::Neuron::IVVariable *ivv, 
				       Nistk::Neuron::IVParameter *ivp)
{
  double diff;       // 微分値用

  diff = ivp->a * (ivp->b * (val[0] + ivp->up_1) - ivp->up_2 * val[1]);

  return diff;
}
