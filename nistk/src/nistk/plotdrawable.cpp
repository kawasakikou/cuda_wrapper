/**
 * @file  plotdrawable.cpp
 * @brief class for plotdrawable used window(実装)
 *
 * @author Masakazu Komori
 * @date 2011-09-10
 * @version $Id: plotdrawable.cpp,v.20110910 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

/*
  二次元配列p_data2dのみp_data2d[y][x]でその他の二次元配列は[x][y]と
  となっているはずなので注意すること！
*/

#include<nistk/plotdrawable.h>
#include<nistk/tools.h>
#include<cairomm/context.h>
#include<pangomm/context.h>
#include<gtkmm.h>
#include<cmath>

/** コンストラクタ 
 */
Nistk::PlotDrawable::PlotDrawable()
{
  set_size_request(100,100);
  p_data_x_num = 0;
  p_data_y_num = 0;
  show_flag = false;
  // Pango用layoutの初期化
  m_layout = create_pango_layout("");
  m_layout->set_font_description(Pango::FontDescription("serif 10"));
}

/** デストラクタ
 */
Nistk::PlotDrawable::~PlotDrawable()
{
  int i;

  if(p_data_x_num != 0){
    for(i=0; i < p_data_y_num; i++) delete[] p_data2d[i]; 
    for(i=0; i < p_data_x_num; i++){
      //      delete[] p_data2d[i]; 
      delete[] data_min_x[i];
      delete[] data_min_y[i];
      delete[] data_max_x[i];
      delete[] data_max_y[i];
      delete[] data_org_x[i];
      delete[] data_org_y[i];
      delete[] data_diff_x[i];
      delete[] data_diff_y[i];
      delete[] x_org[i];
      delete[] x_end[i];
      delete[] y_org[i];
      delete[] y_end[i];
    }
    delete[] p_data2d; 
    delete[] data_min_x;
    delete[] data_min_y;
    delete[] data_max_x;
    delete[] data_max_y;
    delete[] data_org_x;
    delete[] data_org_y;
    delete[] data_diff_x;
    delete[] data_diff_y;
    delete[] x_org;
    delete[] x_end;
    delete[] y_org;
    delete[] y_end;
  }
}

/** プロットするグラフの領域を確保する関数
 *
 * グラフサイズはウィンドウサイズを変更しても変らないようにする。
 *
 * @param x_num グラフの列の個数
 * @param y_num グラフの行の個数
 * @param x_range グラフのx軸の表示幅
 * @param y_range グラフのy軸の表示幅
 * @param x_margin グラフ間の横方向マージン
 * @param y_margin グラフ間の縦方向マージン
 */
void Nistk::PlotDrawable::set_graph_area(int x_num, int y_num, int x_range, 
				 int y_range, int x_margin, int y_margin)
{
  int area_height;
  int area_width;
  int f_x_org;
  int f_y_org;
  int i,j;

  // 配列確保と領域サイズの設定
  p_data2d = new Nistk::PlotData*[y_num];
  for(i = 0; i < y_num; i++) p_data2d[i] = new Nistk::PlotData[x_num];
  p_data_x_num = x_num;
  p_data_y_num = y_num;
  make_array(x_num, y_num);
  // x,yの幅は+1する。（１００だと０〜１００なので。。）
  area_height = (y_range + y_margin + 1) * y_num + y_margin;
  area_width = (x_range + x_margin + 1) * x_num + x_margin;
  set_size_request(area_width, area_height);

  // それぞれのグラフの原点と終点の設定
  for(i = 0; i < y_num; i++){ // 列
    f_y_org = (y_range + y_margin + 1) * i + (y_margin + y_range);    
    for(j = 0; j < x_num; j++){ // 行
      f_x_org = (x_range + x_margin + 1) * j +x_margin;
      x_org[j][i] = f_x_org;
      y_org[j][i] = f_y_org;
      x_end[j][i] = f_x_org + x_range;
      y_end[j][i] = f_y_org - y_range;
    }
  }
  return;
}

/** 位置x,yのグラフのPlotDataのポインタを取得する関数
 *
 * 位置x,yのグラフのPlotDataのポインタを取得する関数。
 * それぞれのグラフにはPlotDataが対応しており、その
 * クラスのポインタを取得する。これにより、PlotData
 * が持っている。メンバ関数を扱うことが可能となる。
 * メンバ関数に関しては、PlotDataを参照のこと。
 *
 * @param x 取得するPlotDataポインタのx位置
 * @param y 取得するPlotDataポインタのy位置
 */
Nistk::PlotData* Nistk::PlotDrawable::pd_ptr(int x, int y)
{
  return &p_data2d[y][x];
}

/** 自動描画の関数でユーザーが使うことはない
 */
bool Nistk::PlotDrawable::on_expose_event(GdkEventExpose *event)
{
  Gtk::Allocation allocation;        // ウィンドウ情報用
  Cairo::RefPtr<Cairo::Context> cr;  // Cairoコンテキストポインタ
  //  Glib::RefPtr<Pango::Context> pc;  // Pangoコンテキストポインタ
  Glib::RefPtr<Gdk::Window> window;  // ウィンドウポインタ
  Glib::RefPtr<Gdk::GC> m_gc;        // グラフィックコンテキスト用ポインタ
  int width;                         // ウィンドウの幅
  int height;                        // ウィンドウ高さ
  int x_diff,y_diff;                 // グラフの幅
  char m_str[12];                    // メモリ用文字配列
  int m_str_para[2];                 // 文字列表示位置
  const int m_str_len=12;            // 文字列の長さ
  const int i_length[2]={4,8};       // 目盛の長さ
  const int m_margin = 2;            // 目盛位置調整用
  int z_pos[2];                      // 0の表示位置パラメータ
  // データプロット用色データ
  const double pd_rgb[4][3] = {{0.0,0.0,0.0},{1.0,0.0,0.0},
			       {0.0,1.0,0.0},{0.0,0.0,1.0}};
  int pd_num;                        // プロットデータ領域の数
  int pd_size;                       // プロットデータ領域のデータサイズ
  double pd_data[2];                 // プロットデータ
  double b_data[2];                  // 直前のプロットデータ
  int  p_point[2];                   // プロットポイント
  bool in_pd_data;                   // pd_dataが範囲内かどうか
  bool in_b_data;                    // b_dataが範囲内かどうか
  bool is_raster;                    // ラスター図とするかどうか
  double data_round;
  double m_step;
  int m_step_order;
  int m_tmp;
  int i,j,k,l;

   // 枠の描画
  window = get_window();
  if(window){
    show_flag = true; 
    allocation = get_allocation();
    width = allocation.get_width();
    height = allocation.get_height();
    cr = window->create_cairo_context();
    //   pc = m_layout->get_context();
    m_gc = Gdk::GC::create(window);

    // 領域を白く塗り潰す
    cr->rectangle(0,0,width,height);
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->paint();
    cr->stroke();
    // 枠線の描画
    cr->set_line_width(1.0);
    for(i = 0; i < p_data_y_num; i++){ // 行 y
      for(j = 0; j < p_data_x_num; j++){ // 列 x
	cr->set_source_rgb(0.0, 0.0, 0.0);
	// 枠線の描画
	cr->move_to(x_org[j][i], y_org[j][i]);
	cr->line_to(x_end[j][i], y_org[j][i]);
	cr->line_to(x_end[j][i], y_end[j][i]);
	cr->line_to(x_org[j][i], y_end[j][i]);
	cr->line_to(x_org[j][i], y_org[j][i]);
	cr->stroke();
	// 仮想原点等の計算
	data_min_x[j][i] = p_data2d[i][j].get_x_data_min();
	data_max_x[j][i] = p_data2d[i][j].get_x_data_max();
	data_min_y[j][i] = p_data2d[i][j].get_y_data_min();
	data_max_y[j][i] = p_data2d[i][j].get_y_data_max();
	// max,minがinfもしくはnanの時、continue
	if(std::isinf(data_min_x[j][i]) || std::isinf(data_max_x[j][i])
	   || std::isinf(data_min_y[j][i]) || std::isinf(data_max_y[j][i])
	   || std::isnan(data_min_x[j][i]) || std::isnan(data_max_x[j][i])
	   || std::isnan(data_min_y[j][i]) || std::isnan(data_max_y[j][i])) continue;
	data_diff_x[j][i] = (data_max_x[j][i] - data_min_x[j][i]) 
	                     / (x_end[j][i] - x_org[j][i]);
	if(data_diff_x[j][i] < 0.0) data_diff_x[j][i] = -1.0*data_diff_x[j][i];
	data_diff_y[j][i] = (data_max_y[j][i] - data_min_y[j][i]) 
                             / (y_org[j][i] - y_end[j][i]);
	if(data_diff_y[j][i] < 0.0) data_diff_y[j][i] = -1.0*data_diff_y[j][i];
	data_org_x[j][i] = x_org[j][i] 
              + (int)((-1.0 * data_min_x[j][i]) / data_diff_x[j][i]);
	data_org_y[j][i] = y_org[j][i] 
	      - (int)((-1.0 * data_min_y[j][i]) / data_diff_y[j][i]);

	// 目盛の描画
	z_pos[0] = -1;
	z_pos[1] = -1;
	// x軸
	// 原点と数字0を表示
	if((data_min_x[j][i] <= 0) && (data_max_x[j][i] >= 0)){
	  if((data_org_x[j][i] != x_org[j][i]) 
	     && (data_org_x[j][i] != x_end[j][i])){
	    cr->move_to(data_org_x[j][i], y_org[j][i]);
	    cr->line_to(data_org_x[j][i], y_org[j][i]-i_length[1]);
	  }
	  //	  pc->set_base_gravity(Pango::GRAVITY_SOUTH);
	  m_layout->set_text("0.0");
	  z_pos[0] = data_org_x[j][i]-
	    m_layout->get_pixel_ink_extents().get_width()/2;
	  z_pos[1] = data_org_x[j][i]+
	    m_layout->get_pixel_ink_extents().get_width()/2;
	  window->draw_layout(m_gc, z_pos[0], 
	      y_org[j][i]+4*m_margin, m_layout, Gdk::Color("black"),
	      Gdk::Color("white"));
	}
	// 目盛幅の決定
	if((data_min_x[j][i] < 0) && (data_max_x[j][i] > 0)){
	  if((-1.0*data_min_x[j][i]) < data_max_x[j][i]){
	    m_step = data_max_x[j][i] / 2.0;
	  }else m_step = data_min_x[j][i] / 2.0;
	}else{
	  m_step = (data_max_x[j][i] - data_min_x[j][i]) / 2.0;
	}
	if(m_step < 0) m_step = -1.0 * m_step;
	m_step_order = Nistk::Tools::calc_order(m_step);
	m_step = pow(10.0,m_step_order);
	// 目盛の描画
	data_round =Nistk::Tools::round_off_data(data_min_x[j][i],m_step_order);
	for(;;){                                         // 目盛の最小位置の決定
	  if (data_round >= data_min_x[j][i]) break;
	  data_round += m_step;
	}
	// 苦し紛れの誤差対策 そのうち対策できたらいいな。
	//
	//
	if((data_round > 0.999999999999) && (data_round <= 1.0)) 
	  data_round =1.0;
	if((data_round < -0.999999999999) && (data_round >= -1.0)) 
	  data_round =-1.0;
	//
	//
	//
	m_tmp = data_org_x[j][i] + (int)(data_round / data_diff_x[j][i]);
	make_str(m_str, m_str_len, data_round, 
		 data_max_x[j][i] - data_min_x[j][i]);  // 目盛用文字列取得
	m_layout->set_text(m_str);                      // 目盛描画
	m_str_para[0] = m_tmp 
	  - m_layout->get_pixel_ink_extents().get_width()/2;
	m_str_para[1] = m_tmp 
	  + m_layout->get_pixel_ink_extents().get_width()/2;
	if((m_str_para[0] > z_pos[1]) || (m_str_para[1] < z_pos[0])){
	  window->draw_layout(m_gc, m_str_para[0], 
	      y_org[j][i]+4*m_margin, m_layout, Gdk::Color("black"),
	      Gdk::Color("white"));
	  if(m_tmp != x_org[j][i]){
	    cr->move_to(m_tmp, y_org[j][i]);
	    cr->line_to(m_tmp, y_org[j][i]-i_length[1]);
	  }
	} else if(m_tmp != x_org[j][i]){
	  cr->move_to(m_tmp, y_org[j][i]);
	  cr->line_to(m_tmp, y_org[j][i]-i_length[0]);
	}
	for(;;){                                         // 中間の目盛
	  data_round += m_step;
	  m_tmp = data_org_x[j][i] + (int)(data_round / data_diff_x[j][i]);
	  if((data_round + m_step) > data_max_x[j][i]) break;
	  if(m_tmp != data_org_x[j][i]){                 // 原点以外
	    cr->move_to(m_tmp, y_org[j][i]);
	    cr->line_to(m_tmp, y_org[j][i]-i_length[0]);
	  } 
	}
	// 苦し紛れの誤差対策 そのうち対策できたらいいな。
	//
	//
	if((data_round > 0.999999999999) && (data_round <= 1.0)) 
	  data_round =1.0;
	if((data_round < -0.999999999999) && (data_round >= -1.0)) 
	  data_round =-1.0;
	//
	//
	//
	make_str(m_str, m_str_len, data_round,          // 目盛最大位置
		 data_max_x[j][i] - data_min_x[j][i]);  // 目盛用文字列取得
	m_layout->set_text(m_str);                      // 目盛描画
	m_str_para[0] = m_tmp 
	  - m_layout->get_pixel_ink_extents().get_width()/2;
	m_str_para[1] = m_tmp 
	  + m_layout->get_pixel_ink_extents().get_width()/2;
	if((m_str_para[0] > z_pos[1]) || (m_str_para[1] < z_pos[0])){
	  window->draw_layout(m_gc, m_str_para[0], 
	      y_org[j][i]+4*m_margin, m_layout, Gdk::Color("black"),
	      Gdk::Color("white"));
	  if(m_tmp != x_end[j][i]){
	    cr->move_to(m_tmp, y_org[j][i]);
	    cr->line_to(m_tmp, y_org[j][i]-i_length[1]);
	  }
	} else if(m_tmp != x_end[j][i]){
	  cr->move_to(m_tmp, y_org[j][i]);
	  cr->line_to(m_tmp, y_org[j][i]-i_length[0]);
	}

	// y軸
	z_pos[0] = -1;
	z_pos[1] = -1;
	// 原点と数字0を表示
	if((data_min_y[j][i] <= 0) && (data_max_y[j][i] >= 0)){
	  if((data_org_y[j][i] != y_org[j][i]) 
	     && (data_org_y[j][i] != y_end[j][i])){
	    cr->move_to(x_org[j][i], data_org_y[j][i]);
	    cr->line_to(x_org[j][i]+i_length[1], data_org_y[j][i]);
	  }
	    // 数字0を表示
	    //	  pc->set_base_gravity(Pango::GRAVITY_WEST);
	    m_layout->set_text("0.0");
	    z_pos[0] = data_org_y[j][i]
	      - m_layout->get_pixel_ink_extents().get_height() + m_margin;
	    z_pos[1] = data_org_y[j][i]
	      + m_layout->get_pixel_ink_extents().get_height() + m_margin;
	    window->draw_layout(m_gc,
	    x_org[j][i]-m_layout->get_pixel_ink_extents().get_width()-m_margin, 
            z_pos[0], m_layout, Gdk::Color("black"), Gdk::Color("white"));
	}
	// 目盛幅の決定
	if((data_min_y[j][i] < 0) && (data_max_y[j][i] > 0)){
	  if((-1.0*data_min_y[j][i]) < data_max_y[j][i]){
	    m_step = data_max_y[j][i] / 2.0;
	  }else m_step = data_min_y[j][i] / 2.0;
	}else{
	  m_step = (data_max_y[j][i] - data_min_y[j][i]) / 2.0;
	}
	if(m_step < 0) m_step = -1.0 * m_step;
	m_step_order = Nistk::Tools::calc_order(m_step);
	m_step = pow(10.0,m_step_order);
	// 目盛の描画
	data_round =Nistk::Tools::round_off_data(data_min_y[j][i],m_step_order);
	for(;;){                                         // 目盛の最小位置の決定
	  if (data_round >= data_min_y[j][i]) break;
	  data_round += m_step;
	}
	// 苦し紛れの誤差対策 そのうち対策できたらいいな。
	//
	//
	if((data_round > 0.999999999999) && (data_round <= 1.0)) 
	  data_round =1.0;
	if((data_round < -0.999999999999) && (data_round >= -1.0)) 
	  data_round =-1.0;
	//
	//
	//
	m_tmp = data_org_y[j][i] - (int)(data_round / data_diff_y[j][i]);
	make_str(m_str, m_str_len, data_round, 
		 data_max_y[j][i] - data_min_y[j][i]);  // 目盛用文字列取得
	m_layout->set_text(m_str);                      // 目盛描画
	m_str_para[0] = m_tmp 
	  - m_layout->get_pixel_ink_extents().get_height() + m_margin;
	m_str_para[1] = m_tmp 
	  + m_layout->get_pixel_ink_extents().get_height() + m_margin;
	if((m_str_para[0] > z_pos[1]) || (m_str_para[1] < z_pos[0])){
	  window->draw_layout(m_gc, 
	    x_org[j][i]-m_layout->get_pixel_ink_extents().get_width()-m_margin, 
            m_str_para[0], m_layout, Gdk::Color("black"), Gdk::Color("white"));
	  if(m_tmp != y_org[j][i]){
	    cr->move_to(x_org[j][i], m_tmp);
	    cr->line_to(x_org[j][i]+i_length[1],m_tmp);
	  }
	} else if(m_tmp != y_org[j][i]){
	  cr->move_to(x_org[j][i], m_tmp);
	  cr->line_to(x_org[j][i]+i_length[1],m_tmp);
	}
	for(;;){                                         // 中間の目盛
	  data_round += m_step;
	  m_tmp = data_org_y[j][i] - (int)(data_round / data_diff_y[j][i]);
	  if((data_round + m_step) > data_max_y[j][i]) break;
	  if(m_tmp != data_org_y[j][i]){                 // 原点以外
	    cr->move_to(x_org[j][i], m_tmp);
	    cr->line_to(x_org[j][i]+i_length[0],m_tmp);
	  }
	}
	// 苦し紛れの誤差対策 そのうち対策できたらいいな。
	//
	//
	if((data_round > 0.999999999999) && (data_round <= 1.0)) 
	  data_round =1.0;
	if((data_round < -0.999999999999) && (data_round >= -1.0)) 
	  data_round =-1.0;
	//
	//
	//
	make_str(m_str, m_str_len, data_round,          // 目盛最大位置
		 data_max_y[j][i] - data_min_y[j][i]);  // 目盛用文字列取得
	m_layout->set_text(m_str);                      // 目盛描画
	m_str_para[0] = m_tmp 
	  - m_layout->get_pixel_ink_extents().get_height() + m_margin;
	m_str_para[1] = m_tmp 
	  + m_layout->get_pixel_ink_extents().get_height() + m_margin;
	if((m_str_para[0] > z_pos[1]) || (m_str_para[1] < z_pos[0])){
	  window->draw_layout(m_gc, 
	    x_org[j][i]-m_layout->get_pixel_ink_extents().get_width()-m_margin, 
            m_str_para[0], m_layout, Gdk::Color("black"), Gdk::Color("white"));
	  if(m_tmp != y_end[j][i]){
	    cr->move_to(x_org[j][i], m_tmp);
	    cr->line_to(x_org[j][i]+i_length[1],m_tmp);
	  }
	} else if(m_tmp != y_end[j][i]){
	  cr->move_to(x_org[j][i], m_tmp);
	  cr->line_to(x_org[j][i]+i_length[1],m_tmp);
	}
	cr->stroke();

	// データのプロット
	pd_num = p_data2d[i][j].get_data_num();
	for(k = 0; k < pd_num; k++){
	  // 前準備
	  l = k % 4;
	  cr->set_source_rgb(pd_rgb[l][0], pd_rgb[l][1], pd_rgb[l][2]);
	  pd_size = p_data2d[i][j].get_size(k);
	  pd_data[0] = p_data2d[i][j].get_x(k, 0);
	  pd_data[1] = p_data2d[i][j].get_y(k, 0);
	  is_raster = p_data2d[i][j].get_raster(k);
	  if((pd_data[0] >= data_min_x[j][i]) 
	     && (pd_data[0] <= data_max_x[j][i]) 
	     && (pd_data[1] >= data_min_y[j][i]) 
	     && (pd_data[1] <= data_max_y[j][i])) in_pd_data = true;
	  else in_pd_data = false;
	  if(in_pd_data == true){
	    if(is_raster == false){
	      p_point[0] = data_org_x[j][i] 
		+ (int)(pd_data[0] / data_diff_x[j][i]);
	      p_point[1] = data_org_y[j][i] 
		- (int)(pd_data[1] / data_diff_y[j][i]);
	      cr->move_to(p_point[0], p_point[1]);
	    } else {
	      cr->move_to(p_point[0], p_point[1]);
	      cr->line_to(p_point[0] + 1, p_point[1]);
	    }
	  }
	  // プロット開始
	  for(l = 1; l < pd_size; l++){
	    // 100回ラインを引くととりあえずドロー
	    if((l % 100) == 0){
	      cr->stroke();
	      cr->move_to(p_point[0], p_point[1]);
	    } 
	    // データ取得
	    b_data[0] = pd_data[0];
	    b_data[1] = pd_data[1];
	    pd_data[0] = p_data2d[i][j].get_x(k, l);
	    pd_data[1] = p_data2d[i][j].get_y(k, l);
	    // 範囲に入っているかチェック
	    if(in_pd_data == true) in_b_data = true;
	    else in_b_data = false;
	    if((pd_data[0] >= data_min_x[j][i]) 
	       && (pd_data[0] <= data_max_x[j][i]) 
	       && (pd_data[1] >= data_min_y[j][i]) 
	       && (pd_data[1] <= data_max_y[j][i])) in_pd_data = true;
	    else in_pd_data = false;
	    // 範囲を確認しながらラインを引く
	    if(is_raster == false){
	      if((in_pd_data == true) && (in_b_data == true)){ // 始点終点in
		p_point[0] = data_org_x[j][i] 
		  + (int)(pd_data[0] / data_diff_x[j][i]);
		p_point[1] = data_org_y[j][i] 
		  - (int)(pd_data[1] / data_diff_y[j][i]);
		cr->line_to(p_point[0],p_point[1]);
	      } else if((in_pd_data == true) && (in_b_data == false)){ // 終点in
		calc_p_point(j, i, pd_data, b_data, p_point);
		cr->move_to(p_point[0], p_point[1]);
		p_point[0] = data_org_x[j][i] 
		  + (int)(pd_data[0] / data_diff_x[j][i]);
		p_point[1] = data_org_y[j][i] 
		  - (int)(pd_data[1] / data_diff_y[j][i]);
		cr->line_to(p_point[0],p_point[1]);
	      } else if((in_pd_data == false) && (in_b_data == true)){ // 始点in
		calc_p_point(j, i, b_data, pd_data, p_point);
		cr->line_to(p_point[0],p_point[1]);
	      }
	    } else {
	      if(in_pd_data == true){
		p_point[0] = data_org_x[j][i] 
		  + (int)(pd_data[0] / data_diff_x[j][i]);
		p_point[1] = data_org_y[j][i] 
		  - (int)(pd_data[1] / data_diff_y[j][i]);
		cr->move_to(p_point[0], p_point[1]);
		cr->line_to(p_point[0] + 1, p_point[1]);
	      }
	    }
	  }
	  cr->stroke();
	}
	
      }
    }
  } 

  return true;
}

/** プロット関係の配列確保の関数
 *
 * グラフサイズはウィンドウサイズを変更しても変らないようにする。
 *
 * @param x_num グラフの列の個数
 * @param y_num グラフの行の個数
 */
void Nistk::PlotDrawable::make_array(int x_num, int y_num)
{
  int i;

 // 配列の確保 配列[x][y] 面倒臭いので二次元配列
  data_min_x = new double*[x_num];
  data_min_y = new double*[x_num];
  data_max_x = new double*[x_num];
  data_max_y = new double*[x_num];
  data_org_x = new int*[x_num];
  data_org_y = new int*[x_num];
  data_diff_x = new double*[x_num];
  data_diff_y = new double*[x_num];
  x_org = new int*[x_num];
  x_end = new int*[x_num];
  y_org = new int*[x_num];
  y_end = new int*[x_num];
  for(i=0; i < x_num; i++){
    data_min_x[i] = new double[y_num];
    data_min_y[i] = new double[y_num];
    data_max_x[i] = new double[y_num];
    data_max_y[i] = new double[y_num];
    data_org_x[i] = new int[y_num];
    data_org_y[i] = new int[y_num];
    data_diff_x[i] = new double[y_num];
    data_diff_y[i] = new double[y_num];
    x_org[i] = new int[y_num];
    x_end[i] = new int[y_num];
    y_org[i] = new int[y_num];
    y_end[i] = new int[y_num];
  }

  return;
}

/** 目盛用文字列生成関数
 *
 * グラフの目盛用文字列生成のための関数。
 *
 * @param *o_str 変換した文字列を格納する文字列のポインタ
 * @param str_len 変換する文字列の最大長
 * @param o_data 変換対象のデータ
 * @param diff_data 変換対象のデータの最小変位
 */
void Nistk::PlotDrawable::make_str(char *o_str, int str_len, 
			    double o_data, double diff_data)
{
  int max_order;         // o_dataの最大次数
  int diff_order;        // diff_dataの最大次数
  int diff;              // o_dataの最大次数とdiff_dataの最大次数の差

  max_order =Nistk::Tools:: calc_order(o_data);
  diff_order = Nistk::Tools::calc_order(diff_data);
  diff = max_order - diff_order;
 
  if((max_order > 4) || (max_order < -4)){   // 指数形式で変換
    Nistk::Tools::num_to_str(o_data, o_str, max_order, max_order-3,
			     str_len, true);
  } else {  // それ以外
    if(max_order >= 0){   // オーダが正の時
      if(diff <= 6){          // 桁の差が6以下の時
	Nistk::Tools::num_to_str(o_data, o_str, max_order, diff_order-1,
				 str_len, false);
      } else {
		Nistk::Tools::num_to_str(o_data, o_str, max_order, 0,
				 str_len, false);
      }
    } else {             // オーダが負の時
      if(diff <= 2){          // 桁の差が2以下の時
	Nistk::Tools::num_to_str(o_data, o_str, max_order, diff_order-1,
				 str_len, false);
      } else {
	Nistk::Tools::num_to_str(o_data, o_str, max_order, max_order-3,
				 str_len, false);
      }
    }
  }

  return;
}

/** 範囲外の時のプロット点計算関数
 *
 * 範囲外の時のプロット点計算関数。計算点2つから直線の式を求め、
 * それからグラフの枠との切片を求める。
 *
 * @param x_num PlotDataのx位置
 * @param y_num PlotDataのy位置
 * @param *a_data 計算点1のポインタ(範囲内の点)
 * @param *b_data 計算点1のポインタ(範囲外の点)
 * @param *p_point グラフの枠での点
 */
void Nistk::PlotDrawable::calc_p_point(int x_num, int y_num, double *a_data,
		    double *b_data, int *p_point)
{
  double a;    // 直線の傾き
  double b;    // 直線の切片
  double x;    // 切片のx
  double y;    // 切片のy
  bool a_zero = false; // 傾きが0かどうか
  bool a_inf = false;  // 傾きが無限大かどうか

  // 傾きが0もしくは無限大にならないかのチェック
  if((a_data[1] - b_data[1]) == 0.0) a_zero = true;
  if((a_data[0] - b_data[0]) == 0.0) a_inf = true;

  // 傾きが0もしくは無限大でない時
  if((a_zero == false) && (a_inf == false)){
    a = (a_data[1] - b_data[1]) / (a_data[0] - b_data[0]);
    b = a_data[1] - (a * a_data[0]);
    // y
    if(b_data[0] <= data_min_x[x_num][y_num]){
      y = a * data_min_x[x_num][y_num] +b; 
    }
    else if(b_data[0] >= data_max_x[x_num][y_num]) {
      y = a * data_max_x[x_num][y_num] +b; 
    } else {
      if(b_data[1] > data_max_y[x_num][y_num])
	y = data_max_y[x_num][y_num];
      else y = data_min_y[x_num][y_num];
    }
    if(y > data_max_y[x_num][y_num]) y = data_max_y[x_num][y_num];
    else if(y < data_min_y[x_num][y_num]) y = data_min_y[x_num][y_num];
  

    // x
    if(b_data[1] <= data_min_y[x_num][y_num]){
      x = (data_min_y[x_num][y_num] - b) / a;
    }
    else if(b_data[1] >= data_max_y[x_num][y_num]){
      x = (data_max_y[x_num][y_num] - b) / a;
    } else {
      if(b_data[0] > data_max_x[x_num][y_num])
	x = data_max_x[x_num][y_num];
      else x = data_min_x[x_num][y_num];
    }
    if(x < data_min_x[x_num][y_num]) x = data_min_x[x_num][y_num];
    else if(x > data_max_x[x_num][y_num]) x = data_max_x[x_num][y_num];
  }

  // 傾きが0の時
  if(a_zero == true){
    if(b_data[0] > data_max_x[x_num][y_num])
      x = data_max_x[x_num][y_num];
    else x = data_min_x[x_num][y_num];
    y = a_data[1];
  }
  // 傾きが無限大の時
  if(a_inf == true){
    x = a_data[0];
    if(b_data[1] > data_max_y[x_num][y_num])
      y = data_max_y[x_num][y_num];
    else y = data_min_y[x_num][y_num];
  }

  // プロットポイント計算
  p_point[0] = data_org_x[x_num][y_num] 
    + (int)(x / data_diff_x[x_num][y_num]);
  p_point[1] = data_org_y[x_num][y_num] 
    - (int)(y / data_diff_y[x_num][y_num]);

  return;
}

/** showフラグの値を取得する関数
 *
 * showフラグの値を取得する関数
 *
 * @return 画面が表示されているとtrue
 */
bool Nistk::PlotDrawable::is_show()
{
  return show_flag;
}

/** showフラグをセットする関数
 *
 * showフラグをセットする関数
 *
 * @param sflag showフラグの値
 */
void Nistk::PlotDrawable::set_show_flag(bool sflag)
{
  show_flag = sflag;
  
  return;
}

/** 強制再描画用関数
 *
 * 強制再描画用関数。threadを使用するさいの強制再描画用関数。
 * 通常は使用しなくて良い。
 *
 */
void Nistk::PlotDrawable::redraw()
{
  // windowの取得
  Glib::RefPtr<Gdk::Window> win = get_window();
  if(win){
    // Rectangleで領域を取得
    Gdk::Rectangle r(0, 0, get_allocation().get_width(),
		     get_allocation().get_height());
    // invalidateでwindowに通知
    win->invalidate_rect(r, false);
  }
  
  return;
}
