/**
 * @file  ivvariable.cpp
 * @brief class for Variable of Izhikevich's Simple Spiking Neuron(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-05
 * @version $Id: ivvariable.cpp,v.20110805 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<nistk/ivvariable.h>

/** コンストラクタ
 */
Nistk::Neuron::IVVariable::IVVariable()
{
}

/** デストラクタ
 */
Nistk::Neuron::IVVariable::~IVVariable()
{
}
