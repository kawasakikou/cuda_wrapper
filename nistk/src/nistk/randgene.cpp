/**
 * @file  randgene.cpp
 * @brief class for random generator using stdlib(実装)
 *
 * @author Masakazu Komori
 * @date 2009-01-10
 * @version $Id: randgene.cpp,v.20090110 $
 *
 * Copyright (C) 2009 Masakazu Komori
 */

#include<cstdlib>
#include<ctime>
#include<randgene.h>

/** コンストラクタ
 */
Nistk::RandGene::RandGene()
{
  srand((unsigned)time(NULL));
}

/** デストラクタ
 */
Nistk::RandGene::~RandGene()
{
}

/** ノーマルの乱数(範囲はコンパイラ次第)
 *
 * stdlibにおけるrand関数による乱数をそのまま返す関数。
 * よって、範囲指定などはできない。その場合は,get_rand_i
 * もしくはget_rand_dを用いること。
 *
 * @return rand関数による乱数そのまま
 */
int Nistk::RandGene::get_rand()
{
  return rand();
}

/** startからendまでの範囲の乱数を発生させる
 *
 * startからendまでの範囲の乱数を発生させる関数。
 * rand関数で発生させた値を(end-start)+1で割った余り
 * にstartの値を加算してを返しているだけである。
 * rand関数は発生させた下位のビットが余りよくない
 * と言う話もあるので、発生させた乱数を右にshiftビット
 * だけシフトさせて剰余を求めるようにもしている。
 *
 * @param start 乱数の開始位置
 * @param end   乱数の終了位置
 * @param shift シフトさせるビット数
 * @return startからendまでの範囲の乱数
 */
int Nistk::RandGene::get_rand_i(int start, int end, int shift)
{
  return (rand()>>shift)%(end-start+1)+start;
}

/** 0から1までの実数型乱数の発生
 *
 * 0から1までの実数の乱数を発生させる関数。
 * やっていることはrand関数で発生させた値を
 * RAND_MAXで割っているだけである。
 *
 * @return 0から1までの実数の乱数
 */
double Nistk::RandGene::get_rand_d()
{
  return ((double)rand())/RAND_MAX;
}
