/**
 * @file  plotview.cpp
 * @brief class for plot drawable(実装)
 *
 * @author Masakazu Komori
 * @date 2011-08-09
 * @version $Id: plotview.cpp,v.20110809 $
 *
 * Copyright (C) 2011 Masakazu Komori
 */

#include<nistk/plotview.h>
#include<nistk/imagedata.h>

/** コンストラクタ 引数無し
 */
Nistk::PlotView::PlotView() 
  : m_Button_Close("Close"),
    m_Button_Save("Save")
{
  // ウィンドウの初期化
  set_up_window();
  // 実体化
  show_all_children();
}

/** デストラクタ
 */
Nistk::PlotView::~PlotView()
{
}

/** コンストラクタで呼ばれる関数 ユーザは使う必要はない
 */
void Nistk::PlotView::set_up_window()
{
  char test_name[] = "test";

  // 大きさや外枠のマージンを設定する
  set_border_width(0);
  set_size_request(200,200);
  m_ScrolledWindow.set_border_width(10);
  set_name(test_name);
  // スクロールバーのポリシーを設定
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC);
  // 子ウィジットをパックする
  get_vbox()->pack_start(m_ScrolledWindow);
  m_ScrolledWindow.add(m_draw);

  // ボタンのイベントと動作させる関数を結合し,配置する
  m_Button_Save.signal_clicked().connect(sigc::mem_fun(*this,
				       &Nistk::PlotView::on_button_save));
  m_Button_Close.signal_clicked().connect(sigc::mem_fun(*this,
  				       &Nistk::PlotView::on_button_close));
  m_Button_Close.set_flags(Gtk::CAN_DEFAULT);
  m_Hbox.pack_start(m_Button_Save);
  m_Hbox.pack_start(m_Button_Close);
  Gtk::Box *pBox=get_action_area();
  if(pBox) pBox->pack_start(m_Hbox);

  return;
}

/** グラフ領域を確保する関数
 *
 * グラフサイズはウィンドウサイズを変更しても変らないようにする。
 *
 * @param x_num グラフの列の個数
 * @param y_num グラフの行の個数
 * @param x_range グラフのx軸の表示幅
 * @param y_range グラフのy軸の表示幅
 * @param x_margin グラフ間の横方向マージン
 * @param y_margin グラフ間の縦方向マージン
 */

void Nistk::PlotView::set_graph_area(int x_num, int y_num, int x_range,
				     int y_range, int x_margin, int y_margin)
{
  m_draw.set_graph_area(x_num, y_num, x_range, y_range, x_margin, y_margin);
}

/** 位置x,yのグラフのPlotDataのポインタを取得する関数
 *
 * 位置x,yのグラフのPlotDataのポインタを取得する関数。
 * それぞれのグラフにはPlotDataが対応しており、その
 * クラスのポインタを取得する。これにより、PlotData
 * が持っている。メンバ関数を扱うことが可能となる。
 * メンバ関数に関しては、PlotDataを参照のこと。
 *
 * @param x 取得するPlotDataポインタのx位置
 * @param y 取得するPlotDataポインタのy位置
 * @return 位置(x,y)のグラフに対応するPlotDataのポインタ
 */
Nistk::PlotData* Nistk::PlotView::pd_ptr(int x, int y)
{
  m_draw.pd_ptr(x, y);
}

/** 名前をセットする関数
 *
 * Drawableを表示しているウィンドウに名前を
 * 付ける関数。現在何を表示しているか分かる
 * ようにするためにも付けるべきである。
 * また、表示しているDrawableを保存する
 * 際にもファイル名として使用される。
 * 名前を付けないと、名前はtestになる。
 *
 * @param i_name ウィンドウに付ける名前
 */
void Nistk::PlotView::set_name(const char *i_name)
{
  m_name = i_name;
  set_title(m_name.c_str());
  return;
}

/** ボタンが押されると現在描画しているイメージをファイルにセーブする関数
 *
 * ボタンが押されると現在描画しているイメージを
 * ファイルにセーブする関数。ファイル名は
 * ウィンドウに付けているファイル名.pngになる。
 * つまり、ファイル形式はpngとなる。
 *
 */
void Nistk::PlotView::on_button_save()
{
  Glib::RefPtr<Gdk::Pixbuf> s_pixbuf;
  int f_length,i;
  char *f_name;
  char file_type[] = "png";

  // セーブファイル名を文字列にセット
  // stringではうまいこといかないので
  f_length = m_name.length();
  f_name = new char[f_length+1];
  for(i=0; i< f_length; i++) f_name[i] = m_name[i];
  f_name[f_length] = '\0';

  // drawableからpixbufを取得してファイルにセーブ
  Glib::RefPtr<Gdk::Drawable> window = m_draw.get_window();
  s_pixbuf = Gdk::Pixbuf::create(window,0,0,m_draw.get_width(),
				 m_draw.get_height());
  Nistk::ImageData::save_pixbuf(s_pixbuf,f_name,file_type);

  delete[] f_name;
}

/** ボタンが押されるとウィンドウを閉じる関数 ユーザは使う必要はない
 */
void Nistk::PlotView::on_button_close()
{
  m_draw.set_show_flag(false);
  hide();
}

/** windowの表示フラグの取得関数
 *
 * windowの表示フラグの取得関数
 *
 * @return 表示されているかどうかのフラグ
 */
bool Nistk::PlotView::is_window_show()
{
  return m_draw.is_show();
}

/** windowのshowフラグをセットする関数
 *
 * windowのshowフラグをセットする関数
 *
 * @param sflag showフラグの値
 */
void Nistk::PlotView::set_show_window(bool sflag)
{
  m_draw.set_show_flag(sflag);

  return; 
}

/** 表示の再描画用関数
 * 表示の再描画用関数。threadを使用した際に使用する。
 * 通常は使う必要はない。
 *
 */
void Nistk::PlotView::redraw_window()
{
  m_draw.redraw();
}

/** xボタンが押されたときの処理関数
 *  xボタンが押されたときの処理関数。押されても閉じないようにする。
 */
bool Nistk::PlotView::on_delete_event(GdkEventAny*)
{
  return true;
}
