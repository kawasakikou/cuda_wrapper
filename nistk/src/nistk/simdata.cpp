/**
 * @file  simdata.cpp
 * @brief class for simulation data(実装)
 *
 * @author Masakazu Komori
 * @date 2017-09-01
 * @version $Id: simdata.cpp,v.20170901 $
 *
 * Copyright (C) 2009-2017 Masakazu Komori
 */

#include<simdata.h>

/** コンストラクタ 引数無し
 */
Nistk::SimData::SimData()
{
  height = 0;
  width = 0;
  length = 0;
}

/** コンストラクタ 配列の確保もする
 * @param w 確保する2次元配列の幅
 * @param h 確保する2次元配列の高さ
 */
Nistk::SimData::SimData(int w,int h)
{
  create_data_size(w, h);
}

/** コピーコンストラクタ
 */
Nistk::SimData::SimData(const Nistk::SimData &obj)
{
  int i,j;

  height = obj.height;     // 高さのコピー
  width = obj.width;       // 幅のコピー
  length = obj.length;     // 長さのコピー
  //  data2d = new double*[height];   // 配列の確保
  // for(i = 0; i < height; i++) data2d[i] = new double[width];
  data2d = new double*[height];   // 配列の確保
  data2d[0] = new double[height * width];
  for(i = 1; i < height; i++) data2d[i] = data2d[0] + (i * width);

  // 配列要素の格納
  for(i=0; i < height; i++){  // 縦軸 y
    for(j=0; j < width; j++){  // 横軸 x
        data2d[i][j] = obj.data2d[i][j];
    }
  }
}

/** デストラクタ
 */
Nistk::SimData::~SimData()
{
  delete_data();
}

/** 代入演算子=のオーバーロード
 */
Nistk::SimData &Nistk::SimData::operator=(Nistk::SimData &obj)
{
  int i,j;

  this->delete_data();
  this->height = obj.height;     // 高さのコピー
  this->width = obj.width;       // 幅のコピー
  this->length = obj.length;     // 長さのコピー
  //  this->data2d = new double*[this->height];   // 配列の確保
  // for(i = 0; i < this->height; i++) 
  //  this->data2d[i] = new double[this->width];
  this->data2d = new double*[this->height];   // 配列の確保
  this->data2d[0] = new double[this->height * this->width];
  for(i = 1; i < this->height; i++) 
    this->data2d[i] = this->data2d[0] + (i * this->width);

  // 配列要素の格納
  for(i=0; i < this->height; i++){  // 縦軸 y
    for(j=0; j < this->width; j++){  // 横軸 x
        this->data2d[i][j] = obj.data2d[i][j];
    }
  }

  return *this;
}

/** 配列の確保をする関数
 *
 * 初期化は行わないので注意すること。
 * @param w 確保する2次元配列の幅
 * @param h 確保する2次元配列の高さ
 */
void Nistk::SimData::create_data_size(int w,int h)
{
  int i;

  delete_data();
  // data2d = new double*[h];
  // for(i = 0; i < h; i++) data2d[i] = new double[w];
  data2d = new double*[h];
  data2d[0] = new double[h * w];
  for(i = 1; i < h; i++) data2d[i] = data2d[0] + (i * w);
  height = h;
  width = w;
  length = h * w;

  return;
}

/** 配列の大きさを変更する関数
 *
 * 配列の大きさを変更する。実際にはdeleteして再確保なので
 * データはきえてしまう。
 * @param w 確保する2次元配列の幅
 * @param h 確保する2次元配列の高さ
 */
void Nistk::SimData::recreate_data_size(int w,int h)
{
  int i;

  delete_data();
  //  data2d = new double*[h];
  // for(i = 0; i < h; i++) data2d[i] = new double[w];
  data2d = new double*[h];
  data2d[0] = new double[h * w];
  for(i = 1; i < h; i++) data2d[i] = data2d[0] + (i * w);
  height = h;
  width = w;
  length = h * w;

  return;
}

/** 配列の高さを取得する関数
 *
 *  @return 配列の高さ
 */
int Nistk::SimData::get_height()
{
  return height;
}

/** 配列の幅を取得する関数
 *
 *  @return 配列の幅
 */
int Nistk::SimData::get_width()
{
  return width;
}

/** 配列の長さを取得する関数
 *
 *  @return 一次元配列とした時の長さ
 */
int Nistk::SimData::get_length()
{
  return length;
}

/** 2Dデータの読み出しを行う関数
 *
 *  二次元配列の先頭(平面の左上 x=0, y=0)からみた
 *  横位置x, 縦位置yのデータを取得する
 *
 *  @param x 配列の列
 *  @param y 配列の行
 *  @return 位置x, yのデータ
 */
double Nistk::SimData::get_data(int x, int y)
{
  return data2d[y][x];
}

/** 2Dデータの格納を行う関数
 *
 *  二次元配列の先頭(平面の左上 x=0, y=0)からみた
 *  横位置x, 縦位置yにデータi_dataを格納する。
 *
 *  @param x 配列の列
 *  @param y 配列の行
 *  @param i_data 格納データ
 */
void Nistk::SimData::put_data(int x, int y, double i_data)
{
  data2d[y][x] = i_data;

  return;
}

/** 1Dデータの読み出しを行う関数
 *
 *  一次元配列のとした時の
 *  位置xのデータを取得する
 *
 *  @param x 一次元配列での位置
 *  @return 位置xのデータ
 */
double Nistk::SimData::get_data_1d(int x)
{
  int h,w;

  h = x / width;
  w = x % width;

  return   data2d[h][w];
}

/** 1Dデータの格納を行う関数
 *
 *  一次元配列のとした時の
 *  位置xにデータi_dataを格納する
 *
 *  @param x 一次元配列での位置
 *  @param i_data 格納データ
 */
void Nistk::SimData::put_data_1d(int x, double i_data)
{
  int h,w;

  h = x / width;
  w = x % width;
  data2d[h][w] = i_data;

  return;
}


/** 配列を消去する関数
 *
 *  確保している配列データを開放する。
 *  これをした後はもう一度確保しないと,
 *  配列のメモリ領域が確保されてていないので使えない。
 *
 */
void Nistk::SimData::delete_data()
{
  int i;

  if(length != 0){
    //   for(i = 0; i < height; i++){
    //     delete[] data2d[i];
    //   }
    delete[] data2d[0];
    delete[] data2d;
    height = 0;
    width = 0;
    length = 0;
  }

  return;
}

/** 配列を消去する関数
 *
 *  SimDataのデータは１次元の連続領域に確保された配列を
 *  ２次元で扱えるようにしているので、その連続領域の
 *  先頭アドレスを返す。
 *
 *  @return データ用配列の先頭アドレス
 *
 */
double* Nistk::SimData::get_data_ptr()
{
  return data2d[0];
}
